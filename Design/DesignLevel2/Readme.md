﻿﻿﻿﻿#### Logical Diagram Level 2
###### UML and an alternative for implementing the SPA
  
![Containers_Diagram](Logical/ContainersLogic.png)
![Logical_Alternative](Logical/LogicAlternative.png)
       
    
#### Process Diagrams Level 2
###### UML


**Create Machine and Assign Machine Type**  
  
![UC02](Process/UC02_LVL2.png)
  
  
**Create Production Line assigning Machines**  
  
![UC03](Process/UC03_LVL2.png)  
  
    
**Change Operation From Machine Type**  
  
![UC06](Process/UC06_LVL2.png)  
    
      
**Consult Operations from Machine Type**  
  
![UC09](Process/UC09_LVL2.png)  
    
      
**Consult Machines with specific Machine Type**  
  
![UC11](Process/UC11_LVL2.png) 
    
    
      
#### Physical Diagram Level 2
###### UML  
  
![PhysicalL2](Physical/PhysicalL2.png)
    
        
          
#### Implementation Diagram Level 2
###### UML  
  
  
![ImplementationViewL2](Implementation/ImplementationLevel2.png)
  
