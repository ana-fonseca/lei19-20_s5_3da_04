
#### Logical Diagram Level 3
###### UML  
  
![LogicalViewL3](Logical/LogicalViewL3.png)
       
    
#### Process Diagrams Level 3
###### UML


**Create Machine and Assign Machine Type (and the version with the SPA)**  
  
![UC02](Process/UC02_LVL3.png)
![UC02_SPA](Process/UC02_LVL3_SPA.png)
  
  
**Create Production Line assigning Machines (and the version with the SPA)**  
  
![UC03](Process/UC03_LV3.png)  
![UC03_SPA](Process/UC03_LV3_SPA.png) 
  
    
**Change Operation From Machine Type (and the version with the SPA)**  
  
![UC06](Process/UC06_LVL3.png)  
![UC06_SPA](Process/UC06_LVL3_SPA.png) 

      
**Consult Machines with specific Machine Type (and the version with the SPA)**  
  
![UC11](Process/UC11_LVL3.png) 
![UC11_SPA](Process/UC11_LVL3_SPA.png) 
    
          
#### Implementation Diagram Level 3
###### UML  
  
  
![ImplementationViewL3](Implementation/ImplementationViewL3.png)
  
