import clients from '../controllers/clientController';
import orders from "../controllers/orderController"
import admins from "../controllers/adminController"
import permissions from "../controllers/permissionController"

export default (app) => {
    app.route('/client')
        .get(clients.getClients)
        .post(clients.createClient)

    app.route('/client/:clientID')
        .get(clients.getClient)
        .put(clients.updateClient)
        .delete(clients.deleteClient)

    app.route("/client/nif/:nif")
        .get(clients.getByNif)
        .delete(clients.deleteByNif)

    app.route("/client/email/:email")
        .get(clients.getByEmail)
        .delete(clients.deleteByEmail)
        .post(clients.verifyClient)

    app.route("/order")
        .get(orders.getOrders)
        .post(orders.createOrder)
        .delete(orders.delete)

    app.route("/order/:orderID")
        .get(orders.getOrder)
        .put(orders.updateOrder)
        .delete(orders.deleteOrder)

    app.route("/order/deliveryDate/:orderID")
        .put(orders.setDeliveryDate)

    app.route("/order/cancel/:orderID")
        .delete(orders.cancelOrder)

    app.route("/order/client/:clientID")
        .get(orders.getClientOrders)

    app.route("/admin")
        .get(admins.getAdmins)
        .post(admins.createAdmin)

    app.route("/admin/:adminID")
        .get(admins.getAdmin)
        .put(admins.updateAdmin)

    app.route("/admin/username/:username")
        .get(admins.getAdminByUsername)
        .post(admins.verifyAdmin)
        .delete(admins.deleteAdmin)

    app.route("/permissions")
        .get(permissions.getPermissions)
        .put(permissions.updatePermissions)
        .delete(permissions.resetPermissions)
};