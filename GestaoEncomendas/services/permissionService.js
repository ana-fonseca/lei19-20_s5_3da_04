import permissionRepo from "../repos/permissionRepo"

exports.getPermissions = (callback) => {
    permissionRepo.getPermissions(callback)
}

exports.updatePermissions = (updatedPermissions, callback) => {
    permissionRepo.updatePermissions(updatedPermissions, callback)
}

exports.resetPermissions = (callback) => {
    permissionRepo.resetPermissions(callback)
}

exports.createDefaultPermissions = (callback) => {
    permissionRepo.createDefaultPermissions(callback)
}