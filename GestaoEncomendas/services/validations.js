import client from '../models/clientModel';
var axios = require("axios")
var https = require("https")

const MDP_PRODUCT_URL = "https://localhost:4001/mdp/product/"

const instance = axios.create({
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
});

exports.validateProducts = async (products, callback) => {
    var prodToValidate = []
    for (var i = 0; i < products.length; i++) {
        await instance.get(MDP_PRODUCT_URL + products[i].productID)
            .then((result) => {
                prodToValidate[prodToValidate.length] = { productID: result.data.productID, productDescription: result.data.description, quantity: products[i].quantity }
            })
            .catch((err) => {
                callback(false, null)
            })
    }
    
    callback(true, prodToValidate)
}

exports.validateClientByID = (clientID, callback) => {
    var valid = true;
    client.findById(clientID, (err, client) => {
        if (err) {
            valid = false
        }

        callback(valid)
    })
}

exports.validateClientByEmail = (email) => {
    email = email.toLowerCase()
    client.findOne({ 'email': email }, (err, client) => {
        if (err) {
            return false
        }

        return true;
    })
}

exports.validateClientByNIF = (nif) => {
    client.findOne({ 'nif': nif }, (err, client) => {
        if (err) {
            return false
        }

        return true
    })
}