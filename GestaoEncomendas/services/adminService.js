import mongoose from 'mongoose';
import adminRepo from '../repos/adminRepo';
var adminMapper = require("../mapper/adminMapper")
 
exports.getAdmin = (adminID, callback) => {
    adminRepo.getAdmin(adminID, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var adminDomain = adminMapper.modelToDomain(response.data)
        callback(adminDomain, true)
    })
}

exports.getAdmins = (callback) => {
    adminRepo.getAdmins((response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var admins = []

        for (var i = 0;i < response.data.length;i++) {
            admins.push(adminMapper.modelToDomain(response.data[i]))
        }

        callback(admins, true)
    })
}

exports.getByUsername = (username, callback) => {
    adminRepo.getByUsername(username, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        } else{
            var adminDomain = adminMapper.modelToDomain(response.data)
            callback(adminDomain, true)
        }
    })
}

exports.verifyAdmin = (username, password, callback) => {
    adminRepo.verifyAdmin(username, password, callback)
}

exports.createAdmin = (adminDTO, callback) => {
    var adminDomain = null

    try {
        adminDomain = adminMapper.dtoToDomain(adminDTO)
    } catch (error) {
        callback({ status: 500, error: { errorMessage: "Your information is invalid!" }, data: null })
        return
    }

    adminRepo.createAdmin(adminDomain, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var adminDomain = adminMapper.modelToDomain(response.data)
        callback(adminDomain, true)
    })
}

exports.updateAdmin = (adminID, updatedAdmin, callback) => {
    adminRepo.updateAdmin(adminID, updatedAdmin, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var adminDomain = adminMapper.modelToDomain(response.data)
        callback(adminDomain, true)
    })
}

exports.deleteAdmin = (username, callback) => {
    adminRepo.deleteAdmin(username, callback)
}

exports.createRootUser = (callback) => {
    adminRepo.createRootUser(callback)
}