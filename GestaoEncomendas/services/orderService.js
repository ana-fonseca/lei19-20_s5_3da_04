import mongoose from 'mongoose';
import orderRepo from '../repos/orderRepo';
var orderMapper = require("../mapper/orderMapper")

exports.getOrder = (orderID, callback) => {
    orderRepo.getOrder(orderID, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var orderDomain = orderMapper.modelToDomain(response.data)
        callback(orderDomain, true)
    })
}

exports.getOrders = (callback) => {
    orderRepo.getOrders((response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var orders = []

        for (var i = 0;i < response.data.length;i++) {
            orders.push(orderMapper.modelToDomain(response.data[i]))
        }

        callback(orders, true)
    })
}

exports.getClientOrders = (clientID, callback) => {
    orderRepo.getClientOrders(clientID, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var orders = []

        for (var i = 0;i < response.data.length;i++) {
            orders.push(orderMapper.modelToDomain(response.data[i]))
        }

        callback(orders, true)
    })
}

exports.createOrder = (orderDTO, callback) => {
    var orderDomain = null

    try {
        orderDomain = orderMapper.dtoToDomain(orderDTO)
    } catch (error) {
        callback({ status: 500, error: { errorMessage: "Your information is invalid!" }, data: null })
        return
    }

    orderRepo.createOrder(orderDomain, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var orderDomain = orderMapper.modelToDomain(response.data)
        callback(orderDomain, true)
    })
}

exports.updateOrder = (orderID, updatedOrder, callback) => {
    orderRepo.updateOrder(orderID, updatedOrder, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var orderDomain = orderMapper.modelToDomain(response.data)
        callback(orderDomain, true)
    })
}

exports.setDeliveryDate = (orderID, deliveryDate, callback) => {
    orderRepo.setDeliveryDate(orderID, deliveryDate, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var orderDomain = orderMapper.modelToDomain(response.data)
        callback(orderDomain, true)
    })
}

exports.deleteOrder = (orderID, callback) => {
    orderRepo.deleteOrder(orderID, callback)
}

exports.deleteAll = (callback) => {
    orderRepo.deleteAll(callback)
}

exports.cancelOrder = (orderID, callback) => {
    orderRepo.cancelOrder(orderID, callback)
}