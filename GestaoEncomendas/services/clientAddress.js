import client from '../models/clientModel.js';

exports.getUserAddress = async (clientID) => {
    var address = null
    await client.findById(clientID, (err, order) => {
        if (err) {
            return
        }
        address = order.address;
    })
    return address
}