import mongoose from 'mongoose';
import clientRepo from '../repos/clientRepo';
var clientMapper = require("../mapper/clientMapper")
var emailValidator = require("email-validator")
 
exports.getClient = (clientID, callback) => {
    clientRepo.getClient(clientID, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var clientDomain = clientMapper.modelToDomain(response.data)
        callback(clientDomain, true)
    })
}

exports.getClients = (callback) => {
    clientRepo.getClients((response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var clients = []

        for (var i = 0;i < response.data.length;i++) {
            clients.push(clientMapper.modelToDomain(response.data[i]))
        }

        callback(clients, true)
    })
}

exports.getByEmail = (email, callback) => {
    if (!emailValidator.validate(email)) {
        callback({ status: 500, error: { errorMessage: "The inputted e-mail is invalid!" }, data: null })
        return
    }
    clientRepo.getByEmail(email, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var clientDomain = clientMapper.modelToDomain(response.data)
        callback(clientDomain, true)
    })
}

exports.getByNif = (nif, callback) => {
    clientRepo.getByNif(nif, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var clientDomain = clientMapper.modelToDomain(response.data)
        callback(clientDomain, true)
    })
}

exports.verifyClient = (email, password, callback) => {
    if (!emailValidator.validate(email)) {
        callback({ status: 500, error: { errorMessage: "The inputted e-mail is invalid!" }, data: null })
        return
    }
    clientRepo.verifyClient(email, password, callback)
}

exports.createClient = (clientDTO, callback) => {
    var clientDomain = null

    try {
        clientDomain = clientMapper.dtoToDomain(clientDTO)
    } catch (error) {
        callback({ status: 500, error: { errorMessage: "Your information is invalid!" }, data: null })
        return
    }

    clientRepo.createClient(clientDomain, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var clientDomain = clientMapper.modelToDomain(response.data)
        callback(clientDomain, true)
    })
}

exports.updateClient = (clientID, updatedClient, callback) => {
    clientRepo.updateClient(clientID, updatedClient, (response) => {
        if (response.error != null) {
            callback(response, false)
            return
        }
        var clientDomain = clientMapper.modelToDomain(response.data)
        callback(clientDomain, true)
    })
}

exports.deleteClient = (clientID, callback) => {
    clientRepo.deleteClient(clientID, callback)
}

exports.deleteByNif = (nif, callback) => {
    clientRepo.deleteByNif(nif, callback)
}

exports.deleteByEmail = (email, callback) => {
    if (!emailValidator.validate(email)) {
        callback({ status: 500, error: { errorMessage: "The inputted e-mail is invalid!" }, data: null })
        return
    }
    clientRepo.deleteByEmail(email, callback)
}