var permissionService = require("../services/permissionService")

function respondToRequest(res, response) {
    res.status(response.status)

    if (response.error != null) {
        res.json(response.error)
        return
    }

    res.json(response.data)
}

exports.getPermissions = (req, res) => {
    permissionService.getPermissions((response) => {
        respondToRequest(res, response)
    })
}

exports.updatePermissions = (req, res) => {
    permissionService.updatePermissions(req.body, (response) => {
        respondToRequest(res, response)
    })
}

exports.resetPermissions = (req, res) => {
    permissionService.resetPermissions((response) => {
        respondToRequest(res, response)
    })
}