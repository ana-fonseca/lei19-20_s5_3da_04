import mongoose from 'mongoose';
import ClientDTO from "../DTO/clientDTO"
var clientService = require("../services/clientService")
var md5 = require('md5')
var clientMapper = require("../mapper/clientMapper")

function respondToRequest(res, response) {
    res.status(response.status)

    if (response.error != null) {
        res.json(response.error)
        return
    }

    res.json(response.data)
}

exports.getClient = (req, res) => {
    clientService.getClient(req.params.clientID, (response, isDomain) => {
        if (isDomain) {
            var clientDTO = clientMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: clientDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.getClients = (req, res) => {
    clientService.getClients((response, isDomain) => {
        if (isDomain) {
            var clients = []

            for (var i = 0; i < response.length; i++) {
                clients.push(clientMapper.domainToDTO(response[i]))
            }

            respondToRequest(res, { status: 200, error: null, data: clients })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.getByEmail = (req, res) => {
    var email = req.params.email.toLowerCase()
    clientService.getByEmail(email, (response, isDomain) => {
        if (isDomain) {
            var clientDTO = clientMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: clientDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.getByNif = (req, res) => {
    clientService.getByNif(req.params.nif, (response, isDomain) => {
        if (isDomain) {
            var clientDTO = clientMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: clientDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.verifyClient = (req, res) => {
    clientService.verifyClient(req.params.email.toLowerCase(), req.body.password, (response) => {
        respondToRequest(res, response)
    })
}

exports.createClient = (req, res) => {
    req.body.password = md5(req.body.password);

    clientService.createClient(clientMapper.jsonToDTO(req.body), (response, isDomain) => {
        if (isDomain) {
            var clientDTO = clientMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: clientDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.updateClient = (req, res) => {
    if (req.body.address == undefined && req.body.name == undefined) {
        respondToRequest(res, { status: 500, error: { errorMessage: "You can only update your e-mail address and name!" }, data: null })
        return
    }

    clientService.updateClient(req.params.clientID, req.body, (response, isDomain) => {
        if (isDomain) {
            var clientDTO = clientMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: clientDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.deleteClient = (req, res) => {
    clientService.deleteClient(req.params.clientID, (response) => {
        respondToRequest(res, response)
    })
};

exports.deleteByNif = (req, res) => {
    clientService.deleteByNif(req.params.nif, (response) => {
        respondToRequest(res, response)
    })
};

exports.deleteByEmail = (req, res) => {
    clientService.deleteByEmail(req.params.email, (response) => {
        respondToRequest(res, response)
    })
};