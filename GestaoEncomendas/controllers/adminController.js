import mongoose from 'mongoose';
var adminService = require("../services/adminService")
var adminMapper = require("../mapper/adminMapper")
var md5 = require("md5")

function respondToRequest (res, response) {
    res.status(response.status)

    if (response.error != null) {
        res.json(response.error)
        return
    }

    res.json(response.data)
}

exports.getAdmin = (req, res) => {
    adminService.getAdmin(req.params.adminID, (response, isDomain) => {
        if (isDomain) {
            var adminDTO = adminMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: adminDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.getAdmins = (req, res) => {
    adminService.getAdmins((response, isDomain) => {
        if (isDomain) {
            var admins = []

            for (var i = 0; i < response.length; i++) {
                admins.push(adminMapper.domainToDTO(response[i]))
            }

            respondToRequest(res, { status: 200, error: null, data: admins })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.getAdminByUsername = (req, res) => {
    adminService.getByUsername(req.params.username, (response, isDomain) => {
        if (isDomain) {
            var adminDTO = adminMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: adminDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.verifyAdmin = (req, res) => {
    adminService.verifyAdmin(req.params.username, req.body.password, (response) => {
        respondToRequest(res, response)
    })
}

exports.createAdmin = (req, res) => {
    req.body.password = md5(req.body.password);

    adminService.createAdmin(adminMapper.jsonToDTO(req.body), (response, isDomain) => {
        if (isDomain) {
            var adminDTO = adminMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: adminDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.updateAdmin = (req, res) => {
    adminService.updateAdmin(req.params.adminID, req.body, (response, isDomain) => {
        if (isDomain) {
            var adminDTO = adminMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: adminDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.deleteAdmin = (req, res) => {
    if (req.params.username == "root") {
        respondToRequest(res, { status: 500, error: { errorMessage: "You cannot delete the root admin!" }, data: null})
        return
    } 
    adminService.deleteAdmin(req.params.username, (response) => {
        respondToRequest(res, response)
    })
};