import mongoose from 'mongoose';
import validations from "../services/validations"
var orderService = require("../services/orderService")
var orderMapper = require("../mapper/orderMapper")

function respondToRequest(res, response) {
    res.status(response.status)

    if (response.error != null) {
        res.json(response.error)
        return
    }

    res.json(response.data)
}

exports.getOrder = (req, res) => {
    orderService.getOrder(req.params.orderID, (response, isDomain) => {
        if (isDomain) {
            var orderDTO = orderMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: orderDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.getOrders = (req, res) => {
    orderService.getOrders((response, isDomain) => {
        if (isDomain) {
            var orders = []

            for (var i = 0; i < response.length; i++) {
                orders.push(orderMapper.domainToDTO(response[i]))
            }

            respondToRequest(res, { status: 200, error: null, data: orders })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.getClientOrders = (req, res) => {
    orderService.getClientOrders(req.params.clientID, (response, isDomain) => {
        if (isDomain) {
            var orders = []

            for (var i = 0; i < response.length; i++) {
                orders.push(orderMapper.domainToDTO(response[i]))
            }

            respondToRequest(res, { status: 200, error: null, data: orders })
        } else {
            respondToRequest(res, response)
        }
    })
}

exports.createOrder = async (req, res) => {
    var clientID = req.body.clientID
    var products = req.body.products

    validations.validateClientByID(clientID, (validClient) => {
        if (!validClient) {
            respondToRequest(res, { status: 404, errorMessage: "There is no client with that ID!", data: null })
            return
        } else {
            validations.validateProducts(products, (validProducts, productsValid) => {
                if (!validProducts) {
                    respondToRequest(res, { status: 404, errorMessage: "There is at least one invalid product!", data: null })
                    return
                } else {
                    req.body.products = productsValid
                    req.body.isActive = true
                    
                    orderService.createOrder(orderMapper.jsonToDTO(req.body), (response, isDomain) => {
                        if (isDomain) {
                            var orderDTO = orderMapper.domainToDTO(response)
                            respondToRequest(res, { status: 200, error: null, data: orderDTO })
                        } else {
                            respondToRequest(res, response)
                        }
                    })
                }
            })
        }
    })
};

exports.updateOrder = (req, res) => {
    orderService.updateOrder(req.params.orderID, req.body, (response, isDomain) => {
        if (isDomain) {
            var orderDTO = orderMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: orderDTO })
        } else {
            respondToRequest(res, response)
        }
    })
};

exports.setDeliveryDate = (req, res) => {
    orderService.setDeliveryDate(req.params.orderID, req.body, (response, isDomain) => {
        if (isDomain) {
            var orderDTO = orderMapper.domainToDTO(response)
            respondToRequest(res, { status: 200, error: null, data: orderDTO })
        } else {
            respondToRequest(res, response)
        }
    })
}

exports.deleteOrder = (req, res) => {
    orderService.deleteOrder(req.params.orderID, (response) => {
        respondToRequest(res, response)
    })
};

exports.delete = (req, res) => {
    orderService.deleteAll((response) => {
        respondToRequest(res, response)
    })
};

exports.cancelOrder = (req, res) => {
    orderService.cancelOrder(req.params.orderID, (response) => {
        respondToRequest(res, response)
    })
}

