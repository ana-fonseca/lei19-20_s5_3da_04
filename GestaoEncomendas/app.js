// Transpile all code following this line with babel and use '@babel/preset-env' (aka ES6) preset.
require("@babel/register")({
    presets: ["@babel/preset-env"]
  });

import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';



import routes from './routes/index.js';
import { resolve } from 'dns';

var adminService = require('./services/adminService')
var permissionService = require("./services/permissionService")

var cors = require('cors');

const app = express();


/**
    * Connect to the database
    */
mongoose.connect(
    'mongodb+srv://lapr5_3da4:3DA4arqsi@lapr5-3da4-cm6xy.gcp.mongodb.net/test?retryWrites=true&w=majority',
    { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false }
);



/**
    * Middleware
    */

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

var startRoot = async (req, res, next) => {
    await adminService.createRootUser(() => {})
    next()
}

var startPermissions = async (req, res, next) => {
    await permissionService.createDefaultPermissions(() => {})
    next()
}

app.use(startRoot)
app.use(startPermissions)

// catch 400
app.use((err, req, res, next) => {
    console.log(err.stack);
    res.status(400).send(`Error: ${res.originUrl} not found`);
    next();
});

// catch 500
app.use((err, req, res, next) => {
    console.log(err.stack)
    res.status(500).send(`Error: ${err}`);
    next();
});

/**
    * Register the routes
    */

routes(app);

export default app;