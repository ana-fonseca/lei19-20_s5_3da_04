process.env.NODE_ENV = 'test';

import chai  from 'chai'
import Client from '../../models/clientModel'
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server'

var clientMapper = require("../../mapper/clientMapper")
var md5 = require("md5")
var ClientService = require('../../services/clientService.js')

let mongoServer;
var expect = chai.expect

describe('client Service Unit Testing', () => {

    before((done) => {
      
        mongoServer = new MongoMemoryServer();
        mongoServer
          .getConnectionString()
          .then((mongoUri) => {
            return mongoose.connect(mongoUri, (err) => {
              if (err) done(err);
            });
          })
          .then(() => done());
      });

    var nameClient1 = "client Test"
    var emailClient1 = "email@test.com";
    var nifClient1 = "123456789"
    var passwordclient1 = "aA1234561"
    var address = {
        country: "Portugal",
        city: "Porto",
        street: "Rua da boavista",
        number: "123",
        postalCode: "4440-352"
    }

    //create 2 client
    var client1Aux = {
        name: nameClient1,
        email: emailClient1,
        nif: nifClient1,
        password: md5(passwordclient1),
        address: address
    }
    var client1 = new Client(client1Aux)

    var client2Aux = {
        name: "Name Client 2",
        email: "email2@test.com",
        nif: "987654321",
        password: md5("aAaA198234"),
        address: {
            country: "Portugal",
            city: "Porto",
            street: "Rua da boavista2",
            number: "1232",
            postalCode: "4440-352"
        }
    }
    var client2 = new Client(client2Aux)

    client1.save((err, client) => {
        console.log(client)
    });
    
    client2.save((err, client) => {
    });

    /*
    it('Get client from DB - valid getByEmail',async () => {
        setTimeout(function () {
            

          }, 1000);
        var emailAux = client1.email.toLowerCase()
        ClientService.getByEmail(emailAux, (response, clientDomain) => {
            console.log(response)
            if (clientDomain) {
                var clientDTO = clientMapper.domainToDTO(response)
                expect(true).to.equal(true)
                expect(clientDTO).to.equal(clientMapper.domainToDTO(client1))
            } else {
                expect(false).to.equal(true)
            }
        })
    })
    */

   
    var invalidEmailclient1 ="clientTestEmail@test.com"
    it('Get clients from DB - invalid getByEmail',async () => {
        setTimeout(function () {
            

          }, 1000);
        var email = invalidEmailclient1.toLowerCase()
        ClientService.getByEmail(email, (response, isDomain) => {
            expect(response===null || response.error !== null).to.equal(true)
            
        })
    })
 /*
    it('Get client from DB - valid getByNif',async () => {
        setTimeout(function () {
            

          }, 1000);
        ClientService.getByNif(nifClient1, (response, isDomain) => {
            console.log("isDomain")
            console.log(response)
            if (isDomain) {
                var clientDTO = clientMapper.domainToDTO(response)
                expect(true).to.equal(true)
                expect(clientDTO).to.equal(clientMapper.domainToDTO(client1))
            } else {
                expect(false).to.equal(true)
            }
        })
    })
    */
    var invalidNifclient1 ="clientTestNif"
    it('Get clients from DB - invalid getByNif',async () => {
        setTimeout(function () {
            

          }, 1000);
        ClientService.getByNif(invalidNifclient1, (response, isDomain) => {
            expect (response.error !== null).to.equal(true)
        })
    })
    
    it('Verify client - valid client',async () => {
        setTimeout(function () {
            

          }, 1000);
        ClientService.verifyClient(client1.email.toLowerCase(), client1.password, (response) => {
            expect(response.error!==null).to.equal(true)
        })
    })

    it('Verify client - invalid client',async () => {
        setTimeout(function () {
            

          }, 1000);
        var emailInvalid = "asdasfas"
        ClientService.verifyClient(emailInvalid.toLowerCase(), client1.password, (response) => {
            expect(response.error!==null).to.equal(true)
        })
    })
        
    it('Create client - valid client',async () => {
        setTimeout(function () {
          }, 1000);
        ClientService.createClient(clientMapper.jsonToDTO(client1Aux), (response, isDomain) => {
            if (isDomain) {
                var clientDTO = clientMapper.domainToDTO(response)
                expect(clientDTO.name).to.equal(client1Aux.name)
                expect(clientDTO.email).to.equal(client1Aux.email)
                expect(clientDTO.nif+'').to.equal(client1Aux.nif)
                expect(clientDTO.password).to.equal(client1Aux.password)
                expect(clientDTO.address.country).to.equal(client1Aux.address.country)
                expect(clientDTO.address.street).to.equal(client1Aux.address.street)
            } else {
                expect(false).to.equal(true)
            }
        })
    })

    it('Create client - invalid client',async () => {
        setTimeout(function () {
            

          }, 1000);
        var emailInvalid = "asdasfas"
        var clientInvalid = {
            name: nameClient1,
            email: emailInvalid,
            nif: nifClient1,
            password: md5(passwordclient1),
            address: address
        }

        ClientService.createClient(clientMapper.jsonToDTO(clientInvalid), (response, isDomain) => {
            expect(response.error === null).to.equal(false)
        })
    })
})