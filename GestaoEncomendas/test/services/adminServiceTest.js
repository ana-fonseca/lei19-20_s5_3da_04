process.env.NODE_ENV = 'test';

import chai  from 'chai'
import Admin from '../../models/adminModel'
import mongoose, { mongo } from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server'

var adminMapper = require("../../mapper/adminMapper")
var md5 = require("md5")
var AdminService = require('../../services/adminService.js')

let mongoServer;
var expect = chai.expect

describe('Admin Service Unit Testing', () => {
    
    before((done) => {
      
        mongoServer = new MongoMemoryServer();
        mongoServer
          .getConnectionString()
          .then((mongoUri) => {
            return mongoose.connect(mongoUri, (err) => {
              if (err) done(err);
            });
          })
          .then(() => done());
      });
    
    var nameAdmin1 = "Admin Test"
    var usernameAdmin1 ="adminTest"
    var passwordAdmin1 = "aA1234561"

    var invalidUsernameAdmin1 ="adminTest"
    var usernameAdmin5 = "adminTest5"

    //create 5 admin
    var admin1Aux = {
        name: nameAdmin1,
        username: usernameAdmin1,
        password: md5(passwordAdmin1)
    }
    var admin1 = new Admin(admin1Aux)

    var admin2Aux = {
        name: "Admin Test2",
        username: "adminTest2",
        password: md5("aA1234562")
    }
    var admin2 = new Admin(admin2Aux)
    
    var admin3Aux = {
        name: "Admin Test3",
        username: "adminTest3",
        password: md5("aA1234563")
    }
    var admin3 = new Admin(admin3Aux )

    var admin4Aux = {
        name: "Admin Test4",
        username: "adminTest4",
        password: md5("aA1234564")
    }
    var admin4 = new Admin(admin4Aux )

    var admin5Aux = {
        name: "Admin Test5",
        username: usernameAdmin5,
        password: md5("aA1234565")
    }
    var admin5 = new Admin(admin5Aux )
    

    admin1.save((err, admin) => {
    });
    
    admin2.save((err, admin) => {
    });

    admin3.save((err, admin) => {
    });

    admin4.save((err, admin) => {
    });

    admin5.save((err, admin) => {
    });
    

    it('Get Admins from DB', async () => {
        setTimeout(function () {

          }, 1000);
        AdminService.getAdmins((response, isDomain) => {
            var admins = []
            if (isDomain) {
                for (var i = 0; i < response.length; i++) {
                    admins.push(adminMapper.domainToModel(response[i]))
                }
            } else {
                expect(false).to.equal(true)
            }
            expect(admins).to.be.an('array').that.is.not.empty;
        })
    })

    it('Get Admin from DB - valid username', async () => {
        setTimeout(function () {

          }, 1000);
        AdminService.getByUsername(usernameAdmin1, (response, isDomain) => {
            if (isDomain) {
                var adminAux = adminMapper.domainToModel(response)
                expect(response.error === undefined || response.error === null).to.equal(true)
                expect(adminAux.name).to.equal(admin1.name)
                expect(adminAux.username).to.equal(admin1.username)
                expect(adminAux.password).to.equal(admin1.password)
                
            } else {
                expect(true).to.equal(true)
            }
        })
    })

    it('Get Admins from DB - invalid username', async () => {
        setTimeout(function () {

          }, 1000);
        AdminService.getByUsername("as a", (response, isDomain) => {
            expect(isDomain).to.equal(false)
        })
    })
    
    it('Verify Admin - valid admin', async () => {
        setTimeout(function () {

          }, 1000);
        AdminService.verifyAdmin(usernameAdmin1, md5(passwordAdmin1), (response) => {
          
            expect(response.status!== null).to.equal(true)
        })
    })

    
    it('Verify Admin - invalid admin', async () => {
        setTimeout(function () {

          }, 1000);
        var invalidUsernameAdmin ="adminTestNotExist"
        AdminService.verifyAdmin(invalidUsernameAdmin,md5(passwordAdmin1), (response) => {

            expect(response.error !== null).to.equal(true)
        })
    })
        
    it('Create Admin - valid admin', async () => {
        setTimeout(function () {

          }, 1000);
        var nameAux="Admin Test6"
        var usernameAux = "adminTest6"
        var passwordAux =md5("aA1234565")

        var adminAux = {
            name: nameAux,
            username: usernameAux,
            password: passwordAux
        }

        AdminService.createAdmin(adminMapper.jsonToDTO(adminAux), (response, isDomain) => {
            setTimeout(function () {
    
              }, 1000);
            if (isDomain) {
                var adminAux2 = adminMapper.domainToModel(response)
                expect(adminAux2.name).to.equal(adminAux.name)
                expect(adminAux2.username).to.equal(adminAux.username)
                expect(adminAux2.password).to.equal(adminAux.password)
            } else {
                expect(false).to.equal(true)
            }
        })
    })

    it('Create Admin - invalid admin', async () => {
        setTimeout(function () {

          }, 1000);
        var adminInvalid = {
            name: "Admin Test5",
            username: invalidUsernameAdmin1,
            password: md5("aA1234565")
        }

        AdminService.createAdmin(adminMapper.jsonToDTO(adminInvalid), (response, isDomain) => {
            if (isDomain) {
                expect(false).to.equal(true)
            } else {
                expect(true).to.equal(true)
            }
        })
    })
    
    it('Create Admin - invalid admin - Already registed', async () => {
        setTimeout(function () {

          }, 1000);

        AdminService.createAdmin(adminMapper.jsonToDTO(admin1Aux), (response, isDomain) => {
            if (isDomain) {
                expect(false).to.equal(true)
            } else {
                expect(true).to.equal(true)
            }
        })
    })

    it('Delete Admin - invalid user - Root', async () => {
        setTimeout(function () {

          }, 1000);
        var usernameAux = "root"
        if (usernameAux == "root") {
            expect(true).to.equal(true)
        }else{
            expect(false).to.equal(true)
        }
    })

    it('Delete Admin - valid username', async () => {
        setTimeout(function () {

          }, 1000);
        var usernameAux = usernameAdmin5
        if (usernameAux == "root") {
            expect(false).to.equal(true)
        } 
        AdminService.deleteAdmin(usernameAux, (response) => {
            expect(true).to.equal(true)
        })
    })

    it('Delete Admin - invalid username', async () => {
        setTimeout(function () {

          }, 1000);
        var usernameAux = "invalidUsernameAdmin1"
        if (usernameAux == "root") {
            expect(false).to.equal(true)
        } 
        AdminService.deleteAdmin(usernameAux, (response) => {
            expect(response.error!== undefined||response.error !== null).to.equal(true)
        })
    })
})