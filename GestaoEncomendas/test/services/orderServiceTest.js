process.env.NODE_ENV = 'test';

import chai  from 'chai'
import Order from '../../models/orderModel'
import Client from '../../models/clientModel'
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server'

var orderMapper = require("../../mapper/orderMapper")
var md5 = require("md5")
var OrderService = require('../../services/orderService.js')

let mongoServer;
var expect = chai.expect

describe('order Service Unit Testing', () => {
    
    var useUserAddress1 =true
    var billingAddress1 = {
        country:"Portugal",
        city:"Porto",
        number: "123",
        street: "rua da boavista",
        postalCode: "4440-542"
    }
    var products1 ={
        productID:123,
        productDescription:"prod1 Desc",
        quantity: 2
    }
    var products2 ={
        productID:321,
        productDescription:"prod2 Desc",
        quantity: 1
    }
    var dueDate1 = new Date("2020/04/19")

    var invalidUsernameorder1 ="orderTest"
    var usernameorder5 = "orderTest5"
    
    var nameClient1 = "client Test"
    var emailClient1 = "email@test.com";
    var nifClient1 = "123456789"
    var passwordclient1 = "aA1234561"
    var address = {
        country: "Portugal",
        city: "Porto",
        street: "Rua da boavista",
        number: "123",
        postalCode: "4440-352"
    }

    var client1Aux = {
        name: nameClient1,
        email: emailClient1,
        nif: nifClient1,
        password: md5(passwordclient1),
        address: address
    }
    var client1 = new Client(client1Aux)

    //create 2 order
    var order1Aux = {
        clientID: client1._id,
        useUserAddress: useUserAddress1,
        billingAddress: billingAddress1,
        products: [products1,products2],
        dueDate: dueDate1,
        isActive:true
    }
    var order1 = new Order(order1Aux)

    var order2Aux = {
        clientID: client1._id,
        useUserAddress: false,
        billingAddress: billingAddress1,
        products: [products2],
        dueDate: dueDate1,
        isActive:true
    }
    var order2 = new Order(order2Aux)

    before((done) => {
      
        mongoServer = new MongoMemoryServer();
        mongoServer
          .getConnectionString()
          .then((mongoUri) => {
            return mongoose.connect(mongoUri, (err) => {
              if (err) done(err);
            });
          })
          .then(() => done());
      });

    order1.save((err, order) => {
        });
        
    order2.save((err, order) => {
        });

    it('Get orders from DB', async () => {
        setTimeout(function () {
          }, 1000);
        OrderService.getOrders((response, isDomain) => {
            var orders = []
            if (isDomain) {
                for (var i = 0; i < response.length; i++) {
                    orders.push(orderMapper.domainToModel(response[i]))
                }
            } else {
                expect(false).to.equal(true)
            }
        })
    })

    it('Create order - valid order', async () => {
        setTimeout(function () {

          }, 1000);

        OrderService.createOrder(orderMapper.jsonToDTO(order2Aux), (response, isDomain) => {
            setTimeout(function () {
              }, 1000);
            expect(isDomain).to.equal(true)
            
        })
    })

    it('Create order - invalid order', async () => {
        setTimeout(function () {

          }, 1000);
        var orderAux = {
            clientID: client1._id,
            useUserAddress: "false",
            billingAddress: billingAddress1,
            products: [products2],
            dueDate: dueDate1
        }

        OrderService.createOrder(orderMapper.jsonToDTO(orderAux), (response, isDomain) => {
            setTimeout(function () {
              }, 1000);
            expect(isDomain).to.equal(false)
            
        })
    })
    
    it('Create order - invalid order - Already registed', async () => {
        setTimeout(function () {

          }, 1000);

        OrderService.createOrder(orderMapper.jsonToDTO(order1Aux), (response, isDomain) => {
            if (isDomain) {
                expect(false).to.equal(true)
            } else {
                expect(true).to.equal(true)
            }
        })
    })

    it('Delete order - valid id', async () => {
        setTimeout(function () {

          }, 1000);
        OrderService.deleteOrder(order1._id, (response) => {
            expect(true).to.equal(true)
        })
    })

    it('Delete order - invalid id', async () => {
        setTimeout(function () {

          }, 1000);
        OrderService.deleteOrder("order1._id", (response) => {
            expect(response.error!== undefined||response.error !== null).to.equal(true)
        })
    })
    
})