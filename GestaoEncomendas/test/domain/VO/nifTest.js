process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import nif from './../../../domain/valueObjects/nif.js';

var expect = chai.expect

describe('Nif Unit Testing - Constructor tests', () => {
   
    var nifInfo2="1234567890"

    it('Create an nif valid - All infomation valid - nif with only numbers', () => {
        
        var nifAux = new nif( nifInfo2)

        expect(nifAux.nif).to.equal(nifInfo2)
    })

    it('Create an nif valid - infomation valid - empty', () => {
        
        var nifAux = new nif( "")

        expect(nifAux.nif).to.equal("")
    })

    it('Create an nif valid - null', () => {
        var nifAux = new nif(null )

        expect(nifAux.nif).to.equal(null)
    })

}) 