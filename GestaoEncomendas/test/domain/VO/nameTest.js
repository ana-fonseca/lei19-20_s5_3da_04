process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import  Name  from './../../../domain/valueObjects/name.js';

var expect = chai.expect

describe('Name Unit Testing - Constructor tests', () => {
   
    var nameInfo="Name Test"
    var nameInfo2="NameTest"
   
    it('Create an name valid - All infomation valid - 2 names, with 1 space', () => {
        
        var nameAux = new Name(nameInfo)

        var a = nameAux.toString()

        expect(a).to.equal(nameInfo)
    })

    it('Create an name valid - All infomation valid - more than 2 names', () => {
        
        //expect(1).to.equal(1)
    })

    it('Create an name valid - invalid - name with 0 spaces', () => {
        try {
            var nameAux = new Name(nameInfo2)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid name!")
         }

    })

    it('Create an name valid - infomation valid - empty', () => {
        
        try {
            var nameAux = new Name("")
         }
         catch (e) {
            expect(e.message).to.equal("Invalid name!")
         }
    })

    it('Create an name valid - null', () => {
        try {
            var nameAux = new Name(null)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid name!")
         }
    })


}) 