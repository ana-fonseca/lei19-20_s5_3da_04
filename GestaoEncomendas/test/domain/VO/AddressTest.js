process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import  Address  from './../../../domain/valueObjects/address.js';

var expect = chai.expect

describe('address Unit Testing - Constructor tests', () => {

    var validStreet = "Rua de Teste1"
    var validNumber= "123"
    var validCity="Porto"
    var validCountry= "Portugal"
    var validPostalCode= "4440-352"

    var invalidStreet = "Rua de Teste1"
    var invalidNumber= "123"
    var invalidCity="RandomCity"
    var invalidCountry= "RandomCountry"
    var invalidPostalCode= "4440352"
   //street, number, city, country, postalCode

   
    it('Create an address valid - All infomation valid', () => {
        
        var addressAux = new Address(validStreet, validNumber, validCity, validCountry, validPostalCode)

        expect(addressAux.street).to.equal(validStreet)
        expect(addressAux.number).to.equal(validNumber)
        expect(addressAux.city).to.equal(validCity)
        expect(addressAux.country).to.equal(validCountry)
        expect(addressAux.postalCode).to.equal(validPostalCode)
    })

    it('Create an address invalid - invalid country', () => {

        try {
            var addressAux = new Address(validStreet, validNumber, validCity, invalidCountry, validPostalCode)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid country!")
         }

    })

    it('Create an address invalid - invalid city', () => {

        try {
            var addressAux = new Address(validStreet, validNumber, invalidCity, validCountry, validPostalCode)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid city!")
         }

    })

    it('Create an address invalid - invalid postal code', () => {

        try {
            var addressAux = new Address(validStreet, validNumber, validCity, validCountry, invalidPostalCode)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid postal code!")
         }

    })
 

    it('Create an address infomation valid - empty', () => {
        
        try {
            var addressAux = new Address("")
         }
         catch (e) {
            expect(e.message).to.equal("Invalid country!")
         }
    })

    it('Create an address null', () => {
        try {
            var addressAux = new Address(null)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid country!")
         }
    })

}) 