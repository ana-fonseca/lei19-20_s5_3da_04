process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import  email  from './../../../domain/valueObjects/email.js';

var expect = chai.expect

describe('email Unit Testing - Constructor tests', () => {
   
    var emailInfo="name@test.com".toLowerCase()
    var emailInfo2="emailtest.com"
    var emailInfo3="email@testcom"
   
    it('Create an email valid - All infomation valid - name@Test.com', () => {
        
        var emailAux = new email(emailInfo )

        expect(emailAux.email).to.equal(emailInfo)
    })

    it('Create an email invalid - email with no @', () => {
        try {
            var emailAux = new email(emailInfo2)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid e-mail address!")
         }

    })

    it('Create an email invalid - email with no .', () => {
        try {
            var emailAux = new email(emailInfo2 )
         }
         catch (e) {
            expect(e.message).to.equal("Invalid e-mail address!")
         }

    })

    it('Create an email infomation valid - empty', () => {
        
        try {
            var emailAux = new email("")
         }
         catch (e) {
            expect(e.message).to.equal("Invalid e-mail address!")
         }
    })

    it('Create an email null', () => {
        try {
            var emailAux = new email(null)
         }
         catch (e) {
            expect(e.message).to.equal("Invalid e-mail address!")
         }
    })

}) 