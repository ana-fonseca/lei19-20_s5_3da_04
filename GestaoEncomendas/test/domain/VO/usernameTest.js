process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import username from './../../../domain/valueObjects/username.js';

var expect = chai.expect

describe('Username Unit Testing - Constructor tests', () => {

    var usernameInfo="usernameTest1"
    var usernameInfo2="1234567890"
   
    it('Create an username valid - All infomation valid - username with letters and numbers', () => {
        
        var usernameAux = new username( usernameInfo)

        expect(usernameAux.username).to.equal(usernameInfo)
    })

    it('Create an username valid - All infomation valid - username with only numbers', () => {
        
        var usernameAux = new username(usernameInfo2)

        expect(usernameAux.username).to.equal(usernameInfo2)
    })

    it('Create an username valid - infomation valid - empty', () => {
        
        var usernameAux = new username("" )

        expect(usernameAux.username).to.equal("")
    })

    it('Create an username valid - null', () => {
        var usernameAux = new username( null)

        expect(usernameAux.username).to.equal(null)
    })


}) 