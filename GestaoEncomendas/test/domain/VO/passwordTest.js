process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import password from './../../../domain/valueObjects/password.js';

var expect = chai.expect

describe('Password Unit Testing - Constructor tests', () => {
   
    var passwordInfo="passwordTest1"
    var passwordInfo2="1234567890"
   
    it('Create an password valid - All infomation valid - password with letters and numbers', () => {
        
        var passwordAux = new password(passwordInfo )

        expect(passwordAux.password).to.equal(passwordInfo)
    })

    it('Create an password valid - All infomation valid - password with only numbers', () => {
        
        var passwordAux = new password(passwordInfo2)

        expect(passwordAux.password).to.equal(passwordInfo2)
    })

    it('Create an password valid - infomation valid - empty', () => {
        
        var passwordAux = new password("")

        expect(passwordAux.password).to.equal("")
    })

    it('Create an password valid - null', () => {
        var passwordAux = new password(null)

        expect(passwordAux.password).to.equal(null)
    })
}) 