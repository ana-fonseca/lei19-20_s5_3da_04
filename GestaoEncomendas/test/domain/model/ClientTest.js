process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import  client  from './../../../domain/models/clientDomain.js';
var md5 = require("md5")

var expect = chai.expect
   //name, email, nif, password, address
describe('client Unit Testing - Constructor tests', () => {

    var validName = "Test Name1"
    var validEmail = "test@email.com"
    var validNif= "1234567890"
    var passWithoutMD5 = "aasfafs"
    var validPassword=md5(passWithoutMD5)
    var validAddress = "Rua Test, 123"
   
    it('Create an client valid - All infomation valid', () => {
        
        var clientAux = new client(
            validName ,
            validEmail,
            validNif,
            validPassword,
            validAddress
        )

        expect(clientAux.name).to.equal(validName)
        expect(clientAux.nif).to.equal(validNif)
        expect(clientAux.password).to.equal(validPassword)
        expect(clientAux.email).to.equal(validEmail)
        expect(clientAux.address).to.equal(validAddress)
    })

}) 