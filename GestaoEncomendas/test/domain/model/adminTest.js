process.env.NODE_ENV = 'test';

import mongoose from 'mongoose';
import chai  from 'chai'
import request from  'supertest'
import  admin  from './../../../domain/models/adminDomain.js';
import  name  from './../../../domain/valueObjects/name.js';
import username from './../../../domain/valueObjects/username.js';
import password from './../../../domain/valueObjects/password.js';
var md5 = require("md5")

var expect = chai.expect
    //name, username, password
describe('admin Unit Testing - Constructor tests', () => {

    var validName = "Test Name1"
    var validUsername= "testUsername1"
    var passWithoutMD5 = "aasfafs"
    var validPassword=md5(passWithoutMD5)

    var invalidName = "Rua de Teste1"
    var invalidUsername= "123"
    var invalidPassword=passWithoutMD5

    var nameAux = new name({
            "name": validName 
        });
    var usernameAux = new username({
        "username": validUsername
        } );
    var passwordAux = new password({
        "password": validPassword
        } );


   
    it('Create an admin valid - All infomation valid', () => {
        
        var adminAux = new admin(
            validName ,
            validUsername,
             validPassword
        )

        expect(adminAux.name).to.equal(validName)
        expect(adminAux.username).to.equal(validUsername)
        expect(adminAux.password).to.equal(validPassword)
    })

}) 