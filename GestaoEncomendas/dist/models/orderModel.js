'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ObjectId = _mongoose2.default.Schema.Types.ObjectId;

var AddressScheme = new _mongoose.Schema({
    country: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    number: {
        type: String,
        required: true
    },
    street: {
        type: String,
        required: true
    }
}, {
    _id: false
});

var ProductScheme = new _mongoose.Schema({
    productID: {
        type: Number,
        required: true,
        index: true
    },

    quantity: {
        type: Number,
        required: true,
        index: true
    }
}, {
    _id: false
});

/**
 * Create database scheme for clients
 */
var OrderScheme = new _mongoose.Schema({
    clientID: {
        type: ObjectId,
        required: true,
        index: true
    },

    useUserAddress: {
        type: Boolean,
        required: true,
        index: true
    },

    billingAddress: {
        type: AddressScheme,
        required: false
    },

    products: [ProductScheme],

    salt: String
}, { timestamps: true });

exports.default = _mongoose2.default.model('Order', OrderScheme);
//# sourceMappingURL=orderModel.js.map