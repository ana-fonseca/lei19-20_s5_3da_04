'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddressScheme = new _mongoose.Schema({
  country: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  number: {
    type: String,
    required: true
  },
  street: {
    type: String,
    required: true
  }
}, {
  _id: false
});

/**
 * Create database scheme for clients
 */
var ClientScheme = new _mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please enter a full name'],
    index: true
  },

  email: {
    type: String,
    lowercase: true,
    unique: true,
    index: true
  },

  nif: {
    type: Number,
    required: true,
    index: true,
    unique: true
  },

  address: {
    type: AddressScheme
  },

  password: {
    type: String,
    required: true
  },

  salt: String
}, { timestamps: true });

exports.default = _mongoose2.default.model('Client', ClientScheme);
//# sourceMappingURL=clientModel.js.map