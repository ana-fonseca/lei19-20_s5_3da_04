'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AdminScheme = new _mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please enter a full name'],
        index: true
    },

    username: {
        type: String,
        required: true,
        index: true
    },

    password: {
        type: String,
        required: true
    },

    salt: String
}, { timestamps: true });

exports.default = _mongoose2.default.model('Admin', AdminScheme);
//# sourceMappingURL=adminModel.js.map