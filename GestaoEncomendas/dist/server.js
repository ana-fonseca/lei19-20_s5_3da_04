'use strict';

var _app = require('./app');

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var port = process.env.PORT || '3030';_app2.default.listen(port);

console.log('################################################\n        Server listening on port: ' + port + '\n################################################');
//# sourceMappingURL=server.js.map