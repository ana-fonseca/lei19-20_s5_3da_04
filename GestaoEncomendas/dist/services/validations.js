"use strict";

var _clientModel = require("../models/clientModel.js");

var _clientModel2 = _interopRequireDefault(_clientModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var axios = require("axios");
var https = require("https");

var MDP_PRODUCT_URL = "https://localhost:4001/mdp/product/";

var instance = axios.create({
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
});

exports.validateProducts = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(products) {
        var valid, i;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        valid = true;
                        i = 0;

                    case 2:
                        if (!(i < products.length)) {
                            _context.next = 8;
                            break;
                        }

                        _context.next = 5;
                        return instance.get(MDP_PRODUCT_URL + products[i].productID).catch(function (err) {
                            valid = false;
                            return;
                        });

                    case 5:
                        i++;
                        _context.next = 2;
                        break;

                    case 8:
                        return _context.abrupt("return", valid);

                    case 9:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function (_x) {
        return _ref.apply(this, arguments);
    };
}();

exports.validateClientByID = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(clientID) {
        var valid;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        valid = true;
                        _context2.next = 3;
                        return _clientModel2.default.findById(clientID, function (err, client) {
                            if (err) {
                                valid = false;
                            }
                        });

                    case 3:
                        return _context2.abrupt("return", valid);

                    case 4:
                    case "end":
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function (_x2) {
        return _ref2.apply(this, arguments);
    };
}();

exports.validateClientByEmail = function (email) {
    email = email.toLowerCase();
    _clientModel2.default.findOne({ 'email': email }, function (err, client) {
        if (err) {
            return false;
        }

        return true;
    });
};

exports.validateClientByNIF = function (nif) {
    _clientModel2.default.findOne({ 'nif': nif }, function (err, client) {
        if (err) {
            return false;
        }

        return true;
    });
};
//# sourceMappingURL=validations.js.map