"use strict";

var _adminModel = require("../models/adminModel");

var _adminModel2 = _interopRequireDefault(_adminModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var md5 = require("md5");

exports.createRootUser = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    _context.next = 2;
                    return _adminModel2.default.findOne({ username: "root" }, function (err, searchAdmin) {
                        if (searchAdmin == null) {
                            var root = {};
                            root.name = "Data Administrator";
                            root.username = "root";
                            root.password = md5("3DA4root");

                            var rootAdmin = new _adminModel2.default(root);
                            rootAdmin.save(function (err, admin) {
                                if (err) {
                                    console.log(err);
                                    return;
                                }
                                return;
                            });
                        }
                        return;
                    });

                case 2:
                case "end":
                    return _context.stop();
            }
        }
    }, _callee, undefined);
}));
//# sourceMappingURL=adminService.js.map