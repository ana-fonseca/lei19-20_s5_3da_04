'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _adminService = require('./services/adminService');

var _adminService2 = _interopRequireDefault(_adminService);

var _index = require('./routes/index.js');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var cors = require('cors');

var app = (0, _express2.default)();

/**
    * Connect to the database
    */

_mongoose2.default.connect('mongodb+srv://lapr5_3da4:3DA4arqsi@lapr5-3da4-cluster-cm6xy.azure.mongodb.net/test?retryWrites=true&w=majority', { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });

/**
    * Middleware
    */

app.use(_bodyParser2.default.urlencoded({ extended: true }));
app.use(_bodyParser2.default.json());
app.use(cors());

var startRoot = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res, next) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return _adminService2.default.createRootUser();

                    case 2:
                        next();

                    case 3:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function startRoot(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
    };
}();

app.use(startRoot);

// catch 400
app.use(function (err, req, res, next) {
    console.log(err.stack);
    res.status(400).send('Error: ' + res.originUrl + ' not found');
    next();
});

// catch 500
app.use(function (err, req, res, next) {
    console.log(err.stack);
    res.status(500).send('Error: ' + err);
    next();
});

/**
    * Register the routes
    */

(0, _index2.default)(app);

exports.default = app;
//# sourceMappingURL=app.js.map