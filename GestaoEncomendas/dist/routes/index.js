"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _clientController = require("../controllers/clientController");

var _clientController2 = _interopRequireDefault(_clientController);

var _orderController = require("../controllers/orderController");

var _orderController2 = _interopRequireDefault(_orderController);

var _adminController = require("../controllers/adminController");

var _adminController2 = _interopRequireDefault(_adminController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (app) {
    app.route('/client').get(_clientController2.default.getClients).post(_clientController2.default.createClient);

    app.route('/client/:clientID').get(_clientController2.default.getClient).put(_clientController2.default.updateClient).delete(_clientController2.default.deleteClient);

    app.route("/client/nif/:nif").get(_clientController2.default.getByNif).delete(_clientController2.default.deleteByNif);

    app.route("/client/email/:email").get(_clientController2.default.getByEmail).delete(_clientController2.default.deleteByEmail).post(_clientController2.default.verifyClient);

    app.route("/order").get(_orderController2.default.getOrders).post(_orderController2.default.createOrder).delete(_orderController2.default.delete);

    app.route("/order/:orderID").get(_orderController2.default.getOrder).put(_orderController2.default.updateOrder).delete(_orderController2.default.deleteOrder);

    app.route("/order/client/:clientID").get(_orderController2.default.getClientOrders);

    app.route("/admin").get(_adminController2.default.getAdmins).post(_adminController2.default.createAdmin);

    app.route("/admin/:adminID").get(_adminController2.default.getAdmin).put(_adminController2.default.updateAdmin).delete(_adminController2.default.deleteAdmin);

    app.route("/admin/username/:username").get(_adminController2.default.getAdminByUsername).post(_adminController2.default.verifyAdmin);
};
//# sourceMappingURL=index.js.map