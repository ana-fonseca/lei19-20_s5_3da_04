'use strict';

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _orderModel = require('../models/orderModel');

var _orderModel2 = _interopRequireDefault(_orderModel);

var _validations = require('../services/validations');

var _validations2 = _interopRequireDefault(_validations);

var _clientAddress = require('../services/clientAddress');

var _clientAddress2 = _interopRequireDefault(_clientAddress);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.getOrder = function (req, res) {
    _orderModel2.default.findById(req.params.orderID, function (err, order) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (order == null) {
            res.status(404).json({
                message: "There is no order with that ID!"
            });
            return;
        }

        res.json(order);
    });
};

exports.getOrders = function (req, res) {
    _orderModel2.default.find({}, function (err, orders) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json(orders);
    });
};

exports.createOrder = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
        var clientID, products, useUserAddress, newOrder;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        clientID = req.body.clientID;
                        products = req.body.products;
                        useUserAddress = req.body.useUserAddress;
                        _context.next = 5;
                        return _validations2.default.validateClientByID(clientID);

                    case 5:
                        if (_context.sent) {
                            _context.next = 8;
                            break;
                        }

                        res.status(404).json({
                            message: "There is no client with that ID!"
                        });
                        return _context.abrupt('return');

                    case 8:
                        _context.next = 10;
                        return _validations2.default.validateProducts(products);

                    case 10:
                        if (_context.sent) {
                            _context.next = 13;
                            break;
                        }

                        res.status(404).json({
                            message: "There is at least one invalid product!"
                        });
                        return _context.abrupt('return');

                    case 13:
                        if (!useUserAddress) {
                            _context.next = 17;
                            break;
                        }

                        _context.next = 16;
                        return _clientAddress2.default.getUserAddress(clientID);

                    case 16:
                        req.body.billingAddress = _context.sent;

                    case 17:
                        newOrder = new _orderModel2.default(req.body);


                        newOrder.save(function (err, order) {
                            if (err) {
                                res.status(500).send(err);
                                return;
                            }

                            res.json(order);
                        });

                    case 19:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}();

exports.updateOrder = function (req, res) {
    _orderModel2.default.findOneAndUpdate({
        _id: req.params.orderID
    }, req.body, function (err, order) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json(order);
    });
};

exports.deleteOrder = function (req, res) {
    _orderModel2.default.deleteOne({
        _id: req.params.orderID
    }, function (err) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json({
            message: 'Order ' + req.params.orderID + ' successfully deleted'
        });
    });
};

exports.delete = function (req, res) {
    _orderModel2.default.deleteMany({}, function (err) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json({
            message: 'All orders successfully deleted'
        });
    });
};

exports.getClientOrders = function (req, res) {
    _orderModel2.default.find({ 'clientID': req.params.clientID }, function (err, order) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (order == null) {
            res.status(404).json({
                message: "That client hasn't made an order!"
            });
            return;
        }

        res.json(order);
    });
};
//# sourceMappingURL=orderController.js.map