'use strict';

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _clientModel = require('../models/clientModel.js');

var _clientModel2 = _interopRequireDefault(_clientModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var md5 = require('md5');

exports.getClient = function (req, res) {
    _clientModel2.default.findById(req.params.clientID, function (err, client) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (client == null) {
            res.status(404).json({
                message: "There is no client with that ID!"
            });
            return;
        }

        res.json(client);
    });
};

exports.getByNif = function (req, res) {
    _clientModel2.default.findOne({ 'nif': req.params.nif }, function (err, client) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (client == null) {
            res.status(404).json({
                message: "There is no client with that NIF!"
            });
            return;
        }

        res.json(client);
    });
};

exports.getByEmail = function (req, res) {
    var email = req.params.email.toLowerCase();
    _clientModel2.default.findOne({ 'email': email }, function (err, client) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (client == null) {
            res.status(404).json({
                message: "There is no client with that e-mail address!"
            });
            return;
        }

        res.json(client);
    });
};

exports.verifyClient = function (req, res) {
    _clientModel2.default.findOne({ email: req.params.email }, function (err, foundClient) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (req.body.password == foundClient.password) {
            res.json({
                verified: true
            });
        } else {
            res.json({
                verified: false
            });
        }
    });
};

exports.getClients = function (req, res) {
    _clientModel2.default.find({}, function (err, clients) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json(clients);
    });
};

exports.createClient = function (req, res) {
    req.body.password = md5(req.body.password);
    var newClient = new _clientModel2.default(req.body);

    newClient.save(function (err, client) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json(client);
    });
};

exports.updateClient = function (req, res) {
    _clientModel2.default.findOneAndUpdate({
        _id: req.params.clientID
    }, req.body, function (err, client) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json(client);
    });
};

exports.deleteClient = function (req, res) {
    _clientModel2.default.deleteOne({
        _id: req.params.clientID
    }, function (err) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json({
            message: 'Client ' + req.params.clientID + ' successfully deleted'
        });
    });
};

exports.deleteByNif = function (req, res) {
    _clientModel2.default.deleteOne({
        nif: req.params.nif
    }, function (err) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json({
            message: 'Client ' + req.params.nif + ' successfully deleted'
        });
    });
};

exports.deleteByEmail = function (req, res) {
    _clientModel2.default.deleteOne({
        email: req.params.email
    }, function (err) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json({
            message: 'Client ' + req.params.email + ' successfully deleted'
        });
    });
};
//# sourceMappingURL=clientController.js.map