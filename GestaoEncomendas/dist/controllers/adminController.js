"use strict";

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _adminModel = require("../models/adminModel");

var _adminModel2 = _interopRequireDefault(_adminModel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.getAdmins = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _adminModel2.default.find({}, function (err, admins) {
                            if (err) {
                                res.status(500).send(err);
                                return;
                            }

                            res.json(admins);
                        });

                    case 1:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function (_x, _x2) {
        return _ref.apply(this, arguments);
    };
}();

exports.getAdmin = function (req, res) {
    _adminModel2.default.findById(req.params.adminID, function (err, admin) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (admin == null) {
            res.status(404).json({
                message: "There is no admin with that ID!"
            });
            return;
        }

        res.json(admin);
    });
};

exports.getAdminByUsername = function (req, res) {
    _adminModel2.default.findOne({ username: req.params.username }, function (err, admin) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (admin == null) {
            res.status(404).json({
                message: "There is no admin with that username!"
            });
            return;
        }

        res.json(admin);
    });
};

exports.verifyAdmin = function (req, res) {
    _adminModel2.default.findOne({ username: req.params.username }, function (err, foundAdmin) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        if (req.body.password == foundAdmin.password) {
            res.json({
                verified: true
            });
        } else {
            res.json({
                verified: false
            });
        }
    });
};

exports.createAdmin = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
        var newAdmin;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        newAdmin = new _adminModel2.default(req.body);


                        newAdmin.save(function (err, admin) {
                            if (err) {
                                res.status(500).send(err);
                                return;
                            }

                            res.json(admin);
                        });

                    case 2:
                    case "end":
                        return _context2.stop();
                }
            }
        }, _callee2, undefined);
    }));

    return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
    };
}();

exports.updateAdmin = function (req, res) {
    _adminModel2.default.findOneAndUpdate({
        _id: req.params.adminID
    }, req.body, function (err, admin) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json(admin);
    });
};

exports.deleteAdmin = function (req, res) {
    _adminModel2.default.deleteOne({
        _id: req.params.adminID
    }, function (err) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.json({
            message: "Admin " + req.params.adminID + " successfully deleted"
        });
    });
};
//# sourceMappingURL=adminController.js.map