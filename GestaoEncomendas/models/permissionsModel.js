import mongoose, {
    Schema
} from 'mongoose';

const PermissionScheme = new Schema(
    {
        default: {
            type: String,
            default: "default",
            required: true
        },

        login: {
            type: Boolean,
            default: true,
            index: true
        },

        register: {
            type: Boolean,
            default: true,
            index: true
        },

        clientChangeName: {
            type: Boolean,
            default: true,
            index: true
        },

        clientChangeAddress: {
            type: Boolean,
            default: true,
            index: true
        },

        placeOrder: {
            type: Boolean,
            default: true,
            index: true
        },

        viewOrders: {
            type: Boolean,
            default: true,
            index: true
        },

        orderUpdateAddress: {
            type: Boolean,
            default: true,
            index: true
        },

        cancelOrder: {
            type: Boolean,
            default: true,
            index: true
        },

        viewMostBoughtProducts: {
            type: Boolean,
            default: true,
            index: true
        },

        viewMostTimeBoughtProducts: {
            type: Boolean,
            default: true,
            index: true
        },

        viewFastestToMakeProducts: {
            type: Boolean,
            default: true,
            index: true
        }
    },
    { timestamps: true }
)

export default mongoose.model('Permissions', PermissionScheme);