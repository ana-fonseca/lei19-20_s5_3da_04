import mongoose, {
    Schema
} from 'mongoose';

const AdminScheme = new Schema(
    {
        name: {
            type: String,
            required: [true, 'Please enter a full name'],
            index: true,
        },

        username: {
            type: String,
            required: true,
            index: true,
            unique: true
        },

        password: {
            type: String,
            required: true
        },

        salt: String,
    },
    { timestamps: true },
);

export default mongoose.model('Admin', AdminScheme)