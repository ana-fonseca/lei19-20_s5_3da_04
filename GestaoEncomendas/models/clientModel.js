import mongoose, {
  Schema
} from 'mongoose';

const AddressScheme = new Schema(
  {
    country: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    number: {
      type: String,
      required: true,
    },
    street: {
      type: String,
      required: true,
    },
    postalCode: {
      type: String,
      required: true
    }
  }, {
  _id: false
}
)

/**
 * Create database scheme for clients
 */
const ClientScheme = new Schema(
  {
    name: {
      type: String,
      required: [true, 'Please enter a full name'],
      index: true,
    },

    email: {
      type: String,
      lowercase: true,
      unique: true,
      index: true,
    },

    nif: {
      type: Number,
      required: true,
      index: true,
      unique: true
    },

    address: {
      type: AddressScheme
    },

    password: {
      type: String,
      required: true
    },

    salt: String,
  },
  { timestamps: true },
);

export default mongoose.model('Client', ClientScheme);