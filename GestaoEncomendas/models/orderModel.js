import mongoose, {
    Schema
} from 'mongoose';

var ObjectId = mongoose.Schema.Types.ObjectId;

const AddressScheme = new Schema(
    {
        country: {
            type: String,
            required: true,
        },
        city: {
            type: String,
            required: true,
        },
        number: {
            type: String,
            required: true,
        },
        street: {
            type: String,
            required: true,
        },
        postalCode: {
            type: String,
            required: true
        }
    },{
      _id: false
    }
  )

const ProductScheme = new Schema(
    {
        productID: {
            type: Number,
            required: true,
            index: true
        },

        productDescription: {
            type: String,
            required: true,
            index: true
        },

        quantity: {
            type: Number,
            required: true,
            index: true,
        }
    }, {    
        _id: false
    }
)

/**
 * Create database scheme for clients
 */
const OrderScheme = new Schema(
    {
        clientID: {
            type: ObjectId,
            required: true,
            index: true
        },

        useUserAddress: {
            type: Boolean,
            required: true,
            index: true
        },

        billingAddress: {
            type: AddressScheme,
            required: false
        },

        products: [
            ProductScheme
        ],

        dueDate: {
            type: Date,
            required: true,
            min: () => new Date(+new Date() + 3*24*60*60*1000)
        },

        deliveryDate: {
            type: Date,
            required: false,
        },

        isActive: {
            type: Boolean,
            required: true
        },

        salt: String,
    },
    { timestamps: true },
);

export default mongoose.model('Order', OrderScheme);