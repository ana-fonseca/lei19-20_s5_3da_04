import mongoose from 'mongoose';
import client from '../models/clientModel.js';
var clientMapper = require("../mapper/clientMapper")

var response = {
    status: 200,
    error: null,
    data: null
}

function resetResponse() {
    response = {
        status: 200,
        error: null,
        data: null
    }
}
 
exports.getClient = (clientID, callback) => {
    resetResponse()
    client.findOne( { _id : clientID }, (err, client) => {
        response.data = client
        if (err || client == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "Invalid ID!" : "There is no client with that ID!") }
        }

        callback(response)
    })
}

exports.getClients = (callback) => {
    resetResponse()
    client.find({}, (err, clients) => {
        response.data = clients
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }
        }

        callback(response)
    })
}

exports.getByEmail = (email, callback) => {
    resetResponse()
    client.findOne({ 'email': email }, (err, client) => {
        response.data = client
        if (err || client == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "An error has occurred!" : "There is no client with that e-mail address!") }
        }

        callback(response)
    })
}

exports.getByNif = (nif, callback) => {
    resetResponse()
    client.findOne({ 'nif': nif }, (err, client) => {
        response.data = client
        if (err || client == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "An error has occurred!" : "There is no client with that NIF!") }
        }

        callback(response)
    })
}

exports.verifyClient = (email, password, callback) => {
    resetResponse()
    client.findOne({ email: email }, (err, client) => {
        if (err || client == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "An error has occurred!" : "There is no client with that e-mail address!") }

            callback(response)
            return
        }

        response.data = { verified: client.password == password }

        callback(response)
    })
}

exports.createClient = (clientDomain, callback) => {
    resetResponse()

    var modelClient = clientMapper.domainToModel(clientDomain)
    modelClient.save((err, client) => {
        response.data = client
        if (err) {
            console.log(err)
            response.status = 500
            var errorMessage = (err.errmsg.includes("email") || err.errmsg.includes("nif")) ? "There already is a client with that " + (err.errmsg.includes("email") ? "e-mail address!" : "NIF") : "An error has occurred!"
            response.error = { errorMessage: errorMessage }
        }

        callback(response)
    });
}

exports.updateClient = (clientID, updatedClient, callback) => {
    resetResponse()
    client.findOneAndUpdate({ _id: clientID }, updatedClient, (err, client) => {
        response.data = client
        if (err) {
            response.status = 500
            var errorMessage = err.errmsg.includes("email") ? "There already is a client with that e-mail address" : "An error has occurred!"
            response.error = { errorMessage: errorMessage }
        }

        callback(response)
    });
}

exports.deleteClient = (clientID, callback) => {
    resetResponse()
    client.deleteOne({ _id: clientID }, (err) => {
        response.data = { message: `Client ${clientID} deleted successfully!`}
        if (err) {
            response.status = 500
            response.error = { errorMessage: "There is no client with that ID!" }
        }

        callback(response)
    });
}

exports.deleteByNif = (nif, callback) => {
    resetResponse()
    client.deleteOne({ nif: nif }, (err) => {
        response.data = { message: `Client with the ${nif} NIF deleted successfully!`}
        if (err) {
            response.status = 500
            response.error = { errorMessage: "There is no client with that NIF!" }
        }

        callback(response)
    });
}

exports.deleteByEmail = (email, callback) => {
    resetResponse()
    client.deleteOne({ email: email }, (err) => {
        response.data = { message: `Client with the ${email} e-mail address deleted successfully!`}
        if (err) {
            response.status = 500
            response.error = { errorMessage: "There is no client with that e-mail address!" }
        }

        callback(response)
    });
}