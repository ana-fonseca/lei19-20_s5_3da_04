import mongoose from 'mongoose';
import admin from '../models/adminModel'
var adminMapper = require("../mapper/adminMapper")
var md5 = require("md5")

var response = {
    status: 200,
    error: null,
    data: null
}

function resetResponse() {
    response = {
        status: 200,
        error: null,
        data: null
    }
}
 
exports.getAdmin = (adminID, callback) => {
    resetResponse()
    admin.findOne( { _id : adminID }, (err, admin) => {
        response.data = admin
        if (err || admin == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "Invalid ID!" : "There is no admin with that ID!") }
        }

        callback(response)
    })
}

exports.getAdmins = (callback) => {
    resetResponse()
    admin.find({}, (err, admins) => {
        response.data = admins
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }
        }

        callback(response)
    })
}

exports.getByUsername = (username, callback) => {
    resetResponse()
    admin.findOne({ 'username': username }, (err, admin) => {
        response.data = admin
        if (err || admin == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "An error has occurred!" : "There is no admin with that username!") }
        }

        callback(response)
    })
}

exports.verifyAdmin = (username, password, callback) => {
    resetResponse()
    admin.findOne({ username: username }, (err, admin) => {
        if (err || admin == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "An error has occurred!" : "There is no admin with that username!") }

            callback(response)
            return
        }

        response.data = { verified: admin.password == password }

        callback(response)
    })
}

exports.createAdmin = (adminBody, callback) => {
    resetResponse()

    var modelAdmin = adminMapper.domainToModel(adminBody)
    modelAdmin.save((err, admin) => {
        response.data = admin
        if (err) {
            response.status = 500
            response.error = { errorMessage: err.errmsg.includes("username") ? "That username is already being used!" : "An error has occurred!" }
        }

        callback(response)
    });
}

exports.updateAdmin = (adminID, updatedAdmin, callback) => {
    resetResponse()
    admin.findOneAndUpdate({ _id: adminID }, updatedAdmin, (err, admin) => {
        response.data = admin
        if (err) {
            response.status = 500
            response.error = { errorMessage: err.errmsg.includes("username") ? "That username is already being used!" : "An error has occurred!" }
        }

        callback(response)
    });
}

exports.deleteAdmin = (username, callback) => {
    resetResponse()
    admin.deleteOne({ username: username }, (err) => {
        response.data = { message: `Admin ${username} deleted successfully!`}
        if (err) {
            response.status = 500
            response.error = { errorMessage: "There is no admin with that username!" }
        }

        callback(response)
    });
}

exports.createRootUser = (callback) => {
    admin.findOne({ username: "root" }, (err, searchAdmin) => {
        if (searchAdmin == null) {
            var root = {
                name: "Data Administrator",
                username: "root",
                password: md5("3DA4root")
            }

            const rootAdmin = new admin(root)
            rootAdmin.save((err, admin) => {
                if (err) {
                    return
                }
                return
            })
        }
        callback()
    })
}