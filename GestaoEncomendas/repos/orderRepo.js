import mongoose from 'mongoose';
import order from '../models/orderModel';
import client from '../models/clientModel.js';
var clientRepo = require("./clientRepo")
var clientMapper = require("../mapper/clientMapper")
var orderMapper = require("../mapper/orderMapper")

var response = {
    status: 200,
    error: null,
    data: null
}

function resetResponse() {
    response = {
        status: 200,
        error: null,
        data: null
    }
}

exports.getOrder = (orderID, callback) => {
    resetResponse()
    order.findOne({ _id: orderID }, (err, order) => {
        response.data = order
        if (err || order == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: (err ? "Invalid ID!" : "There is no order with that ID!") }
        }

        callback(response)
    })
}

exports.getOrders = (callback) => {
    resetResponse()
    order.find({}, (err, orders) => {
        response.data = orders
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }
        }

        callback(response)
    })
}

exports.getClientOrders = (clientID, callback) => {
    resetResponse()
    order.find({ 'clientID': clientID }, (err, orders) => {
        response.data = orders
        if (err) {
            response.status = 500
            response.error = { errorMessage: "Invalid ID!" }
        }

        callback(response)
    })
}

exports.createOrder = (orderBody, callback) => {
    resetResponse()
    if (orderBody.useUserAddress) {
        clientRepo.getClient(orderBody.clientID.clientID, (response) => {
            if (response.error != null) {
                callback(response)
                return
            }

            var domainClient = clientMapper.modelToDomain(response.data)

            orderBody.billingAddress = domainClient.address
            saveOrder(orderBody, callback)
        })
    } else {
        saveOrder(orderBody, callback)
    }
}

function saveOrder (orderBody, callback) {
    var modelOrder = orderMapper.domainToModel(orderBody)
    modelOrder.save((err, order) => {
        response.data = order
        if (err) {
            response.status = 500
            response.error = { errorMessage: err.message.includes("dueDate") ? "The due date is invalid! It must be at least 3 days from now!" : err }
        }

        callback(response)
    });
}

exports.updateOrder = (orderID, updatedOrder, callback) => {
    resetResponse()
    order.findOneAndUpdate({ _id: orderID }, updatedOrder, (err, order) => {
        response.data = order
        if (err || order == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: err ? (err.message.includes("dueDate") ? "The due date is invalid! It must be at least 3 days from now!" : "An error has occurred") : "There is no order with that ID!" }
        }

        callback(response)
    });
}

exports.setDeliveryDate = (orderID, deliveryDate, callback) => {
    resetResponse()
    order.findOneAndUpdate({ _id: orderID }, deliveryDate, (err, order) => {
        response.data = order
        if (err || order == null) {
            response.status = err ? 500 : 404
            response.error = { errorMessage: err ? "An error has occurred" : "There is no order with that ID!" }
        }

        callback(response)
    });
}

exports.deleteOrder = (orderID, callback) => {
    resetResponse()
    order.deleteOne({ _id: orderID }, (err) => {
        response.data = { message: `Order ${orderID} deleted successfully!` }
        if (err) {
            response.status = 404
            response.error = { errorMessage: "There is no order with that ID!" }
        }

        callback(response)
    });
}

exports.deleteAll = (callback) => {
    resetResponse()
    order.deleteMany({}, (err) => {
        response.data = { message: `All orders deleted successfully!` }
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }
        }

        callback(response)
    });
}

exports.cancelOrder = (orderID, callback) => {
    resetResponse()
    order.findOneAndUpdate({ _id: orderID }, { isActive: false }, (err, order) => {
        response.data = order
        if (err || order == null) {
            response.status = 404
            response.error = { errorMessage: "There is no order with that ID!" }
        }

        callback(response)
    })
}