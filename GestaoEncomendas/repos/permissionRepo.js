import permissions from "../models/permissionsModel"

var response = {
    status: 200,
    error: null,
    data: null
}

function resetResponse() {
    response = {
        status: 200,
        error: null,
        data: null
    }
}

exports.getPermissions = (callback) => {
    resetResponse()
    permissions.findOne({ default: "default" }, (err, permissions) => {
        response.data = permissions
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }
        }

        callback(response)
    })
}

exports.updatePermissions = (updatedPermissions, callback) => {
    resetResponse()
    permissions.findOneAndUpdate({ default: "default" }, updatedPermissions, (err, permissions) => {
        response.data = permissions
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }
        }

        callback(response)
    })
}

exports.resetPermissions = (callback) => {
    resetResponse()
    permissions.findOneAndDelete({ default: "default" }, (err) => {
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }

            callback(response)
        } else {
            createDefaultPermissionsInternal(callback)
        }
    })
}

exports.createDefaultPermissions = (callback) => {
    permissions.findOne({ default: "default" }, (err, permissions) => {
        if (permissions == null) {
            createDefaultPermissionsInternal(callback)
        }
    })
}

function createDefaultPermissionsInternal (callback) {
    resetResponse()
    const newPermission = new permissions({})
    newPermission.save((err, permissions) => {
        response.data = permissions
        if (err) {
            response.status = 500
            response.error = { errorMessage: "An error has occurred!" }
        }

        callback(response)
    })
}