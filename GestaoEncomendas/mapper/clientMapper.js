import Client from "../domain/models/clientDomain"
import Address from "../domain/valueObjects/address"
import Email from "../domain/valueObjects/email"
import Name from "../domain/valueObjects/name"
import Password from "../domain/valueObjects/password"
import Nif from "../domain/valueObjects/nif"
import ClientDTO from "../DTO/clientDTO"
import client from "../models/clientModel"

exports.jsonToDTO = (jsonBody) => {
    var name = jsonBody.name
    var email = jsonBody.email
    var password = jsonBody.password
    var nif = jsonBody.nif
    var street = jsonBody.address.street
    var number = jsonBody.address.number
    var city = jsonBody.address.city
    var country = jsonBody.address.country
    var postalCode = jsonBody.address.postalCode

    return new ClientDTO(name, email, password, nif, street, number, city, country, postalCode)
}

exports.modelToDomain = (modelClient) => {
    var address = new Address(modelClient.address.street, modelClient.address.number, modelClient.address.city, modelClient.address.country, modelClient.address.postalCode)
    var email = new Email(modelClient.email)
    var nif = new Nif(modelClient.nif)
    var name = new Name(modelClient.name)
    var password = new Password(modelClient.password)

    var client = new Client(name, email, nif, password, address)
    client.insertID(modelClient._id)

    return client
}

exports.domainToDTO = (domainClient) => {
    var name = domainClient.name.toString()
    var email = domainClient.email.email
    var password = domainClient.password.password
    var nif = domainClient.nif.nif
    var street = domainClient.address.getStreet()
    var number = domainClient.address.getNumber()
    var city = domainClient.address.getCity()
    var country = domainClient.address.getCountry()
    var postalCode = domainClient.address.getPostalCode()

    var client = new ClientDTO(name, email, password, nif, street, number, city, country, postalCode)
    client.insertID(domainClient._id)

    return client
}

exports.dtoToDomain = (dtoClient) => {
    var address = new Address(dtoClient.address.street, dtoClient.address.number, dtoClient.address.city, dtoClient.address.country, dtoClient.address.postalCode)
    var nif = new Nif(dtoClient.nif)
    var email = new Email(dtoClient.email)
    var name = new Name(dtoClient.name)
    var password = new Password(dtoClient.password)

    return new Client(name, email, nif, password, address)
}

exports.domainToModel = (domainClient) => {
    var modelClient = {
        name: domainClient.name.toString(),
        email: domainClient.email.email,
        nif: domainClient.nif.nif,
        password: domainClient.password.password,
        address: {
            street: domainClient.address.street,
            number: domainClient.address.number,
            city: domainClient.address.city,
            country: domainClient.address.country,
            postalCode: domainClient.address.postalCode
        }
    }

    return new client(modelClient)
}