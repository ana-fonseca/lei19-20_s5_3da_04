import Order from "../domain/models/orderDomain"
import Address from "../domain/valueObjects/address"
import ClientID from "../domain/valueObjects/clientID"
import Product from "../domain/valueObjects/product"
import DueDate from "../domain/valueObjects/dueDate"
import OrderDTO from "../DTO/orderDTO"
import order from "../models/orderModel"

exports.jsonToDTO = (jsonBody) => {
    var clientID = jsonBody.clientID
    var useUserAddress = jsonBody.useUserAddress

    var street = null
    var number = null
    var city = null
    var country = null
    var postalCode = null

    if (!useUserAddress) {
        street = jsonBody.billingAddress.street
        number = jsonBody.billingAddress.number
        city = jsonBody.billingAddress.city
        country = jsonBody.billingAddress.country
        postalCode = jsonBody.billingAddress.postalCode
    }

    var products = jsonBody.products
    var dueDate = jsonBody.dueDate
    var isActive = jsonBody.isActive

    return new OrderDTO(clientID, useUserAddress, street, number, city, country, postalCode, products, dueDate, isActive)
}

exports.modelToDomain = (modelOrder) => {
    var billingAddress = new Address(modelOrder.billingAddress.street, modelOrder.billingAddress.number, modelOrder.billingAddress.city, modelOrder.billingAddress.country, modelOrder.billingAddress.postalCode)
    var clientID = new ClientID(modelOrder.clientID)
    var useUserAddress = modelOrder.useUserAddress
    var modelProducts = modelOrder.products

    var products = []
    for (var i = 0; i < modelProducts.length; i++) {
        products.push(new Product(modelProducts[i].productID, modelProducts[i].productDescription, modelProducts[i].quantity))
    }

    var dueDate = new DueDate(modelOrder.dueDate)
    var isActive = modelOrder.isActive
    

    var order = new Order(clientID, useUserAddress, billingAddress, products, dueDate, isActive)
    order.insertID(modelOrder._id)

    if (modelOrder.deliveryDate != undefined) {
        order.setDeliveryDate(new DueDate(modelOrder.deliveryDate))
    }

    return order
}

exports.domainToDTO = (domainOrder) => {
    var clientID = domainOrder.clientID.clientID
    var useUserAddress = domainOrder.useUserAddress
    var domainProducts = domainOrder.products

    var products = []
    for (var i = 0; i < domainProducts.length; i++) {
        products.push({ productID: domainProducts[i].productID, productDescription: domainProducts[i].productDescription, quantity: domainProducts[i].quantity })
    }

    var street = domainOrder.billingAddress.getStreet()
    var number = domainOrder.billingAddress.getNumber()
    var city = domainOrder.billingAddress.getCity()
    var country = domainOrder.billingAddress.getCountry()
    var postalCode = domainOrder.billingAddress.getPostalCode()

    var dueDate = domainOrder.dueDate.toString()
    var isActive = domainOrder.isActive

    var order = new OrderDTO(clientID, useUserAddress, street, number, city, country, postalCode, products, dueDate, isActive)
    order.insertID(domainOrder._id)

    if (domainOrder.deliveryDate != undefined && domainOrder.deliveryDate != null) {
        order.setDeliveryDate(domainOrder.deliveryDate.toString())
    }

    return order
}

exports.dtoToDomain = (dtoOrder) => {
    var useUserAddress = dtoOrder.useUserAddress

    var billingAddress = null
    if (!useUserAddress) {
        billingAddress = new Address(dtoOrder.billingAddress.street, dtoOrder.billingAddress.number, dtoOrder.billingAddress.city, dtoOrder.billingAddress.country, dtoOrder.billingAddress.postalCode)
    }

    var clientID = new ClientID(dtoOrder.clientID)
    var dtoProducts = dtoOrder.products

    var products = []
    for (var i = 0; i < dtoProducts.length; i++) {
        products.push(new Product(dtoProducts[i].productID, dtoProducts[i].productDescription, dtoProducts[i].quantity))
    }

    var dueDate = new DueDate(dtoOrder.dueDate)
    var isActive = dtoOrder.isActive

    return new Order(clientID, useUserAddress, billingAddress, products, dueDate, isActive)
}

exports.domainToModel = (domainOrder) => {
    var domainProducts = domainOrder.products

    var products = []
    for (var i = 0; i < domainProducts.length; i++) {
        products.push({ productID: domainProducts[i].productID, productDescription: domainProducts[i].productDescription, quantity: domainProducts[i].quantity })
    }

    var modelOrder = {
        clientID: domainOrder.clientID.clientID,
        useUserAddress: domainOrder.useUserAddress,
        billingAddress: {
            street: domainOrder.billingAddress.street,
            number: domainOrder.billingAddress.number,
            city: domainOrder.billingAddress.city,
            country: domainOrder.billingAddress.country,
            postalCode: domainOrder.billingAddress.postalCode
        },
        products: products,
        dueDate: domainOrder.dueDate.toString(),
        isActive: domainOrder.isActive
    }

    return new order(modelOrder)
}