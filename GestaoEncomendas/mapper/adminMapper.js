import Admin from "../domain/models/adminDomain"
import Username from "../domain/valueObjects/username"
import Name from "../domain/valueObjects/name"
import Password from "../domain/valueObjects/password"
import AdminDTO from "../DTO/adminDTO"
import admin from "../models/adminModel"

exports.jsonToDTO = (jsonBody) => {
    var name = jsonBody.name
    var username = jsonBody.username
    var password = jsonBody.password

    return new AdminDTO(name, username, password)
}

exports.modelToDomain = (modelAdmin) => {
    var username = new Username(modelAdmin.username)
    var name = new Name(modelAdmin.name)
    var password = new Password(modelAdmin.password)

    var admin = new Admin(name, username, password)
    admin.insertID(modelAdmin._id)

    return admin
}

exports.domainToDTO = (domainAdmin) => {
    var name = domainAdmin.name.toString()
    var username = domainAdmin.username.username
    var password = domainAdmin.password.password

    var admin = new AdminDTO(name, username, password)
    admin.insertID(domainAdmin._id)

    return admin
}

exports.dtoToDomain = (dtoAdmin) => {
    var username = new Username(dtoAdmin.username)
    var name = new Name(dtoAdmin.name)
    var password = new Password(dtoAdmin.password)

    return new Admin(name, username, password)
}

exports.domainToModel = (domainAdmin) => {
    var modelAdmin = {
        name: domainAdmin.name.toString(),
        username: domainAdmin.username.username,
        password: domainAdmin.password.password,
    }

    return new admin(modelAdmin)
}