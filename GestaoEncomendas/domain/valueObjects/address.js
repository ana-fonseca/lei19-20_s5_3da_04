"use strict";
exports.__esModule = true;
var countryList = require("countries-cities");
var Address = /** @class */ (function () {
    function Address(street, number, city, country, postalCode) {
        var _this = this;

        this.getStreet = () => {
            return _this.street
        }
        this.getNumber = () => {
            return _this.number
        }
        this.getCity = () => {
            return _this.city
        }
        this.getCountry = () => {
            return _this.country
        }
        this.getPostalCode = () => {
            return _this.postalCode
        }

        if (countryList.getCountries().includes(country)) {
            this.country = country;
            if (countryList.getCities(this.country).includes(city)) {
                this.city = city;
                if (postalCode.split("-").length == 2) {
                    this.street = street;
                    this.number = number;
                    this.postalCode = postalCode
                } else {
                    throw new Error("Invalid postal code!");
                }
            }
            else {
                throw new Error("Invalid city!");
            }
        }
        else {
            throw new Error("Invalid country!");
        }
    }
    return Address;
}());
module.exports = Address;
exports["default"] = Address;
