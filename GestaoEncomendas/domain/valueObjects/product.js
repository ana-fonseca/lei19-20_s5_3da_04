"use strict";
exports.__esModule = true;
var Product = /** @class */ (function () {
    function Product(productID, productDescription, quantity) {
        var _this = this;

        this.getProductID = () => {
            return _this.productID
        }
        this.getProductDescription = () => {
            return _this.productDescription
        }
        this.getQuantity = () => {
            return _this.quantity
        }

        this.productID = productID
        this.productDescription = productDescription
        this.quantity = quantity
    }
    return Product;
}());
module.exports = Product;
exports["default"] = Product;
