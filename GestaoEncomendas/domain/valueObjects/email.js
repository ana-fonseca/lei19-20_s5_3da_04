"use strict";
exports.__esModule = true;
var validator = require("email-validator");
var Email = /** @class */ (function () {
    function Email(email) {
        if (validator.validate(email)) {
            this.email = email;
        }
        else {
            throw new Error("Invalid e-mail address!");
        }
    }
    return Email;
}());
exports["default"] = Email;
