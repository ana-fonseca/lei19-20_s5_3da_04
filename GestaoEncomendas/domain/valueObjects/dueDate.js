"use strict";
exports.__esModule = true;
var DueDate = /** @class */ (function () {
    function DueDate(dueDate) {
        var _this = this;
        this.toString = () => {
            return _this.dueDate.toJSON().split("T")[0]
        }

        var date = null

        try {
            date = new Date(dueDate)
        } catch (error) {
            throw new Error("Invalid Date Format!")
        }

        this.dueDate = date
    }
    return DueDate;
}());
exports["default"] = DueDate;