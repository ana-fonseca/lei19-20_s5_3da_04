"use strict";
exports.__esModule = true;

var Name = /** @class */ (function () {
    
    function Name(name) {
        
        var _this = this;
        this.toString = function () {
            return ""+_this.firstName + " " + _this.lastName;
        };

        if ((name+'').split(' ').length > 1) {
            this.firstName = (name+'').split(" ")[0].trim();
            this.lastName = (name+'').substr(this.firstName.length).trim();
        }
        else {
            throw new Error("Invalid name!");
        }
    }
    return Name;
}());
exports["default"] = Name;
