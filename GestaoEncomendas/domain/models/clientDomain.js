"use strict";
exports.__esModule = true;
var Client = /** @class */ (function () {
    function Client(name, email, nif, password, address) {
        this.insertID = (id) => {
            this._id = id
        }

        this.name = name;
        this.email = email;
        this.nif = nif
        this.password = password;
        this.address = address;
    }
    return Client;
}());
exports["default"] = Client;
