"use strict";
exports.__esModule = true;
var Admin = /** @class */ (function () {
    function Admin(name, username, password) {
        this.insertID = (id) => {
            this._id = id
        }
        
        this.name = name;
        this.username = username
        this.password = password;
    }
    return Admin;
}());
exports["default"] = Admin;