"use strict";
exports.__esModule = true;
var Order = /** @class */ (function () {
    function Order(clientID, useUserAddress, billingAddress, products, dueDate, isActive) {
        this.insertID = (id) => {
            this._id = id
        }

        this.setDeliveryDate = (deliveryDate) => {
            this.deliveryDate = deliveryDate
        }
        
        if (!Array.isArray(products)) {
            throw new Error("There must be a list of products!")
        }
        this.clientID = clientID
        this.useUserAddress = useUserAddress
        this.billingAddress = billingAddress
        this.products = products
        this.dueDate = dueDate
        this.isActive = isActive
    }
    return Order;
}());
exports["default"] = Order;
