"use strict";
exports.__esModule = true;
var OrderDTO = /** @class */ (function () {
    function OrderDTO(clientID, useUserAddress, street, number, city, country, postalCode, products, dueDate, isActive) {
        this.insertID = (id) => {
            this._id = id
        }

        this.setDeliveryDate = (deliveryDate) => {
            this.deliveryDate = deliveryDate
        }

        this.clientID = clientID;
        this.useUserAddress = useUserAddress;
        this.billingAddress = {
            country: country,
            city: city,
            street: street,
            number: number,
            postalCode: postalCode
        }
        this.products = products
        this.dueDate = dueDate
        this.isActive = isActive
    }
    return OrderDTO;
}());
exports["default"] = OrderDTO;
