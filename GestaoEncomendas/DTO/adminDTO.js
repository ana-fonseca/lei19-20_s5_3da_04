"use strict";
exports.__esModule = true;
var AdminDTO = /** @class */ (function () {
    function AdminDTO(name, username, password) {
        this.insertID = (id) => {
            this._id = id
        }

        this.name = name;
        this.username = username
        this.password = password;
    }
    return AdminDTO;
}());
exports["default"] = AdminDTO;