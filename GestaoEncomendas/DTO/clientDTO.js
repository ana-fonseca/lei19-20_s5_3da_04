"use strict";
exports.__esModule = true;
var ClientDTO = /** @class */ (function () {
    function ClientDTO(name, email, password, nif, street, number, city, country, postalCode) {
        this.insertID = (id) => {
            this._id = id
        }

        this.name = name;
        this.email = email;
        this.password = password;
        this.nif = nif
        this.address = {
            country: country,
            city: city,
            street: street,
            number: number,
            postalCode: postalCode
        }
    }
    return ClientDTO;
}());
exports["default"] = ClientDTO;
