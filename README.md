## C4 Model

To provide an effective way to communicate software architecture we will be using Unified Modified Language (UML) in all the diagrams located in the Design Folder.
As a team, to meet the project requirements, we will be using C4 Model to document this project.

The C4 model provided us a way to efficiently and effectively communicate the project's architecture, at different levels of detail. It is an abstraction-first approach.
C4 Model divides architecture into 4 levels, this includes:

##### Level 1: 

**System Context.** This should provide a starting point, It should show how the software interacts with it's users (world). This means, however you think users (actors, roles,...) will use your software, which is the highest level of abstraction and describes something that delivers value.  
In this level detail is not important and It should be simple.  

##### Level 2: 
  
**Container.** It should zoom into the software system, showing the high level technical building blocks (projects) and how they interact with each other.   
  
**What is a Container?**  
A container represents a host code or data. It is somethig that needs to be runing in order for the software to work. It could be a Server-side web application, Client-side web application, Client-side desktop application, Database,...
In our project, the containers are Master Data Factory (MDF), Master Data Product (MDP), Order Management (GE) and It's REST APIs.  
"A container is essentially a context or boundary inside which some code is executed or some data is stored. And each container is a separately deployable/runnable thing or runtime environment, typically (but not always) running in its own process space. Because of this, communication between containers typically takes the form of an inter-process communication." (http://c4model.com)

##### Level 3: 

**Component.** These diagrams should zoom into the containers, showing it's components and how they interact with each other.  
  
**What is a Component?**  
in this context a component is simply a grouping of related functionality encapsulated behind a well-defined interface.  

##### Level 4: 

**Code.** Zoom into each component and show implementation.
  
  
  
## 4+1 Model  
  
    
      
    "Software architecture deals with the design and implementation of the high-level structure of the software. It
    is the result of assembling a certain number of architectural elements in some well-chosen forms to satisfy
    the major functionality and performance requirements of the system, as well as some other, non-functional
    requirements such as reliability, scalability, portability, and availability."
    Philippe Kruchten, a University Professor and Software Engineer  
    
  
  Architecture deals with abstraction, decomposition and composition, style and aesthetics. For that reason, to describe It, we use multiple **views** or perspectives.  

  
  **Logical**: object model of the design.  
  
  **Process**: concurrency and synchronization aspects of the design.  
    
  **Physical**: mapping(s) of the software onto the hardware and reflects its
distributed aspect.  
    
  **Development**: static organization of the software in its development
environment.  
  
The description of an architecture—the decisions made—can be organized around these four views, and
then illustrated by a few selected use cases, or **scenarios** _which become a fifth view._
  
![4+1Model](/Design/4+1.png)

## 4+1 Diagrams, using C4 Model

|   | 1 | 2 | 3  (MDF) | 
|---|---|---|---|---|---|
| Logical  | **[L1](/Design/ContextDiagram/)**  | **[L2](/Design/DesignLevel2/Logical/)** | **[L3](/Design/DesignLevel3/Logical/)** |  
| Process  | - | **[P2](/Design/DesignLevel2/Process/)**  | **[P3](/Design/DesignLevel2/Process/)**  |   
| Implementation | - | **[I2](/Design/DesignLevel2/Implementation/)**  | **[I3](/Design/DesignLevel2/Implementation/)**  |
| Physical  | - | **[Ph2](/Design/DesignLevel2/Physical/)**  | - |   
| Scenario | - | - | - |   
  
    
      
        
          
## React  
  
  
React is a JavaScript library that aims to ease UI development and make It simple.  
React will render the right components when your data changes. It is component based,
which means It builds encapsulated components and manages them to make complex UIs. Since component
logic is written in JavaScript instead of templates, you can easily pass rich data through your app
and keep state out of the DOM.


## Postman

### Master Data Factory (5000)  
#### GET Requests
  
###### Consult Operation  
  
```
GET localhost:5000/api/Operation
GET localhost:5000/api/Operation/operationID
```
  
  
###### Consult Machine Type

```
GET localhost:5000/api/MachineType
GET localhost:5000/api/MachineType/machineTypeID
```  
  
    
###### Consult Machine || Consult Machines from Machine Types   
  
```
GET localhost:5000/api/Machine
GET localhost:5000/api/Machine/machineID
GET localhost:5000/api/Machine/FromMachineType/machineTypeID
```  
  
    
###### Consult Production Line    
    
      
```
GET localhost:5000/api/ProductionLine
GET localhost:5000/api/ProductionLine/productionLineID
```
  
  
  
#### POST Requests
  
  
###### Create Operation
    
```
POST localhost:5000/api/Operation
```

- Body (example):

```
{
	"operationID":1,
	"description":"descriptionOp1"
}

{
	"operationID":2,
	"description":"descriptionOp2"
}
```
  
###### Create Machine Type  
  
```
POST localhost:5000/api/MachineType
```  
  
- Body (example):  
  
```
{
	"machineTypeID":1,
	"operationList":[1,2],
	"description":"descriptionMf1"
}
```
  
###### Create Machine 
  
```
POST localhost:5000/api/Machine
```  
  
- Body (example):  
  
```
{
	"machineID":1,
	"machineTypeID":1,
	"description":"descriptionM1"
}
```  
  
  
###### Create Production Line 
  
```
POST localhost:5000/api/ProductionLine
```  
  
- Body (example):  
  
```
{
	"productionLineID":1,
	"machineSequence":[1],
	"description":"descriptionPl1"
}

```  
  
  
    
### Master Data Product (4000)  
#### GET Requests  

###### Consult Product  
  
```
GET localhost:4000/api/Product
GET localhost:4000/api/Product/productID  
```  
 
###### Consult Manufacture Plan  
  
  
```
GET localhost:4000/api/ManufacturePlan
GET localhost:4000/api/ManufacturePlan/manufacturePlanID
```  
  
#### POST Requests  
  
###### Create Product
  
```
POST localhost:4000/api/Products
``` 
  
  
- Body (example):
```
{
	"productID":1,
	"description":"descriptionP1"
}
```
  
###### Create Manufacture Plan
  
```
POST localhost:4000/api/ManufacturePlan
``` 
  
  
- Body (example):
```
{
	"manufacturingPlanID":1,
	"productID":1,
	"operations":[1,2],
	"description":"descriptionMp1"
}
```  
  
    
## Integration Testing

### Master Data Production  

[Integration Test File](IntegrationTests/TestMDP.postman_collection.json)  
  
    
  
### Master Data Factory  

[Integration Test File](IntegrationTests/TestMDF.postman_collection.json)