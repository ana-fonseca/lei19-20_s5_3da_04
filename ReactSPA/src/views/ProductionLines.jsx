/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";
import axios from "axios";
import http from "http";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

const MDF_URL = process.env.REACT_APP_MDF_URL
const instance = axios.create({
  httpAgent: new http.Agent({
    rejectUnauthorized: false
  })
});

class ProductionLines extends Component {
  constructor(props) {
    super(props);
    this.state = {
      machines: [],
      productionLines: []
    }

    this.handleSubmitPost = this.handleSubmitPost.bind(this)
    this.handleDeleteProductionLine = this.handleDeleteProductionLine.bind(this)
    this.resetComponent = this.resetComponent.bind(this)
  }

  componentDidMount() {
    instance.get(MDF_URL + "Machine")
      .then((result) => {
        this.setState({
          machines: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDF_URL + "ProductionLine")
      .then((result) => {
        this.setState({
          productionLines: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  resetComponent() {
    document.getElementById("productionLineID").value = null
    document.getElementById("productionLineDescription").value = null
    this.state.machines.map(item => {
      document.getElementById("create_" + item.machineID).value = null
    })
  }

  handleSubmitPost(e) {
    var productionLineID = document.getElementById("productionLineID").value;
    var productionLineDescription = document.getElementById("productionLineDescription").value;
    var machineSequence = []

    var last = 0
    var exit = false;

    this.state.machines.map(item => {
      var counter = document.getElementById("create_" + item.machineID)
      if (counter && counter.value) {
        var order = counter.value

        if (order > last) {
          last = order
        }
      }

      return null;
    })

    for (var i = 0; i < last + 1; i++) {
      machineSequence.splice(i, 0, "...")
    }

    this.state.machines.map(item => {
      var counter = document.getElementById("create_" + item.machineID)
      if (counter && counter.value) {
        var position = counter.value

        // eslint-disable-next-line
        if (position == 0) {
          return null;
        }

        // eslint-disable-next-line
        if (machineSequence[position] != "...") {
          alert("There are at least two machines in the same position!")
          exit = true;
          return null
        }

        machineSequence[position] = item.machineID
      }

      return null
    })

    if (exit) {
      return
    }

    machineSequence = machineSequence.filter(machine => machine !== "...")

    instance.post(MDF_URL + "ProductionLine", {
      productionLineID: productionLineID,
      description: productionLineDescription,
      machineSequence: machineSequence
    }).then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }

  handleDeleteProductionLine(e) {
    var select = document.getElementById("deleteSelector");
    var productionLine = select.options[select.selectedIndex].text;

    instance.delete(MDF_URL + "ProductionLine/" + productionLine)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
        }
      })
  }

  render() {
    const { machines, productionLines } = this.state
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={8}>
              <Card
                title="Create Production Line"
                content={
                  <form>
                    <FormInputs
                      ncols={["col-md-6", "col-md-6"]}
                      properties={[
                        {
                          label: "Production Line ID",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Production Line ID",
                          defaultValue: "",
                          id: "productionLineID"
                        },
                        {
                          label: "Production Line Description",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Production Line Description",
                          defaultValue: "",
                          id: "productionLineDescription"
                        }
                      ]}
                    />
                    <label>Machines (0 or empty to ignore)</label><br />
                    <ul>
                      {machines.map(item => (
                        <li key={item.machineID}>
                          <div>{item.machineID}&nbsp;&nbsp;<input type="number" min="0" id={"create_" + item.machineID} /></div>
                        </li>
                      ))}
                    </ul>
                    <Button bsStyle="info" pullRight fill onClick={this.handleSubmitPost} id="createProductionLineBtn">
                      Create Production Line
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                id="productionLineList"
                title="Production Lines"
                content={
                  <div>
                    <ul>
                      {productionLines.map(item => (
                        <li key={item.productionLineID} id={"list_" + item.productionLineID}>
                          <div><b><u>Production Line ID:</u></b> {item.productionLineID}</div>
                          <ul><u>Description:</u> {item.description}</ul>
                          <ul><u>Machine Sequence:</u></ul>
                          {item.machineSequence.map(newItem => (
                            <ul key={item.productionLineID + "_" + newItem} id={item.productionLineID + "_" + newItem}>
                              <ul>
                                <li>
                                  <div>{newItem}</div>
                                </li>
                              </ul>
                            </ul>
                          ))}
                        </li>
                      ))}
                    </ul>
                    <br />
                    <label>Delete Production Line</label>
                    <form>
                      <select id="deleteSelector">
                        {productionLines.map(item => (
                          <option key={"delete_" + item.productionLineID} value={"delete_" + item.productionLineID}>
                            {item.productionLineID}
                          </option>
                        ))}
                      </select><br />
                      <Button bsStyle="info" pullRight fill onClick={this.handleDeleteProductionLine} id="deleteProductionLineBtn">
                        Delete Production Line
                    </Button>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ProductionLines;
