/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";
import axios from "axios";
import http from "http";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

const OPERATION_URL = process.env.REACT_APP_OPERATION_URL
const instance = axios.create({
  httpAgent: new http.Agent({
    rejectUnauthorized: false
  })
});

class Operations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: []
    }

    this.handleSubmitPost = this.handleSubmitPost.bind(this)
    this.handleDeleteOperation = this.handleDeleteOperation.bind(this)
  }

  componentDidMount() {
    instance.get(OPERATION_URL)
      .then((result) => {
        this.setState({
          operations: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  resetComponent() {
    document.getElementById("operationID").value = null
    document.getElementById("operationDescription").value = null
    document.getElementById("operationTime").value = null
  }

  handleSubmitPost(e) {
    var operationID = document.getElementById("operationID").value;
    var operationDescription = document.getElementById("operationDescription").value;
    var operationTime = document.getElementById("operationTime").value;

    instance.post(OPERATION_URL, {
      operationID: operationID,
      description: operationDescription,
      time: operationTime
    }).then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }

  handleDeleteOperation(e) {
    var select = document.getElementById("deleteSelector");
    var operation = select.options[select.selectedIndex].text;

    instance.delete(OPERATION_URL + operation)
    .then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }

  render() {
    const { operations } = this.state

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={8}>
              <Card
                title="Create Operation"
                content={
                  <form>
                    <FormInputs
                      ncols={["col-md-4", "col-md-4", "col-md-4"]}
                      properties={[
                        {
                          label: "Operation ID",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Operation ID",
                          defaultValue: "",
                          id: "operationID"
                        },
                        {
                          label: "Operation Description",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Operation Description",
                          defaultValue: "",
                          id: "operationDescription"
                        },
                        {
                          label: "Operation Time (in seconds)",
                          type: "number",
                          min: 0,
                          bsClass: "form-control",
                          placeholder: "Operation Time",
                          defaultValue: "",
                          id: "operationTime"
                        }
                      ]}
                    />
                    <Button bsStyle="info" pullRight fill onClick={this.handleSubmitPost} id="createOperationBtn">
                      Create Operation
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                id="operationList"
                title="Operations"
                content={
                  <div>
                    <ul>
                      {operations.map(item => (
                        <li key={item.operationID} id={item.operationID}>
                          <div><u><b>Operation ID:</b></u> {item.operationID}</div>
                          <ul><u>Description:</u> {item.description}</ul>
                          <ul><u>Time:</u> {item.time} seconds</ul>
                        </li>
                      ))}
                    </ul>
                    <br/>
                    <label>Delete Operation</label>
                    <form>
                      <select id="deleteSelector">
                        {operations.map(item => (
                          <option key={"delete_" + item.operationID} value={"delete_" + item.operationID}>
                            {item.operationID}
                          </option>
                        ))}
                      </select><br/>
                      <Button bsStyle="info" pullRight fill onClick={this.handleDeleteOperation} id="deleteOperationBtn">
                        Delete Operation
                    </Button>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Operations;
