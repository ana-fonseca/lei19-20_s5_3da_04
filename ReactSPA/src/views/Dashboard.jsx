/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import axios from "axios";
import http from "http";
import { Grid, Row, Col } from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

const MDF_URL = process.env.REACT_APP_MDF_URL
const MDP_URL = process.env.REACT_APP_MDP_URL

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: [],
      machines: [],
      machineTypes: [],
      productionLines: [],
      products: [],
      manufacturePlans: [],
      adminID: null,
    }
  }

  componentDidMount() {
    const instance = axios.create({
      httpAgent: new http.Agent({
        rejectUnauthorized: false
      })
    });

    instance.get(MDF_URL + "Operation")
      .then((result) => {
        this.setState({
          operations: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDF_URL + "Machine")
      .then((result) => {
        this.setState({
          machines: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDF_URL + "MachineType")
      .then((result) => {
        this.setState({
          machineTypes: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDF_URL + "ProductionLine")
      .then((result) => {
        this.setState({
          productionLines: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDP_URL + "Product")
      .then((result) => {
        this.setState({
          products: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDP_URL + "ManufacturePlan")
      .then((result) => {
        this.setState({
          manufacturePlans: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  render() {
    const { operations, machines, machineTypes, productionLines, products, manufacturePlans } = this.state
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={6}>
              <Card
                statsIcon="fa fa-clock-o"
                id="operationList"
                title="Operations"
                content={
                  <ul>
                    {operations.map(item => (
                      <li key={item.operationID}>
                        <div><u><b>Operation ID:</b></u> {item.operationID}</div>
                        <ul><u>Description:</u> {item.description}</ul>
                        <ul><u>Time:</u> {item.time} seconds</ul>
                      </li>
                    ))}
                  </ul>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                statsIcon="fa fa-clock-o"
                id="machineTypeList"
                title="Machine Types"
                content={
                  <ul>
                    {machineTypes.map(item => (
                      <li key={item.machineTypeID}>
                        <div><b><u>Machine Type ID:</u></b> {item.machineTypeID}</div>
                        <ul><u>Description:</u> {item.description}</ul>
                        <ul><u>Operation List:</u></ul>
                        {item.operationList.map(newItem => (
                          <ul key={item.machineTypeID + "_" + newItem}>
                            <ul>
                              <li>
                                <div>{newItem}</div>
                              </li>
                            </ul>
                          </ul>
                        ))}
                      </li>
                    ))}
                  </ul>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Card
                statsIcon="fa fa-clock-o"
                id="machineList"
                title="Machines"
                content={
                  <ul>
                    {machines.map(item => (
                      <li key={item.machineID}>
                        <div><u><b>Machine ID:</b></u> {item.machineID}</div>
                        <ul><u>Description:</u> {item.description}</ul>
                        <ul><u>MachineType:</u> {item.machineTypeID}</ul>
                      </li>
                    ))}
                  </ul>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                statsIcon="fa fa-clock-o"
                id="productionLineList"
                title="Production Lines"
                content={
                  <ul>
                    {productionLines.map(item => (
                      <li key={item.productionLineID}>
                        <div><b><u>Production Line ID:</u></b> {item.productionLineID}</div>
                        <ul><u>Description:</u> {item.description}</ul>
                        <ul><u>Machine Sequence:</u></ul>
                        {item.machineSequence.map(newItem => (
                          <ul key={item.productionLineID + "_" + newItem}>
                            <ul>
                              <li>
                                <div>{newItem}</div>
                              </li>
                            </ul>
                          </ul>
                        ))}
                      </li>
                    ))}
                  </ul>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Card
                statsIcon="fa fa-clock-o"
                id="productList"
                title="Products"
                content={
                  <ul>
                    {products.map(item => (
                      <li key={item.productID}>
                        <div><b><u>Product ID:</u></b> {item.productID}</div>
                        <ul><u>Description:</u> {item.description}</ul>
                      </li>
                    ))}
                  </ul>
                }
              />
            </Col>
            <Col md={6}>
              <Card
                statsIcon="fa fa-clock-o"
                id="manufacturePlanList"
                title="Manufacture Plans"
                content={
                  <ul>
                    {manufacturePlans.map(item => (
                      <li key={item.manufacturePlanID}>
                        <div><b><u>Manufacture Plan ID:</u></b> {item.manufacturePlanID}</div>
                        <ul><u>Product ID:</u> {item.productID}</ul>
                        <ul><u>Description:</u> {item.description}</ul>
                        <ul><u>Operation Sequence:</u></ul>
                        {item.operationSequence.map(newItem => (
                          <ul key={item.manufacturePlanID + "_" + newItem}>
                            <ul>
                              <li>
                                <div>{newItem}</div>
                              </li>
                            </ul>
                          </ul>
                        ))}
                        <ul><u>Time:</u> {item.operationsTime} seconds</ul>
                      </li>
                    ))}
                  </ul>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div >
    );
  }
}

export default Dashboard;
