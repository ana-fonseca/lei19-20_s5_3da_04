import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";
import axios from "axios";
import http from "http";
import ReactTooltip from 'react-tooltip'

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


const countryList = require('countries-cities')
const GE_CLIENT_URL = process.env.REACT_APP_GE_CLIENT_URL
const ORDER_URL = process.env.REACT_APP_GE_ORDER_URL
const GE_PERMISSIONS_URL = process.env.REACT_APP_GE_PERMISSIONS_URL

const instance = axios.create({
  httpAgent: new http.Agent({
    rejectUnauthorized: false
  })
});

class OrdersClients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      clients: [],
      countries: [],
      cities: [],
      permissions: {},
      selectedCountry: null,
      selectedCity: null,
      selectedCountryOrder: null,
      selectedCityOrder: null,
      selectedClient: null,
      selectedOrder: null,
      selectedOrderBody: null,
      selectedOrderCancel: null,
      clientName: null,
      updateName: false,
      updateAddress: false,
      setDeliveryDate: false,
      updateOrderAddress: false,
      deliveryDate: null
    }

    this.handleCheckUpdateName = this.handleCheckUpdateName.bind(this)
    this.handleCheckUpdateAddress = this.handleCheckUpdateAddress.bind(this)
    this.handleChangeClientID = this.handleChangeClientID.bind(this)
    this.handleCountryChange = this.handleCountryChange.bind(this)
    this.handleCityChange = this.handleCityChange.bind(this)
    this.handleUpdateInformation = this.handleUpdateInformation.bind(this)

    this.handleCancelOrder = this.handleCancelOrder.bind(this)
    this.handleChangeOrderCancel = this.handleChangeOrderCancel.bind(this)

    this.handleChangeOrderId = this.handleChangeOrderId.bind(this)
    this.handleCountryChangeOrder = this.handleCountryChangeOrder.bind(this)
    this.handleCityChangeOrder = this.handleCityChangeOrder.bind(this)
    this.handleUpdateOrder = this.handleUpdateOrder.bind(this)
    this.handleSetDeliveryDate = this.handleSetDeliveryDate.bind(this)

    this.resetComponent = this.resetComponent.bind(this)

    this.handleResetPermissions = this.handleResetPermissions.bind(this)
    this.handleUpdatePermissions = this.handleUpdatePermissions.bind(this)
  }


  componentDidMount() {
    this.setState({
      countries: countryList.getCountries(),
      cities: countryList.getCities(countryList.getCountries()[0])
    }, () => {
      this.setState({
        selectedCountry: this.state.countries[0],
        selectedCity: this.state.cities[0],
        selectedCountryOrder: this.state.countries[0],
        selectedCityOrder: this.state.cities[0],
      })
    })

    instance.get(GE_PERMISSIONS_URL)
      .then((result) => {
        this.setState({
          permissions: result.data
        })
      })

    instance.get(ORDER_URL)
      .then((result) => {
        this.setState({
          orders: result.data
        }, () => {
          if (this.state.orders.length > 0) {
            this.setState({
              selectedOrder: this.state.orders[0]._id,
              selectedOrderBody: this.state.orders[0],
              deliveryDate: this.state.orders[0].dueDate,
              selectedOrderCancel: this.state.orders[0]._id
            })
          }
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(GE_CLIENT_URL)
      .then((result) => {
        this.setState({
          clients: result.data
        }, () => {
          this.setState({
            selectedClient: this.state.clients[0]._id,
            clientName: this.state.clients[0].name,
          })
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  resetComponent() {
    if (document.getElementById("checkUpdateName").checked) {
      document.getElementById("nameUpdate").value = null
    }

    if (document.getElementById("checkUpdateAddress").checked) {
      document.getElementById("street").value = null
      document.getElementById("postalCode").value = null
    }

    document.getElementById("streetOrder").value = null
    document.getElementById("postalCodeOrder").value = null

    document.getElementById("checkUpdateName").checked = false
    document.getElementById("checkUpdateAddress").checked = false

    this.setState({
      updateName: false,
      updateAddress: false
    })
  }

  handleCheckUpdateName(e) {
    this.setState({
      updateName: e.target.checked
    })
  }


  handleCheckUpdateAddress(e) {
    this.setState({
      updateAddress: e.target.checked
    })
  }

  handleChangeClientID(e) {
    var clientID = e.target.value.substr(7)

    var selectedClient = null
    for (var i = 0; i < this.state.clients.length; i++) {
      if (this.state.clients[i]._id == clientID) {
        selectedClient = this.state.clients[i].name
      }
    }
    this.setState({
      selectedClient: clientID,
      clientName: selectedClient
    })
  }

  handleCountryChange(e) {
    this.setState({
      selectedCountry: e.target.value,
      cities: countryList.getCities(e.target.value)
    })
  }

  handleCityChange(e) {
    this.setState({
      selectedCity: e.target.value
    })
  }

  handleUpdateInformation() {
    var updateInformation = {}

    if (this.state.updateName) {
      var name = document.getElementById("nameUpdate").value

      if (name.split(" ").length < 2) {
        alert("Please input your first and last name.")
        return
      }

      updateInformation.name = name
    }

    if (this.state.updateAddress) {
      var fullStreet = document.getElementById("street").value.split(", ")
      var postalCode = document.getElementById("postalCode").value

      if (fullStreet.length !== 2) {
        alert("Please input your street address as 'Street, Number'.")
        return
      }

      if (postalCode.split("-").length != 2) {
        alert("Please input your postal code as 'XXXX-YYY'.")
        return
      }

      var street = fullStreet[0]
      var number = fullStreet[1]
      var country = this.state.selectedCountry
      var city = this.state.selectedCity

      updateInformation.address = {
        street: street,
        number: number,
        country: country,
        city: city,
        postalCode: postalCode
      }
    }

    instance.put(GE_CLIENT_URL + this.state.selectedClient, updateInformation)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
          this.componentDidMount()
          this.resetComponent()
        }
      })
  }

  handleChangeOrderCancel(e) {
    var orderID = e.target.value.substr(7)

    this.setState({
      selectedOrderCancel: orderID,
    })
  }

  handleCancelOrder(e) {
    instance.delete(ORDER_URL + "cancel/" + this.state.selectedOrderCancel)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
          this.componentDidMount()
          this.resetComponent()
        }
      })
  }

  handleChangeOrderId(e) {
    var orderID = e.target.value.substr(7)

    var selectedOrder = null
    for (var i = 0; i < this.state.orders.length; i++) {
      if (this.state.orders[i]._id == orderID) {
        selectedOrder = this.state.orders[i]
      }
    }

    this.setState({
      selectedOrder: orderID,
      selectedOrderBody: selectedOrder,
      deliveryDate: new Date(selectedOrder.dueDate)
    })
  }

  handleCountryChangeOrder(e) {
    this.setState({
      selectedCountryOrder: e.target.value,
      cities: countryList.getCities(e.target.value)
    })
  }

  handleCityChangeOrder(e) {
    this.setState({
      selectedCityOrder: e.target.value
    })
  }

  handleSetDeliveryDate() {
    var information = {
      deliveryDate: this.state.deliveryDate
    }

    instance.put(ORDER_URL + "deliveryDate/" + this.state.selectedOrder, information)
    .then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
        this.componentDidMount()
        this.resetComponent()
      }
    })
  }

  handleUpdateOrder() {
    var updateInformation = {}

    var fullStreet = document.getElementById("streetOrder").value.split(", ")
    var postalCode = document.getElementById("postalCodeOrder").value

    if (fullStreet.length !== 2) {
      alert("Please input your street address as 'Street, Number'.")
      return
    }

    if (postalCode.split("-").length != 2) {
      alert("Please input your postal code as 'XXXX-YYY'.")
      return
    }

    var street = fullStreet[0]
    var number = fullStreet[1]
    var country = this.state.selectedCountryOrder
    var city = this.state.selectedCityOrder

    updateInformation.billingAddress = {
      street: street,
      number: number,
      country: country,
      city: city,
      postalCode: postalCode
    }

    instance.put(ORDER_URL + this.state.selectedOrder, updateInformation)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
          this.componentDidMount()
          this.resetComponent()
        }
      })
  }

  handleUpdatePermissions() {
    var updateInformation = {
      login: document.getElementById("permissionLogin").checked,
      register: document.getElementById("permissionRegister").checked,
      clientChangeName: document.getElementById("permissionChangeName").checked,
      clientChangeAddress: document.getElementById("permissionChangeAddress").checked,
      placeOrder: document.getElementById("permissionPlaceOrder").checked,
      viewOrders: document.getElementById("permissionViewOrders").checked,
      orderUpdateAddress: document.getElementById("permissionUpdateOrderAddress").checked,
      cancelOrder: document.getElementById("permissionCancelOrder").checked,
      viewMostBoughtProducts: document.getElementById("permissionMostBoughtProducts").checked,
      viewMostTimeBoughtProducts: document.getElementById("permissionMostTimeBoughtProducts").checked,
      viewFastestToMakeProducts: document.getElementById("permissionFastestToMakeProducts").checked
    }

    instance.put(GE_PERMISSIONS_URL, updateInformation)
      .then((result) => {
        alert("The clients' permissions have been updated.")
        this.componentDidMount()
        this.resetComponent()
      })
  }

  handleResetPermissions() {
    instance.delete(GE_PERMISSIONS_URL)
      .then((result) => {
        alert("The clients' permissions have been resetted to their default values (all true).")
        this.componentDidMount()
        this.resetComponent()
      })
  }

  render() {
    const { clients, orders } = this.state

    return (
      <div className="content">
        <ReactTooltip />
        <Grid fluid>
          <Row>
            <Col md={3}>
              <Card
                statsIcon="fa fa-clock-o"
                id="clientsList"
                title="Clients"
                content={
                  <ul>
                    {clients.map(item => (
                      <li key={item._id}>
                        <div><u><b>Client ID:</b></u> {item._id}</div>
                        <ul><u>Name:</u> {item.name}</ul>
                      </li>
                    ))}
                  </ul>
                }
              />
            </Col>
            <Col md={5}>
              <Card
                statsIcon="fa fa-clock-o"
                id="updateClient"
                title="Update Client"
                content={
                  <div>
                    <label>Clients:            </label>&nbsp;
                                <select id="updateClientSelector" onChange={this.handleChangeClientID}>
                      {clients.map(item => (
                        <option key={"update_" + item._id} value={"update_" + item._id}>
                          {item._id}
                        </option>
                      ))}
                    </select>
                    <form>
                      <div>
                        <input type="checkBox" id="checkUpdateName" label="Update Name" onClick={this.handleCheckUpdateName} />&nbsp;Update Name&nbsp;&nbsp;&nbsp;
                        <input type="checkBox" id="checkUpdateAddress" label="Update Address" onClick={this.handleCheckUpdateAddress} />&nbsp;Update Address
                      </div>
                      {
                        this.state.updateName &&
                        <div>
                          <FormInputs
                            ncols={["col-md-12"]}
                            properties={[
                              {
                                label: "Name",
                                type: "text",
                                bsClass: "form-control",
                                placeholder: this.state.clientName,
                                defaultValue: "",
                                id: "nameUpdate"
                              }
                            ]}
                          />
                        </div>
                      }
                      {
                        this.state.updateAddress &&
                        <div>
                          <FormInputs
                            ncols={["col-md-6", "col-md-6"]}
                            properties={[
                              {
                                label: "Street",
                                type: "text",
                                bsClass: "form-control",
                                placeholder: "Street, Number",
                                defaultValue: "",
                                id: "street"
                              },
                              {
                                label: "Postal Code",
                                type: "text",
                                bsClass: "form-control",
                                placeholder: "XXXX-YYY",
                                defaultValue: "",
                                id: "postalCode"
                              }
                            ]}
                          />
                          <FormInputs
                            ncols={["col-md-6", "col-md-6"]}
                            properties={[
                              {
                                label: "Country",
                                type: "select",
                                bsClass: "form-control",
                                placeholder: "Country",
                                defaultValue: "country",
                                id: "country",
                                options: this.state.countries,
                                onChange: this.handleCountryChange
                              },
                              {
                                label: "City",
                                type: "select",
                                bsClass: "form-control",
                                placeholder: "City",
                                defaultValue: "city",
                                id: "city",
                                options: this.state.cities,
                                onChange: this.handleCityChange
                              }
                            ]}
                          />
                        </div>
                      }
                      {
                        (this.state.updateAddress || this.state.updateName) &&
                        <div>
                          <Button bsStyle="info" pullRight fill onClick={this.handleUpdateInformation} id="updateInformationBtn">
                            Update Information
                          </Button>
                          <div className="clearfix" />
                        </div>
                      }
                    </form>
                  </div>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                id="clientPermissions"
                title="Clients' Permissions"
                content={
                  <div>
                    <form>
                      <ul>
                        <div data-tip="The ability for a client to log-in to the system.">
                          {this.state.permissions.login ?
                            <input type="checkbox" value="" id="permissionLogin" checked="false" /> : <input type="checkbox" value="" id="permissionLogin" />
                          }
                          &nbsp;&nbsp;Login
                        </div>
                        <div data-tip="The ability for a new client to register into the system.">
                          {this.state.permissions.register ?
                            <input type="checkbox" value="" id="permissionRegister" checked="false" /> : <input type="checkbox" value="" id="permissionRegister" />
                          }
                          &nbsp;&nbsp;Register
                        </div>
                        <div data-tip="The ability for a client to update their name.">
                          {this.state.permissions.clientChangeName ?
                            <input type="checkbox" value="" id="permissionChangeName" checked="false" /> : <input type="checkbox" value="" id="permissionChangeName" />
                          }
                          &nbsp;&nbsp;Update their name
                        </div>
                        <div data-tip="The ability for a client to update their address.">
                          {this.state.permissions.clientChangeAddress ?
                            <input type="checkbox" value="" id="permissionChangeAddress" checked="false" /> : <input type="checkbox" value="" id="permissionChangeAddress" />
                          }
                          &nbsp;&nbsp;Update their address
                        </div>
                        <div data-tip="The ability for a client to place an order.">
                          {this.state.permissions.placeOrder ?
                            <input type="checkbox" value="" id="permissionPlaceOrder" checked="false" /> : <input type="checkbox" value="" id="permissionPlaceOrder" />
                          }
                          &nbsp;&nbsp;Place an order
                        </div>
                        <div data-tip="The ability for a client to view their orders.">
                          {this.state.permissions.viewOrders ?
                            <input type="checkbox" value="" id="permissionViewOrders" checked="false" /> : <input type="checkbox" value="" id="permissionViewOrders" />
                          }
                          &nbsp;&nbsp;View their orders
                        </div>
                        <div data-tip="The ability for a client to update one of their orders' address.">
                          {this.state.permissions.orderUpdateAddress ?
                            <input type="checkbox" value="" id="permissionUpdateOrderAddress" checked="false" /> : <input type="checkbox" value="" id="permissionUpdateOrderAddress" />
                          }
                          &nbsp;&nbsp;Update an order's Address
                        </div>
                        <div data-tip="The ability for a client to cancel of their orders.">
                          {this.state.permissions.cancelOrder ?
                            <input type="checkbox" value="" id="permissionCancelOrder" checked="false" /> : <input type="checkbox" value="" id="permissionCancelOrder" />
                          }
                          &nbsp;&nbsp;Cancel an order
                        </div>
                        <div data-tip="The ability for a client to view the most bought products (the number of times they were ordered).">
                          {this.state.permissions.viewMostBoughtProducts ?
                            <input type="checkbox" value="" id="permissionMostBoughtProducts" checked="false" /> : <input type="checkbox" value="" id="permissionMostBoughtProducts" />
                          }
                          &nbsp;&nbsp;View the most bought products
                        </div>
                        <div data-tip="The ability for a client to view the most frequently ordered products (the quantity in each order times the number of times they were ordered).">
                          {this.state.permissions.viewMostTimeBoughtProducts ?
                            <input type="checkbox" value="" id="permissionMostTimeBoughtProducts" checked="false" /> : <input type="checkbox" value="" id="permissionMostTimeBoughtProducts" />
                          }
                          &nbsp;&nbsp;View the most frequently bought products
                        </div>
                        <div data-tip="The ability for a client to view the fastest products to make (the amount of time needed for a product to be made).">
                          {this.state.permissions.viewFastestToMakeProducts ?
                            <input type="checkbox" value="" id="permissionFastestToMakeProducts" checked="false" /> : <input type="checkbox" value="" id="permissionFastestToMakeProducts" />
                          }
                          &nbsp;&nbsp;View the fastest products to make
                        </div>
                      </ul>
                      <br /><br />
                      <div>
                        <Button bsStyle="info" pullRight fill onClick={this.handleUpdatePermissions} id="updatePermissionsBtn">
                          Save Permissions
                          </Button>
                        <Button bsStyle="info" fill onClick={this.handleResetPermissions} id="resetPermissionsBtn">
                          Reset Permissions
                          </Button>
                      </div>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                id="ordersList"
                title="Orders"
                content={
                  <div>
                    <ul>
                      {orders.map(item => (
                        <li key={item._id} id={"list_" + item._id}>
                          <div><u><b>Order ID:</b></u> {item._id}</div>
                          <ul><u>Client ID:</u> {item.clientID}</ul>
                          <ul><u>Products:</u>
                            <ul>
                              {item.products !== undefined &&
                                item.products.map(newItem => (
                                  <li key={item._id + "_" + newItem.productID} id={item._id + "_" + newItem.productID}>
                                    <div>{newItem.quantity} {newItem.productDescription}</div>
                                  </li>
                                ))}
                            </ul>
                          </ul>
                          <ul><u>Billing Address:</u><br />
                            <ul>
                              {item.billingAddress !== undefined &&
                                <div>
                                  {item.billingAddress.street + " " + item.billingAddress.number}<br />
                                  {item.billingAddress.postalCode}<br />
                                  {item.billingAddress.city + ", " + item.billingAddress.country}
                                </div>
                              }
                            </ul>
                          </ul>
                          <ul>
                            <u>Due Date:</u> {item.dueDate.split("T")[0]}
                          </ul>
                          <ul>
                            <u>Delivery Date:</u> {item.deliveryDate == undefined ? "N/A" : item.deliveryDate}
                          </ul>
                          <ul>
                            <u>{item.isActive ? "The order is still active" : "The order is no longer active"}</u>
                          </ul>
                        </li>
                      ))}
                    </ul>
                    <br />
                    <label>Cancel an Order</label>
                    <form>
                      <select id="cancelOrder" onChange={this.handleChangeOrderCancel}>
                        {orders.map(item => (
                          <option key={"cancel_" + item._id} value={"cancel_" + item._id}>
                            {item._id}
                          </option>
                        ))}
                      </select>
                      <br /><br />
                      <Button bsStyle="info" pullRight fill onClick={this.handleCancelOrder} id="cancelOrderBtn">
                        Cancel Order
                      </Button>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
            <Col md={8}>
              <Card
                statsIcon="fa fa-clock-o"
                id="updateOrder"
                title="Update Order"
                content={
                  <div>
                    <label>Orders: </label>
                    <select id="updateOrderSelector" onChange={this.handleChangeOrderId}>
                      {orders.map(item => (
                        <option key={"update_" + item._id} value={"update_" + item._id}>
                          {item._id}
                        </option>
                      ))}
                    </select>
                    {!this.state.setDeliveryDate &&
                      <div>
                        <input type="checkBox" id="checkUpdateOrderAddress" label="Update Address" onClick={(e) => { this.setState({ updateOrderAddress: e.target.checked }) }} /> &nbsp;Update Address&nbsp;&nbsp;&nbsp;
                        </div>
                    }
                    {!this.state.updateOrderAddress &&
                      <div>
                        <input type="checkBox" id="checkSetDeliveryDate" label="Set Delivery Date" onClick={(e) => { this.setState({ setDeliveryDate: e.target.checked }) }} /> &nbsp;Set Delivery Date
                        </div>
                    }
                    {this.state.updateOrderAddress &&
                      <form>
                        <FormInputs
                          ncols={["col-md-6", "col-md-6"]}
                          properties={[
                            {
                              label: "Street",
                              type: "text",
                              bsClass: "form-control",
                              placeholder: "Street, Number",
                              defaultValue: "",
                              id: "streetOrder"
                            },
                            {
                              label: "Postal Code",
                              type: "text",
                              bsClass: "form-control",
                              placeholder: "XXXX-YYY",
                              defaultValue: "",
                              id: "postalCodeOrder"
                            }
                          ]}
                        />
                        <FormInputs
                          ncols={["col-md-6", "col-md-6"]}
                          properties={[
                            {
                              label: "Country",
                              type: "select",
                              bsClass: "form-control",
                              placeholder: "Country",
                              defaultValue: "country",
                              id: "countryOrder",
                              options: this.state.countries,
                              onChange: this.handleCountryChangeOrder
                            },
                            {
                              label: "City",
                              type: "select",
                              bsClass: "form-control",
                              placeholder: "City",
                              defaultValue: "city",
                              id: "cityOrder",
                              options: this.state.cities,
                              onChange: this.handleCityChangeOrder
                            }
                          ]}
                        />
                        <div>
                          <Button bsStyle="info" pullRight fill onClick={this.handleUpdateOrder} id="updateOrderBtn">
                            Update Order
                          </Button>
                          <div className="clearfix" />
                        </div>
                      </form>
                    }
                    {this.state.setDeliveryDate && this.state.orders.length > 0 &&
                      <div>
                        <label><b>Delivery Date</b></label><br />
                        <DatePicker
                          selected={new Date(this.state.deliveryDate)}
                          onChange={(date) => { this.setState({ deliveryDate: date }) }}
                          allowSameDay="false"
                          dateFormat="yyyy/MM/dd"
                          filterDate={date => {
                            return date >= new Date(this.state.selectedOrderBody.dueDate)
                          }}
                          id="deliveryDatePicker"
                        />
                        <div>
                          <Button bsStyle="info" pullRight fill onClick={this.handleSetDeliveryDate} id="setDeliveryDateBtn">
                            Set Delivery Date
                          </Button>
                          <div className="clearfix" />
                        </div>
                      </div>
                    }
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }

}

export default OrdersClients;