import React, { Component } from "react";
import axios from "axios";
import http from "http";
import { Grid, Row, Col } from "react-bootstrap";
import UserCard from "../components/UserCard/UserCard"

import { Card } from "../components/Card/Card.jsx";
import auth from "../auther"
import FormInputs from "../components/FormInputs/FormInputs";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import Button from "components/CustomButton/CustomButton.jsx";

const GE_CLIENT_URL = process.env.REACT_APP_GE_CLIENT_URL
const GE_ORDER_URL = process.env.REACT_APP_GE_ORDER_URL
const GE_PERMISSIONS_URL = process.env.REACT_APP_GE_PERMISSIONS_URL
const MDP_URL = process.env.REACT_APP_MDP_URL
const countryList = require('countries-cities')

const instance = axios.create({
    httpAgent: new http.Agent({
        rejectUnauthorized: false
    })
});

class Orders extends Component {
    constructor(props) {
        super(props)

        this.state = ({
            clientOrders: [],
            products: [],
            permissions: {},
            selectedOrderCancel: null,
            selectedOrder: null,
            selectedOrderBody: null,
            useUserAddress: false,
            countries: countryList.getCountries(),
            cities: countryList.getCities(countryList.getCountries()[0]),
            selectedCountry: countryList.getCountries()[0],
            selectedCity: countryList.getCities(countryList.getCountries()[0])[0],
            dueDate: new Date(+new Date() + 4 * 24 * 60 * 60 * 1000)
        })

        this.handleUseUserAddress = this.handleUseUserAddress.bind(this)
        this.handleCountryChange = this.handleCountryChange.bind(this)
        this.handleCityChange = this.handleCityChange.bind(this)
        this.handleSubmitPost = this.handleSubmitPost.bind(this)
        this.handleChangeDueDate = this.handleChangeDueDate.bind(this)
        this.handleCancelOrder = this.handleCancelOrder.bind(this)
        this.resetComponents = this.resetComponents.bind(this)

        this.handleChangeOrderId = this.handleChangeOrderId.bind(this)
        this.handleCountryChangeOrder = this.handleCountryChangeOrder.bind(this)
        this.handleCityChangeOrder = this.handleCityChangeOrder.bind(this)
        this.handleUpdateOrder = this.handleUpdateOrder.bind(this)
    }

    componentDidMount() {
        instance.get(GE_ORDER_URL + "client/" + auth.getUser()._id)
            .then((result) => {
                this.setState({
                    clientOrders: result.data
                }, () => {
                    if (this.state.clientOrders.length > 0) {
                        this.setState({
                            selectedOrderCancel: this.state.clientOrders[0]._id
                        })
                    }
                })
            })

        instance.get(MDP_URL + "product/")
            .then((result) => {
                this.setState({
                    products: result.data
                })
            })

        instance.get(GE_PERMISSIONS_URL)
            .then((result) => {
                this.setState({
                    permissions: result.data
                })
            })
    }

    handleCancelOrder(e) {
        instance.delete(GE_ORDER_URL + "cancel/" + this.state.selectedOrderCancel)
            .then((result) => {
                this.componentDidMount()
            }).catch((error) => {
                if (error.response) {
                    alert(error.response.data);
                    this.componentDidMount()
                }
            })
    }

    handleChangeDueDate(date) {
        this.setState({
            dueDate: date
        })
    }

    handleCountryChange(e) {
        this.setState({
            selectedCountry: e.target.value,
            cities: countryList.getCities(e.target.value)
        })
    }

    handleCityChange(e) {
        this.setState({
            selectedCity: e.target.value
        })
    }

    handleUseUserAddress(e) {
        this.setState({
            useUserAddress: e.target.checked
        })
    }

    resetComponents() {
        if (!this.state.useUserAddress) {
            document.getElementById("street").value = null
            document.getElementById("postalCode").value = null
        }

        this.setState({
            useUserAddress: false,
            selectedCountry: countryList.getCountries()[0],
            cities: countryList.getCities(this.state.countries[0]),
            selectedCity: countryList.getCities(this.state.countries[0])[0],
            dueDate: new Date(+new Date() + 4 * 24 * 60 * 60 * 1000)
        })

        document.getElementById("useUserAddress").checked = false
        this.state.products.forEach(product => {
            document.getElementById("create_" + product.productID).value = null
        })
    }

    handleSubmitPost() {
        if (!this.state.permissions.placeOrder) {
            alert("This feature has been disabled by an admin")
            return
        }

        var postBody = {
            clientID: auth.getUser()._id,
            useUserAddress: this.state.useUserAddress,
            products: [],
            dueDate: document.getElementById("dueDatePicker").value
        }

        if (!postBody.useUserAddress) {
            postBody.billingAddress = {}
            var fullStreet = document.getElementById("street").value.split(", ")
            var postalCode = document.getElementById("postalCode").value

            if (fullStreet.length !== 2) {
                alert("Please input your street address as 'Street, Number'.")
                return
            }

            if (postalCode.split("-").length != 2) {
                alert("Please input your postal code as 'XXXX-YYY'.")
                return
            }

            postBody.billingAddress.postalCode = postalCode
            postBody.billingAddress.street = fullStreet[0]
            postBody.billingAddress.number = fullStreet[1]
            postBody.billingAddress.country = this.state.selectedCountry
            postBody.billingAddress.city = this.state.selectedCity
        }

        this.state.products.forEach(product => {
            var prodValue = document.getElementById("create_" + product.productID).value
            if (prodValue !== null && prodValue > 0) {
                postBody.products.push({
                    productID: product.productID,
                    quantity: prodValue
                })
            }
        })

        if (postBody.products.length === 0) {
            alert("You must select at least one unit of at least one product!")
            return
        }

        instance.post(GE_ORDER_URL, postBody)
            .then((result) => {
                alert("Your order has been placed!")
                this.componentDidMount()
                this.resetComponents()
                return
            })
            .catch((err) => {
                console.log(err)
                alert(err)
                return
            })
    }

    handleChangeOrderId(e) {
        var orderID = e.target.value.substr(7)

        var selectedOrder = null
        for (var i = 0; i < this.state.clientOrders.length; i++) {
            if (this.state.clientOrders[i]._id == orderID) {
                selectedOrder = this.state.clientOrders[i]
            }
        }

        this.setState({
            selectedOrder: orderID,
            selectedOrderBody: selectedOrder,
            deliveryDate: new Date(selectedOrder.dueDate)
        })
    }

    handleCountryChangeOrder(e) {
        this.setState({
            selectedCountryOrder: e.target.value,
            cities: countryList.getCities(e.target.value)
        })
    }

    handleCityChangeOrder(e) {
        this.setState({
            selectedCityOrder: e.target.value
        })
    }

    handleUpdateOrder() {
        var updateInformation = {}

        var fullStreet = document.getElementById("streetOrder").value.split(", ")
        var postalCode = document.getElementById("postalCodeOrder").value

        if (fullStreet.length !== 2) {
            alert("Please input your street address as 'Street, Number'.")
            return
        }

        if (postalCode.split("-").length != 2) {
            alert("Please input your postal code as 'XXXX-YYY'.")
            return
        }

        var street = fullStreet[0]
        var number = fullStreet[1]
        var country = this.state.selectedCountryOrder
        var city = this.state.selectedCityOrder

        updateInformation.billingAddress = {
            street: street,
            number: number,
            country: country,
            city: city,
            postalCode: postalCode
        }

        instance.put(GE_ORDER_URL + this.state.selectedOrder, updateInformation)
            .then((result) => {
                this.componentDidMount()
                this.resetComponent()
            }).catch((error) => {
                if (error.response) {
                    alert(error.response.data);
                    this.componentDidMount()
                    this.resetComponent()
                }
            })
    }

    render() {
        const { clientOrders, products } = this.state

        const is3DaysFromNow = date => {
            return date > new Date(+new Date() + 3 * 24 * 60 * 60 * 1000)
        }

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={7}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="placeOrder"
                                title={this.state.permissions.placeOrder ? "Place Order" : "Place Order (this feature has been disabled by an admin)"}
                                content={
                                    <form>
                                        <label><b>Products</b></label>
                                        <ul>
                                            {products.map(item => (
                                                <li key={item.productID}>
                                                    {item.description}&nbsp;&nbsp;<input type="number" min="0" id={"create_" + item.productID} placeholder="Quantity" />
                                                </li>
                                            ))}
                                        </ul>
                                        <br />
                                        <br />
                                        <div>
                                            <input type="checkbox" label="Use your address as the billing address" id="useUserAddress" onClick={this.handleUseUserAddress} />
                                            &nbsp;&nbsp;Use your address as the billing address
                                        </div>

                                        {!this.state.useUserAddress &&
                                            <div>
                                                <FormInputs
                                                    ncols={["col-md-6", "col-md-6"]}
                                                    properties={[
                                                        {
                                                            label: "Street",
                                                            type: "text",
                                                            bsClass: "form-control",
                                                            placeholder: "Street, Number",
                                                            defaultValue: "",
                                                            id: "street",
                                                            disabled: !this.state.permissions.placeOrder
                                                        },
                                                        {
                                                            label: "Postal Code",
                                                            type: "text",
                                                            bsClass: "form-control",
                                                            placeholder: "Postal Code (XXXX-YYY)",
                                                            defaultValue: "",
                                                            id: "postalCode",
                                                            disabled: !this.state.permissions.placeOrder
                                                        }
                                                    ]}
                                                />
                                                <FormInputs
                                                    ncols={["col-md-6", "col-md-6"]}
                                                    properties={[
                                                        {
                                                            label: "Country",
                                                            type: "select",
                                                            bsClass: "form-control",
                                                            placeholder: "Country",
                                                            defaultValue: "",
                                                            id: "country",
                                                            options: this.state.countries,
                                                            onChange: this.handleCountryChange,
                                                            disabled: !this.state.permissions.placeOrder
                                                        },
                                                        {
                                                            label: "City",
                                                            type: "select",
                                                            bsClass: "form-control",
                                                            placeholder: "City",
                                                            defaultValue: "",
                                                            id: "city",
                                                            options: this.state.cities,
                                                            onChange: this.handleCityChange,
                                                            disabled: !this.state.permissions.placeOrder
                                                        }
                                                    ]}
                                                />
                                            </div>
                                        }
                                        <br />
                                        <div>
                                            <label><b>Due Date</b></label><br />
                                            <DatePicker
                                                selected={this.state.dueDate}
                                                onChange={this.handleChangeDueDate}
                                                allowSameDay="false"
                                                dateFormat="yyyy/MM/dd"
                                                filterDate={is3DaysFromNow}
                                                id="dueDatePicker"
                                            />
                                        </div>
                                        <Button bsStyle="info" pullRight fill onClick={this.handleSubmitPost} id="placeOrderBtn">
                                            Place Order
                                                    </Button>
                                        <div className="clearfix" />
                                    </form>
                                }
                            />
                        </Col>
                        <Col md={5}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="myOrders"
                                title={this.state.permissions.viewOrders ? "My Orders" : "My Orders (this feature has been disabled by an admin)"}
                                content={
                                    <div>
                                        {this.state.permissions.viewOrders &&
                                            <div>
                                                <ul>
                                                    {clientOrders.map(item => (
                                                        <li key={item._id} id={"list_" + item._id}>
                                                            <div><u><b>Order ID:</b></u> {item._id}</div>
                                                            <ul><u>Products:</u>
                                                                <ul>
                                                                    {item.products !== undefined &&
                                                                        item.products.map(newItem => (
                                                                            <li key={item._id + "_" + newItem.productID} id={item._id + "_" + newItem.productID}>
                                                                                <div>{newItem.quantity} {newItem.productDescription}</div>
                                                                            </li>
                                                                        ))}
                                                                </ul>
                                                            </ul>
                                                            <ul><u>Billing Address:</u><br />
                                                                <ul>
                                                                    {item.billingAddress !== undefined &&
                                                                        <div>
                                                                            {item.billingAddress.street + " " + item.billingAddress.number}<br />
                                                                            {item.billingAddress.postalCode}<br />
                                                                            {item.billingAddress.city + ", " + item.billingAddress.country}
                                                                        </div>
                                                                    }
                                                                </ul>
                                                            </ul>
                                                            <ul>
                                                                <u>Due Date:</u> {item.dueDate.split("T")[0]}
                                                            </ul>
                                                            <ul>
                                                                <u>Delivery Date:</u> {item.deliveryDate == undefined ? "N/A" : item.deliveryDate}
                                                            </ul>
                                                            <ul>
                                                                {item.isActive ? "The order is still active" : "The order is no longer active"}
                                                            </ul>
                                                        </li>
                                                    ))}
                                                </ul>
                                                {this.state.permissions.cancelOrder &&
                                                    <div>
                                                        <label>Cancel an Order</label>
                                                        <form>
                                                            <select id="cancelOrder" onChange={(e) => { this.setState({ selectedOrderCancel: e.target.value }) }}>
                                                                {clientOrders.map(item => (
                                                                    <option key={"cancel_" + item._id} value={"cancel_" + item._id}>
                                                                        {item._id}
                                                                    </option>
                                                                ))}
                                                            </select>
                                                            <br /><br />

                                                            {this.state.clientOrders.length > 0 &&
                                                                <Button bsStyle="info" pullRight fill onClick={this.handleCancelOrder} id="cancelOrderBtn">
                                                                    Cancel Order
                                                                </Button>
                                                            }
                                                            <div className="clearfix" />
                                                        </form>
                                                    </div>
                                                }
                                            </div>
                                        }

                                    </div>
                                }
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="updateOrder"
                                title="Update Order"
                                content={
                                    <div>
                                        <label>Orders: </label>
                                        <select id="updateOrderSelector" onChange={this.handleChangeOrderId}>
                                            {clientOrders.map(item => (
                                                <option key={"update_" + item._id} value={"update_" + item._id}>
                                                    {item._id}
                                                </option>
                                            ))}
                                        </select>
                                        {this.state.permissions.orderUpdateAddress && clientOrders.length > 0 &&
                                            < form >
                                                <FormInputs
                                                    ncols={["col-md-6", "col-md-6"]}
                                                    properties={[
                                                        {
                                                            label: "Street",
                                                            type: "text",
                                                            bsClass: "form-control",
                                                            placeholder: "Street, Number",
                                                            defaultValue: "",
                                                            id: "streetOrder"
                                                        },
                                                        {
                                                            label: "Postal Code",
                                                            type: "text",
                                                            bsClass: "form-control",
                                                            placeholder: "XXXX-YYY",
                                                            defaultValue: "",
                                                            id: "postalCodeOrder"
                                                        }
                                                    ]}
                                                />
                                                <FormInputs
                                                    ncols={["col-md-6", "col-md-6"]}
                                                    properties={[
                                                        {
                                                            label: "Country",
                                                            type: "select",
                                                            bsClass: "form-control",
                                                            placeholder: "Country",
                                                            defaultValue: "country",
                                                            id: "countryOrder",
                                                            options: this.state.countries,
                                                            onChange: this.handleCountryChangeOrder
                                                        },
                                                        {
                                                            label: "City",
                                                            type: "select",
                                                            bsClass: "form-control",
                                                            placeholder: "City",
                                                            defaultValue: "city",
                                                            id: "cityOrder",
                                                            options: this.state.cities,
                                                            onChange: this.handleCityChangeOrder
                                                        }
                                                    ]}
                                                />
                                                <div>
                                                    <Button bsStyle="info" pullRight fill onClick={this.handleUpdateOrder} id="updateOrderBtn">
                                                        Update Order
                                                    </Button>
                                                    <div className="clearfix" />
                                                </div>
                                            </form>
                                        }
                                    </div>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

export default Orders;