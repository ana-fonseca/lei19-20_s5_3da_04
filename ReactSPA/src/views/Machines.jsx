/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";
import axios from "axios";
import http from "http";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

const instance = axios.create({
  httpAgent: new http.Agent({
    rejectUnauthorized: false
  })
});

const MDF_URL = process.env.REACT_APP_MDF_URL

class Machines extends Component {
  constructor(props) {
    super(props);
    this.state = {
      machines: [],
      machineTypes: [],
      machinesFiltered: [],
      chosenMachine: undefined
    }

    this.handleFilterClick = this.handleFilterClick.bind(this)
    this.handleSubmitPost = this.handleSubmitPost.bind(this)
    this.handleDeleteMachine = this.handleDeleteMachine.bind(this)
    this.handleDeActivateStatusMachine = this.handleDeActivateStatusMachine.bind(this)
    this.handleUpdateMachine = this.handleUpdateMachine.bind(this)
  }

  componentDidMount() {
    instance.get(MDF_URL + "Machine")
      .then((result) => {
        this.setState({
          machines: result.data
        }, () => {
          this.setState({
            chosenMachine: this.state.machines[0]
          })
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDF_URL + "MachineType")
      .then((result) => {
        this.setState({
          machineTypes: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  resetComponent() {
    document.getElementById("machineID").value = null
    document.getElementById("machineDescription").value = null
    this.setState({
      machinesFiltered: []
    })
  }

  handleSubmitPost(e) {
    var machineID = document.getElementById("machineID").value;
    var machineDescription = document.getElementById("machineDescription").value;
    var select = document.getElementById("createSelector");
    var machineType = select.options[select.selectedIndex].text;

    instance.post(MDF_URL + "Machine", {
      machineID: machineID,
      description: machineDescription,
      machineTypeID: machineType
    }).then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }

  handleFilterClick(e) {
    var select = document.getElementById("filterSelect");
    var machineFilter = select.options[select.selectedIndex].text;

    var machineTypeFilter = MDF_URL + "Machine/FromMachineType/" + machineFilter;
    instance.get(machineTypeFilter)
      .then((result) => {
        this.setState({
          machinesFiltered: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  handleDeleteMachine(e) {
    var select = document.getElementById("deleteSelector");
    var machine = select.options[select.selectedIndex].text;

    instance.delete(MDF_URL + "Machine/" + machine)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
        }
      })
  }

  handleDeActivateStatusMachine(e) {
    var select = document.getElementById("deleteSelector");
    var machineID = select.options[select.selectedIndex].text;

    var machine = null

    for (var i = 0;i < this.state.machines.length;i++) {
      if (this.state.machines[i].machineID.toString() === machineID) {
        machine = this.state.machines[i]
        break;
      }
    }

    instance.post(MDF_URL + "Machine/" + machineID + (machine.status ? "/deactivate" : "/activate")
    ).then((result) => {
        alert(machine.status ? "The machine " + machineID + " has been deactivated." : "The machine " + machineID + " has been activated.")
        this.componentDidMount()
        this.resetComponent()
    }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
        }
    })
  }

  handleUpdateMachine(e) {
    var selectMachine = document.getElementById("updateMachineSelector");
    var machine = selectMachine.options[selectMachine.selectedIndex].text;

    var selectMachineType = document.getElementById("updateMachineTypeSelector");
    var machineType = selectMachineType.options[selectMachineType.selectedIndex].text;

    instance.put(MDF_URL + "Machine/" + machine, {
      machineTypeID: machineType
    }).then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }

  render() {
    const { machines, machineTypes, machinesFiltered } = this.state
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={8}>
              <Card
                title="Create Machine"
                content={
                  <form>
                    <FormInputs
                      ncols={["col-md-6", "col-md-6"]}
                      properties={[
                        {
                          label: "Machine ID",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Machine ID",
                          defaultValue: "",
                          id: "machineID"
                        },
                        {
                          label: "Machine Description",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Machine Description",
                          defaultValue: "",
                          id: "machineDescription"
                        }
                      ]}
                    />
                    <label>Machine Type</label><br />
                    <select id="createSelector">
                      {machineTypes.map(item => (
                        <option key={"createMachine_" + item.machineTypeID} value={"createMachine_" + item.machineTypeID}>
                          {item.machineTypeID}
                        </option>
                      ))}
                    </select>
                    <div key="filter"></div>
                    <Button bsStyle="info" pullRight fill onClick={this.handleSubmitPost} id="createMachineBtn">
                      Create Machine
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                id="machineList"
                title="Machines"
                content={
                  <div>
                    <ul>
                      {machines.map(item => (
                        <li key={item.machineID} id={"list_" + item.machineID}>
                          <div><u><b>Machine ID:</b></u> {item.machineID}</div>
                          <ul><u>Description:</u> {item.description}</ul>
                          <ul><u>Machine Type:</u> {item.machineTypeID}</ul>
                          <ul><u>Status:</u> {item.status ? "Active" : "Inactive"}</ul>
                        </li>
                      ))}
                    </ul>
                    <br />
                    <label>Delete Machine</label>
                    <form>
                      <select id="deleteSelector">
                        {machines.map(item => (
                          <option key={"delete_" + item.machineID} value={"delete_" + item.machineID}>
                            {item.machineID}
                          </option>
                        ))}
                      </select><br /><br />
                      <Button bsStyle="info" fill onClick={this.handleDeActivateStatusMachine} id="deActivateMachine">
                        De/Activate Machine
                      </Button>
                      <Button bsStyle="info" pullRight fill onClick={this.handleDeleteMachine} id="deleteMachineBtn">
                        Delete Machine
                      </Button>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={5}>
              <Card
                title="Update Machine"
                content={
                  <form>
                    <label>Machine to be updated</label><br />
                    <select id="updateMachineSelector">
                      {machines.map(item => (
                        <option key={"update_" + item.machineID} value={"update_" + item.machineID}>
                          {item.machineID}
                        </option>
                      ))}
                    </select>
                    <br /><br />
                    <label>Machine Type</label><br />
                    <select id="updateMachineTypeSelector">
                      {machineTypes.map(item => (
                        <option key={"update_" + item.machineTypeID} value={"update_" + item.machineTypeID}>
                          {item.machineTypeID}
                        </option>
                      ))}
                    </select>
                    <br />
                    <Button bsStyle="info" pullRight fill onClick={this.handleUpdateMachine} id="updateMachineBtn">
                      Update Machine
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
            <Col md={7}>
              <Card
                title="Machines of a specific Machine Type"
                content={
                  <form>
                    <label>Machine Type filter</label><br />
                    <select id="filterSelect">
                      {machineTypes.map(item => (
                        <option key={"filter_" + item.machineTypeID} value={"filter_" + item.machineTypeID}>
                          {item.machineTypeID}
                        </option>
                      ))}
                    </select><br /><br />
                    <ul>
                      {machinesFiltered.map(item => (
                        <li key={"filter_" + item.machineID} id={"filter_" + item.machineID}>
                          <div><u><b>Machine ID:</b></u> {item.machineID}</div>
                          <ul><u>Description:</u> {item.description}</ul>
                          <ul><u>MachineType:</u> {item.machineTypeID}</ul>
                        </li>
                      ))}
                    </ul>
                    <Button bsStyle="info" pullRight fill onClick={this.handleFilterClick} id="filterMachineBtn">
                      Display Machines
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Machines;
