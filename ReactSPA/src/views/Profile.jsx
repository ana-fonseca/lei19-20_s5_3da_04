import React, { Component } from "react";
import axios from "axios";
import http from "http";
import { Grid, Row, Col } from "react-bootstrap";
import UserCard from "../components/UserCard/UserCard"

import { Card } from "../components/Card/Card.jsx";
import auth from "../auther"
import FormInputs from "../components/FormInputs/FormInputs";
import Button from "components/CustomButton/CustomButton.jsx";

const GE_CLIENT_URL = process.env.REACT_APP_GE_CLIENT_URL
const GE_PERMISSIONS_URL = process.env.REACT_APP_GE_PERMISSIONS_URL
const countryList = require('countries-cities')

const instance = axios.create({
    httpAgent: new http.Agent({
        rejectUnauthorized: false
    })
});

const AVATAR = "https://icon-library.net/images/default-user-icon/default-user-icon-4.jpg"

export default class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            permissions: {},
            name: null,
            email: null,
            nif: null,
            address: null,
            addressPart1: null,
            addressPart2: null,
            updateName: true,
            updateAddress: true,
            countries: [],
            cities: [],
            selectedCountry: null,
            selectedCity: null,
        }

        this.handleCheckUpdateName = this.handleCheckUpdateName.bind(this)
        this.handleCheckUpdateAddress = this.handleCheckUpdateAddress.bind(this)
        this.handleCountryChange = this.handleCountryChange.bind(this)
        this.handleCityChange = this.handleCityChange.bind(this)
        this.handleUpdateInformation = this.handleUpdateInformation.bind(this)
        this.handleDeleteAccount = this.handleDeleteAccount.bind(this)
    }

    componentDidMount() {
        document.getElementById("checkUpdateName").checked = true
        document.getElementById("checkUpdateAddress").checked = true

        this.setState({
            name: auth.getUser().name,
            email: auth.getUser().email,
            nif: auth.getUser().nif,
            address: auth.getUser().address,
            countries: countryList.getCountries(),
            cities: countryList.getCities(countryList.getCountries()[0])
        }, () => {
            this.setState({
                selectedCountry: this.state.countries[0],
                selectedCity: this.state.cities[0],
                addressPart1: this.state.address.street + " " + this.state.address.number,
                addressPart2: this.state.address.postalCode + " " + this.state.address.city + ", " + this.state.address.country,
            })
        })

        instance.get(GE_PERMISSIONS_URL)
        .then((result) => {
            this.setState({
                permissions: result.data
            })
        })
    }

    handleUpdateInformation() {
        if (this.state.updateName && !this.state.permissions.clientChangeName) {
            alert("Updating your name has been disabled by an admin")
            return
        }

        if (this.state.updateAddress && !this.state.permissions.clientChangeAddress) {
            alert("Updating your address has been disabled by an admin")
            return
        }

        var updateInformation = {}

        if (this.state.updateName) {
            var name = document.getElementById("nameUpdate").value

            if (name.split(" ").length < 2) {
                alert("Please input your first and last name.")
                return
            }

            updateInformation.name = name
        }

        if (this.state.updateAddress) {
            var fullStreet = document.getElementById("street").value.split(", ")
            var postalCode = document.getElementById("postalCode").value

            if (fullStreet.length !== 2) {
                alert("Please input your street address as 'Street, Number'.")
                return
            }

            if (postalCode.split("-").length != 2) {
                alert("Please input your postal code as 'XXXX-YYY'.")
                return
            }

            var street = fullStreet[0]
            var number = fullStreet[1]
            var country = this.state.selectedCountry
            var city = this.state.selectedCity

            updateInformation.address = {
                street: street,
                number: number,
                country: country,
                city: city,
                postalCode: postalCode
            }
        }

        instance.put(GE_CLIENT_URL + auth.getUser()._id, updateInformation)
            .then((result) => {
                alert("Your information has been updated!")
                if (this.state.updateName)
                    auth.updateClientName(updateInformation.name)

                if (this.state.updateAddress)
                    auth.updateClientAddress(updateInformation.address)

                this.setState({
                    name: auth.getUser().name,
                    address: auth.getUser().address
                }, () => {
                    this.setState({
                        addressPart1: this.state.address.street + " " + this.state.address.number,
                        addressPart2: this.state.address.postalCode + " " + this.state.address.city + ", " + this.state.address.country,
                        selectedCountry: this.state.countries[0]
                    }, () => {
                        if (this.state.updateName)
                            document.getElementById("nameUpdate").value = null
                        if (this.state.updateAddress) {
                            document.getElementById("street").value = null
                            document.getElementById("postalCode").value = null
                            document.getElementById("country").value = this.state.countries[0]
                            document.getElementById("city").value = countryList.getCities(this.state.countries[0])[0]
                            this.setState({
                                selectedCountry: this.state.countries[0],
                                cities: countryList.getCities(this.state.countries[0]),
                                selectedCity: countryList.getCities(this.state.countries[0])[0],
                                updateName: false,
                                updateAddress: false
                            })
                        }

                        document.getElementById("checkUpdateName").checked = false
                        document.getElementById("checkUpdateAddress").checked = false
                    })
                })
            }).catch((error) => {
                if (error.response) {
                    alert(error.response.data);
                    this.componentDidMount()
                }
            })
    }

    handleCountryChange(e) {
        this.setState({
            selectedCountry: e.target.value,
            cities: countryList.getCities(e.target.value)
        })
    }

    handleCityChange(e) {
        this.setState({
            selectedCity: e.target.value
        })
    }

    handleCheckUpdateName(e) {
        this.setState({
            updateName: e.target.checked
        })
    }

    handleCheckUpdateAddress(e) {
        this.setState({
            updateAddress: e.target.checked
        })
    }

    handleDeleteAccount() {
        instance.delete(GE_CLIENT_URL + auth.getUser()._id)
            .then((result) => {
                alert("Your account has been deleted from the system!")
                auth.signOut()
            }).catch((error) => {
                if (error.response) {
                    alert(error.response.data);
                    this.componentDidMount()
                }
            })
    }

    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="updateClient"
                                title="Update Information"
                                content={
                                    <div>
                                        <form>
                                            <div>
                                                <input type="checkBox" id="checkUpdateName" label="Update Name" onClick={this.handleCheckUpdateName} />&nbsp;Update Name&nbsp;&nbsp;&nbsp;
                                                <input type="checkBox" id="checkUpdateAddress" label="Update Address" onClick={this.handleCheckUpdateAddress} />&nbsp;Update Address
                                            </div>
                                            {
                                                this.state.updateName &&
                                                <div>
                                                    <FormInputs
                                                        ncols={["col-md-12"]}
                                                        properties={[
                                                            {
                                                                label: "Name",
                                                                type: "text",
                                                                bsClass: "form-control",
                                                                placeholder: this.state.name,
                                                                defaultValue: "",
                                                                id: "nameUpdate",
                                                                disabled: !this.state.permissions.clientChangeName
                                                            }
                                                        ]}
                                                    />
                                                </div>
                                            }
                                            {
                                                this.state.updateAddress &&
                                                <div>
                                                    <FormInputs
                                                        ncols={["col-md-6", "col-md-6"]}
                                                        properties={[
                                                            {
                                                                label: "Street",
                                                                type: "text",
                                                                bsClass: "form-control",
                                                                placeholder: auth.getUser().address.street + ", " + auth.getUser().address.number,
                                                                defaultValue: "",
                                                                id: "street",
                                                                disabled: !this.state.permissions.clientChangeAddress
                                                            },
                                                            {
                                                                label: "Postal Code",
                                                                type: "text",
                                                                bsClass: "form-control",
                                                                placeholder: auth.getUser().address.postalCode,
                                                                defaultValue: "",
                                                                id: "postalCode",
                                                                disabled: !this.state.permissions.clientChangeAddress
                                                            }
                                                        ]}
                                                    />
                                                    <FormInputs
                                                        ncols={["col-md-6", "col-md-6"]}
                                                        properties={[
                                                            {
                                                                label: "Country",
                                                                type: "select",
                                                                bsClass: "form-control",
                                                                placeholder: "Country",
                                                                defaultValue: auth.getUser().address.country,
                                                                id: "country",
                                                                options: this.state.countries,
                                                                onChange: this.handleCountryChange,
                                                                disabled: !this.state.permissions.clientChangeAddress
                                                            },
                                                            {
                                                                label: "City",
                                                                type: "select",
                                                                bsClass: "form-control",
                                                                placeholder: "City",
                                                                defaultValue: auth.getUser().address.city,
                                                                id: "city",
                                                                options: this.state.cities,
                                                                onChange: this.handleCityChange,
                                                                disabled: !this.state.permissions.clientChangeAddress
                                                            }
                                                        ]}
                                                    />
                                                </div>
                                            }
                                            {
                                                (this.state.updateAddress || this.state.updateName) &&
                                                <div>
                                                    <Button bsStyle="info" pullRight fill onClick={this.handleUpdateInformation} id="updateInformationBtn">
                                                        Update Information
                                                    </Button>
                                                    <div className="clearfix" />
                                                </div>
                                            }
                                        </form>
                                    </div>
                                }
                            />
                        </Col>
                        <Col md={4}>
                            <UserCard
                                name={this.state.name}
                                idInfo={this.state.email}
                                nif={this.state.nif}
                                address1={this.state.addressPart1}
                                address2={this.state.addressPart2}
                            />
                            <Button bsStyle="info" pullRight fill onClick={this.handleDeleteAccount} id="deleteAccount">
                                Delete Account
                            </Button>
                            <div className="clearfix" />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}