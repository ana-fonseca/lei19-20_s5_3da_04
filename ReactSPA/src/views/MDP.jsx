/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";
import axios from "axios";
import http from "http";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

const MDP_URL = process.env.REACT_APP_MDP_URL
const OPERATION_URL = process.env.REACT_APP_OPERATION_URL

const instance = axios.create({
  httpAgent: new http.Agent({
    rejectUnauthorized: false
  })
});

class MDP extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      manufacturePlans: [],
      operations: []
    }

    this.handleSubmitPost = this.handleSubmitPost.bind(this)
    this.handleDeleteProduct = this.handleDeleteProduct.bind(this)
    this.handleDeleteManufacturePlan = this.handleDeleteManufacturePlan.bind(this)
    this.resetComponent = this.resetComponent.bind(this)
  }

  componentDidMount() {
    instance.get(MDP_URL + "Product")
      .then((result) => {
        this.setState({
          products: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDP_URL + "ManufacturePlan")
      .then((result) => {
        this.setState({
          manufacturePlans: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(OPERATION_URL)
      .then((result) => {
        this.setState({
          operations: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  resetComponent() {
    document.getElementById("productID").value = null
    document.getElementById("manufacturePlanID").value = null
    document.getElementById("productDescription").value = null
    document.getElementById("manufacturePlanDescription").value = null
    this.state.operations.map(item => {
      document.getElementById("create_" + item.operationID).value = null
    })
  }

  handleDeleteProduct(e) {
    var select = document.getElementById("deleteSelectorProduct");
    var product = select.options[select.selectedIndex].text;

    instance.delete(MDP_URL + "ByProduct/" + product)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
        }
      })
  }

  handleDeleteManufacturePlan(e) {
    var select = document.getElementById("deleteSelectorManufacturePlan");
    var manufacturePlan = select.options[select.selectedIndex].text;

    instance.delete(MDP_URL + "ByManufacturePlan/" + manufacturePlan)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
        }
      })
  }

  handleSubmitPost(e) {
    var productID = document.getElementById("productID").value
    var manufacturePlanID = document.getElementById("manufacturePlanID").value
    var productDescription = document.getElementById("productDescription").value
    var manufacturePlanDescription = document.getElementById("manufacturePlanDescription").value
    var operationSequence = []

    var last = 0
    var exit = false;

    this.state.operations.map(item => {
      var counter = document.getElementById("create_" + item.operationID)
      if (counter && counter.value) {
        var order = counter.value

        if (order > last) {
          last = order
        }
      }

      return null;
    })

    for (var i = 0; i < last + 1; i++) {
      operationSequence.splice(i, 0, "...")
    }

    this.state.operations.map(item => {
      var counter = document.getElementById("create_" + item.operationID)
      if (counter && counter.value) {
        var position = counter.value

        // eslint-disable-next-line
        if (position == 0) {
          return null;
        }

        // eslint-disable-next-line
        if (operationSequence[position] != "...") {
          alert("There are at least two operations in the same position!")
          exit = true;
          return null
        }

        operationSequence[position] = item.operationID
      }

      return null
    })

    if (exit) {
      return
    }

    operationSequence = operationSequence.filter(operation => operation !== "...")

    instance.post(MDP_URL, {
      productID: productID,
      descriptionP: productDescription,
      manufacturePlanID: manufacturePlanID,
      descriptionM: manufacturePlanDescription,
      operationSequence: operationSequence
    }).then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }

  render() {
    const { products, manufacturePlans, operations } = this.state

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={8}>
              <Card
                title="Create Product and Manufacture Plan"
                content={
                  <form>
                    <FormInputs
                      ncols={["col-md-6", "col-md-6", "col-md-6", "col-md-6"]}
                      properties={[
                        {
                          label: "Product ID",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Product ID",
                          defaultValue: "",
                          id: "productID"
                        },
                        {
                          label: "Manufacture Plan ID",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Manufacture Plan ID",
                          defaultValue: "",
                          id: "manufacturePlanID"
                        },
                        {
                          label: "Product Description",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Product Description",
                          defaultValue: "",
                          id: "productDescription"
                        },
                        {
                          label: "Manufacture Plan Description",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Manufacture Plan Description",
                          defaultValue: "",
                          id: "manufacturePlanDescription"
                        }
                      ]}
                    />
                    <label>Operations (0 or empty to ignore)</label><br />
                    <ul>
                      {operations.map(item => (
                        <li key={item.operationID}>
                          <div>{item.operationID}&nbsp;&nbsp;<input type="number" min="0" id={"create_" + item.operationID} /></div>
                        </li>
                      ))}
                    </ul>
                    <Button bsStyle="info" pullRight fill onClick={this.handleSubmitPost} id="createMDPBtn">
                      Create Product and Manufacture Plan
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                id="productList"
                title="Products"
                content={
                  <div>
                    <ul>
                      {products.map(item => (
                        <li key={item.productID}>
                          <div><u><b>Product ID:</b></u> {item.productID}</div>
                          <ul><u>Description:</u> {item.description}</ul>
                        </li>
                      ))}
                    </ul>
                    <br />
                    <label>Delete Product (also deletes its manufacture plan)</label>
                    <form>
                      <select id="deleteSelectorProduct">
                        {products.map(item => (
                          <option key={"delete_" + item.productID} value={"delete_" + item.productID}>
                            {item.productID}
                          </option>
                        ))}
                      </select><br />
                      <Button bsStyle="info" pullRight fill onClick={this.handleDeleteProduct} id="deleteProductBtn">
                        Delete Product
                    </Button>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                id="manufacturePlanList"
                title="Manufacture Plans"
                content={
                  <div>
                    <ul>
                      {manufacturePlans.map(item => (
                        <li key={item.manufacturePlanID}>
                          <div><b><u>Manufacture Plan ID:</u></b> {item.manufacturePlanID}</div>
                          <ul><u>Product ID:</u> {item.productID}</ul>
                          <ul><u>Description:</u> {item.description}</ul>
                          <ul><u>Operation Sequence:</u></ul>
                          {item.operationSequence.map(newItem => (
                            <ul key={item.manufacturePlanID + "_" + newItem} id={item.manufacturePlanID + "_" + newItem}>
                              <ul>
                                <li>
                                  <div>{newItem}</div>
                                </li>
                              </ul>
                            </ul>
                          ))}
                          <ul><u>Time:</u> {item.operationsTime} seconds</ul>
                        </li>
                      ))}
                    </ul>
                    <br />
                    <label>Delete Manufacture Plan (also deletes its product)</label>
                    <form>
                      <select id="deleteSelectorManufacturePlan">
                        {manufacturePlans.map(item => (
                          <option key={"delete_" + item.manufacturePlanID} value={"delete_" + item.manufacturePlanID}>
                            {item.manufacturePlanID}
                          </option>
                        ))}
                      </select><br />
                      <Button bsStyle="info" pullRight fill onClick={this.handleDeleteManufacturePlan} id="deleteManufacturePlanBtn">
                        Delete Product
                    </Button>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default MDP;
