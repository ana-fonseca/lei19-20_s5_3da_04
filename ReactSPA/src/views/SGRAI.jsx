import React, { Component } from "react";
import Iframe from 'react-iframe'

import { Grid, Row, Col } from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";

class SGRAI extends Component {

  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                statsIcon="fa fa-clock-o"
                id="sgrai"  
                content={
                  <Iframe url={process.env.REACT_APP_SGRAI_URL}
                    width="1550px"
                    height="1024px"
                    id="sgraiID"
                    display="initial"
                    position="relative" />
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    )
  }
}

export default SGRAI;