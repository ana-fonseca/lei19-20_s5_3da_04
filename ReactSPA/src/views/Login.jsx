/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
import axios from "axios";
import http from "http";
import { Grid, Row, Col } from "react-bootstrap";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import { Card } from "components/Card/Card.jsx";
import auth from "../auther"

const md5 = require("md5")
const emailRegex = require('email-regex');
const countryList = require('countries-cities')

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

const GE_CLIENT_URL = process.env.REACT_APP_GE_CLIENT_URL
const GE_ADMIN_URL = process.env.REACT_APP_GE_ADMIN_URL
const GE_PERMISSIONS_URL = process.env.REACT_APP_GE_PERMISSIONS_URL

const instance = axios.create({
    httpAgent: new http.Agent({
        rejectUnauthorized: false
    })
});

var passwordValidator = require('password-validator');
var schema = new passwordValidator();

schema
    .is().min(8)
    .is().max(100)
    .has().uppercase()
    .has().lowercase()
    .has().digits()
    .has().not().spaces()

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            countries: [],
            cities: [],
            permissions: {},
            selectedCountry: null,
            selectedCity: null,
            redirectToDashboard: false,
            redirectToProfile: false
        }

        this.handleLoginBtn = this.handleLoginBtn.bind(this)
        this.handleRegisterBtn = this.handleRegisterBtn.bind(this)
        this.handleCountryChange = this.handleCountryChange.bind(this)
        this.handleCityChange = this.handleCityChange.bind(this)
        this.redirectToDashboard = this.redirectToDashboard.bind(this)
        this.redirectToProfile = this.redirectToProfile.bind(this)
    }

    componentDidMount() {
        this.setState({
            countries: countryList.getCountries(),
            cities: countryList.getCities(countryList.getCountries()[0])
        }, () => {
            this.setState({
                selectedCountry: this.state.countries[0],
                selectedCity: this.state.cities[0]
            })
        })

        instance.get(GE_PERMISSIONS_URL)
            .then((result) => {
                this.setState({
                    permissions: result.data
                })
            })
    }

    handleLoginBtn() {
        if (document.getElementById("emailAddressLogin").value === null) {
            alert("You must input an e-mail!")
            return
        }

        if (document.getElementById("passwordLogin").value === null) {
            alert("You must input a password!")
            return
        }

        var email = document.getElementById("emailAddressLogin").value
        var password = md5(document.getElementById("passwordLogin").value)

        if (!email.includes("@")) {
            instance.post(GE_ADMIN_URL + "username/" + email, {
                password: password
            })
                .then((result) => {
                    if (result.data.verified) {
                        instance.get(GE_ADMIN_URL + "username/" + email)
                            .then((result) => {
                                auth.loginAdmin(result.data)
                                this.setState({
                                    redirectToDashboard: true
                                })
                            })
                    } else {
                        alert("That user is not implemented!")
                        window.location.reload()
                    }
                })
                .catch((err) => {
                    alert("That user is not implemented!")
                })
        } else {
            if (!this.state.permissions.login) {
                alert("The log-in function has been deactivated by an admin!")
                return
            }

            instance.post(GE_CLIENT_URL + "email/" + email, {
                password: password
            })
                .then((result) => {
                    if (result.data.verified) {
                        instance.get(GE_CLIENT_URL + "email/" + email)
                            .then((result) => {
                                auth.loginClient(result.data)
                                this.setState({
                                    redirectToProfile: true
                                })
                            })
                    } else {
                        alert("The password is wrong!")
                        window.location.reload()
                    }
                })
                .catch((error) => {
                    alert("That user is not implemented!")
                })
        }
    }

    redirectToDashboard() {
        if (this.state.redirectToDashboard) {
            return <Redirect to="/dashboard"/>
        }
    }

    redirectToProfile() {
        if (this.state.redirectToProfile) {
            return <Redirect to="/profile"/>
        }
    }

    handleRegisterBtn() {
        if (!this.state.permissions.register) {
            alert("This feature has been disabled by an admin")
            return
        }

        var name = document.getElementById("name").value
        var email = document.getElementById("emailAddressRegister").value
        var password = document.getElementById("passwordRegister").value
        var confirmPassword = document.getElementById("confirmPassword").value
        var nif = document.getElementById("nif").value

        var fullStreet = document.getElementById("street").value.split(", ")
        var postalCode = document.getElementById("postalCode").value

        if (fullStreet.length !== 2) {
            alert("Please input your street address as 'Street, Number'.")
            return
        }

        if (postalCode.split("-").length != 2) {
            alert("Please input your postal code as 'XXXX-YYY'.")
            return
        }

        var street = fullStreet[0]
        var number = fullStreet[1]
        var country = this.state.selectedCountry
        var city = this.state.selectedCity

        if (name.split(" ").length < 2) {
            alert("Please input your first and last name.")
            return
        }

        if (!emailRegex({ exact: true }).test(email)) {
            alert("Please input a valid e-mail address.")
            return
        }

        if (!schema.validate(password)) {
            alert("Invalid Password!\nYour password should be between 8 and 100 characters long, should have at least one uppercase character, one digit, one lowercase character and no blank spaces!")
            return
        }

        if (password !== confirmPassword) {
            alert("The inputted passwords do not match!")
            return
        }

        instance.post(GE_CLIENT_URL, {
            name: name,
            email: email,
            password: password,
            nif: nif,
            address: {
                street: street,
                number: number,
                postalCode: postalCode,
                city: city,
                country: country
            }
        }).then((result) => {
            alert("You can now login using your credentials")
            window.location.reload()
        }).catch((error) => {
            if (error.response) {
                alert(error.response.data);
                window.location.reload()
            }
        })
    }

    handleCountryChange(e) {
        this.setState({
            selectedCountry: e.target.value,
            cities: countryList.getCities(e.target.value)
        })
    }

    handleCityChange(e) {
        this.setState({
            selectedCity: e.target.value
        })
    }

    render() {
        return (
            <div className="content">
                {this.redirectToDashboard()}
                {this.redirectToProfile()}
                <Grid fluid>
                    <Row>
                        <Col md={6}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="loginCard"
                                title="Log-in"
                                content={
                                    <form>
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            properties={[
                                                {
                                                    label: "E-mail Address or Username",
                                                    type: "email",
                                                    bsClass: "form-control",
                                                    placeholder: "E-mail Address or Username",
                                                    defaultValue: "",
                                                    id: "emailAddressLogin"
                                                },
                                                {
                                                    label: "Password",
                                                    type: "password",
                                                    bsClass: "form-control",
                                                    placeholder: "Password",
                                                    defaultValue: "",
                                                    id: "passwordLogin"
                                                }
                                            ]}
                                        />
                                        <Button bsStyle="info" pullRight onClick={this.handleLoginBtn} fill id="loginClient">
                                            Log-in
                                        </Button>
                                        <div className="clearfix" />
                                    </form>
                                }
                            />
                        </Col>
                        <Col md={6}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="registerCard"
                                title={this.state.permissions.register ? "Register" : "Register (this feature has been disabled by an admin)"}
                                content={
                                    <form>
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            properties={[
                                                {
                                                    label: "Name",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "First and last name",
                                                    defaultValue: "",
                                                    id: "name",
                                                    disabled: !this.state.permissions.register
                                                },
                                                {
                                                    label: "E-mail Address",
                                                    type: "email",
                                                    bsClass: "form-control",
                                                    placeholder: "E-mail Address",
                                                    defaultValue: "",
                                                    id: "emailAddressRegister",
                                                    disabled: !this.state.permissions.register
                                                }
                                            ]}
                                        />
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            properties={[
                                                {
                                                    label: "Password",
                                                    type: "password",
                                                    bsClass: "form-control",
                                                    placeholder: "Password",
                                                    defaultValue: "",
                                                    id: "passwordRegister",
                                                    disabled: !this.state.permissions.register
                                                },
                                                {
                                                    label: "Confirm Password",
                                                    type: "password",
                                                    bsClass: "form-control",
                                                    placeholder: "Confirm Password",
                                                    defaultValue: "",
                                                    id: "confirmPassword",
                                                    disabled: !this.state.permissions.register
                                                }
                                            ]}
                                        />
                                        <FormInputs
                                            ncols={["col-md-3", "col-md-5", "col-md-4"]}
                                            properties={[
                                                {
                                                    label: "NIF",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "NIF",
                                                    defaultValue: "",
                                                    id: "nif",
                                                    disabled: !this.state.permissions.register
                                                },
                                                {
                                                    label: "Street",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "Street, Number",
                                                    defaultValue: "",
                                                    id: "street",
                                                    disabled: !this.state.permissions.register
                                                },
                                                {
                                                    label: "Postal Code",
                                                    type: "text",
                                                    bsClass: "form-control",
                                                    placeholder: "Postal Code (XXXX-YYY)",
                                                    defaultValue: "",
                                                    id: "postalCode",
                                                    disabled: !this.state.permissions.register
                                                }
                                            ]}
                                        />
                                        <FormInputs
                                            ncols={["col-md-6", "col-md-6"]}
                                            properties={[
                                                {
                                                    label: "Country",
                                                    type: "select",
                                                    bsClass: "form-control",
                                                    placeholder: "Country",
                                                    defaultValue: "",
                                                    id: "country",
                                                    options: this.state.countries,
                                                    onChange: this.handleCountryChange,
                                                    disabled: !this.state.permissions.register
                                                },
                                                {
                                                    label: "City",
                                                    type: "select",
                                                    bsClass: "form-control",
                                                    placeholder: "City",
                                                    defaultValue: "",
                                                    id: "city",
                                                    options: this.state.cities,
                                                    onChange: this.handleCityChange,
                                                    disabled: !this.state.permissions.register
                                                }
                                            ]}
                                        />
                                            <a href="https://download1501.mediafire.com/o8hj4f1na1fg/cprj9juw3souulh/3DA4_1170426_1170595_1170656_1170620+%282%29.pdf">RGPD Terms</a>
                                        <br/>
                                        <Button bsStyle="info" pullRight fill id="registerClient" onClick={this.handleRegisterBtn}>
                                            Register
                                        </Button>
                                        <div className="clearfix" />
                                    </form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div >
        );
    }
}

export default Login;
