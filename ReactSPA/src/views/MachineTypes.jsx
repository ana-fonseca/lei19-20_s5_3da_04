/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { Component } from "react";
import {
  Grid,
  Row,
  Col
} from "react-bootstrap";
import axios from "axios";
import http from "http";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { throwStatement } from "@babel/types";

const MDF_URL = process.env.REACT_APP_MDF_URL
const instance = axios.create({
  httpAgent: new http.Agent({
    rejectUnauthorized: false
  })
});

class MachineTypes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operations: [],
      machineTypes: [],
      selectedOperations: []
    }

    this.handleDeleteMachineType = this.handleDeleteMachineType.bind(this)
    this.handleSubmitPost = this.handleSubmitPost.bind(this)
    this.handleUpdateMachineType = this.handleUpdateMachineType.bind(this)
    this.handleChangeMachineType = this.handleChangeMachineType.bind(this)
    this.changeOperations = this.changeOperations.bind(this)
    this.resetComponent = this.resetComponent.bind(this)
  }

  componentDidMount() {
    instance.get(MDF_URL + "Operation")
      .then((result) => {
        this.setState({
          operations: result.data
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })

    instance.get(MDF_URL + "MachineType")
      .then((result) => {
        this.setState({
          machineTypes: result.data
        }, () => {
          this.changeOperations(this.state.machineTypes[0].machineTypeID)
        })
      }
      ).catch((err) => {
        console.log(err.toString())
      })
  }

  resetComponent() {
    const { operations } = this.state
    
    document.getElementById("machineTypeID").value = null
    document.getElementById("machineTypeDescription").value = null
    
    operations.map(item => {
      document.getElementById(item.operationID).checked = null
    })
  }

  handleDeleteMachineType(e) {
    var select = document.getElementById("deleteSelector");
    var machineType = select.options[select.selectedIndex].text;

    instance.delete(MDF_URL + "MachineType/" + machineType)
      .then((result) => {
        this.componentDidMount()
        this.resetComponent()
      }).catch((error) => {
        if (error.response) {
          alert(error.response.data);
        }
      })
  }

  handleChangeMachineType(e) {
    this.changeOperations(parseInt(e.target.value.substr(7, e.target.value.length)))
  }

  changeOperations(machineType) {
    var newOperations = []
    const { operations, machineTypes } = this.state

    for (var i = 0; i < machineTypes.length; i++) {
      if (machineTypes[i].machineTypeID === machineType) {
        var operationList = machineTypes[i].operationList
        for (var i = 0;i < operations.length;i++) {
          newOperations[i] = operations[i]
          newOperations[i].checked = operationList.includes(operations[i].operationID)
        }

        this.setState({
          operations: newOperations
        })
      }
    }
  }

  handleSubmitPost(e) {
    var machineType = document.getElementById("machineTypeID").value;
    var machineTypeDescription = document.getElementById("machineTypeDescription").value;

    var chosenOperations = [];

    this.state.operations.map(item => {
      var checkbox = document.getElementById(item.operationID).checked;
      if (checkbox) {
        chosenOperations.push(item.operationID)
      }
      return null;
    })

    // eslint-disable-next-line
    if (chosenOperations.length == 0) {
      alert("You must select at least one operation!")
      return
    }

    instance.post(MDF_URL + "MachineType", {
      machineTypeID: machineType,
      description: machineTypeDescription,
      operationList: chosenOperations
    }).then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }

  handleUpdateMachineType(e) {
    var selectMachineType = document.getElementById("updateMachineTypeSelector");
    var machineType = selectMachineType.options[selectMachineType.selectedIndex].text;

    var chosenOperations = [];

    this.state.operations.map(item => {
      var checkbox = document.getElementById("update_" + item.operationID).checked;
      if (checkbox) {
        chosenOperations.push(item.operationID)
      }
      return null;
    })

    // eslint-disable-next-line
    if (chosenOperations.length == 0) {
      alert("You must select at least one operation!")
      return
    }

    instance.put(MDF_URL + "MachineType/ChangeMachineType/" + machineType, {
      operationList: chosenOperations
    }).then((result) => {
      this.componentDidMount()
      this.resetComponent()
    }).catch((error) => {
      if (error.response) {
        alert(error.response.data);
      }
    })
  }
 
  render() {
    const { operations, machineTypes, selectedOperations } = this.state

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={8}>
              <Card
                title="Create Machine Type"
                content={
                  <form>
                    <FormInputs
                      ncols={["col-md-6", "col-md-6"]}
                      properties={[
                        {
                          label: "Machine Type ID",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Machine Type ID",
                          defaultValue: "",
                          id: "machineTypeID"
                        },
                        {
                          label: "Machine Type Description",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Machine Type Description",
                          defaultValue: "",
                          id: "machineTypeDescription"
                        }
                      ]}
                    />
                    <div>
                      {operations.map(item => (
                        <div key={item.operationID}>
                          <label>
                            <input id={item.operationID} type="checkbox" name={item.operationID} label={item.operationID} vertical-align="middle" /> {item.operationID}
                          </label>
                        </div>
                      ))}
                    </div>
                    <Button bsStyle="info" pullRight fill onClick={this.handleSubmitPost} id="createMachineTypeBtn">
                      Create Machine Type
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                id="machineTypes"
                title="Machine Types"
                content={
                  <div>
                    <ul>
                      {machineTypes.map(item => (
                        <li key={item.machineTypeID}>
                          <div><b><u>Machine Type ID:</u></b> {item.machineTypeID}</div>
                          <ul><u>Description:</u> {item.description}</ul>
                          <ul><u>Operation List:</u></ul>
                          {item.operationList.map(newItem => (
                            <ul key={item.machineTypeID + "_" + newItem} id={item.machineTypeID + "_" + newItem}>
                              <ul>
                                <li>
                                  <div>{newItem}</div>
                                </li>
                              </ul>
                            </ul>
                          ))}
                        </li>
                      ))}
                    </ul>
                    <br />
                    <label>Delete Machine Type</label>
                    <form>
                      <select id="deleteSelector">
                        {machineTypes.map(item => (
                          <option key={"delete_" + item.machineTypeID} value={"delete_" + item.machineTypeID}>
                            {item.machineTypeID}
                          </option>
                        ))}
                      </select><br />
                      <Button bsStyle="info" pullRight fill onClick={this.handleDeleteMachineType} id="deleteMachineTypeBtn">
                        Delete Machine
                      </Button>
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={8}>
            <Card
                title="Update Machine Type Operations"
                content={
                  <form>
                    <label>Machine Type to be updated</label><br />
                    <select id="updateMachineTypeSelector" onChange={this.handleChangeMachineType}>
                      {machineTypes.map(item => (
                        <option key={"update_" + item.machineTypeID} value={"update_" + item.machineTypeID}>
                          {item.machineTypeID}
                        </option>
                      ))}
                    </select>
                    <br /><br />
                    <label>Operations</label><br />
                    <div>
                      {operations.map(item => (
                        <div key={item.operationID}>
                          <label>
                            <input id={"update_" + item.operationID} type="checkbox" name={"update_" + item.operationID} label={item.operationID} vertical-align="middle" /> {item.operationID}&nbsp;{item.checked ? "(the machine type knows this operation)" : null}
                          </label>
                        </div>
                      ))}
                    </div>
                    <Button bsStyle="info" pullRight fill onClick={this.handleUpdateMachineType} id="updateMachineTypeBtn">
                      Update Machine Type Operations
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default MachineTypes;
