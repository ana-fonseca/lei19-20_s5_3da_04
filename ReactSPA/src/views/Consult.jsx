import React, { Component } from "react";
import axios from "axios";
import http from "http";
import { Grid, Row, Col } from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import auth from "../auther";
import "react-datepicker/dist/react-datepicker.css";
import ReactTooltip from 'react-tooltip'
import Button from "components/CustomButton/CustomButton.jsx";

const GE_CLIENT_URL = process.env.REACT_APP_GE_CLIENT_URL
const GE_ORDER_URL = process.env.REACT_APP_GE_ORDER_URL
const GE_PERMISSIONS_URL = process.env.REACT_APP_GE_PERMISSIONS_URL
const MDP_URL = process.env.REACT_APP_MDP_URL
const countryList = require('countries-cities')

const instance = axios.create({
    httpAgent: new http.Agent({
        rejectUnauthorized: false
    })
});

class Consult extends Component {

    constructor(props) {
        super(props);
        this.state = ({
            orders: [],
            clientOrders: [],
            products: [],
            permissions: {},
            manufacturePlans: [],
            useUserAddress: false,
            countries: countryList.getCountries(),
            cities: countryList.getCities(countryList.getCountries()[0]),
            selectedCountry: countryList.getCountries()[0],
            selectedCity: countryList.getCities(countryList.getCountries()[0])[0],
            dueDate: new Date(+new Date() + 4 * 24 * 60 * 60 * 1000),
            bestBoughtProducts: [],
            bestOrderedProducts: [],
            fastestProducts: [],
        })

        this.mostBoughtProducts = this.mostBoughtProducts.bind(this)
        this.frequencyForMostBought = this.frequencyForMostBought.bind(this)
        this.mostOrderedProducts = this.mostOrderedProducts.bind(this)
        this.frequencyForMostOrdered = this.frequencyForMostOrdered.bind(this)
        this.sortMap = this.sortMap.bind(this)
        this.bestThree = this.bestThree.bind(this)
        this.getProdFromProdID = this.getProdFromProdID.bind(this)
    }

    fastestProduct() {
        const { manufacturePlans } = this.state
        var fastestProductsMap = new Map();

        for (var i = 0; i < manufacturePlans.length; i++) {
            var product = this.getProdFromProdID(manufacturePlans[i].productID)
            product.time = manufacturePlans[i].operationsTime
            fastestProductsMap.set(product, manufacturePlans[i].operationsTime);
        }

        var bestThree = this.bestThree(this.sortMap(fastestProductsMap, false));
        this.setState({
            fastestProducts: bestThree
        })
    }

    getProdFromProdID(prodID) {
        const { products } = this.state

        for (var i = 0; i < products.length; i++) {
            if (products[i].productID == prodID) {
                return products[i]
            }
        }
    }

    mostBoughtProducts() {
        var frequencyMap = this.frequencyForMostBought();
        var bestThree = this.bestThree(this.sortMap(frequencyMap, true));

        this.setState({
            bestBoughtProducts: bestThree
        })
    }

    //This method counts frequencies according to ordered quantities
    frequencyForMostBought() {
        const { orders } = this.state

        var mostBoughtMap = new Map();

        for (var i = 0; i < orders.length; i++) {
            for (var n = 0; n < orders[i].products.length; n++) {
                var prodObj = { productID: orders[i].products[n].productID, productDescription: orders[i].products[n].productDescription }
                if (mostBoughtMap.has(prodObj)) {
                    var frequency = mostBoughtMap.get(prodObj);
                    frequency += orders[i].products[n].quantity;
                    mostBoughtMap.set(prodObj, frequency);
                } else {
                    mostBoughtMap.set(prodObj, orders[i].products[n].quantity);
                }
            }
        }
        return mostBoughtMap;
    }

    mostOrderedProducts() {
        var frequencyMap = this.frequencyForMostOrdered();

        var bestThree = this.bestThree(this.sortMap(frequencyMap, true));

        this.setState({
            bestOrderedProducts: bestThree
        })
    }

    //This method counts frequencies according to orders
    frequencyForMostOrdered() {
        const { orders } = this.state

        var mostOrderedMap = new Map();

        for (var i = 0; i < orders.length; i++) {
            for (var n = 0; n < orders[i].products.length; n++) {
                if (mostOrderedMap.has(orders[i].products[n])) {
                    var frequency = mostOrderedMap.get(orders[i].products[n]);
                    frequency++;
                    mostOrderedMap.set(orders[i].products[n], frequency);
                } else {
                    mostOrderedMap.set(orders[i].products[n], 1);
                }
            }
        }
        return mostOrderedMap;
    }

    //Returns Best Three Products according to Sorting method
    bestThree(sortedMap) {
        var bestProducts = [];

        for (const product of sortedMap.entries()) {
            if (bestProducts.length == 3)
                break

            product[0].quantity = product[1]
            bestProducts.push(product[0])
        }

        return bestProducts;
    }

    //This method returns a sorted Map in descending order
    //https://stackoverflow.com/questions/37982476/how-to-sort-a-map-by-value-in-javascript
    sortMap(myMap, descending) {
        if (descending)
            return new Map([...myMap.entries()].sort((a, b) => b[1] - a[1]));

        return new Map([...myMap.entries()].sort((a, b) => a[1] - b[1]));
    }

    componentDidMount() {
        instance.get(MDP_URL + "ManufacturePlan")
            .then((result) => {
                this.setState({
                    manufacturePlans: result.data
                }, () => {
                    instance.get(MDP_URL + "product/")
                        .then((result) => {
                            this.setState({
                                products: result.data
                            }, () => {
                                this.fastestProduct()
                            })
                        })
                })
            }
            ).catch((err) => {
                console.log(err.toString())
            })

        instance.get(GE_PERMISSIONS_URL)
            .then((result) => {
                this.setState({
                    permissions: result.data
                })
            })

        instance.get(GE_ORDER_URL)
            .then((result) => {
                this.setState({
                    orders: result.data
                }, () => {
                    this.mostBoughtProducts()
                    this.mostOrderedProducts()
                })
            })
    }

    render() {

        const { clientOrders, bestBoughtProducts, bestOrderedProducts, fastestProducts } = this.state

        return (
            <div className="content">
                <ReactTooltip />
                <Grid fluid>
                    <Row>
                        <Col md={4}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="mostBoughtProducts"
                                title={this.state.permissions.viewMostBoughtProducts ? "Most Bought Products" : "Most Bought Products (this feature has been disabled by an admin)"}
                                content={
                                    <div>
                                        {this.state.permissions.viewMostBoughtProducts &&
                                            <div>
                                                <ul>
                                                    {bestBoughtProducts.map(orderedItem => (
                                                        <li key={orderedItem.productID}>
                                                            <u>Product:</u> {orderedItem.productDescription} ({orderedItem.productID}) <br />
                                                            <u>Total Quantity:</u> {orderedItem.quantity}
                                                        </li>
                                                    ))}
                                                </ul>
                                            </div>
                                        }

                                    </div>
                                }
                            />
                        </Col>
                        <Col md={4}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="mostTimeBoughtProducts"
                                title={this.state.permissions.viewMostTimeBoughtProducts ? "Most Frequently Ordered Products" : "Most Frequently Ordered Products (this feature has been disabled by an admin)"}
                                content={
                                    <div>
                                        {this.state.permissions.viewMostTimeBoughtProducts &&
                                            <div>
                                                <ul>
                                                    {bestOrderedProducts.map(orderedItem => (
                                                        <li key={orderedItem.productID}>
                                                            <u>Product:</u> {orderedItem.productDescription} ({orderedItem.productID}) <br />
                                                            <u>Number of times it has been bought:</u> {orderedItem.quantity}
                                                        </li>
                                                    ))}
                                                </ul>
                                            </div>
                                        }

                                    </div>
                                }
                            />
                        </Col>
                        <Col md={4}>
                            <Card
                                statsIcon="fa fa-clock-o"
                                id="fastestToMakeProducts"
                                title={this.state.permissions.viewFastestToMakeProducts ? "Fastest Products to Make" : "Fastest Products to Make (this feature has been disabled by an admin)"}
                                content={
                                    <div>
                                        {this.state.permissions.viewFastestToMakeProducts &&
                                            <div>
                                                <ul>
                                                    {fastestProducts.map(orderedItem => (
                                                        <li key={orderedItem.productID}>
                                                            <u>Product:</u> {orderedItem.description} ({orderedItem.productID}) <br />
                                                            <u>Manufacturing Time:</u> {orderedItem.time} seconds
                                                        </li>
                                                    ))}
                                                </ul>
                                            </div>
                                        }

                                    </div>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

export default Consult;