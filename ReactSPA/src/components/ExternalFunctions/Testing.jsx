import React, { Component } from "react";
import ReactDOM from "react-dom";

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import AdminLayout from "../../layouts/Admin.jsx";

class AdminRender extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" render={props => <AdminLayout {...props} />} />
                    <Redirect from="/" to="/login" />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default AdminRender;
