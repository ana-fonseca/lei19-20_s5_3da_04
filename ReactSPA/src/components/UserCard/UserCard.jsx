/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";

export class UserCard extends Component {
  render() {
    return (
      <div className="card card-user">
        <div className="image">
        </div>
        <div className="content">
          <div className="author">
            <a href="#pablo">
              <h4 className="title">
                {this.props.name}
                <br />
                <small>{this.props.idInfo}</small>
              </h4>
            </a>
            <a>
              <p className="description text-center">{this.props.nif}</p>
              <h5 className="title">
              {this.props.address1}<br/><small>{this.props.address2}</small>
              </h5>
            </a>
          </div>
        </div>
        <hr />
        <div className="text-center">{this.props.socials}</div>
      </div>
    );
  }
}

export default UserCard;
