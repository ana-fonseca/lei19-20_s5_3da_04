class Auth {
    constructor() {
        this.user = null

        this.loginAdmin = this.loginAdmin.bind(this)
        this.loginClient = this.loginClient.bind(this)
        this.signOut = this.signOut.bind(this)
        this.isAuthenticated = this.isAuthenticated.bind(this)
    }

    loginClient(newClient) {
        this.user = {
            _id: newClient._id,
            email: newClient.email,
            name: newClient.name,
            address: newClient.address,
            nif: newClient.nif,
            isClient: true,
            loginTime: new Date()
        }
    }

    loginAdmin(newAdmin) {
        this.user = {
            _id: newAdmin._id,
            username: newAdmin.username,
            name: newAdmin.name,
            isClient: false,
            loginTime: new Date()
        }
    }

    isClient() {
        return this.user.isClient 
    }

    signOut() {
        this.user = null;
        window.location.href = window.location.origin + "/login"
    }

    getUser() {
        return this.user
    }

    updateClientName(name) {
        this.user.name = name
    }

    updateClientAddress(address) {
        this.user.address = address
    }

    isAuthenticated() {
        if (this.user === null) {
            return false
        }
        var endDate = this.user.loginTime
        endDate.setHours(endDate.getHours() + 2) 
        return new Date() < endDate
    }
}

const auth = new Auth();

export default auth;