/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "components/Sidebar/Sidebar";

import routes from "routes.js";

import image from "assets/img/sidebar-3.jpg";
import axios from "axios";
import http from "http";
import auth from "../auther"

class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _notificationSystem: null,
      image: image,
      color: "black",
      hasImage: true
    };

     if (!auth.isAuthenticated() && window.location.pathname !== "/login") {
      auth.signOut()
     }
  }

  getRoutes = routes => {
    return routes.map((prop, key) => {
      return (
        <Route
          path={prop.path}
          render={props => (
            <prop.component
              {...props}
              handleClick={this.handleNotificationClick}
            />
          )}
          key={key}
        />
      )
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        window.location.pathname.indexOf(
          routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    window.location.href = "/login"
    return null;
  };
  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
    }
    if (e.history.action === "PUSH") {
      document.documentElement.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
      this.refs.mainPanel.scrollTop = 0;
    }
  }
  render() {
    var SideBarComponent = <Sidebar {...this.props} routes={routes} image={this.state.image}
      color={this.state.color}
      hasImage={this.state.hasImage} />

    var classN = "main-panel"
    if (window.location.pathname === "/login") {
      SideBarComponent = null;
      classN = ""
    }
    return (
      <div className="wrapper">
        {SideBarComponent}
        <div id="main-panel" className={classN} ref="mainPanel">
          <AdminNavbar
            {...this.props}
            brandText={this.getBrandText(window.location.pathname)}
          />
          <Switch>{this.getRoutes(routes)}</Switch>
          <Footer />
        </div>
      </div>
    );
  }
}

export default Admin;
