/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "views/Dashboard.jsx";
import Operations from "views/Operations.jsx";
import MachineTypes from "views/MachineTypes.jsx";
import Machines from "views/Machines.jsx";
import ProductionLines from "views/ProductionLines.jsx";
import Orders from "./views/Orders"
import MDP from "views/MDP.jsx";
import SGRAI from "views/SGRAI.jsx";
import Login from "./views/Login"
import Profile from "./views/Profile"
import OrdersClients from "./views/OrdersClients";
import Consult from './views/Consult'

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-menu",
    component: Dashboard,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/clients-orders",
    name: "Clients & Orders",
    icon: "pe-7s-user",
    component: OrdersClients,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/consult",
    name: "Consult",
    icon: "pe-7s-user",
    component: Consult,
    displayAdmin: false,
    displayClient: true
  },
  {
    path: "/profile",
    name: "Profile",
    icon: "pe-7s-user",
    component: Profile,
    displayAdmin: false,
    displayClient: true
  },
  {
    path: "/orders",
    name: "Orders",
    icon: "pe-7s-cart",
    component: Orders,
    displayAdmin: false,
    displayClient: true
  },
  {
    path: "/operations",
    name: "Operations",
    icon: "pe-7s-tools",
    component: Operations,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/machine-types",
    name: "Machine Types",
    icon: "pe-7s-settings",
    component: MachineTypes,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/machines",
    name: "Machines",
    icon: "pe-7s-airplay",
    component: Machines,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/production-lines",
    name: "Production Lines",
    icon: "pe-7s-hammer",
    component: ProductionLines,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/mdp",
    name: "Products & Manufacture Plans",
    icon: "pe-7s-box1",
    component: MDP,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/sgrai",
    name: "Visual Simulation",
    icon: "pe-7s-look",
    component: SGRAI,
    displayAdmin: true,
    displayClient: false
  },
  {
    path: "/login",
    name: "Log-in",
    component: Login,
    displayAdmin: false,
    displayClient: false
  }
];

export default dashboardRoutes;
