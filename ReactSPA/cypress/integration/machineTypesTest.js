const LOGIN_URL = "http://10.9.10.4:3000/login"
const USERNAME = "root"
const PASSWORD = "3DA4root"

describe('Testing the Machine Type page - Create, create with the same ID, update and delete', function () {

    var idOp1 = Math.floor(Math.random() * (500) + 10000) //will be >= 10000 && <=10499 
    var idOp2 = Math.floor(Math.random() * (500) + 10500) //will be >= 10500 && <=10999 

    var idMt1 = Math.floor(Math.random() * (500) + 10000) //will be >= 10000 && <=10499 

    before(() => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Operation'
        }).as('postOperation')

        cy.visit(LOGIN_URL)
        cy.get("#emailAddressLogin").type(USERNAME)
        cy.get("#passwordLogin").type(PASSWORD)
        cy.get("#loginClient").click()

        cy.wait(1000)

        cy.get("#\\/operations").click()

        //Create two operations for the rest of the test and checks that they exist
        cy.get("#operationID").type(idOp1)
        cy.get("#operationDescription").type("cypressTest1")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.get("#operationID").type(idOp2)
        cy.get("#operationDescription").type("cypressTest2")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait("@postOperation")

        cy.contains("Operation ID: " + idOp1)
        cy.contains("Operation ID: " + idOp2)

        cy.get("#\\/machine-types").click()
    })

    it('Should successfully create Machine type with operation', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/MachineType'
        }).as('postMachineType')

        cy.get('#machineTypeID').type(idMt1)
        cy.get('#machineTypeDescription').type("cypressTest")
        cy.get('#' + idOp1).click()
        cy.get('#createMachineTypeBtn').click()

        cy.wait("@postMachineType");

        cy.contains("Machine Type ID: " + idMt1)
        cy.get("#" + idMt1 + "_" + idOp1)
    });

    it('Should unsuccessfully create Machine type with operation, already is a machine type with the id', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/MachineType'
        }).as('postMachineType')

        cy.get('#machineTypeID').type(idMt1)
        cy.get('#machineTypeDescription').type("cypressTest")
        cy.get('#' + idOp1).click()
        cy.get("#createMachineTypeBtn").click()

        cy.wait('@postMachineType').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, 'There already is a machine type with that ID!')
        })
    });

    it('Should unsuccessfully create Machine type with operation, should have at least one operation selected', function () {
        cy.get('#' + idOp1).click()
        cy.get("#createMachineTypeBtn").click()
    });

    it('Should successfully update Machine type with operation', function () {
        cy.get("#updateMachineTypeSelector").select(idMt1.toString())
        cy.get("#update_" + idOp1).click()
        cy.get("#update_" + idOp2).click()
        cy.get("#updateMachineTypeBtn").click()

        cy.wait(500);

        cy.contains("Machine Type ID: " + idMt1)
        cy.get("#" + idMt1 + "_" + idOp1)
        cy.get("#" + idMt1 + "_" + idOp2)

    });

    it('Should delete the machine type', () => {
        cy.get("#deleteSelector").contains(idMt1.toString()).then(() => {
            cy.get("#deleteSelector").select(idMt1.toString())
            cy.get('#deleteMachineTypeBtn').click().then(() => {
                cy.contains("Machine Type ID: " + idMt1).should('not.exist')
            })
        })
    })

    after(() => {
        cy.get("#\\/operations").click()

        cy.get("#deleteSelector").contains(idOp1.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp1.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idOp2.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp2.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp2).should('not.exist')
            })
        })
    })
})