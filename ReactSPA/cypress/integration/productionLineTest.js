const LOGIN_URL = "http://10.9.10.4:3000/login"
const USERNAME = "root"
const PASSWORD = "3DA4root"

describe('Create and delete production line', function () {

    var idOp1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 
    var idOp2 = Math.floor(Math.random() * (500) + 10500); //will be >= 10500 && <=10999 

    var idMt1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 
    var idMt2 = Math.floor(Math.random() * (500) + 10500); //will be >= 10500 && <=10999 

    var idMa1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 
    var idMa2 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 

    var idPl1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 

    before(() => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Operation'
        }).as('postOperation')
        cy.route({
            method: 'POST',
            url: '/mdf/MachineType'
        }).as('postMachineType')
        cy.route({
            method: 'POST',
            url: '/mdf/Machine'
        }).as('postMachine')

        cy.visit(LOGIN_URL)
        cy.get("#emailAddressLogin").type(USERNAME)
        cy.get("#passwordLogin").type(PASSWORD)
        cy.get("#loginClient").click()

        cy.wait(1000)

        cy.get("#\\/operations").click()

        //Create two operations for the rest of the test and checks that they exist
        cy.get("#operationID").type(idOp1)
        cy.get("#operationDescription").type("cypressTest1")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.get("#operationID").type(idOp2)
        cy.get("#operationDescription").type("cypressTest2")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.contains("Operation ID: " + idOp1)
        cy.contains("Operation ID: " + idOp2)

        cy.get("#\\/machine-types").click()

        //Create two machine types for the rest of the test and checks that they exist
        cy.get('#machineTypeID').type(idMt1)
        cy.get('#machineTypeDescription').type("cypressTest")
        cy.get('#' + idOp1).click()
        cy.get('#createMachineTypeBtn').click()

        cy.wait('@postMachineType')

        cy.get('#machineTypeID').type(idMt2)
        cy.get('#machineTypeDescription').type("cypressTest")
        cy.get('#' + idOp1).click()
        cy.get('#' + idOp2).click()
        cy.get('#createMachineTypeBtn').click()

        cy.wait('@postMachineType')

        cy.contains("Machine Type ID: " + idMt1)
        cy.get("#" + idMt1 + "_" + idOp1)

        cy.contains("Machine Type ID: " + idMt2)
        cy.get("#" + idMt2 + "_" + idOp1)
        cy.get("#" + idMt2 + "_" + idOp2)

        cy.get("#\\/machines").click()

        cy.get("#machineID").type(idMa1)
        cy.get("#machineDescription").type("cypressTest")
        cy.get("#createSelector").select(idMt1.toString())
        cy.get("#createMachineBtn").click()

        cy.wait('@postMachine')

        cy.get("#machineID").type(idMa2)
        cy.get("#machineDescription").type("cypressTest")
        cy.get("#createSelector").select(idMt2.toString())
        cy.get("#createMachineBtn").click()

        cy.wait('@postMachine')

        cy.get("#\\/production-lines").click()
    })

    it('Should successfully create Production line', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/ProductionLine'
        }).as('postProductionLine')

        cy.get("#productionLineID").type(idPl1)
        cy.get("#productionLineDescription").type("cypressTest")
        cy.get("#create_" + idMa1).type("3")
        cy.get("#create_" + idMa2).type("1")
        cy.get("#createProductionLineBtn").click()

        cy.wait("@postProductionLine");

        cy.get("#list_" + idPl1)
        cy.get("#" + idPl1 + "_" + idMa1)
        cy.get("#" + idPl1 + "_" + idMa2)

    });

    it('Should unsuccessfully create Production line, once already is a production line with the id', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/ProductionLine'
        }).as('postProductionLine')

        cy.get("#productionLineID").type(idPl1)
        cy.get("#productionLineDescription").type("cypressTest")
        cy.get("#create_" + idMa1).type("3")
        cy.get("#create_" + idMa2).type("1")
        cy.get("#createProductionLineBtn").click()

        cy.wait('@postProductionLine').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, 'There already is a production line with that ID!')
        })
    });

    it('Should unsuccessfully create Production line, once none of the submitted machines exist', function () {
        cy.get("#create_" + idMa1).clear()
        cy.get("#create_" + idMa2).clear()
        cy.get("#createProductionLineBtn").click()
    });

    it('Should successfully delete Production line', function () {
        cy.get("#deleteSelector").contains(idPl1.toString()).then(() => {
            cy.get("#deleteSelector").select(idPl1.toString())
            cy.get('#deleteProductionLineBtn').click().then(() => {
                cy.contains("Production Line ID: " + idPl1).should('not.exist')
            })
        })        
    });

    after(() => {
        cy.get("#\\/machines").click()

        cy.get("#deleteSelector").contains(idMa1.toString()).then(() => {
            cy.get("#deleteSelector").select(idMa1.toString())
            cy.get('#deleteMachineBtn').click().then(() => {
                cy.contains("Machine ID: " + idMa1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idMa2.toString()).then(() => {
            cy.get("#deleteSelector").select(idMa2.toString())
            cy.get('#deleteMachineBtn').click().then(() => {
                cy.contains("Machine ID: " + idMa2).should('not.exist')
            })
        })

        cy.get("#\\/machine-types").click()

        cy.get("#deleteSelector").contains(idMt1.toString()).then(() => {
            cy.get("#deleteSelector").select(idMt1.toString())
            cy.get('#deleteMachineTypeBtn').click().then(() => {
                cy.contains("Machine Type ID: " + idMt1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idMt2.toString()).then(() => {
            cy.get("#deleteSelector").select(idMt2.toString())
            cy.get('#deleteMachineTypeBtn').click().then(() => {
                cy.contains("Machine Type ID: " + idMt2).should('not.exist')
            })
        })

        cy.get("#\\/operations").click()

        cy.get("#deleteSelector").contains(idOp1.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp1.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idOp2.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp2.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp2).should('not.exist')
            })
        })
    })
})