const LOGIN_URL = "http://10.9.10.4:3000/login"
const USERNAME = "root"
const PASSWORD = "3DA4root"

describe('Testing the Machine page - Create, create with the same ID, update, filter and delete', function () {

    var idOp1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 
    var idOp2 = Math.floor(Math.random() * (500) + 10500); //will be >= 10500 && <=10999 

    var idMt1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 
    var idMt2 = Math.floor(Math.random() * (500) + 10500); //will be >= 10500 && <=10999 

    var idMa1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 

    before(() => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Operation'
        }).as('postOperation')
        cy.route({
            method: 'POST',
            url: '/mdf/MachineType'
        }).as('postMachineType')

        cy.visit(LOGIN_URL)
        cy.get("#emailAddressLogin").type(USERNAME)
        cy.get("#passwordLogin").type(PASSWORD)
        cy.get("#loginClient").click()

        cy.wait(1000)

        cy.get("#\\/operations").click()

        //Create two operations for the rest of the test and checks that they exist
        cy.get("#operationID").type(idOp1)
        cy.get("#operationDescription").type("cypressTest1")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.get("#operationID").type(idOp2)
        cy.get("#operationDescription").type("cypressTest2")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.contains("Operation ID: " + idOp1)
        cy.contains("Operation ID: " + idOp2)

        cy.get("#\\/machine-types").click()

        //Create two machine types for the rest of the test and checks that they exist
        cy.get('#machineTypeID').type(idMt1)
        cy.get('#machineTypeDescription').type("cypressTest")
        cy.get('#' + idOp1).click()
        cy.get('#createMachineTypeBtn').click()

        cy.wait('@postMachineType')

        cy.get('#machineTypeID').type(idMt2)
        cy.get('#machineTypeDescription').type("cypressTest")
        cy.get('#' + idOp1).click()
        cy.get('#' + idOp2).click()
        cy.get('#createMachineTypeBtn').click()

        cy.wait('@postMachineType')

        cy.contains("Machine Type ID: " + idMt1)
        cy.get("#" + idMt1 + "_" + idOp1)

        cy.contains("Machine Type ID: " + idMt2)
        cy.get("#" + idMt2 + "_" + idOp1)
        cy.get("#" + idMt2 + "_" + idOp2)

        cy.get("#\\/machines").click()
    })

    it('Should successfully create Machine with Machine type with operation & The information be on the machine list', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Machine'
        }).as('postMachine')

        //Create a machine for the rest of the test
        cy.get("#machineID").type(idMa1)
        cy.get("#machineDescription").type("cypressTest")
        cy.get("#createSelector").select(idMt1.toString())
        cy.get("#createMachineBtn").click()

        cy.wait("@postMachine")

        cy.get("#list_" + idMa1)
        cy.contains("Machine Type: " + idMt1.toString())
        cy.contains("Status: Active")
    });

    it('Should unsuccessfully create Machine, since there already is a machine with the ID', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Machine'
        }).as('postMachine')


        cy.get("#machineID").type(idMa1)
        cy.get("#machineDescription").type("cypressTest")
        cy.get("#createSelector").select(idMt1.toString())
        cy.get("#createMachineBtn").click()

        cy.wait('@postMachine').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, 'There already is a machine with that ID!')
        })
    });

    it('Should successfully filter by the machine type id, and get machine from it', function () {
        cy.get("#filterSelect").select(idMt1.toString())
        cy.get("#filterMachineBtn").click()
        cy.get("#filter_" + idMa1)

    });

    it('Should successfully update Machine type from a machine', function () {
        cy.get("#updateMachineSelector").select(idMa1.toString())
        cy.get("#updateMachineTypeSelector").select(idMt2.toString())
        cy.get("#updateMachineBtn").click()

        cy.get("#list_" + idMa1)
        cy.contains("Machine Type: " + idMt2)
    });

    it('Should successfully filter by the machine type id, and get machine from it after update', function () {
        cy.get("#filterSelect").select(idMt2.toString())
        cy.get("#filterMachineBtn").click()
        cy.get("#filter_" + idMa1)
    });

    it('Should successfully deactivate the machine', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Machine/' + idMa1 + "/deactivate"
        }).as('putMachine')

        cy.get("#deleteSelector").select(idMa1.toString())
        cy.get("#deActivateMachine").click()

        cy.wait("@putMachine")

        cy.contains("Status: Inactive")
    })

    it('Should successfully activate the machine', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Machine/' + idMa1 + "/activate"
        }).as('putMachine')

        cy.get("#deleteSelector").select(idMa1.toString())
        cy.get("#deActivateMachine").click()

        cy.wait("@putMachine")

        cy.contains("Status: Active")
    })

    it('Should successfully delete the created machine', () => {
        cy.get("#deleteSelector").contains(idMa1.toString()).then(() => {
            cy.get("#deleteSelector").select(idMa1.toString())
            cy.get('#deleteMachineBtn').click().then(() => {
                cy.contains("Machine ID: " + idMa1).should('not.exist')
            })
        })
    })

    after(() => {
        cy.get("#\\/machine-types").click()

        cy.get("#deleteSelector").contains(idMt1.toString()).then(() => {
            cy.get("#deleteSelector").select(idMt1.toString())
            cy.get('#deleteMachineTypeBtn').click().then(() => {
                cy.contains("Machine Type ID: " + idMt1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idMt2.toString()).then(() => {
            cy.get("#deleteSelector").select(idMt2.toString())
            cy.get('#deleteMachineTypeBtn').click().then(() => {
                cy.contains("Machine Type ID: " + idMt2).should('not.exist')
            })
        })

        cy.get("#\\/operations").click()

        cy.get("#deleteSelector").contains(idOp1.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp1.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idOp2.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp2.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp2).should('not.exist')
            })
        })
    })
})