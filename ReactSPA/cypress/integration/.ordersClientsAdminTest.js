const LOGIN_URL = "http://10.9.10.4:3000/login"
const USERNAME = "root"
const PASSWORD = "3DA4root"

describe('Testing the MDP page - Create, create with the same ID and delete', function () {

    var idOp1 = Math.floor(Math.random() * (500)+10000); //will be >= 10000 && <=10499 
    var idOp2 = Math.floor(Math.random() * (500)+10500); //will be >= 10500 && <=10999 

    var idProd1 = Math.floor(Math.random() * (500)+10000); //will be >= 10000 && <=10499 
    var idProd2 = Math.floor(Math.random() * (500)+10000); //will be >= 10500 && <=10999 
    
    var idMP1 = Math.floor(Math.random() * (500)+10000); //will be >= 10000 && <=10499 
    var idMP2 = Math.floor(Math.random() * (500)+10000); //will be >= 10500 && <=10999 

    var orderID = null
    var clientID = null

    const name = "Cypress Test"
    const emailGen = Math.floor(Math.random() * (500) + 10000) + "@isep.ipp.pt" //will be >= 10000 && <=10499 
    const passGen = "3DA4cypressTest"
    const nif = Math.floor(Math.random() * 1000000000);
    const address = {
        street: "R. Dr. António Bernardino de Almeida, 431",
        postalCode: "4200-072",
        country: "Portugal",
        city: "Porto"
    }

    before(() => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Operation'
        }).as('postOperation')

        cy.visit(LOGIN_URL)
        cy.get("#emailAddressLogin").type(USERNAME)
        cy.get("#passwordLogin").type(PASSWORD)
        cy.get("#loginClient").click()

        cy.wait(1000)

        cy.get("#\\/operations").click()

        //Create two operations for the rest of the test and checks that they exist
        cy.get("#operationID").type(idOp1)
        cy.get("#operationDescription").type("cypressTest1")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.get("#operationID").type(idOp2)
        cy.get("#operationDescription").type("cypressTest2")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.contains("Operation ID: "+idOp1)
        cy.contains("Operation ID: "+idOp2)

        cy.get("#\\/mdp").click()

        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdp'
        }).as('postMDP')

        //Create a machine for the rest of the test
        cy.get("#productID").type(idProd1)
        cy.get("#manufacturePlanID").type(idMP1)
        cy.get("#productDescription").type("cypressTest1")
        cy.get("#manufacturePlanDescription").type("cypressTest")
        cy.get("#create_" + idOp1).type("3")
        cy.get("#create_" + idOp2).type("1")
        cy.get("#createMDPBtn").click()

        cy.wait("@postMDP")

        cy.contains("Product ID: " + idProd1)
        cy.contains("Manufacture Plan ID: " + idMP1)
        cy.get("#" + idMP1 + "_" + idOp2)
        cy.get("#" + idMP1 + "_" + idOp1)

        //Create a machine for the rest of the test
        cy.get("#productID").type(idProd2)
        cy.get("#manufacturePlanID").type(idMP2)
        cy.get("#productDescription").type("cypressTest2")
        cy.get("#manufacturePlanDescription").type("cypressTest")
        cy.get("#create_" + idOp1).type("2")
        cy.get("#createMDPBtn").click()

        cy.wait("@postMDP")

        cy.contains("Product ID: " + idProd2)
        cy.contains("Manufacture Plan ID: " + idMP2)
        cy.get("#" + idMP2 + "_" + idOp1)

        cy.get("#signOutBtn").click()

        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/',
            onResponse: (xhr) => {
                clientID = xhr.response.body._id
            }
        }).as('postRegister')

        cy.get("#name").type(name)
        cy.get('#emailAddressRegister').type(emailGen)
        cy.get('#passwordRegister').type(passGen)
        cy.get('#confirmPassword').type(passGen)
        cy.get('#nif').type(nif)
        cy.get('#street').type(address.street)
        cy.get('#postalCode').type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get('#registerClient').click()

        cy.wait('@postRegister').then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })

        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/email/' + emailGen
        }).as('postLogin')

        cy.get("#emailAddressLogin").clear().type(emailGen)
        cy.get("#passwordLogin").clear().type(passGen)
        cy.get("#loginClient").click()

        cy.wait('@postLogin').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, '"verified":true')
        })

        cy.url().should('eq', 'http://10.9.10.4:3000/profile')

        cy.server()
        cy.route({
            method: 'GET',
            url: '/mdp/product/', 
            response: {}
        }).as('getMDP')

        cy.get('#\\/orders').click()

        cy.wait("@getMDP")

        cy.server()
        cy.route({
            method: 'POST',
            url: '/order',
            onResponse: (xhr) => {
                orderID = xhr.response.body._id
            }
        }).as('postOrder')

        cy.get("#create_" + idProd1).type(3)
        cy.get("#create_" + idProd2).type(1)

        cy.get("#useUserAddress").check()

        var date = (new Date(+new Date() + 4*24*60*60*1000)).toJSON().split("T")[0]

        cy.get("#dueDatePicker").clear().type(date)

        cy.get("#placeOrderBtn").click()

        cy.wait("@postOrder").then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })

        cy.contains(date)
        cy.contains("3 cypressTest1")
        cy.contains("1 cypressTest2")

        cy.contains(address.street.split(",")[0])
        cy.contains(address.street.split(",")[1].trim())
        cy.contains(address.postalCode)
        cy.contains(address.country)
        cy.contains(address.city)

        cy.get("#signOutBtn").click()

        cy.visit(LOGIN_URL)
        cy.get("#emailAddressLogin").type(USERNAME)
        cy.get("#passwordLogin").type(PASSWORD)
        cy.get("#loginClient").click()

        cy.get("#\\/clients-orders")
    })

    it('Should successfully update an order address', () => {
        cy.get("#updateOrderSelector").select(orderID)

        cy.server()
        cy.route({
            method: 'PUT',
            url: '/order/' + orderID
        }).as('putOrder')

        cy.get("#streetOrder").type(address.street + "1")
        cy.get('#postalCodeOrder').clear().type(address.postalCode)
        cy.get('#countryOrder').select(address.country)
        cy.get('#cityOrder').select(address.city)
        cy.get("#updateOrderBtn").click()

        cy.wait("@putOrder")

        cy.contains(address.street.split(", ")[1].trim() + "1")
    })

    it('Should unsuccessfully update an order address because the street is invalid', () => {
        cy.get("#updateOrderSelector").select(orderID)

        cy.get("#streetOrder").type("Error street")
        cy.get('#postalCodeOrder').clear().type(address.postalCode)
        cy.get('#countryOrder').select(address.country)
        cy.get('#cityOrder').select(address.city)
        cy.get("#updateOrderBtn").click()

        cy.contains(address.street.split(", ")[1].trim() + "1")
    })

    it('Should unsuccessfully update an order address because the street is invalid', () => {
        cy.get("#updateOrderSelector").select(orderID)

        cy.server()
        cy.route({
            method: 'PUT',
            url: '/order'
        }).as('putOrder')

        cy.get("#streetOrder").type(address.street)
        cy.get('#postalCodeOrder').clear().type("3333 111")
        cy.get('#countryOrder').select(address.country)
        cy.get('#cityOrder').select(address.city)
        cy.get("#updateOrderBtn").click()

        cy.contains(address.postalCode)
    })

    it('Should update the client name', () => {
        cy.get("#updateClientSelector").select(clientID)

        cy.server()
        cy.route({
            method: 'PUT',
            url: '/client/' + clientID
        }).as('putClient')

        cy.get("#checkUpdateName").uncheck()
        cy.get("#checkUpdateName").check()
        cy.get("#checkUpdateAddress").check()
        cy.get("#checkUpdateAddress").uncheck()
        
        cy.get("#nameUpdate").type(name2)
        cy.get("#updateInformationBtn").click()

        cy.wait("@putClient").then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })

        cy.contains(name2)
    })

    it('Should update the client address', () => {
        cy.get("#updateClientSelector").select(clientID)

        cy.server()
        cy.route({
            method: 'PUT',
            url: '/client/' + clientID
        }).as('putClient')

        cy.get("#checkUpdateName").check()
        cy.get("#checkUpdateName").uncheck()
        cy.get("#checkUpdateAddress").uncheck()
        cy.get("#checkUpdateAddress").check()

        cy.get("#street").type(address.street + "1")
        cy.get('#postalCode').clear().type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get("#updateInformationBtn").click()

        cy.wait("@putClient").then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })

        cy.contains(address.street.split(", ")[1].trim() + "1")
    })

    it('Should update both the client address and name', () => {
        cy.get("#updateClientSelector").select(clientID)

        cy.server()
        cy.route({
            method: 'PUT',
            url: '/client/' + clientID
        }).as('putClient')

        cy.get("#checkUpdateName").uncheck()
        cy.get("#checkUpdateName").check()
        cy.get("#checkUpdateAddress").uncheck()
        cy.get("#checkUpdateAddress").check()

        cy.get("#nameUpdate").type(name)
        cy.get("#checkUpdateAddress").click()
        cy.get("#street").type(address.street)
        cy.get('#postalCode').clear().type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get("#updateInformationBtn").click()

        cy.wait("@putClient").then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })

        cy.contains(name)
        cy.contains(emailGen)
        cy.contains(address.street.split(",")[0])
        cy.contains(address.street.split(",")[1].trim())
        cy.contains(address.postalCode)
        cy.contains(address.country)
        cy.contains(address.city)
    })

    it('Should successfully cancel an order', () => {cy.server()
        cy.route({
            method: 'PUT',
            url: '/order/cancel/' + orderID
        }).as('cancelOrder')

        cy.get("#cancelOrder").select(orderID)
        cy.get("#cancelOrderBtn").click()

        cy.wait("@cancelOrder").then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })

        cy.contains("The order is no longer active")
    })

    after(() => {
        cy.request('DELETE', 'http://10.9.11.4:3030/order/' + orderID)
        cy.request('DELETE', 'http://10.9.11.4:3030/client/email/' + emailGen)

        cy.get("#\\/mdp")

        cy.get("#deleteSelectorProduct").contains(idProd1.toString()).then(() => {
            cy.get("#deleteSelectorProduct").select(idProd1.toString())
            cy.get('#deleteProductBtn').click()
        })

        cy.get("#deleteSelectorProduct").contains(idProd2.toString()).then(() => {
            cy.get("#deleteSelectorProduct").select(idProd2.toString())
            cy.get('#deleteProductBtn').click()
        })

        cy.get("#\\/operations").click()

        cy.get("#deleteSelector").contains(idOp1.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp1.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idOp2.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp2.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp2).should('not.exist')
            })
        })
    })
})
