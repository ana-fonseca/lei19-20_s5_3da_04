const LOGIN_URL = "http://10.9.10.4:3000/login"

describe('Testing the Login and Profile page - Fail login, register, register with the same email, same nif and delete', function () {

    const name = "Cypress Test"
    const name2 = "Cypress Test 2"
    const emailGen = Math.floor(Math.random() * (500) + 10000) + "@isep.ipp.pt" //will be >= 10000 && <=10499 
    const emailGen2 = Math.floor(Math.random() * (500) + 10000) + "@isep.ipp.pt"
    const passGen = "3DA4cypressTest"
    const nif = Math.floor(Math.random() * 1000000000);
    const nif2 = Math.floor(Math.random() * 1000000000);
    const address = {
        street: "R. Dr. António Bernardino de Almeida, 431",
        postalCode: "4200-072",
        country: "Portugal",
        city: "Porto"
    }
    var clientID = null

    //Logs the user in and enters the "Operations" page
    before(() => {
        cy.visit(LOGIN_URL)
    })

    it('Should unsuccessfully try to login because there is no client with that email', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/email/' + emailGen
        }).as('postLogin')

        cy.get("#emailAddressLogin").type(emailGen)
        cy.get("#passwordLogin").type(passGen)
        cy.get("#loginClient").click()

        cy.wait('@postLogin').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, '"verified":false')
        })
    })

    it('Should successfully register the client', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/',
            onResponse: (xhr) => {
                clientID = xhr.response.body._id
            }
        }).as('postRegister')

        cy.get("#name").type(name)
        cy.get('#emailAddressRegister').type(emailGen)
        cy.get('#passwordRegister').type(passGen)
        cy.get('#confirmPassword').type(passGen)
        cy.get('#nif').type(nif)
        cy.get('#street').type(address.street)
        cy.get('#postalCode').type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get('#registerClient').click()

        cy.wait('@postRegister').then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })
    })

    it('Should unsuccessfully register the client because the email is the same', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/'
        }).as('postRegister')

        cy.get("#name").type(name)
        cy.get('#emailAddressRegister').type(emailGen)
        cy.get('#passwordRegister').type(passGen)
        cy.get('#confirmPassword').type(passGen)
        cy.get('#nif').type(nif2)
        cy.get('#street').type(address.street)
        cy.get('#postalCode').type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get('#registerClient').click()

        cy.wait('@postRegister').then((xhr) => {
            assert.isNotNull(xhr.response.status, 500)
        })
    })

    it('Should unsuccessfully register the client because the address street is the invalid', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/'
        }).as('postRegister')

        cy.get("#name").clear().type(name)
        cy.get('#emailAddressRegister').clear().type(emailGen2)
        cy.get('#passwordRegister').clear().type(passGen)
        cy.get('#confirmPassword').clear().type(passGen)
        cy.get('#nif').clear().type(nif2)
        cy.get('#street').clear().type("Address Street Error")
        cy.get('#postalCode').clear().type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get('#registerClient').click()
    })

    it('Should unsuccessfully register the client because the address postal code is the invalid', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/'
        }).as('postRegister')

        cy.get("#name").clear().type(name)
        cy.get('#emailAddressRegister').clear().type(emailGen2)
        cy.get('#passwordRegister').clear().type(passGen)
        cy.get('#confirmPassword').clear().type(passGen)
        cy.get('#nif').clear().type(nif2)
        cy.get('#street').clear().type(address.street)
        cy.get('#postalCode').clear().type("1234 123")
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get('#registerClient').click()
    })

    it('Should unsuccessfully register the client because the name is the invalid', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/'
        }).as('postRegister')

        cy.get("#name").clear().type("CypressError")
        cy.get('#emailAddressRegister').clear().type(emailGen2)
        cy.get('#passwordRegister').clear().type(passGen)
        cy.get('#confirmPassword').clear().type(passGen)
        cy.get('#nif').clear().type(nif2)
        cy.get('#street').clear().type(address.street)
        cy.get('#postalCode').clear().type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get('#registerClient').click()
    })

    it('Should unsuccessfully register the client because the nif is the same', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/'
        }).as('postRegister')

        cy.get("#name").clear().type(name)
        cy.get('#emailAddressRegister').clear().type(emailGen2)
        cy.get('#passwordRegister').clear().type(passGen)
        cy.get('#confirmPassword').clear().type(passGen)
        cy.get('#nif').clear().type(nif)
        cy.get('#street').clear().type(address.street)
        cy.get('#postalCode').clear().type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get('#registerClient').click()

        cy.wait('@postRegister').then((xhr) => {
            assert.isNotNull(xhr.response.status, 500)
        })
    })

    it('Should successfully try to login', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/email/' + emailGen
        }).as('postLogin')

        cy.get("#emailAddressLogin").clear().type(emailGen)
        cy.get("#passwordLogin").clear().type(passGen)
        cy.get("#loginClient").click()

        cy.wait('@postLogin').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, '"verified":true')
        })

        cy.url().should('eq', 'http://10.9.10.4:3000/profile')
    })

    it('Should check that the registered information is correct', () => {
        cy.contains(name)
        cy.contains(emailGen)
        cy.contains(address.street.split(",")[0])
        cy.contains(address.street.split(",")[1].trim())
        cy.contains(address.postalCode)
        cy.contains(address.country)
        cy.contains(address.city)
    })

    it('Should update the client name', () => {
        cy.get("#checkUpdateName").uncheck()
        cy.get("#checkUpdateName").check()
        cy.get("#checkUpdateAddress").check()
        cy.get("#checkUpdateAddress").uncheck()
        
        cy.get("#nameUpdate").type(name2)
        cy.get("#updateInformationBtn").click()

        cy.wait(500)

        cy.contains(name2)
    })

    it('Should update the client address', () => {
        cy.get("#checkUpdateName").check()
        cy.get("#checkUpdateName").uncheck()
        cy.get("#checkUpdateAddress").uncheck()
        cy.get("#checkUpdateAddress").check()

        cy.get("#street").type(address.street + "1")
        cy.get('#postalCode').clear().type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get("#updateInformationBtn").click()

        cy.wait(500)

        cy.contains(address.street.split(", ")[1].trim() + "1")
    })

    it('Should update both the client address and name', () => {
        cy.get("#checkUpdateName").uncheck()
        cy.get("#checkUpdateName").check()

        cy.get("#nameUpdate").type(name)
        cy.get("#checkUpdateAddress").click()
        cy.get("#street").type(address.street)
        cy.get('#postalCode').clear().type(address.postalCode)
        cy.get('#country').select(address.country)
        cy.get('#city').select(address.city)
        cy.get("#updateInformationBtn").click()

        cy.contains(name)
        cy.contains(emailGen)
        cy.contains(address.street.split(",")[0])
        cy.contains(address.street.split(",")[1].trim())
        cy.contains(address.postalCode)
        cy.contains(address.country)
        cy.contains(address.city)
    })

    it('Should delete the account and not be able to log in again', () => {
        cy.server()
        cy.route({
            method: "DELETE",
            url: "/client/" + clientID
        }).as('deleteClient')

        cy.get("#deleteAccount").click()

        cy.wait("@deleteClient").then((xhr) => {
            assert.isNotNull(xhr.response.status, 200)
        })

        cy.url().should('eq', 'http://10.9.10.4:3000/login')

        cy.server()
        cy.route({
            method: 'POST',
            url: '/client/email/' + emailGen
        }).as('postLogin')

        cy.get("#emailAddressLogin").type(emailGen)
        cy.get("#passwordLogin").type(passGen)
        cy.get("#loginClient").click()

        cy.wait('@postLogin').then((xhr) => {
            assert.isNotNull(xhr.response.status, 404)
        })
    })
})