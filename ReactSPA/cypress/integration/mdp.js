const LOGIN_URL = "http://10.9.10.4:3000/login"
const USERNAME = "root"
const PASSWORD = "3DA4root"

describe('Testing the MDP page - Create, create with the same ID and delete', function () {

    var idOp1 = Math.floor(Math.random() * (500)+10000); //will be >= 10000 && <=10499 
    var idOp2 = Math.floor(Math.random() * (500)+10500); //will be >= 10500 && <=10999 

    var idProd1 = Math.floor(Math.random() * (500)+10000); //will be >= 10000 && <=10499 
    var idProd2 = Math.floor(Math.random() * (500)+10000); //will be >= 10500 && <=10999 
    var idProd3 = Math.floor(Math.random() * (500)+10000); //will be >= 10500 && <=10999 
    
    var idMP1 = Math.floor(Math.random() * (500)+10000); //will be >= 10000 && <=10499 
    var idMP2 = Math.floor(Math.random() * (500)+10000); //will be >= 10500 && <=10999 
    var idMP3 = Math.floor(Math.random() * (500)+10000); //will be >= 10500 && <=10999 

    before(() => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Operation'
        }).as('postOperation')

        cy.visit(LOGIN_URL)
        cy.get("#emailAddressLogin").type(USERNAME)
        cy.get("#passwordLogin").type(PASSWORD)
        cy.get("#loginClient").click()

        cy.wait(1000)

        cy.get("#\\/operations").click()

        //Create two operations for the rest of the test and checks that they exist
        cy.get("#operationID").type(idOp1)
        cy.get("#operationDescription").type("cypressTest1")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.get("#operationID").type(idOp2)
        cy.get("#operationDescription").type("cypressTest2")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait('@postOperation')

        cy.contains("Operation ID: "+idOp1)
        cy.contains("Operation ID: "+idOp2)

        cy.get("#\\/mdp").click()
    })

    it('Should successfully create Product and Manufacture Plan with operation & The information be on the mdp list', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdp'
        }).as('postMDP')

        //Create a machine for the rest of the test
        cy.get("#productID").type(idProd1)
        cy.get("#manufacturePlanID").type(idMP1)
        cy.get("#productDescription").type("cypressTest")
        cy.get("#manufacturePlanDescription").type("cypressTest")
        cy.get("#create_" + idOp1).type("3")
        cy.get("#create_" + idOp2).type("1")
        cy.get("#createMDPBtn").click()

        cy.wait("@postMDP")

        cy.contains("Product ID: " + idProd1)
        cy.contains("Manufacture Plan ID: " + idMP1)
        cy.get("#" + idMP1 + "_" + idOp2)
        cy.get("#" + idMP1 + "_" + idOp1)        
    });

    it('Should successfully create Product and Manufacture Plan with operation & The information be on the mdp list', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdp'
        }).as('postMDP')

        //Create a machine for the rest of the test
        cy.get("#productID").type(idProd2)
        cy.get("#manufacturePlanID").type(idMP2)
        cy.get("#productDescription").type("cypressTest")
        cy.get("#manufacturePlanDescription").type("cypressTest")
        cy.get("#create_" + idOp1).type("2")
        cy.get("#createMDPBtn").click()

        cy.wait("@postMDP")

        cy.contains("Product ID: " + idProd2)
        cy.contains("Manufacture Plan ID: " + idMP2)
        cy.get("#" + idMP2 + "_" + idOp1)      
    });

    it('Should unsuccessfully create MDP, since there already is a product with the ID', function() {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdp'
        }).as('postMDP')

        //Create a machine for the rest of the test
        cy.get("#productID").type(idProd1)
        cy.get("#manufacturePlanID").type(idMP3)
        cy.get("#productDescription").type("cypressTest")
        cy.get("#manufacturePlanDescription").type("cypressTest")
        cy.get("#create_" + idOp1).type("3")
        cy.get("#create_" + idOp2).type("1")
        cy.get("#createMDPBtn").click()

        cy.wait('@postMDP').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, 'There already is a product with that ID!')
        })
    });

    it('Should unsuccessfully create MDP, since there already is a manufacture plan with the ID', function() {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdp'
        }).as('postMDP')

        //Create a machine for the rest of the test
        
        cy.get("#productID").clear().type(idProd3)
        cy.get("#manufacturePlanID").clear().type(idMP1)
        cy.get("#createMDPBtn").click()

        cy.wait('@postMDP').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, 'There already is a manufacture plan with that ID!')
        })
    });

    it('Should unsuccessfully create MDP, since none of the submitted operations are valid', function() {
        cy.get("#create_" + idOp1).clear()
        cy.get("#create_" + idOp2).clear()

        cy.get("#productID").clear().type(idProd3)
        cy.get("#manufacturePlanID").clear().type(idMP3)
        cy.get("#createMDPBtn").click()
    });

    it('Should successfully delete the created product', () => {
        cy.get("#deleteSelectorProduct").contains(idProd1.toString()).then(() => {
            cy.get("#deleteSelectorProduct").select(idProd1.toString())
            cy.get('#deleteProductBtn').click().then(() => {
                cy.contains("Product ID: " + idProd1).should('not.exist')
            })
        })
    })

    it("Should successfully delete the created manufacture plan", () => {
        cy.get("#deleteSelectorManufacturePlan").contains(idMP2.toString()).then(() => {
            cy.get("#deleteSelectorManufacturePlan").select(idMP2.toString())
            cy.get('#deleteManufacturePlanBtn').click().then(() => {
                cy.contains("Manufacture Plan ID: " + idMP2).should('not.exist')
            })
        })
    })

    after(() => {
        cy.get("#\\/operations").click()

        cy.get("#deleteSelector").contains(idOp1.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp1.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp1).should('not.exist')
            })
        })

        cy.get("#deleteSelector").contains(idOp2.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp2.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp2).should('not.exist')
            })
        })
    })
})