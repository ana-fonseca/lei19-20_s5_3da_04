const LOGIN_URL = "http://10.9.10.4:3000/login"
const USERNAME = "root"
const PASSWORD = "3DA4root"

describe('Testing the Operation page - Create, create with the same ID and delete', function () {

    var idOp1 = Math.floor(Math.random() * (500) + 10000); //will be >= 10000 && <=10499 

    //Logs the user in and enters the "Operations" page
    before(() => {
        cy.visit(LOGIN_URL)
        cy.get("#emailAddressLogin").type(USERNAME)
        cy.get("#passwordLogin").type(PASSWORD)
        cy.get("#loginClient").click()

        cy.wait(1000)

        cy.get("#\\/operations").click()
    })

    it('This test should check that the previously created operation is being displayed', function () {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Operation'
        }).as('postOperation')

        //Create an operation for the rest of the test
        cy.get("#operationID").type(idOp1)
        cy.get("#operationDescription").type("cypressTest1")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        cy.wait("@postOperation")
        
        cy.contains("Operation ID: " + idOp1)        
    })

    it('This test should try to create an operation with an used ID and fail', () => {
        cy.server()
        cy.route({
            method: 'POST',
            url: '/mdf/Operation'
        }).as('postOperation')

        cy.get("#operationID").type(idOp1)
        cy.get("#operationDescription").type("cypressTest1")
        cy.get("#operationTime").type("3")
        cy.get("#createOperationBtn").click()

        //Verifies the received response
        cy.wait('@postOperation').then((xhr) => {
            assert.isNotNull(xhr.response.body.data, 'There already is an operation with that ID!')
        })
    })
    
    //Deletes the previously created operation
    after(() => {
        cy.get("#deleteSelector").contains(idOp1.toString()).then(() => {
            cy.get("#deleteSelector").select(idOp1.toString())
            cy.get('#deleteOperationBtn').click().then(() => {
                cy.contains("Operation ID: " + idOp1).should('not.exist')
            })
        })
    })
})