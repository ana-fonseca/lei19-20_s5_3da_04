import logo from './logo.svg';
import './App.css';

import React, { Component } from "react";
import * as THREE from 'three'
import * as dat from 'dat.gui';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { DragControls } from 'three/examples/jsm/controls/DragControls.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';

import axios from 'axios'
import http from 'http'
const instance = axios.create({
  httpAgent: new http.Agent({
    rejectUnauthorized: false
  })
});


var camera;
const height = 8;
var boolRotating = false;
var scene

const MDF_URL = 'http://10.9.11.4:5000/mdf/'

class App extends Component {

  componentDidMount() {
    const thickness = 0.4;
    const minComp = 30, maxComp = 70, minProf = 30, maxProf = 70, minSpeedTool = 0.01, maxSpeedTool = 0.05, minSpeedMov = 0.01, maxSpeedMov = 0.015;
    const multiplierSpeed = 100.0;
    const numTubes = 41;
    const distSideBars = 10.5;
    const nameProd = 'I\'m product no.', nameMachine = 'I\'m machine no.', nameTool = 'I\'m tool no.', nameProdLine = 'I\'m production line no.', nameProdLine2 = '2.I\'m production line no.';
    const numMaxProdLines = 10;
    const objects = [];

    const sideBarOptions = {
      length: 20,
      width: 4.5,
      width2: 5.5,
      thickness: 1.5
    };

    const waitSpaceOptions = {
      width: 2,
      length: 2,
      height: 0.52
    };


    const tubeOptions = {
      cylRad: 0.20,
      cylHeight: 4.40,
      cylSegments: 20
    };

    const microTubeOptions = {
      cylRad: 0.10,
      cylHeight: 0.6,
      cylSegments: 20,
      distance: 2.5
    };

    const tubePositionOptions = {
      x: 10,
      y: 5.5,
      z: 0.5
    };

    const machineOptions = {
      widthBase: 2,
      lengthBase: 3,
      heightBase: 2,
      widthBraco1: 2,
      lengthBraco1: 1,
      heightBraco1: 2,
      widthBraco2: 0.5,
      lengthBraco2: 3.5,
      heightBraco2: 1,
    };

    const ferramentaOptions = {
      cylRad: 0.05,
      cylHeight: 1.2,
      cylSegments: 20,
    };

    var distMachines = waitSpaceOptions.width * 2 + 0.5;
    var orbitControls
    var ambientLight;

    var posCreateLineX = 0, posCreateLineZ = 0;
    var initialPosConeX, initialPosConeY, initialPosConeZ;
    var productionLine = [], numLines = 0;
    var numMachines = [], numMachinesMax = (sideBarOptions.length - machineOptions.widthBase - distMachines) / (machineOptions.widthBase + (distMachines - machineOptions.widthBase));
    var listMachines = [[]], listTools = [[]], listOperation = [[]];
    var listMoveTool = [[]], getProdToWaitSpace = [[]];
    var listUpDown = [[]], cones = [[]];
    var waitSpaceList = [[]], lineCreated = [];
    var speedProd = 0.01, speedTools = 0.03;
    var numProd = [], listShadowProd = [], distToolMachine;
    var folderLine;
    //var for tooltip
    var mouse = { x: 0, y: 0 }, INTERSECTED;
    var mouseProj = { x: 0, y: 0 }
    var tooltipCanv, difMachineAndWaitSpace;

    //change products
    var prodToChange = [[]];
    var doLoop = [], doTime = [], createNewProd = [], direction = [];
    var doGetProdToWaitSpace = [];
    //general info
    var posInitMachineX = -distMachines, posInitMachineZ = waitSpaceOptions.length, posZWaitSpace, disProdMachineX, disProdMachineZ;
    var prodWorking = true;
    var gui = new dat.GUI();
    var tubes = [[]];
    var microTubes1 = [[]];
    var microTubes2 = [[]];

    //product info
    var sizeHightProduct = 0.8, sizeProduct = 1;

    var floorOptions = {
      length: 50,
      depth: 50
    };

    var cameraOptions = {
      pan: 0,
      orbit: 0,
      zoom: 0
    };

    var floorSliderOptions = {
      length: floorOptions.length,
      depth: floorOptions.depth
    }

    var speedSliderOptions = {
      speedProdAux: 1,
      speedToolsAux: 1
    }

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 0.1, 200);



    var renderer = new THREE.WebGLRenderer({
      antialias: true
    });
    renderer.compile(scene, camera);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
    //renderer.setClearColor(0x7791a6, 1); //background
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;

    document.body.appendChild(renderer.domElement);

    window.addEventListener("resize", () => {
      renderer.setSize(window.innerWidth, window.innerHeight);
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
    });

    //CONTROLS
    orbitControls = new OrbitControls(camera, renderer.domElement);
    orbitControls.enable = true

    orbitControls.target.set(0, height, 0);
    orbitControls.minDistance = 1;
    orbitControls.maxDistance = 25;

    //default materials
    var materialGround = new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load("./resources/img/floor.jpg")
    });
    var materialMetal = new THREE.MeshPhongMaterial({
      map: new THREE.TextureLoader().load("./resources/img/metal.jpg")
    });
    var msMaterials = new THREE.MeshPhongMaterial({ color: 0x666666, wireframe: false });
    var waitSpaceMaterials = new THREE.MeshPhongMaterial({ color: 0xfcf403, wireframe: false });
    var tubeMaterials = new THREE.MeshPhongMaterial({ color: 0x000000, wireframe: false });
    var microTubeMaterials = new THREE.MeshPhongMaterial({ color: 0xffffff, wireframe: false });

    //background
    let bgMesh;
    {
      const loader = new THREE.TextureLoader();
      const materialBackGround = loader.load(
        './resources/img/background.jpg',
      );
      materialBackGround.magFilter = THREE.LinearFilter;
      materialBackGround.minFilter = THREE.LinearFilter;

      const shader = THREE.ShaderLib.equirect;
      const materialAux = new THREE.ShaderMaterial({
        fragmentShader: shader.fragmentShader,
        vertexShader: shader.vertexShader,
        uniforms: shader.uniforms,
        depthWrite: false,
        side: THREE.BackSide,
      });
      materialAux.uniforms.tEquirect.value = materialBackGround;
      const plane = new THREE.BoxBufferGeometry(200, 200, 200);
      bgMesh = new THREE.Mesh(plane, materialAux);
      scene.add(bgMesh);
    }

    // create an AudioListener and add it to the camera
    var listener = new THREE.AudioListener();
    camera.add(listener);

    // create a global audio source
    var sound = new THREE.Audio(listener);

    // load a sound and set it as the Audio object's buffer
    var audioLoader = new THREE.AudioLoader();
    audioLoader.load('./resources/sound/soundBackground.mp3', function (buffer) {
      sound.setBuffer(buffer);
      sound.setLoop(true);
      sound.setVolume(0.1);
      sound.play();
    });

    var listenerMachine = new THREE.AudioListener();
    camera.add(listenerMachine);

    // create a global audio source
    var soundMachine = new THREE.Audio(listenerMachine);

    // load a sound and set it as the Audio object's buffer
    var audioMachineLoader = new THREE.AudioLoader();
    audioMachineLoader.load('./resources/sound/soundMachine.mp3', function (buffer) {
      soundMachine.setBuffer(buffer);
      soundMachine.setLoop(true);
      soundMachine.setVolume(0.5);
    });

    //define groups
    var ground = new THREE.Group();
    var factoryLine = new THREE.Group();
    var machine = new THREE.Group();

    //ground
    var geometryGround = new THREE.BoxGeometry(
      floorOptions.length,
      thickness,
      floorOptions.depth
    );
    var furthestX = floorOptions.length / 2, furthestZ = floorOptions.depth / 2;

    var groundEl = new THREE.Mesh(geometryGround, materialGround);


    //this function takes the current dimensions and creates the objects and sets them up in the correct placement
    setup();

    //production line
    //	machineSupporter
    //		side bar 1
    var machineSupporter = new THREE.Mesh(createGeometrySideLine1(), msMaterials);
    machineSupporter.position.x = 0;
    machineSupporter.position.y = machineSupporter.position.y + thickness / 2 + sideBarOptions.thickness / 2;
    machineSupporter.position.z = (sideBarOptions.width2);
    //		side bar 2
    var machineSupporter2 = new THREE.Mesh(createGeometrySideLine2(), msMaterials);
    machineSupporter2.position.z = machineSupporter.position.z - distSideBars;
    machineSupporter2.position.y = machineSupporter.position.y;
    machineSupporter2.position.x = machineSupporter.position.x;

    //		tubes
    tubes[0] = [numTubes];
    microTubes1[0] = [numTubes]
    microTubes2[0] = [numTubes]
    var tube = new THREE.Mesh(createTube(), tubeMaterials);
    tube.position.x = machineSupporter.position.x + tubePositionOptions.x;
    tube.position.z = machineSupporter.position.z - tubePositionOptions.y;
    tube.position.y = machineSupporter.position.y + tubePositionOptions.z * 1.25;
    tube.rotation.x = THREE.Math.degToRad(90);
    tube.castShadow = true;

    var shadowGeometry = new THREE.BoxGeometry(1, 1, 1);
    var shadowMaterial = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    var shadowProd = new THREE.Mesh(shadowGeometry, shadowMaterial);
    shadowProd.material.opacity = 0;
    shadowProd.material.transparent = true;
    shadowProd.position.x = tube.position.x + 0.5;
    shadowProd.position.y = tube.position.y - 5;
    shadowProd.position.z = tube.position.z;

    listShadowProd[0] = shadowProd;

    var microTube = new THREE.Mesh(createMicroTube(), microTubeMaterials);
    microTube.position.x = tube.position.x;
    microTube.position.y = tube.position.y;
    microTube.position.z = tube.position.z - microTubeOptions.distance;
    microTube.rotation.x = THREE.Math.degToRad(90);
    microTube.castShadow = true;
    microTube.receiveShadow = true;

    var microTube2 = microTube.clone();
    microTube.position.z = tube.position.z + microTubeOptions.distance;

    //create tubes
    for (var i = 0; i <= (numTubes - 1); i++) {
      var obj1 = tube.clone();
      var obj2 = microTube.clone();
      var obj3 = microTube2.clone();
      obj1.position.x = tube.position.x - (0.5 * i);
      tubes[0][i] = obj1;
      obj2.position.x = microTube.position.x - (0.5 * i);
      microTubes1[0][i] = obj2
      obj3.position.x = microTube2.position.x - (0.5 * i);
      microTubes2[0][i] = obj3

      scene.add(obj1);
      scene.add(obj2);
      scene.add(obj3);
    }


    var posToolYInit;
    var posToolZInit;
    //ferramentaOptions

    //add ground
    groundEl.receiveShadow = true;
    ground.add(groundEl);

    //add factoryLine
    machineSupporter.receiveShadow = true;
    machineSupporter.castShadow = true;
    machineSupporter2.receiveShadow = true;
    machineSupporter2.castShadow = true;
    factoryLine.add(machineSupporter);
    factoryLine.add(machineSupporter2);
    machineSupporter.name = nameProdLine + numLines
    machineSupporter2.name = nameProdLine2 + numLines


    objects.push(machineSupporter)

    productionLine[0] = factoryLine;


    numMachines[0] = 1;
    //add machine
    loadMachine();

    defaultCameraSettings(); //changing the location of this line is dangerous -> controls depends on it
    turnOnAmbientLight();
    turnOnDirectionalLight();
    turnOnShadows();

    criarFolders(); //folders de interaçao com o utilizador

    //add groups to scene
    scene.add(ground);
    scene.add(factoryLine);
    scene.add(shadowProd);

    const dragControls = new DragControls(objects, camera, renderer.domElement);
    var lastXDragObject, lastZDragObject;
    dragControls.addEventListener('dragstart', function (event) {
      orbitControls.enableRotate = false;
      lastXDragObject = event.object.position.x
      lastZDragObject = event.object.position.z
    });
    dragControls.addEventListener('drag', function (event) {
      var objectDraging = event.object
      var nameObject = objectDraging.name
      if (nameObject) {
        if (nameObject.includes(nameMachine)) {
          event.object.position.y = 0; // This will prevent moving y axis, but will be on 0 line. change this to your object position of y axis.
        } else if (nameObject.includes(nameProdLine)) {
          event.object.position.y = 0.95; // This will prevent moving y axis, but will be on 0 line. change this to your object position of y axis.
        }
      }
    })

    dragControls.addEventListener('dragend', function (event) {
      orbitControls.enableRotate = true;

      var locationX = event.object.position.x;
      var locationZ = event.object.position.z;

      var objectDraging = event.object
      var nameObject = objectDraging.name
      if (nameObject) {

        if (nameObject.includes(nameMachine)) {
          var arrayOfName = nameObject.split(".");
          var numLine = parseInt(arrayOfName[1].split(" ")[0]);
          var numMachine = parseInt(arrayOfName[2].split(" ")[0]);

          var machineAux = listMachines[numLine][numMachine]
          if (posMachineValid(numLine, numMachine)) {
            var objectWaitSpace = waitSpaceList[numLine][numMachine]
            objectWaitSpace.position.x = machineAux.position.x + machineAux.children[0].position.x

            var objectMachineTool = listTools[numLine][numMachine]
            objectMachineTool.position.x = machineAux.position.x + machineAux.children[0].position.x
            objectMachineTool.position.z = machineAux.position.z + machineAux.children[0].position.z - 4


          } else {
            event.object.position.x = lastXDragObject
            event.object.position.z = lastZDragObject
          }
        } else if (nameObject.includes(nameProdLine)) {
          var arrayOfName = nameObject.split(".");
          var numLine = parseInt(arrayOfName[1].split(" ")[0]);
          if (lineSpaceFree(locationX, locationZ, numLine)) {
            var newProdLine = productionLine[numLine]


            var childrenProdLine = newProdLine.children
            childrenProdLine.forEach(function (child) {
              if (child.name && child.name.includes(nameProdLine2)) {
                child.position.x = event.object.position.x
                child.position.z = event.object.position.z - 10.5
              }
            });

            tubes[numLine][0].position.x = event.object.position.x + 10
            tubes[numLine][0].position.z = event.object.position.y + 4
            tubes[numLine][0].position.z = event.object.position.z - 5.5
            microTubes1[numLine][0].position.x = event.object.position.x + 10
            microTubes1[numLine][0].position.z = event.object.position.z - 3
            microTubes2[numLine][0].position.x = event.object.position.x + 10
            microTubes2[numLine][0].position.z = event.object.position.z - 8
            //tubes and micro tubes
            for (var i = 0; i <= (numTubes - 1); i++) {
              var obj1 = tubes[numLine][i]
              obj1.position.x = tubes[numLine][0].position.x - (0.5 * i);
              obj1.position.z = tubes[numLine][0].position.z;

              microTubes1[numLine][i].position.x = microTubes1[numLine][0].position.x - (0.5 * i);
              microTubes1[numLine][i].position.z = microTubes1[numLine][0].position.z;
              microTubes2[numLine][i].position.x = microTubes2[numLine][0].position.x - (0.5 * i);
              microTubes2[numLine][i].position.z = microTubes2[numLine][0].position.z;
            }

            //machine & wait space && tools 
            for (var j = 0; j < numMachines[numLine]; j++) {
              var posRelMachSupportX = lastXDragObject - listMachines[numLine][j].position.x
              var posRelMachSupportZ = lastZDragObject - listMachines[numLine][j].position.z
              var posRelWaitSupportX = lastXDragObject - waitSpaceList[numLine][j].position.x
              var posRelWaitSupportZ = lastZDragObject - waitSpaceList[numLine][j].position.z
              var posRelToolSupportX = lastXDragObject - listTools[numLine][j].position.x
              var posRelToolSupportZ = lastZDragObject - listTools[numLine][j].position.z

              listMachines[numLine][j].position.x = event.object.position.x - posRelMachSupportX
              listMachines[numLine][j].position.z = event.object.position.z - posRelMachSupportZ
              listTools[numLine][j].position.x = event.object.position.x - posRelToolSupportX
              listTools[numLine][j].position.z = event.object.position.z - posRelToolSupportZ
              waitSpaceList[numLine][j].position.x = event.object.position.x - posRelWaitSupportX
              waitSpaceList[numLine][j].position.z = event.object.position.z - posRelWaitSupportZ
            }

            //products
            for (var c = 0; c < numProd[numLine]; c++) {
              var postRelativeProdSupportZ = lastZDragObject - cones[numLine][c].position.z
              var postRelativeProdSupportX = lastXDragObject - cones[numLine][c].position.x

              cones[numLine][c].position.x = event.object.position.x - postRelativeProdSupportX
              cones[numLine][c].position.z = event.object.position.z - postRelativeProdSupportZ

            }

            changeFloorSizeByNewProdLine(event.object.position.x, event.object.position.z)

          } else {
            event.object.position.x = lastXDragObject
            event.object.position.z = lastZDragObject
          }
        }
      }
    });

    //run
    loop();

    //------------ FUNCTIONS ------------
    function setup() {
      numMachinesMax = (sideBarOptions.length - machineOptions.widthBase - distMachines) / (machineOptions.widthBase + (distMachines - machineOptions.widthBase));

      groundEl.material.transparent = true;


      for (var contNumLines = 0; contNumLines < numMaxProdLines; contNumLines++) {
        waitSpaceList[contNumLines] = [];
        listTools[contNumLines] = [];
        prodToChange[contNumLines] = [];
        listUpDown[contNumLines] = [];
        listMoveTool[contNumLines] = [];
        getProdToWaitSpace[contNumLines] = [];
        listMachines[contNumLines] = [];
        tubes[contNumLines] = [];
        microTubes1[contNumLines] = [];
        microTubes2[contNumLines] = [];
        cones[contNumLines] = [];
        listOperation[contNumLines] = [];
        doLoop[contNumLines] = true;
        doTime[contNumLines] = false;
        createNewProd[contNumLines] = false;
        doGetProdToWaitSpace[contNumLines] = false;
        direction[contNumLines] = true;
        numProd[contNumLines] = 0;

        for (var c = 0; c < numMachinesMax; c++) {
          listUpDown[contNumLines][c] = 1;
          listMoveTool[contNumLines][c] = -1;
          getProdToWaitSpace[contNumLines][c] = 0;
          listOperation[contNumLines][c] = -1;
        }

        if (contNumLines !== 0)
          numMachines[contNumLines] = 0;

        for (var c = 0; c < prodToChange[contNumLines].length; c++) {
          prodToChange[contNumLines][c] = -1;
        }
      }

      // when the mouse moves, call the given function
      document.addEventListener('mousemove', onDocumentMouseMove, false);

      /////// draw text on tooltip /////////

      // create a tooltip element
      tooltipCanv = document.getElementById('tooltip');
    }

    function onDocumentMouseMove(event) {
      // the following line would stop any other event handler from firing
      // (such as the mouse's TrackballControls)
      // event.preventDefault();

      mouseProj.x = event.clientX;
      mouseProj.y = event.clientY;

      // update the mouse variable
      mouse.x = ((event.clientX - renderer.domElement.offsetLeft + 0.5) / window.innerWidth) * 2 - 1;
      mouse.y = -((event.clientY - renderer.domElement.offsetTop + 0.5) / window.innerHeight) * 2 + 1;

    }

    function posMachineValid(numLine, numMachine) {
      //if wait space is moving cant move the machine
      if (getProdToWaitSpace[numLine][numMachine] !== 0) {
        return false;
      }

      var numMachineAux
      var thisMachine = listMachines[numLine][numMachine]
      var posXThisMachine = thisMachine.position.x + thisMachine.children[0].position.x
      var posZThisMachine = thisMachine.position.z + thisMachine.children[0].position.z
      //machine is inside machine support
      var posMachineSuppXMin = tubes[numLine][0].position.x - 1
      var posMachineSuppXMax = tubes[numLine][tubes[numLine].length - 1].position.x + 1
      var posMachineSuppZMin = tubes[numLine][0].position.z + 3.2 + 1
      var posMachineSuppZMax = tubes[numLine][0].position.z + 9 - 1
      if (posXThisMachine > posMachineSuppXMin || posXThisMachine < posMachineSuppXMax || posZThisMachine < posMachineSuppZMin || posZThisMachine > posMachineSuppZMax) {
        return false
      }

      //if there is already a machine in that position
      for (numMachineAux = 0; numMachineAux < numMachines[numLine]; numMachineAux++) {
        if (numMachineAux !== numMachine) {
          var machineComparing = listMachines[numLine][numMachineAux]
          var posXComMachine = machineComparing.position.x + machineComparing.children[0].position.x
          var posZComMachine = machineComparing.position.z + machineComparing.children[0].position.z
          if ((posXComMachine + 2.5) > posXThisMachine && (posXComMachine - 2.5) < posXThisMachine) {
            return false
          }
        }
      }
      return true;
    }

    var onProgress = function (xhr) {
    };

    var onError = function (xhr) { };

    function loadMachine() {
      var machAux;

      var mtlLoader = new MTLLoader()
      mtlLoader.setPath('resources/obj3D/');
      mtlLoader.load('machine.mtl', function (materials) {
        doLoop[0] = true;
        doTime[0] = false;
        createNewProd[0] = false;
        doGetProdToWaitSpace[0] = false;
        direction[0] = true;
        cones[0] = [];
        listOperation[0][0] = getRandomNumer();
        materials.preload();

        var objLoader = new OBJLoader();
        objLoader.setMaterials(materials);
        objLoader.setPath('resources/obj3D/');
        objLoader.load('machine.obj', function (object) {

          object.traverse(function (child) {
            if (child instanceof THREE.Object3D) {

              if (child.name == 'Cube') {
                child.name = "Line no.0 " + nameMachine + (numMachines[0] - 1) + "\nType Operation " + listOperation[0][0];
                child.castShadow = true;
                objects.push(child)
              }

            }
          });

          object.position.y = machineSupporter.position.y;
          object.position.x = machineSupporter.position.x + sideBarOptions.length / 2 + posInitMachineX;
          object.position.z = machineSupporter.position.z + posInitMachineZ;

          object.castShadow = true;

          listMachines[0][0] = object;
          scene.add(listMachines[0][0]);

          disProdMachineX = object.position.x - tube.position.x + 0.5;
          disProdMachineZ = object.position.z - tube.position.z;
          initialPosConeY = tube.position.y + sizeHightProduct / 2 + tubeOptions.cylRad;
          initialPosConeX = object.position.x - disProdMachineX;
          createNewProduct(numLines - 1);

          var waitSpaceAux = new THREE.Mesh(createGeometryWaitSpace(), waitSpaceMaterials);
          waitSpaceAux.position.x = object.position.x;
          waitSpaceAux.position.y = object.position.y + waitSpaceOptions.height;
          waitSpaceAux.position.z = tube.position.z;
          difMachineAndWaitSpace = object.position.z - waitSpaceAux.position.z;
          posZWaitSpace = waitSpaceAux.position.z;
          waitSpaceList[0][0] = waitSpaceAux;
          waitSpaceAux.castShadow = true;
          waitSpaceAux.receiveShadow = true;

          scene.add(waitSpaceList[0][0]);

          var machineTool = new THREE.Mesh(createGeometryFerramenta(), msMaterials);
          posToolYInit = object.position.y + 4.5 - ferramentaOptions.cylHeight / 2;
          posToolZInit = object.position.z - 4;
          machineTool.position.x = object.position.x;
          machineTool.position.y = posToolYInit;
          machineTool.position.z = posToolZInit;
          machineTool.name = nameTool + (numMachines[0] - 1);
          listTools[0][0] = machineTool;
          machineTool.castShadow = true;

          listUpDown[0] = [];
          listMoveTool[0] = [];
          getProdToWaitSpace[0] = [];

          for (var asd = 0; asd < numMachinesMax; asd++) {
            listUpDown[0][asd] = 1;
            listMoveTool[0][asd] = -1;
            getProdToWaitSpace[0][asd] = 0;
          }

          scene.add(listTools[0][0]);
          distToolMachine = object.position.z - machineTool.position.z;

        }, onProgress, onError);
      });
      lineCreated[0] = true;
    }

    function firstMachineInLine(machineName) {

      var prodLineAux = productionLine[numLines].clone();
      var newMachine = listMachines[numLines - 1][0].clone();
      listShadowProd[numLines] = listShadowProd[numLines - 1].clone();
      listOperation[numLines][0] = getRandomNumer();

      newMachine.position.z = (posCreateLineZ + posInitMachineZ + 5.5);
      newMachine.position.x = (posCreateLineX + sideBarOptions.length / 2 + posInitMachineX);
      newMachine.traverse(function (child) {
        if (child instanceof THREE.Object3D) {
          if (child.name)
            child.name = machineName == null ? "Line no." + (numLines) + " " + nameMachine + "0 \nType Operation " + listOperation[numLines][0] : machineName
          objects.push(child)
        }
      });
      listMachines[numLines][0] = newMachine;
      //create Product
      createNewProduct(numLines);

      var newWaitSpace = waitSpaceList[numLines - 1][0].clone();
      newWaitSpace.position.z = newMachine.position.z - 7.5;
      newWaitSpace.position.x = newMachine.position.x;

      var newTool = listTools[numLines - 1][0].clone();
      newTool.position.z = newMachine.position.z - distToolMachine;
      newTool.position.x = newMachine.position.x;

      //create tubes
      var tube = new THREE.Mesh(createTube(), tubeMaterials);
      tube.position.x = posCreateLineX + tubePositionOptions.x;
      tube.position.z = posCreateLineZ + 5.5 - tubePositionOptions.y;
      tube.position.y = prodLineAux.position.y + 1.05 + tubePositionOptions.z;
      tube.rotation.x = THREE.Math.degToRad(90);
      tube.castShadow = true;
      tube.receiveShadow = true;

      for (var i = 0; i <= (numTubes - 1); i++) {
        var obj1 = tube.clone();
        obj1.position.z = tube.position.z
        var obj2 = microTube.clone();
        obj2.position.z = tube.position.z + 2.5
        var obj3 = microTube.clone();
        obj3.position.z = tube.position.z - 2.5

        obj1.position.x = tube.position.x - (0.5 * i);
        tubes[numLines][i] = obj1;

        obj2.position.x = tube.position.x - (0.5 * i);
        obj3.position.x = tube.position.x - (0.5 * i);
        microTubes1[numLines][i] = obj2;
        microTubes2[numLines][i] = obj3;

        scene.add(obj1);
        scene.add(obj2);
        scene.add(obj3);
      }

      listTools[numLines][0] = newTool;
      waitSpaceList[numLines][0] = newWaitSpace;
      numMachines[numLines] = 1;

      scene.add(newMachine);
      scene.add(newTool);
      scene.add(waitSpaceList[numLines][0]);
    }

    function addMachineToLine(numProdLine) {
      if (numMachines[numProdLine] < numMachinesMax && prodWorking) {
        //give a operation type
        do {
          listOperation[numProdLine][numMachines[numProdLine]] = getRandomNumer();
        } while (listOperation[numProdLine][numMachines[numProdLine]] == listOperation[numProdLine][numMachines[numProdLine] - 1]);

        //machine
        var machv2 = listMachines[numProdLine][numMachines[numProdLine] - 1].clone();
        machv2.position.x = listMachines[numProdLine][numMachines[numProdLine] - 1].position.x - distMachines;

        machv2.traverse(function (child) {
          if (child instanceof THREE.Object3D) {
            if (child.name)
              child.name = "Line no." + numProdLine + " " + nameMachine + numMachines[numProdLine] + "\nType Operation " + listOperation[numProdLine][numMachines[numProdLine]];
            objects.push(child)
          }
        });

        var waitSpaceAux = waitSpaceList[numProdLine][numMachines[numProdLine] - 1].clone();
        waitSpaceAux.position.x = waitSpaceList[numProdLine][numMachines[numProdLine] - 1].position.x - distMachines;
        waitSpaceAux.position.z = machv2.position.z - difMachineAndWaitSpace;
        waitSpaceList[numProdLine][numMachines[numProdLine]] = waitSpaceAux;

        scene.add(waitSpaceAux);

        listMachines[numProdLine][numMachines[numProdLine]] = machv2;
        scene.add(machv2);

        var ferr2 = listTools[numProdLine][numMachines[numProdLine] - 1].clone();
        ferr2.position.x = ferr2.position.x - distMachines;
        ferr2.position.y = posToolYInit;
        ferr2.position.z = ferr2.position.z;
        ferr2.name = nameTool + numMachines[numProdLine];
        listTools[numProdLine][numMachines[numProdLine]] = ferr2;
        scene.add(ferr2);

        numMachines[numProdLine] = numMachines[numProdLine] + 1;
      }
    }

    function addMachineToLine2(numProdLine, name) {
      if (numMachines[numProdLine] < numMachinesMax && prodWorking) {
        //give a operation type
        do {
          listOperation[numProdLine][numMachines[numProdLine]] = getRandomNumer();
        } while (listOperation[numProdLine][numMachines[numProdLine]] == listOperation[numProdLine][numMachines[numProdLine] - 1]);

        //machine
        var machv2 = listMachines[numProdLine][numMachines[numProdLine] - 1].clone();
        machv2.position.x = listMachines[numProdLine][numMachines[numProdLine] - 1].position.x - distMachines;

        machv2.traverse(function (child) {
          if (child instanceof THREE.Object3D) {
            if (child.name)
              child.name = name
            objects.push(child)
          }
        });

        var waitSpaceAux = waitSpaceList[numProdLine][numMachines[numProdLine] - 1].clone();
        waitSpaceAux.position.x = waitSpaceList[numProdLine][numMachines[numProdLine] - 1].position.x - distMachines;
        waitSpaceAux.position.z = machv2.position.z - difMachineAndWaitSpace;
        waitSpaceList[numProdLine][numMachines[numProdLine]] = waitSpaceAux;

        scene.add(waitSpaceAux);

        listMachines[numProdLine][numMachines[numProdLine]] = machv2;
        scene.add(machv2);

        var ferr2 = listTools[numProdLine][numMachines[numProdLine] - 1].clone();
        ferr2.position.x = ferr2.position.x - distMachines;
        ferr2.position.y = posToolYInit;
        ferr2.position.z = ferr2.position.z;
        ferr2.name = nameTool + numMachines[numProdLine];
        listTools[numProdLine][numMachines[numProdLine]] = ferr2;
        scene.add(ferr2);

        numMachines[numProdLine] = numMachines[numProdLine] + 1;
      }
    }

    function addLine(name) {
      var newProdLine = productionLine[0].clone();
      var childrenProdLine = newProdLine.children
      childrenProdLine.forEach(function (child) {
        if (child.name) {
          if (child.name.includes(nameProdLine2)) {
            child.name = name == null ? nameProdLine2 + "" + numLines : 'Production Line ID: ' + name
            child.position.z = posCreateLineZ - 5;
            child.position.x = posCreateLineX;
          } else if (child.name.includes(nameProdLine)) {
            child.name = name == null ? nameProdLine + "" + numLines : 'Production Line ID: ' + name
            objects.push(child)
            child.position.z = posCreateLineZ + 5.5;
            child.position.x = posCreateLineX;
          }
        }
      });
      productionLine[numLines] = newProdLine;
      scene.add(productionLine[numLines]);
    }

    function removeMachineFromLine(numProdLine) {
      if (numMachines[numProdLine] > 1 && prodWorking) {
        scene.remove(listMachines[numProdLine][numMachines[numProdLine] - 1])
        scene.remove(listTools[numProdLine][numMachines[numProdLine] - 1])
        scene.remove(waitSpaceList[numProdLine][numMachines[numProdLine] - 1])
      }

      numMachines[numProdLine]--;

    }

    function turnOnAmbientLight() {
      ambientLight = new THREE.AmbientLight(0xffffff, 0.3);
      scene.add(ambientLight);
    }

    function turnOnDirectionalLight() {

      var directionalLight = new THREE.DirectionalLight(0xffffff, 0.6);

      directionalLight.position.set(10, 30, 5);
      scene.add(directionalLight);

      var d = 40;
      directionalLight.castShadow = true;
      directionalLight.shadow.camera.left = - d * 2;
      directionalLight.shadow.camera.right = d;
      directionalLight.shadow.camera.top = d;
      directionalLight.shadow.camera.bottom = - d;

      directionalLight.shadow.camera.near = 1;
      directionalLight.shadow.camera.far = 100;

      directionalLight.shadow.mapSize.x = 4096;
      directionalLight.shadow.mapSize.y = 4096;
    }

    function defaultCameraSettings() {
      camera.position.x = 0;
      camera.position.y = height;
      camera.position.z = -20;
      camera.lookAt(0, height, 0);
    }

    function topCameraSettings() {
      camera.position.x = 0;
      camera.position.y = height * 3;
      camera.position.z = 0;
      camera.lookAt(0, 0, 0);
    }

    function frontCameraSettings() {
      camera.position.x = 0;
      camera.position.y = 5;
      camera.position.z = -20;
      camera.lookAt(0, height, 0);
    }

    function sideCameraSettings() {
      camera.position.x = -20;
      camera.position.y = 5;
      camera.position.z = 0;
      camera.lookAt(0, height, 0);
    }

    function createGeometrySideLine1() {
      return new THREE.BoxGeometry(
        sideBarOptions.length,
        sideBarOptions.thickness,
        sideBarOptions.width * 1.25
      );
    }

    function createGeometrySideLine2() {
      return new THREE.BoxGeometry(
        sideBarOptions.length,
        sideBarOptions.thickness,
        sideBarOptions.width
      );
    }

    function createTube() {
      return new THREE.CylinderGeometry(
        tubeOptions.cylRad,
        tubeOptions.cylRad,
        tubeOptions.cylHeight,
        tubeOptions.cylSegments
      );
    }

    function createMicroTube() {
      return new THREE.CylinderGeometry(
        microTubeOptions.cylRad,
        microTubeOptions.cylRad,
        microTubeOptions.cylHeight,
        microTubeOptions.cylSegments
      );
    }

    function createGeometryWaitSpace() {
      return new THREE.BoxGeometry(
        waitSpaceOptions.width,
        waitSpaceOptions.height,
        waitSpaceOptions.length
      );
    }

    function createGeometryFerramenta() {
      return new THREE.CylinderGeometry(
        ferramentaOptions.cylRad,
        ferramentaOptions.cylRad,
        ferramentaOptions.cylHeight,
        ferramentaOptions.cylSegments
      );
    }

    function createGeometryCone() {
      return new THREE.ConeGeometry(
        sizeProduct,
        sizeHightProduct,
        32
      );
    }

    function createGeometryProductBox() {
      return new THREE.BoxGeometry(
        sizeProduct,
        sizeHightProduct,
        sizeProduct
      );
    }

    function createGeometryProductSphere() {
      return new THREE.SphereGeometry(
        sizeHightProduct * 0.5,
        32,
        32
      );
    }

    function createGeometryProductCilind() {
      return new THREE.CylinderGeometry(
        sizeProduct,
        sizeProduct,
        sizeHightProduct,
        20
      );
    }

    function createGeometryProductCilindDifSize() {
      return new THREE.CylinderGeometry(
        sizeProduct,
        sizeProduct * 0.5,
        sizeHightProduct,
        20
      );
    }

    function getConeMaterial() {
      return new THREE.MeshBasicMaterial({ color: 0xffffff });
    }

    function turnOnShadows() {

    }

    var numLineAt;

    function addProductionLineFolder() {
      var folderLine1 = folderLine.addFolder((numLines));

      var obj = {
        addMachine: function () {
          addMachineToLine(parseInt(folderLine1.name + ""));
        }
      };

      var obj2 = {
        removeMachine: function () {
          removeMachineFromLine(folderLine1.name + "")
        }
      }


      folderLine1.add(obj, 'addMachine')//.name("Add Machine")

      folderLine1.add(obj2, 'removeMachine').name("Remove Machine");
    }

    function addProductionLineFolder2(name) {
      var folderLine1 = folderLine.addFolder('MDF: ' + name);
    }

    function lineSpaceFree(newX, newZ, numLineToIgnore) {
      var valid = true;
      for (var numLinesCont = 0; numLinesCont < numLines; numLinesCont++) {
        if (numLinesCont !== numLineToIgnore) {
          var conerX = newX + 10
          var conerZ = newZ + 13
          var prodLineAux = productionLine[numLinesCont]

          var childrenProdLine = prodLineAux.children
          childrenProdLine.forEach(function (child) {
            if (child.name && child.name.includes(nameProdLine) && !child.name.includes(nameProdLine2)) {

              //borders
              var prodLineAuxPosXMin = prodLineAux.position.x + child.position.x + 10 - 20 - 1 //right
              var prodLineAuxPosXMax = prodLineAux.position.x + child.position.x + 10 + 1//left
              var prodLineAuxPosZMin = prodLineAux.position.x + child.position.z + 8 - 10 - 1 //down
              var prodLineAuxPosZMax = prodLineAux.position.x + child.position.z + 8 + 5 + 1//up

              //verification if there already is any lines
              var validPos = true
              //any right
              //right && top 
              validPos = validPos && !(((conerX - 20) < prodLineAuxPosXMax && (conerX - 20) > prodLineAuxPosXMin) && ((conerZ) < prodLineAuxPosZMax && (conerZ) > prodLineAuxPosZMin))
              //right && down
              validPos = validPos && !(((conerX - 20) < prodLineAuxPosXMax && (conerX - 20) > prodLineAuxPosXMin) && ((conerZ - 15) < prodLineAuxPosZMax && (conerZ - 15) > prodLineAuxPosZMin))
              //any left
              //left && top
              validPos = validPos && !(((conerX) < prodLineAuxPosXMax && (conerX) > prodLineAuxPosXMin) && ((conerZ) < prodLineAuxPosZMax && (conerZ) > prodLineAuxPosZMin))
              //left && down
              validPos = validPos && !(((conerX) < prodLineAuxPosXMax && (conerX) > prodLineAuxPosXMin) && ((conerZ - 15) < prodLineAuxPosZMax && (conerZ - 15) > prodLineAuxPosZMin))

              //if valid pos isnt valid return false
              if (!validPos) {
                valid = false;
              }

            }
          });


        }

      }
      return valid
    }

    function addProductionLine() {
      if (parseInt(posCreateLineX + "") && parseInt(posCreateLineZ + "")) {
        posCreateLineX = parseInt(posCreateLineX + "");
        posCreateLineZ = parseInt(posCreateLineZ + "");


        if (lineSpaceFree(posCreateLineX, posCreateLineZ, -1)) {

          changeFloorSizeByNewProdLine(posCreateLineX, posCreateLineZ)
          lineCreated[numLines] = false;
          addProductionLineFolder();

          addLine(null);
          firstMachineInLine(null);
          requestAnimationFrame(loop);
          numLines++;

          lineCreated[numLines - 1] = true;
        }

      }
    }

    function addProductionLine2(name, machines) {
      if (parseInt(posCreateLineX + "") && parseInt(posCreateLineZ + "")) {
        posCreateLineX = parseInt(posCreateLineX + "");
        posCreateLineZ = parseInt(posCreateLineZ + "");


        if (lineSpaceFree(posCreateLineX, posCreateLineZ, -1)) {

          changeFloorSizeByNewProdLine(posCreateLineX, posCreateLineZ)
          lineCreated[numLines] = false;
          addProductionLineFolder2(name);

          addLine(name);
          firstMachineInLine('Production Line ID: ' + name + ' - Machine ID: ' + machines[0]);
          requestAnimationFrame(loop);
          numLines++;

          lineCreated[numLines - 1] = true;
        }

      }
    }

    function changeFloorSizeByNewProdLine(posCreateLineX, posCreateLineZ) {
      if (Math.abs(posCreateLineX) > furthestX - 10 || Math.abs(posCreateLineZ) > furthestZ - 10) {
        if (Math.abs(posCreateLineX) > furthestX - 10)
          furthestX = Math.abs(posCreateLineX) + 10;
        if (Math.abs(posCreateLineZ) > furthestZ - 10)
          furthestZ = Math.abs(posCreateLineZ) + 10;

        var geometryGroundAux = new THREE.BoxGeometry(
          furthestX * 2,
          thickness,
          furthestZ * 2
        );
        var geoGroundElAux = geometryGroundAux
        groundEl.geometry = geoGroundElAux;

      }

    }

    function criarFolders() {

      //menu
      var checkBox = {
        'Show_Production_Line': true,
        'Show_Floor': true
      };

      var folderGeneral = gui.addFolder("General");
      folderGeneral
        .add(checkBox, 'Show_Production_Line')
        .onChange(function (value) {
          showProductionLine(value)
        })

      folderGeneral
        .add(checkBox, 'Show_Floor')
        .onChange(
          function (value) {
            showFloor(value);
          }
        );

      folderGeneral
        .add(speedSliderOptions, "speedProdAux")
        .name("Speed Production")
        .min(minSpeedMov * multiplierSpeed)
        .max(maxSpeedMov * multiplierSpeed)
        .onChange(() => {
          speedProd = speedSliderOptions.speedProdAux / multiplierSpeed;

        });

      folderGeneral
        .add(speedSliderOptions, "speedToolsAux")
        .name("Speed Tools")
        .min(minSpeedTool * multiplierSpeed)
        .max(maxSpeedTool * multiplierSpeed)
        .onChange(() => {
          speedTools = speedSliderOptions.speedToolsAux / multiplierSpeed;

        });
      folderGeneral.open();


      folderLine = gui.addFolder("Production Line");
      folderLine.add({ addLine: addProductionLine }, 'addLine').name("Add production Line");

      folderLine
        .add({ posFactoryX: "0" }, "posFactoryX")
        .name("Position X Line")
        .onChange(function (value) {
          posCreateLineX = value;

        });
      folderLine
        .add({ posFactoryZ: "0" }, "posFactoryZ")
        .name("Position Z Line")
        .onChange(function (value) {
          posCreateLineZ = value;

        });

      addProductionLineFolder();
      numLines++;
      folderLine.open();
    }

    function getRandomNumer() {
      return Math.floor(Math.random() * 4); //from 0 to 3
    }

    //Function Called From HTML
    var animationId;

    /* rotation and stop
    function viewPlay(){
      boolRotating=true;
      rotateSceneAction();
    }
  	
    function rotateScene() {
      scene.rotation.y += 0.005;
      bgMesh.rotation.y += 0.005;
    	
    }
  	
    function rotateSceneAction() {
      animationId = requestAnimationFrame(rotateSceneAction);
      rotateScene();
    }
  	
    function viewStop(){
      boolRotating=false;
      cancelAnimationFrame(animationId);	
    }
    */
    //End Function Called From HTML

    function showProductionLine(bool = true) {
      prodWorking = bool
      for (var i = 0; i < cones[0].length; i++) {
        cones[i].visible = bool;
      }
      factoryLine.visible = bool;
      for (var i = 0; i < listMachines[0].length; i++) {
        listMachines[0][i].visible = bool;
        waitSpaceList[0][i].visible = bool;
        listTools[i].visible = bool;
      }
    }

    function showFloor(bool) {
      ground.visible = bool;
    }

    var initialPosShadow = shadowProd.position.x;
    var differenceToStop = 0.003 + speedProd * 0.6;
    var speedWaitSpace = 0.03;

    function moveTool(numTool, numLineAt) {
      listTools[numLineAt][numTool].position.y += (0.01 * listUpDown[numLineAt][numTool]);
      if ((listTools[numLineAt][numTool].position.y > posToolYInit + ferramentaOptions.cylHeight / 4) || (listTools[numLineAt][numTool].position.y < posToolYInit - ferramentaOptions.cylHeight / 4)) {
        listUpDown[numLineAt][numTool] = -listUpDown[numLineAt][numTool];
      }
    }

    //mov wait space and product from the production line
    function moveProductToWaitSpace(numWaitSpace, numProdLineAt) {
      var absNumWaitSpace = Math.abs(numWaitSpace);
      var posNegative;
      if (direction[numProdLineAt]) {
        posNegative = 1;
      } else {
        posNegative = -1;
      }

      waitSpaceList[numProdLineAt][absNumWaitSpace].position.z += speedWaitSpace * posNegative;
      cones[numProdLineAt][getProdToWaitSpace[numProdLineAt][absNumWaitSpace] - 1].position.z += speedWaitSpace * posNegative;
      if (direction[numProdLineAt] && (waitSpaceList[numProdLineAt][absNumWaitSpace].position.z > (listTools[numProdLineAt][absNumWaitSpace].position.z - 0.01))) {
        doLoop[numProdLineAt] = false;
        doTime[numProdLineAt] = true;
        doGetProdToWaitSpace[numProdLineAt] = false;
        soundMachine.play();
        listMoveTool[numProdLineAt][absNumWaitSpace] = 1;

        prodToChange[numProdLineAt][getProdToWaitSpace[numProdLineAt][absNumWaitSpace] - 1] = listOperation[numProdLineAt][absNumWaitSpace];
        direction[numProdLineAt] = !direction[numProdLineAt];
        if (absNumWaitSpace == 0) //if is the first machine
          createNewProd[numProdLineAt] = true;

      } else if (!direction[numProdLineAt] && (waitSpaceList[numProdLineAt][absNumWaitSpace].position.z < listMachines[numProdLineAt][0].position.z - disProdMachineZ - 0.01)) {
        doLoop[numProdLineAt] = true;
        doTime[numProdLineAt] = false;
        doGetProdToWaitSpace[numProdLineAt] = false;

        cones[numProdLineAt][getProdToWaitSpace[numProdLineAt][absNumWaitSpace] - 1].position.x -= differenceToStop * 2;
        direction[numProdLineAt] = !direction[numProdLineAt];
        getProdToWaitSpace[numProdLineAt][absNumWaitSpace] = 0;
      }
    }

    function createNewProduct(numLineAt) {

      var newCone = new THREE.Mesh(createGeometryCone(), getConeMaterial());
      newCone.castShadow = true
      newCone.receiveShadow = true
      newCone.name = nameProd + numProd[numLineAt] + " In line no." + numLineAt;
      newCone.position.x = listMachines[numLineAt][0].position.x - disProdMachineX;
      newCone.position.y = initialPosConeY;
      newCone.position.z = listMachines[numLineAt][0].position.z - disProdMachineZ;
      cones[numLineAt][numProd[numLineAt]] = newCone;

      scene.add(newCone);
      createNewProd[numLineAt] = false;
      numProd[numLineAt] = numProd[numLineAt] + 1;

    }

    function changeProductByMachine(listNumProd, numLineAt) {

      for (var c = 0; c < prodToChange[numLineAt].length; c++) {
        var numMachineChanging = prodToChange[numLineAt][c];

        prodToChange[numLineAt][c] = -1;

        if (numMachineChanging >= 0) {
          //change type of product
          switch (numMachineChanging) {

            case 0:
              var newConeMaterial = new THREE.MeshBasicMaterial({ color: 0xfc03f0, opacity: 0.5, transparent: true });

              cones[numLineAt][c].material = newConeMaterial;
              cones[numLineAt][c].geometry = createGeometryProductBox();
              break;
            case 1:
              var newConeMaterial = new THREE.MeshBasicMaterial({ color: 0x78a19b, opacity: 1, transparent: true });

              cones[numLineAt][c].material = newConeMaterial;
              cones[numLineAt][c].geometry = createGeometryProductCilind();
              break;
            case 2:
              var newConeMaterial = new THREE.MeshBasicMaterial({ color: 0xdff700, opacity: 0.7, transparent: true });

              cones[numLineAt][c].material = newConeMaterial;
              cones[numLineAt][c].geometry = createGeometryProductCilindDifSize();
              break;
            case 3:
              var newConeMaterial = new THREE.MeshBasicMaterial({ color: 0xf70000, opacity: 1, transparent: true });

              cones[numLineAt][c].material = newConeMaterial;
              cones[numLineAt][c].geometry = createGeometryProductSphere();
              break;
            default:
          }
        }
      }
    }

    function update() {

      for (var numLinesCont = 0; numLinesCont < numLines; numLinesCont++) {
        if (lineCreated[numLinesCont]) {
          //shadow
          if (doTime[numLinesCont]) {
            listShadowProd[numLinesCont].position.x -= speedTools;

            for (var i = 0; i < numMachines[numLinesCont]; i++) {
              if (listMoveTool[numLinesCont][i] >= 0) {
                moveTool(i, numLinesCont);
              }
            }

            if (listShadowProd[numLinesCont].position.x <= initialPosConeX - 5) {
              doTime[numLinesCont] = false;
              doLoop[numLinesCont] = false;
              doGetProdToWaitSpace[numLinesCont] = true;
              listShadowProd[numLinesCont].position.x = initialPosShadow; //reset shadow

              for (var c = 0; c < numMachines[numLinesCont]; c++) {
                soundMachine.stop();
                listMoveTool[numLinesCont][c] = -1; // reset tool to move
              }
              changeProductByMachine(prodToChange, numLinesCont);
            }
          }
          //product movement
          if (doLoop[numLinesCont]) {
            //rotation tubes
            for (var i = 0; i <= 40; i++) {
              tubes[numLinesCont][i].rotation.y += 0.02;
            }

            for (var i = 0; i < cones[numLinesCont].length; i++) {
              if (listMachines[numLinesCont][0] && cones[numLinesCont][i]) {

                cones[numLinesCont][i].position.x -= speedProd;

                //creation of a new product
                if (createNewProd[numLinesCont]) {
                  createNewProduct(numLinesCont);
                }

                for (var j = 0; j < numMachines[numLinesCont]; j++) {
                  //if it's in front of a wait space
                  if ((cones[numLinesCont][i].position.x > (waitSpaceList[numLinesCont][j].position.x - differenceToStop)) && (cones[numLinesCont][i].position.x < (waitSpaceList[numLinesCont][j].position.x + differenceToStop))) {
                    doLoop[numLinesCont] = false;
                    doTime[numLinesCont] = false;
                    doGetProdToWaitSpace[numLinesCont] = true;

                    getProdToWaitSpace[numLinesCont][j] = i + 1;
                  }
                }
              }
            }
          }
          //product movement to wait space
          if (doGetProdToWaitSpace[numLinesCont]) {

            //get prod to wait space
            //movement elemente to the product & come back
            for (var i = 0; i < numMachines[numLinesCont]; i++) {
              if (getProdToWaitSpace[numLinesCont][i] !== 0) {
                moveProductToWaitSpace(i, numLinesCont);
              }
            }
          }

          //remove products from production Line
          for (i = 0; i < cones[numLinesCont].length; i++) {
            if (cones[numLinesCont][i].position.x <= tubes[numLinesCont][tubes[numLinesCont].length - 1].position.x) {
              scene.remove(cones[numLinesCont][i])
            }
          }
        }
      }
      // create a Ray with origin at the mouse position
      //   and direction into the scene (camera direction)
      var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
      vector.unproject(camera);
      var ray = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());

      // create an array containing all objects in the scene with which the ray intersects
      var intersects = ray.intersectObjects(scene.children, true);

      // INTERSECTED = the object in the scene currently closest to the camera 
      //		and intersected by the Ray projected from the mouse position 	

      // if there is one (or more) intersections
      if (intersects.length > 0) {
        // if the closest object intersected is not the currently stored intersection object
        if (intersects[0].object != INTERSECTED) {

          // update text, if it has a "name" field.
          if (intersects[0].object.name) {
            var message = intersects[0].object.name;

            tooltipCanv.style.left = mouseProj.x + 10 + 'px';
            tooltipCanv.style.top = mouseProj.y + 'px';
            tooltipCanv.innerHTML = message;
            tooltipCanv.style.opacity = '1';
          }
          else {
            tooltipCanv.style.opacity = '0';
          }
        }
      } else // there are no intersections
      {
        // remove previous intersection object reference
        //     by setting current intersection object to "nothing"
        INTERSECTED = null;
      }
    };

    function render() {
      bgMesh.position.copy(camera.position);
      renderer.render(scene, camera);
    };

    function loop() {
      requestAnimationFrame(loop); //run this every frame
      update();
      render();
    }

    function getFromDB() {
      instance.get(MDF_URL + 'ProductionLine')
        .then((result) => {
          var productionLines = result.data

          for (var i = 0; i < productionLines.length; i++) {
            posCreateLineX = i % 2 == 0 ? (i + 1) * 25 : -i * 25
            posCreateLineZ = i % 2 == 0 ? -(i + 1) * 25 : i * 25

            var machines = productionLines[i].machineSequence

            addProductionLine2(productionLines[i].productionLineID, machines)

            if (machines.length > 1) {
              for (var j = 1; j < machines.length; j++) {
                addMachineToLine2(numLines - 1, 'Production Line ID: ' + productionLines[i].productionLineID + ' - Machine ID: ' + machines[j])
              }
            }
          }
        })
        .catch((err) => {
          console.log(err)
        })
    }

    loop()

    setTimeout(function () { //Start the timer
      getFromDB()
    }.bind(this), 1000)

  }

  viewFromTop() {
    scene.rotation.y = 0;
    if (!boolRotating) {
      camera.position.x = 0;
      camera.position.y = height * 3;
      camera.position.z = 0;
      camera.lookAt(0, 0, 0);
    }

  }

  viewFromFront() {
    scene.rotation.y = 0;
    if (!boolRotating) {
      camera.position.x = 0;
      camera.position.y = 5;
      camera.position.z = -20;
      camera.lookAt(0, height, 0);
    }
  }

  viewFromSide() {
    scene.rotation.y = 0;
    if (!boolRotating) {
      camera.position.x = -20;
      camera.position.y = 5;
      camera.position.z = 0;
      camera.lookAt(0, height, 0);
    }
  }

  viewDefaultView() {
    scene.rotation.y = 0;
    if (!boolRotating) {
      camera.position.x = 0;
      camera.position.y = height;
      camera.position.z = -20;
      camera.lookAt(0, height, 0);
    }
  }

  render() {
    return (
      <div>
        <div id="tooltip">ASASGAS</div>

        <div className="btn-group">
          <button id="buttonViewFromTop" align="center" className="button" onClick={this.viewFromTop}>Top View</button>
          <button id="buttonViewFromFront" align="center" className="button" onClick={this.viewFromFront}>Front View</button>
          <button id="buttonViewFromSide" align="center" className="button" onClick={this.viewFromSide}>Side View</button>
          <button id="buttonViewFromDefault" align="center" className="button" onClick={this.viewDefaultView}>Default View</button>

        </div>
      </div>
    )
  }
}

export default App;
