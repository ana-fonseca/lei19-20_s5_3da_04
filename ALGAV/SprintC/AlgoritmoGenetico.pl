%% Declaracoes dinâmicas
:-dynamic geracoes/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.
:-dynamic estab/3.
:-dynamic somaT/1.
:-dynamic tempos/2.
:-dynamic tarefa/4.
:-dynamic tarefas/1.


% tarefa(Id,TempoProcessamento,TempConc,PesoPenalizacao).
% tarefas(NTarefas).




% parameterização
inicializa:-write('Numero de novas Geracoes: '),read(NG), 			(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	% Adição destes limites para o critério de valorização
    write('Limites'),nl,
    write('Limite do tempo:'),read(LT),
    write('Limite de fitness:'),read(Fitness),
	retractall(somaT(_)),
    asserta(somaT(Fitness)),
    write('Limite de estabilizacao:'),read(Estab),
	retractall(estab(_,_,_)),
    asserta(estab(empty,0,Estab)),
    get_time(Ti), retractall(tempos(_,_)),asserta(tempos(Ti,LT)).


% Método main
gera:-
	cria_tarefa,
	inicializa,
	gera_populacao(Pop),
    gera_populacao2(Pop,NovoPop),
	write('Pop='),write(NovoPop),nl,nl,
	avalia_populacao(NovoPop,PopAv),
	write('PopAv='),write(PopAv),nl,nl,
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	get_time(Ti),
	confirmar_criterios(0,NG,PopOrd,Ind),nl,
	get_time(Tf),TComp is Tf-Ti,
	write('Tempo de execucao- '),write(TComp),nl,nl,
	write('Melhor Individuo-'),write(Ind),nl,nl.


gera_populacao(Pop):-
	populacao(TamPop),
    Tam is TamPop-2,
	tarefas(NumT),
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	gera_populacao(Tam,ListaTarefas,NumT,Pop).

gera_populacao(0,_,_,[]):-!.

gera_populacao(TamPop,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaTarefas,NumT,Resto),
	gera_individuo(ListaTarefas,NumT,Ind),
	not(member(Ind,Resto)).
gera_populacao(TamPop,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,ListaTarefas,NumT,L).


% O novo gera_população que recebe a lista da população já gerada pelo gera_populacao
% fornecido e acrescenta 2 novos individuos, 1 por cada heuristica usada
gera_populacao2(Pop,Res):-findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	firstGenEDD(ListaTarefas,Ind1),
	firstGenMinSLACK(ListaTarefas,Ind2),
	append(Pop,[Ind1,Ind2], Res).





gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).

avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(T,Dur,Prazo,Pen),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	(
		(InstFim =< Prazo,!, VT is 0)
  ;
		(VT is (InstFim-Prazo)*Pen)
	),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).


btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).



% Método de entrada para gerar gerações que verifica as condições de paragem
% antes de chamar o gera_geracao2, caso um criterio de paragem já tenha sido cumprido,
% termina a execução
confirmar_criterios(NG,G,Pop,Ind):-!,
    Pop = [X|_],
    estabiliza(Pop,N1),
    numeroGeracoesLimite(NG,G,N2),
	limiteSoma(X,N3),
	limiteTempo(N4),
	checkConditions(N1,N2,N3,N4,R),
	((R is 1,!,getBestInd(NG,Pop,Ind));gera_geracao2(NG,G,Pop,Ind)).

% Critério de paragem de estabilização da população
estabiliza(Pop,NX):-
	estab(ResAnt,N,LimitN),
    ((ResAnt==Pop,!,((N==LimitN,!,NX is 1);retractall(estab(_,_,_)),
	N1 is N+1, assertz(estab(ResAnt,N1,LimitN)),NX is 0)); retractall(estab(_,_,_)),
                        N1 is 1,assertz(estab(Pop,N1,LimitN)),NX is 0).
% Critério de paragem de limite de gerações		
numeroGeracoesLimite(NG,G,N2):-
	((NG==G,!,N2 is 1);N2 is 0).
	
% Critério de paragem do limite da avaliação
limiteSoma(X,N3):-
	somaT(LimitSoma),
    X=_*V,
    ((V<LimitSoma,!,N3 is 1);N3 is 0).
	
% Critério de paragem do limite de tempo
limiteTempo(N4):-
	tempos(Ti,Limit),
    get_time(Tf),Tcomp is Tf-Ti,
    ((Tcomp<Limit,!,N4 is 0);N4 is 1).
    
checkConditions(N1,N2,N3,N4,R):-
	((N1==1;N2==1;N3==1;N4==1,R is 1);R is 0).
	
	
% Método chamado quando o método termina, devolve a melhor solução
% para ser apresentada
getBestInd(G,Pop,Ind):-
	write('Geracao '), write(G), write(':'), nl, write(Pop), nl,nl,
	findall(i(Value,List), member((List*Value),Pop),L1),
    sort(L1,L),
    L = [Ind|_].


% Método que cria as novas gerações
gera_geracao2(N,G,Pop,Ind):-
	write('Geracao '), write(N), write(':'), nl, write(Pop), nl,nl,
    populacao(TamPop),
    Tam is TamPop-2,
    random_permutation(Pop,PopR),
	cruzamento(PopR,NPop1),
	mutacao(NPop1,NPop2),
	% Retiramos as avaliações da população recebida para podermos
	% usar o método uniao
    findall(X,member(X*_,Pop),Lista),
	uniao(NPop2,NPop1,NPop33),
 	uniao(Lista,NPop33,NPopX),
    avalia_populacao(NPopX,NPop5),
	% Retira os dois melhores elementos
    chooseCandidates(NPop5,Choosen,NPop),
    length(NPop,L),
	% Os restantes efetuam torneios
  	torneio(NPop,Tam,L,Result),
    append(Choosen,Result,NP),
	ordena_populacao(NP,NPopOrd),
	N1 is N+1,
	confirmar_criterios(N1,G,NPopOrd,Ind).


uniao([ ],L,L).
uniao([X|L1],L2,LU):-member(X,L2),!,uniao(L1,L2,LU).
uniao([X|L1],L2,[X|LU]):-uniao(L1,L2,LU).


remove_duplicates([],[]):-!.

remove_duplicates([H | T], List) :-    
     member(H, T),!,
     remove_duplicates( T, List).

remove_duplicates([H | T], [H|T1]) :- 
      remove_duplicates( T, T1).

gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Heuristica EDD
firstGenEDD(ListaTarefas, ListaTarefasOrd):-
	%% Aplicar algum tipo de soma de penalização ao tempo
	findall(t(TempoConc,TempoProcessamento,Peso,T), (member(T,ListaTarefas),tarefa(T,TempoProcessamento,TempoConc,Peso)),
				ListaTarefasOrd1),
	sort(ListaTarefasOrd1, ListaTarefasOrd2),
	findall(T, member(t(_,_,_,T),ListaTarefasOrd2), ListaTarefasOrd).
%%	
	
	
% Heuristica MinSlack
firstGenMinSLACK(ListaTarefas, ListaTarefasOrd):-
	firstGenMinSLACK1(ListaTarefas,0,ListaTarefasOrd).

firstGenMinSLACK1([],_,[]).
firstGenMinSLACK1(ListaTarefas,T, [Y|ListaTarefasOrd]):-
	firstGenMinSLACK2(ListaTarefas,T,Y,T1),
    delete(ListaTarefas,Y,ListaTarefas1),
	firstGenMinSLACK1(ListaTarefas1,T1,ListaTarefasOrd).

firstGenMinSLACK2(ListaTarefas,TAntigo,Y,T):-
	findall(t(TT,TempoConc,TempoProcessamento,Peso,T),
				(member(T,ListaTarefas),tarefa(T,TempoProcessamento,TempoConc,Peso), TT is TempoConc-(TAntigo+TempoProcessamento)),
				ListaTarefasOrd1),
    
	sort(ListaTarefasOrd1,ListTarefasOrd),
	ListTarefasOrd = [Z|_],
	Z = t(_,_,TempoProcessamento,_,Y),
	T = TAntigo + TempoProcessamento.
%%


% Método que devolve os dois melhores
chooseCandidates([],[],[]).
chooseCandidates([G],[G],[]).
chooseCandidates(Lista, Choosen, Res):-
    findall(i(Value,List), member((List*Value),Lista),L1),
    sort(L1,L),
    L = [Aux1,Aux2|_],
    LAux = [Aux1,Aux2],
    findall(T*Value, member(i(Value,T),LAux),Choosen),
    Choosen = [C1,C2],
    delete(Lista,C1,R),
    delete(R,C2,Res).
    

% Método dos torneios
% Estas primeiras 3 condições é para casos excecionais
torneio([],_,_,_):-!.
torneio(_,0,_,[]):-!.
torneio([G|[]],_,_,[G]):-!.

% Método executado quando o numero de elementos da lista é igual aos necessários
torneio(L,N,N,R):-R = L.

% Método executado quando o numero de elementos da lista é igual ao dobro dos necessários
torneio(L,N,S,R):-
    N==S*2,!,torneio1(L,R).
	
% Método executado quando o numero de elementos da lista é igual a mais do dobro dos necessários
torneio(L,N,S,R):-
    S>N*2,!,
	torneio1(L,F),
    tt(F,N,R,_).
	
% Método executado quando o numero de elementos da lista é igual a menos do dobro dos necessários	
torneio(L,N,_,R):-
    torneio1(L,F),
	subtract(L,F,Old),
    length(F,Size),
    N1 is N-Size,
    tt(Old,N1,X,_),
    append(F,X,R).


torneio1([],[]):-!.
% Este predicado foi adicionado para os casos em que a lista é ímpar
torneio1([Ind1*V1,Ind2*V2,Ind3*V3|[]], [Ind*V|R]):-
    !,
    R1 is V1/(V1+V2+V3),
    R2 is V2/(V1+V2+V3),
    R3 is V3/(V1+V2+V3),
    random(0.0,R1,X1),
    random(0.0,R2,X2),
    random(0.0,R3,X3),
    % Mesma probabilidade, extremamente raro
    ((X1==X2,X2==X3,!,((V1<V2,!,((V1<V3,!,Ind = Ind1, V = V1);Ind = Ind3, V = V3);
                    V2<V3,!,Ind = Ind2, V = V3);Ind = Ind3, V = V3));
     
	((X1<X2,!,((X1<X3,!,Ind = Ind1, V = V1);Ind = Ind3, V = V3));
     ((X2<X3,!,Ind = Ind2, V = V2);Ind = Ind3, V = V3))),
    torneio1([],R).
torneio1([Ind1*V1,Ind2*V2|L],[Ind*V|R]):-
    !,
    R1 is V1/(V1+V2),
    R2 is V2/(V1+V2),
    random(0.0,R1,X1),
    random(0.0,R2,X2),
	((X1<X2,!,Ind = Ind1, V = V1);
    ((X1>X2,!, Ind = Ind2, V = V2);
    % Quando ambos têm a mesma probabildade, iremos
    % devolver o que tem melhor classificação
    ((V1<V2,!,Ind = Ind1, V = V1);
    Ind = Ind2, V = V2))),
    torneio1(L,R).

% Método usado quando é necessário uma segunda eliminatória
tt(L,T,C,Old):-
    findall(e(R,X),
           	(member(X,L),
            X=_*V,
            random(0,V,R)),
            Lista),
   sort(Lista,ListaOrd),
    chooseBest(ListaOrd,T,D),
    findall(X*V,member(e(_,X*V),D),C),
	subtract(L,C,Old).

% Devolve os melhores T elementos da lista
% a lista já tem de estar previamente ordenado
chooseBest(_,0,[]).
chooseBest([X|L],T,[X|R]):-
    T1 is T-1,
    chooseBest(L,T1,R).
    
    
	
	
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%
%%%%%%%%% Dados fornecidos
%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
% FÁBRICA

% Linhas

linhas([lA]).


% Maquinas


maquinas([ma,mb,mc,md,me]).



% Ferramentas


ferramentas([fa,fb,fc,fd,fe,ff,fg,fh,fi,fj]).


% Maquinas que constituem as Linhas

tipos_maq_linha(lA,[ma,mb,mc,md,me]).
% ...


% Operações

tipo_operacoes([opt1,opt2,opt3,opt4,opt5,opt6,opt7,opt8,opt9,opt10]).

% operacoes/1 vai ser criado dinamicamente
%no exemplo dara' uma lista com 30 operacoes 6 lotes de produtos * 5 operacoes por produto

%operacoes_atrib_maq/2 vai ser criado dinamicamente
%no exemplo cada maquina tera' 6 operacoes atribuidas, uma por cada lote de produtos

% classif_operacoes/2 deve ser criado dinamicamente 
%no exemplo teremos 30 factos deste tipo, um para cada operacao


% Afetação de tipos de operações a tipos de máquinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,1,1).
operacao_maquina(opt2,mb,fb,2.5,2).
operacao_maquina(opt3,mc,fc,1,3).
operacao_maquina(opt4,md,fd,1,1).
operacao_maquina(opt5,me,fe,2,3).
operacao_maquina(opt6,mb,ff,1,4).
operacao_maquina(opt7,md,fg,2,5).
operacao_maquina(opt8,ma,fh,1,6).
operacao_maquina(opt9,me,fi,1,7).
operacao_maquina(opt10,mc,fj,20,2).





%...


% PRODUTOS

produtos([pA,pB,pC,pD,pE,pF]).

operacoes_produto(pA,[opt1,opt2,opt3,opt4,opt5]).
operacoes_produto(pB,[opt1,opt6,opt3,opt4,opt5]).
operacoes_produto(pC,[opt1,opt2,opt3,opt7,opt5]).
operacoes_produto(pD,[opt8,opt2,opt3,opt4,opt5]).
operacoes_produto(pE,[opt1,opt2,opt3,opt4,opt9]).
operacoes_produto(pF,[opt1,opt2,opt10,opt4,opt5]).



% ENCOMENDAS

%Clientes

clientes([clA,clB,clC]).


% prioridades dos clientes

prioridade_cliente(clA,2).
prioridade_cliente(clB,1).
prioridade_cliente(clC,3).

% ...

% Encomendas do cliente, 
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

encomenda(clA,[e(pA,4,50),e(pB,4,70)]).
encomenda(clB,[e(pC,3,30),e(pD,5,200)]).
encomenda(clC,[e(pC,3,30),e(pD,5,200),e(pD,5,200)]).

% ...




% cria_op_enc - fizeram-se correcoes face a versao anterior

:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)).





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%
%%%%%%%%%%%%% Método de criar tarefas
%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- dynamic ttimes/1.


cria_tarefa:-
	retractall(ttimes(_)),retractall(tarefas(_)), retractall(tarefa(_,_,_,_)),
	% Faz uma lista de todos os clientes
    findall(c(Cliente,P), (encomenda(Cliente,_), prioridade_cliente(Cliente,P)), ListaC),
	
	% Faz uma lista de todas as encomendas de um cliente
    findall(ListaProds,(member(c(Cliente,Peso),ListaC),
                     getProdu(Cliente,Peso,ListaProds)), ListaEncomendas1),
	% Flatten à lista para não ficar separada por clientes
    flatten(ListaEncomendas1,ListaEncomendas),
 
	
	% Faz o cálculo do tempo de processamento para cada encomenda
	assertz(ttimes(1)),
    findall(tarefa(T,TProcessamento,TConc,Peso),
            (member(ListaProds,ListaEncomendas),
               calculate(ListaProds,P,TConc,TProcessamento),
                pesoCliente(P,Peso),generateStringTarefa(T),
				assertz(tarefa(T,TProcessamento,TConc,Peso))),ListaTarefas),
				length(ListaTarefas,N),
				assertz(tarefas(N)).


% Devolve o calculo da penalização do atraso da encomenda
% em relação à prioridade do cliente
pesoCliente(P,Peso):-Peso is ((9+P)/10).
          
% Gera o ID da tarefa
generateStringTarefa(T):-
        ttimes(N),
    	retractall(ttimes(_)),
        atomic_concat(t,N,T),
    	N1 is N+1,
    	assertz(ttimes(N1)).


% Devolve todas as encomendas
getProdu(Cliente,Peso,ListaProds):-
	% Vai buscar as encomendas de um dado cliente, e associa a cada encomenda as operações necessárias para a executar
    findall(e(Cliente,Produto,Quantidade,TConc,Ops, Peso),
            	(encomenda(Cliente,X),
                getOps1(Cliente,X,Lista),
                member(e(_,Produto,Quantidade,TConc,Ops),Lista)),
           	ListaProds).

% Método que associa a encomenda à lista de operações necessárias para a executar
getOps1(Cliente,ListaEncomendas,Lista):-
    findall(e(Cliente,Produto,Quantidade,TConc,ListaOps),
            (member(e(Produto,Quantidade,TConc),ListaEncomendas),getOps2(Cliente,Produto,ListaOps)),Lista).

          
% Método que vai buscar todas as operações necessárias para uma encomenda		  
getOps2(Cliente,Produto,ListaProdutos):-
           findall(X,op_prod_client(X,_,_,Produto,Cliente, _,_,_,_),ListaProdutos).




% Método que faz o cálculo de processamento necessário para uma encomenda
calculate(Encomenda,Peso,TConc,TProcessamento):-
   Encomenda = e(_,_,Q,TConc,ListaOps1,Peso),reverse(ListaOps1,ListaOps), calculate1(ListaOps,Q,_,TProcessamento).



% Método que gere o cálculo, itera pela lista de operações e verifica o tempo necessário 
% para executar todas
calculate1([],_,empty,0).
calculate1([X|L],Q,Anterior,TConc):-
    calculate1(L,Q,Anterior1,TC1),
    op_prod_client(X,_,_,_,_,Q,_,TS1,TE1),
    ((Anterior1 == empty, !, TConc is TS1+(TE1*Q));
  	calculate11(Q,TS1,TE1,Anterior1,TC1,TConc)),
    Anterior = X.


% Método que devolve o tempo de processamento para uma dada operação, tendo em conta
% o tempo já gasto até aquele momento e a operação anterior para verificar os tempos de execução
calculate11(Q,TS1,TE1,Anterior1,TC1,TConc):-
    	  op_prod_client(Anterior1,_,_,_,_,_,_,TS2,TE2),
    (((((TC1-TE2*(Q-1)))<TS1),!,somarTemposSetupPenalizacao(TS1,TS2,TE2,P));P is 0),
    ((TE1<TE2,!,TConc is TC1 + TE1 + P);
    calculateAnteriorComMenorTE(TE1,TE2,P,TC1,Q,TConc)).


% Método que retorna o somatório do acréscimo necessário ao tempo para uma dada operação 
% ter tempo de fazer setup em relação à anterior que já efetuou uma operação
somarTemposSetupPenalizacao(TS1,TS2,TE2,T):-
    ((TS1<(TS2+TE2),!,T is (TS2+TE2)-TS1);T is TS1-(TS2+TE2)).


% Fazer o cálculo dos atrasos que uma dada operação irá ter, por esta ter maior
% tempo de execução que a anterior
calculateAnteriorComMenorTE(TE1,TE2,Pen,TC1,Q,T):-
    T1 = TE1-TE2,
    T is TC1 + (T1*(Q-1))+TE1+Pen.





% Chamada de cria_op_enc assim que é feito o load do ficheiro
:-cria_op_enc.