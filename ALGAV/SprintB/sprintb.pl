% FÁBRICA

% Linhas

linhas([lA]).


% Maquinas

maquinas([ma]).



% Ferramentas

ferramentas([fa,fb,fc]).



% Relação entre linhas de produção e máquinas

tipos_maq_linha(lA,[ma]).
% ...


% Operações
tipo_operacoes([opt1,opt2,opt3]).



% operacoes/1 deve ser criado dinamicamente
%operacoes([op1,op2,op3,op4,op5,op6]).



%operacoes_atrib_maq depois deve ser criado dinamicamente
%operacoes_atrib_maq(ma,[op1,op2,op3,op4,op6,op5]).

%Originais: op1-5



% classif_operacoes/2 deve ser criado dinamicamente %%atomic_concat(op,NumOp,Resultado)
/*
classif_operacoes(op6,opt1).
classif_operacoes(op1,opt1).
classif_operacoes(op2,opt2).
classif_operacoes(op3,opt1).
classif_operacoes(op4,opt2).
classif_operacoes(op5,opt3).



classif_operacoes(op7,opt1).
classif_operacoes(op8,opt2).
classif_operacoes(op9,opt2).
classif_operacoes(op10,opt2).

classif_operacoes(op11,opt2).
classif_operacoes(op12,opt2).
classif_operacoes(op13,opt2).
classif_operacoes(op14,opt2).
classif_operacoes(op15,opt2).
*/

% ...


% Afetação de tipos de operações a tipos de máquinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,5,60).
operacao_maquina(opt2,ma,fb,6,30).
operacao_maquina(opt3,ma,fc,8,40).
%...


% PRODUTOS

produtos([pA,pB,pC]).

operacoes_produto(pA,[opt1]).
operacoes_produto(pB,[opt2]).
operacoes_produto(pC,[opt3]).



% ENCOMENDAS

%Clientes

clientes([clA,clB]).


% prioridades dos clientes

prioridade_cliente(clA,1).
prioridade_cliente(clB,2).
% ...


% Encomendas do cliente, 
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

encomenda(clA,[e(pA,1,100),e(pB,1,100),e(pC,1,300)]).
encomenda(clB,[e(pA,1,110),e(pB,1,150),e(pC,1,300)]).
encomenda(clB,[e(pA,1,110),e(pB,1,150),e(pC,1,300)]).
encomenda(clB,[e(pA,1,110),e(pB,1,150),e(pC,1,300)]).
encomenda(clB,[e(pA,1,110),e(pB,1,150),e(pC,1,300)]).




% Separar posteriormente em varios ficheiros



% permuta/2 gera permutações de listas
permuta([ ],[ ]).
permuta(L,[X|L1]):-apaga1(X,L,Li),permuta(Li,L1).

apaga1(X,[X|L],L).
apaga1(X,[Y|L],[Y|L1]):-apaga1(X,L,L1).



% permuta_tempo/3 faz uma permutação das operações atribuídas a uma maquina e calcula tempo de ocupação incluindo trocas de ferramentas

permuta_tempo(M,LP,Tempo):- operacoes_atrib_maq(M,L),
permuta(L,LP),soma_tempos(semfer,M,LP,Tempo).


% Soma o tempo de Setup e o tempo de execução tendo em conta os tempos de setup
% Modificar isto de maneira a ter tempo de atraso
soma_tempos(_,_,[],0).
soma_tempos(Fer,M,[Op|LOp],Tempo):- classif_operacoes(Op,Opt),
									operacao_maquina(Opt,M,Fer1,Tsetup,Texec),
									soma_tempos(Fer1,M,LOp,Tempo1),
									((Fer1==Fer,!,Tempo is Texec+Tempo1); 
											Tempo is Tsetup+Texec+Tempo1).



% melhor escalonamento com findall, gera todas as solucoes e escolhe melhor

melhor_escalonamento(M,Lm,Tm):-
				get_time(Ti),
				findall(p(LP,Tempo), 
				permuta_tempo(M,LP,Tempo), LL),
				melhor_permuta(LL,Lm,Tm),
				get_time(Tf),Tcomp is Tf-Ti,
				write('GERADO EM '),write(Tcomp),
				write(' SEGUNDOS'),nl.

melhor_permuta([p(LP,Tempo)],LP,Tempo):-!.
melhor_permuta([p(LP,Tempo)|LL],LPm,Tm):- melhor_permuta(LL,LP1,T1),
		((Tempo<T1,!,Tm is Tempo,LPm=LP);(Tm is T1,LPm=LP1)).
		
		
	:- dynamic melhor_sol_ta/2.
	:- dynamic melhor_sol_to/2.

% melhor escalonamento (1) sem findall

melhor_escalonamento1(M,Lm,Tm):-
				get_time(Ti),
				(melhor_escalonamento11(M);true),retract(melhor_sol_to(Lm,Tm)),
				get_time(Tf),Tcomp is Tf-Ti,
				write('GERADO EM '),write(Tcomp),
				write(' SEGUNDOS'),nl.

melhor_escalonamento11(M):- asserta(melhor_sol_to(_,10000)),!,
					permuta_tempo(M,LP,Tempo),
					atualiza(LP,Tempo),
					fail.

atualiza(LP,T):-melhor_sol_to(_,Tm),
			T<Tm,retract(melhor_sol_to(_,_)),asserta(melhor_sol_to(LP,T)),!.
			



% ------------------------------------------------ Heurística de Tempo de Atraso ------------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------------

permuta_tempo2(M,LP,Tempo):- operacoes_atrib_maq(M,L),
permuta(L,LP),soma_tempos2(semfer,M,LP,0,0,Tempo).



% Soma o tempo de Setup e o tempo de execução tendo em conta os tempos de setup	
% Modificar isto de maneira a ter tempo de atraso
soma_tempos2(_,_,[],TempoOcupacao,TempoAtrasoAte,TempoAtrasoFinal):- TempoAtrasoFinal is TempoAtrasoAte.
soma_tempos2(Fer,M,[Op|Lop],TempoOcupacao,TempoAtrasoAte,TempoAtrasoFinal):-op_prod_client(Op,M,Fer1,Prod,Client,Qt,Tconc,Tsetup,Texec),
																			((Fer1\==Fer,!,TempoOcupacao1 is TempoOcupacao + Tsetup + Texec); TempoOcupacao1 is TempoOcupacao + Texec),
																			((TempoOcupacao1>Tconc,!,TempoAtrasoOp is TempoOcupacao1 - Tconc); TempoAtrasoOp is 0),
																			TempoAtrasoAte1 is TempoAtrasoOp + TempoAtrasoAte,
																			soma_tempos2(Fer1,M,Lop,TempoOcupacao1, TempoAtrasoAte1,TempoAtrasoFinal).

melhor_escalonamento2(M,Lm,Tm):-
				get_time(Ti),
				(melhor_escalonamento22(M);true),retract(melhor_sol_ta(Lm,Tm)),
				get_time(Tf),Tcomp is Tf-Ti,
				write('GERADO EM '),write(Tcomp),
				write(' SEGUNDOS'),nl.

melhor_escalonamento22(M):- asserta(melhor_sol_ta(_,10000)),!,
					permuta_tempo2(M,LP,Tempo),
					atualiza2(LP,Tempo),
					fail.

atualiza2(LP,T):-melhor_sol_ta(_,Tm),
			T<Tm,retract(melhor_sol_ta(_,_)),asserta(melhor_sol_ta(LP,T)),!.





% ----------------------------------------------- Heurística de Tempo de Ocupação -----------------------------------------------
% -------------------------------------------------------------------------------------------------------------------------------
h_m_tempo_ocupacao(M,Lm,Tm):- get_time(Ti), operacoes_atrib_maq(M,[H|Lops]),h_m_tempo_ocupacao1([H|Lops],[],M,Tm,Lm),!,get_time(Tf),Tcomp is Tf-Ti,
				write('GERADO EM '),write(Tcomp),
				write(' SEGUNDOS'),nl.

h_m_tempo_ocupacao1([],Lt,_,0,Lm):-permuta(Lt,Lm),!.

h_m_tempo_ocupacao1([H|Lops],Lt,M,Tm,Lm):-classif_operacoes(H,Opt),operacao_maquina(Opt,M,_,Tp,Te),!,
					((classif_operacoes(X,Opt),member(X, Lops),
					delete(Lops,X,Nlops),h_m_tempo_ocupacao1([X|Nlops],[H|Lt],M,T,Lm),
					Tm is T + Te); h_m_tempo_ocupacao1(Lops,[H|Lt],M,T,Lm),Tm is T + Tp + Te).




	
:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Prod,Cliente,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(_,_,_,0,_,Ni,Ni):-!.
cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni2):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)),
	Qt2 is Qt -1,
	cria_ops_prod_cliente2(Opt,Prod,Client,Qt2,TConc,Ni,Ni2).




:-cria_op_enc.	
	
	
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

% A STAR


	% ------------------------------------------------------ Tempo de ocupacao ------------------------------------------------------
	% -------------------------------------------------------------------------------------------------------------------------------
	:-dynamic op_prod_client/9.
	aStar(LOp,Caminho,CustoAtual):- aStar2([(_,0,[],LOp)],Caminho,CustoAtual).

	%-condição
	% Podemos retirar o ! para obter todas as solucoes possiveis
	% ordenadas das mais curtas para mais longas
	aStar2([(_,CustoAtual,LOp,[])|_],Caminho,CustoAtual):-!, reverse(LOp,Caminho).

	aStar2([(_,CustoAcum,LOp,LOpFaltam)|Outros],Caminho,CustoAtual):-

		findall((CcE,CustoAtual,[X|LOp],Resto),(

		LOpFaltam\==[],
		% Seleciona-se um dos membros da lista com as operacoes que faltam
		member(X,LOpFaltam),

		% Retira-se esse elemento da lista dos que faltam
		apaga1(X,LOpFaltam,Resto),

		% Obtencao dos dados pertinentes
		op_prod_client(X,_,_,_,_,_,_,Tsetup,Texec),

	    % Se tiver a mesma ferramenta, nao se adiciona o tempo de Setup
		((mesmaFerramenta(X,LOp), ! ,(CustoAtual is CustoAcum + Texec));(CustoAtual is CustoAcum + Texec + Tsetup)),

		% Remove se da lista dos nos que faltam os nos que partilham
		% a mesma ferramenta com o no atual
		removerMesmaFerramenta(X,Resto,RestoX),

		estimativa(RestoX,Estimativa),

		% CustoAtual atual + Estimativa
		CcE is CustoAtual + Estimativa
		),Novos),

		append(Outros,Novos,Todos),
		%write('Novos='),write(Novos),nl,
		sort(Todos,TodosOrd),
		%write('TodosOrd='),write(TodosOrd),nl,nl,
		aStar2(TodosOrd,Caminho,CustoAtual).

	% ------------------------------------------------------ Tempo de atraso --------------------------------------------------------
	% -------------------------------------------------------------------------------------------------------------------------------

	aStarAtraso(LOp,Caminho,CustoAtual):- aStarAtraso2([(_,0,0,[],LOp)],Caminho,CustoAtual).

	%-----Condicao de paragem-----
	% Podemos retirar o ! para obter todas as solucoes possiveis
    % ordenadas das mais curtas para mais longas
	aStarAtraso2([(_,Atraso,_,LOp,[])|_],Caminho,Atraso):-!, reverse(LOp,Caminho).

	aStarAtraso2([(_,AtrasoAcum,CustoAcum,LOp,LOpFaltam)|Outros],Caminho,CustoAtual):-

		findall((AtrasoCEstimativa,Atraso,CustoAtual,[X|LOp],Resto),(

		LOpFaltam\==[],
		% Seleciona-se um dos membros da lista com as operacoes que faltam
		member(X,LOpFaltam),

		% Retira-se esse elemento da lista dos que faltam
		apaga1(X,LOpFaltam,Resto),

		% Obtencao dos dados pertinentes
		op_prod_client(X,_,_,_,_,_,TConcl,Tsetup,Texec),

	    % Se tiver a mesma ferramenta, nao se adiciona o tempo de Setup
		((mesmaFerramenta(X,LOp), ! ,(CustoAtual is CustoAcum + Texec));(CustoAtual is CustoAcum + Texec + Tsetup)),

		% Tmp = CustoAtual acumulado (incluindo o no atual) - tempo de conclusao
		Tmp is (CustoAtual - TConcl),

		% Caso este seja menor que zero, igualar a zero
		menor_zero(Tmp,Tmp2),

		% O atraso atual ira ser o atraso do no mais o atraso acumulado dos nos passados
		Atraso is Tmp2 + AtrasoAcum,

		estimativaAtraso(X, CustoAcum,Resto,Estimativa),

		% Atraso atual + Estimativa
		AtrasoCEstimativa is Atraso + Estimativa

		),Novos),

		append(Outros,Novos,Todos),
		%write('Novos='),writeln(Novos),nl,
		sort(Todos,TodosOrd),
		%write('TodosOrd='),write(TodosOrd),nl,nl,
		aStarAtraso2(TodosOrd,Caminho,CustoAtual).


	% Verifica se a operacao X e C tem a mesma ferramenta
	mesmaFerramenta(X,[C|_]):-classif_operacoes(X,OPT1),classif_operacoes(C,OPT2), operacao_maquina(OPT2,_,F,_,_), operacao_maquina(OPT1,_,F,_,_).

	% Remove as operacoes que tem a mesma ferramenta que a operacao X
	removerMesmaFerramenta(_,[],[]) :- !.
	removerMesmaFerramenta(X, [T|Xs], Y) :- !, classif_operacoes(T,OPT), ((operacao_maquina(OPT,_,X,_,_), ! , removerMesmaFerramenta(X, Xs, Y));(removerMesmaFerramenta(X, Xs, Y2)),append([T], Y2 , Y)).

	% Estimativa pelo tempo de ocupacao
	estimativa(LOp,Estimativa):-
		findall(p(FOp,Tsetup),
		(member(Op,LOp),op_prod_client(Op,_,FOp,_,_,_,_,Tsetup,_)),
		LFTsetup),
		elimina_repetidos(LFTsetup,L),
		soma_setups(L,Estimativa).

	% Estimativa pelo tempo de atraso
	estimativaAtraso(_,_,[],0):- !.
	estimativaAtraso(X,CustoAcum,LOp,Estimativa):-

		findall((TAtra),
		(

			member(Op,LOp),

			op_prod_client(Op,_,_,_,_,_,TConc,Tsetup,Texec),
			%Se tiver a mesma ferramenta, nao se adiciona o tempo de Setup
			((mesmaFerramenta(X,[Op]), ! , (TAtra is (CustoAcum + Texec  - TConc)));(TAtra is (CustoAcum + Texec + Tsetup - TConc)))


		),LFTAtraso),
		maior_atraso(LFTAtraso,Estimativa).

	elimina_repetidos([],[]).
	elimina_repetidos([X|L],L1):-member(X,L),!,elimina_repetidos(L,L1).
	elimina_repetidos([X|L],[X|L1]):-elimina_repetidos(L,L1).

	soma_setups([],0).
	soma_setups([p(_,Tsetup)|L],Ttotal):- soma_setups(L,T1), Ttotal is Tsetup+T1.

	% Retorna o maior atraso de uma lista de valores (o maior valor)
	maior_atraso(L, V) :-
		select(V, L, R), \+((member(X, R), X > V)).

	% Retorna Num ou 0 (se Num < 0)
	menor_zero(Num,Res):-
		((Num<0, ! ,(Res is 0));Res is Num).









