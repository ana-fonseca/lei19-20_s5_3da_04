% Bibliotecas 
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/html_write)).

:- consult('callPredicate').

:- initialization
    http_server(http_dispatch, [port(8000)]).

:- http_handler(root(.),
                http_redirect(moved, location_by_id(home_page)),
                []).
:- http_handler(root(reload), home_page, []).

home_page(_Request) :-
    reply_html_page(
        title('Server Algav'),
        [ h1('Reloading!')
        ]),
	run_predicate.