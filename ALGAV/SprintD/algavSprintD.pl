:-dynamic geracoes/1.
:-dynamic valor_minimo/1.
:-dynamic estabilizar/1.
:-dynamic tempo/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.
:-dynamic opcao/1.
:-dynamic operacoes_atrib_maq/2.
:-dynamic classif_operacoes/2.
:-dynamic op_prod_client/9.
:-dynamic operacoes/1.
:-dynamic tarefas/1.
:-dynamic tarefa/4.
:-dynamic tarefa_op_list/2.
:-dynamic usarHeuristica/1.
:-dynamic eliteInicial/1.
:-dynamic agenda_maq/2.
:-dynamic melhorSeqPorLinha/2.

% Linhas

linhas([lA]).
linhas([lB]).
linhas([lC]).


% Maquinas


% maquinas([ma,mb,mc,md,me]).
maquinas([ma,mb,mc,md,me,mf,mg,mh,mi,mj]).



% Ferramentas


ferramentas([fa,fb,fc,fd,fe,ff,fg,fh,fi,fj]).


% Maquinas que constituem as Linhas

tipos_maq_linha(lA,[ma,mb,mc,md,me]).
tipos_maq_linha(lB,[ma,mb,mc,md,me]).
tipos_maq_linha(lC,[mf,mg,mh,mi,mj]).
% ...


% Opera??es

tipo_operacoes([opt1,opt2,opt3,opt4,opt5,opt6,opt7,opt8,opt9,opt10]).

% operacoes/1 vai ser criado dinamicamente
%no exemplo dara' uma lista com 30 operacoes 6 lotes de produtos * 5 operacoes por produto

%operacoes_atrib_maq/2 vai ser criado dinamicamente
%no exemplo cada maquina tera' 6 operacoes atribuidas, uma por cada lote de produtos

% classif_operacoes/2 deve ser criado dinamicamente
%no exemplo teremos 30 factos deste tipo, um para cada operacao


% Afeta??o de tipos de opera??es a tipos de m?quinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,1,1).
operacao_maquina(opt2,mb,fb,2.5,2).
operacao_maquina(opt3,mc,fc,1,3).
operacao_maquina(opt4,md,fd,1,1).
operacao_maquina(opt5,me,fe,2,3).
operacao_maquina(opt6,mb,ff,1,4).
operacao_maquina(opt7,md,fg,2,5).
operacao_maquina(opt8,ma,fh,1,6).
operacao_maquina(opt9,me,fi,1,7).
operacao_maquina(opt10,mc,fj,20,2).

operacao_maquina(opt1,mf,fa,1,1).
operacao_maquina(opt2,mg,fb,2.5,2).
operacao_maquina(opt3,mh,fc,1,3).
operacao_maquina(opt4,mi,fd,1,1).
operacao_maquina(opt5,mj,fe,2,3).
operacao_maquina(opt6,mg,ff,1,4).
operacao_maquina(opt7,mi,fg,2,5).
operacao_maquina(opt8,mf,fh,1,6).
operacao_maquina(opt9,mj,fi,1,7).
operacao_maquina(opt10,mh,fj,20,2).




%...


% PRODUTOS

produtos([pA,pB,pC,pD,pE,pF]).

operacoes_produto(pA,[opt1,opt2,opt3,opt4,opt5]).
operacoes_produto(pB,[opt1,opt6,opt3,opt4,opt5]).
operacoes_produto(pC,[opt1,opt2,opt3,opt7,opt5]).
operacoes_produto(pD,[opt8,opt2,opt3,opt4,opt5]).
operacoes_produto(pE,[opt1,opt2,opt3,opt4,opt9]).
operacoes_produto(pF,[opt1,opt2,opt10,opt4,opt5]).



% ENCOMENDAS

%Clientes

clientes([clA,clB,clC]).


% prioridades dos clientes

prioridade_cliente(clA,2).
prioridade_cliente(clB,1).
prioridade_cliente(clC,3).

% ...

% Encomendas do cliente,
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

encomenda(clA,[e(pA,4,50),e(pB,4,70)]).
encomenda(clB,[e(pC,3,30),e(pD,5,200)]).
encomenda(clC,[e(pE,4,60),e(pF,6,120)]).
% ...

%--------------------------------------------------- Criar Informação ---------------------------------------------------

cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			get_machine(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)).

get_machine(Opt,M_escolhida,F_escolhida,Tsetup_escolhida,Texec_escolhida):-
	findall(p(Opt,M,F,Tsetup,Texec),operacao_maquina(Opt,M,F,Tsetup,Texec),Maquinas),
	get_numero_op_associado(Maquinas,Maquinas_com_op_num),
	sort_by_min_op(Maquinas_com_op_num,[p(_,M_escolhida,F_escolhida,Tsetup_escolhida,Texec_escolhida,_)|_]).

get_numero_op_associado([],[]):-!.
get_numero_op_associado([p(Opt,M,F,Tsetup,Texec)|Tail],[p(Opt,M,F,Tsetup,Texec,Num_op)|Tail2]):-
	findall(Op,op_prod_client(Op,M,_,_,_,_,_,_,_),OpList),
	length(OpList,Num_op),
	get_numero_op_associado(Tail,Tail2).

sort_by_min_op([],[]).
sort_by_min_op([Head|Tail],Maquinas_sorted):-splitMachines(Head,Tail,Left,Right),
			sort_by_min_op(Left,Left1),
			sort_by_min_op(Right,Right1),
			append(Left1,[Head|Right1],Maquinas_sorted).

splitMachines(_,[],[],[]).
splitMachines(p(Opt,M,F,Tsetup,Texec,Num_op),[p(Opt1,M1,F1,Tsetup1,Texec1,Num_op1)|Tail],[p(Opt1,M1,F1,Tsetup1,Texec1,Num_op1)|Left],Right):-
													Num_op>Num_op1,!,split(p(Opt,M,F,Tsetup,Texec,Num_op),Tail,Left,Right).
splitMachines(p(Opt,M,F,Tsetup,Texec,Num_op),[p(Opt1,M1,F1,Tsetup1,Texec1,Num_op1)|Tail],Left,[p(Opt1,M1,F1,Tsetup1,Texec1,Num_op1)|Right]):-
													split(p(Opt,M,F,Tsetup,Texec,Num_op),Tail,Left,Right).


%-Cria dinamicamente tarefas/1 e tarefa/4.
%-tarefas/1 guarda o n�mero total de tarefas
%-tarefa/4 guarda cada uma das tarefas juntamente com o seu tempo de
% processamento, de concusao e a penalizacao associada
makespan:-retractall(tarefas(_)),
	  retractall(tarefa(_,_,_,_)),
	  retractall(tarefa_op_list(_,_)),
	  findall([C,L],encomenda(C,L),L2),
	  addClientToeList(L2,[],List),
	  proper_length(List,Qt_tarefas),
	  assertz(tarefas(Qt_tarefas)),
	  replace_prod_for_op_list(List,List_Opl),
	  cria_tarefa(List_Opl,0),!.

% -Adiciona o cliente a cada e(Produto,Quantidade,Tempo) ficando
% e(Cliente,Produto,Quantidade,Tempo).
% -1� agrumento -> uma lista com o seguinte formato
% [[Cliente,[e(P,Q,T),...]],[Cliente,[e(P,Q,T),...]],...]
% -2� argumento -> retorna uma lista com o formato
% [e(Cliente,P,Q,T),...]
addClientToeList([],Result,Result).
addClientToeList([Head|Tail],Aux,Result):-put_client_on_e_struct(Head,[],List_with_client,[],0),
			                  append(Aux,List_with_client,Aux2),
					  addClientToeList(Tail,Aux2,Result).

%-Adiciona o cliente �s estruturas que lhe estao associadas
% -1� argumento -> uma lista com o seguinte formato
% [Cliente,[e(P,Q,T),...]]
% -2� argumento -> lista auxiliar que guarda as novas estruturas com o
% cliente para ser retornada no criterio de paragem
% -3� argumento -> retorna uma lista com o formato
% [e(Cliente,P,Q,T),...]
% -4� argumento -> cliente que esta a ser adicionado
% -5� argumento -> algarismo que varia entre 0 e 1 para controlar se ja
% se tem o cliente para adicionar � estrutura
put_client_on_e_struct([],List_with_client,List_with_client,_,_).
put_client_on_e_struct([Head|Tail],Aux,List_with_client,Client,0):-append(Client,Head,Client2),flatten(Tail,Tail2),
			                                           put_client_on_e_struct(Tail2,Aux,List_with_client,Client2,1).
put_client_on_e_struct([e(P,Q,T)|Tail],Aux,List_with_client,Client,1):-append([Aux],e(Client,P,Q,T),Aux2),flatten(Aux2,Aux3),
			                                              put_client_on_e_struct(Tail,Aux3,List_with_client,Client,1).

% -Substitui na lista recebida o produto pela lista de operacoes
% associada
% -1� argumento -> lista com o formato [e(Cliente,P,Q,T),...]
% -2� argumento -> retorna uma lista com o formato [e(Cliente,Lista de
% operacoes,Q,T),...]
replace_prod_for_op_list([],[]).
replace_prod_for_op_list([e(Client,Prod,Qt,Time)|Tail],[e(Client,Op,Qt,Time)|Tail2]):-findall(Op,op_prod_client(Op,_,_,Prod,_,_,_,_,_),Op),
										      replace_prod_for_op_list(Tail,Tail2).
% -Com a lista que recebe por parametro escreve dinamicamente uma tarefa
% para cada uma das estruturas que existem.
% -1� argumento -> lista com o formato [e(Cliente,Lista de
% operacoes,Q,T)]
% -2� argumento -> contador para numerar cada tarefa criada
cria_tarefa([],_):-!.
cria_tarefa([Head|Tail],Ni):-N is Ni+1,cria_tarefa2(Head,N),cria_tarefa(Tail,N).

%-Recebe uma estrutura de cada vez e cria uma tarefa para essa estrutura
% -1� argumento -> uma estrutura com o formato e(Cliente,Lista de
% operacoes,Q,T)
% -2� argumento -> o contador para numerar a tarefa a ser criada
cria_tarefa2(e(Client,Opl,Q,T),Ni):-get_bigger_op_time(Opl,0,Bigger_Texec,[],Op),
				    get_before_and_after_op(Opl,Op,Left,Right,0),
				    get_running_time(Q,Left,Right,Bigger_Texec,Running_time),
				    get_setup_time(Opl,Setup_time),
				    Process_time is Running_time - Setup_time,
				    atomic_concat(t,Ni,Tarefa),
				    prioridade_cliente(Client,Prioridade),
				    Prioridade2 is (9+Prioridade)/10,
					assertz(tarefa(Tarefa,Process_time,T,Prioridade2)),
					assertz(tarefa_op_list(Tarefa,Opl)).

% -Retorna a operacao com o maior tempo de processamento e o tempo de
% processamento respetivo.
% -1� argumento -> lista de operacoes com o formato [Op1,Op2,...]
% -2� argumento -> controlo que guarda o tempo de processamento maior
% ate ao momento
% -3� argumento -> retorna o maior tempo de processamento
% -4� argumento -> controlo que guarda a operacao com o maior tempo de
% processamento ate ao momento
% -5� argumento -> retorna a operacao com o maior tempo de processamento
get_bigger_op_time([],Time,Time,Op,Op).
get_bigger_op_time([Op|Tail],Bigger_time,Time,Aux,Op2):-op_prod_client(Op,_,_,_,_,_,_,_,Texec),
			            ((Texec>Bigger_time,!,Bigger_time2 is Texec,get_bigger_op_time(Tail,Bigger_time2,Time,Op,Op2));
				    get_bigger_op_time(Tail,Bigger_time,Time,Aux,Op2)).

% -Retorna todas as operacoes que estao antes e depois da que se recebe
% no segundo argumento
% -1� argumento -> lista de todas as operacoes com o segunte formato [Op1,Op2,...]
% -2� argumento -> operacao sobre a qual se encontram as anteriores e
% postriores
% -3� argumento -> lista de opracoes que se encontram antes da operacao
% que se encontra no 2� argumento que pode ser vazia ou ter o mesmo
% formato da lista do primeiro argumento
% -4� argumento -> lista de opracoes que se encontram depois da operacao
% que se encontra no 2� argumento que pode ser vazia ou ter o mesmo
% formato da lista do primeiro argumento
% -5� argumento -> numero entre 0 e 1 que controla se ja passamos pela
% operacao recebida como segundo parametro ao precorrer a lista do
% primeiro parametro
get_before_and_after_op([],_,[],[],_).
get_before_and_after_op([Op2|Tail],Op2,Left,Right,_):-Pass_op2 is 1, get_before_and_after_op(Tail,Op2,Left,Right,Pass_op2),!.
get_before_and_after_op([Op|Tail],Op2,[Op|Left],Right,Pass_op):-Pass_op == 0, get_before_and_after_op(Tail,Op2,Left,Right,Pass_op),!.
get_before_and_after_op([Op|Tail],Op2,Left,[Op|Right],Pass_op):-get_before_and_after_op(Tail,Op2,Left,Right,Pass_op).

% -Retorna o tempo de processamento da tarefa aplicando a formula de
% calculo que se encontra nos powerpoints da cadeira
% -1� argumento ->
% quantidade do mesmo produto a produzir
% -2� argumento -> lista com as
% operacoes a executar antes da com maior tempo
% -3� argumento -> lista com as
% operacoes a executar depois da com maior tempo
% -4� argumento -> tempo de processamento da operacao com o maior tempo
% -5� argumento -> retorna o tempo de processamento da tarefa
get_running_time(Q,Left,Right,Bigger_Texec,Running_time):-get_sum_Texe_each_op(Left,Texec_sum),
			                                  get_sum_Texe_each_op(Right,Texec_sum2),
							  Aux is Bigger_Texec*Q, Running_time is Aux + Texec_sum + Texec_sum2.

% -Retorna o somatorio do tempo de processamento de cada operacao da
% lista recebida no primeiro argumento
% -1� argumento -> lista com operacoes
% -2� argumento -> retorna o somatorio do tempo de processamento das
% operacoes da lista
get_sum_Texe_each_op([],0):-!.
get_sum_Texe_each_op([Op|Tail],Texec_sum):-op_prod_client(Op,_,_,_,_,_,_,_,Texec),
			                  get_sum_Texe_each_op(Tail,Texec_sum2),Texec_sum is Texec_sum2 + Texec.

% -Retorna o setup time a subtrair do running time 
% calculados aplicando a formula de calculo que se encontra nos
% powerpoints da cadeira
%-1� argumento -> lista de operacoes
% -2� argumento -> setup time a subtrair do running time
get_setup_time(Opl,Setup_time):-get_list_each_setup_time_negative(Opl,[],Setup_time_list),
			        sort_by_smallest_number(Setup_time_list,[op_Tsetup(_,Setup_time)|_]).

% -Calcula e retorna a lista de op_Tsetup(Op,Tsetup) usada pelo predicado anterior
% -1� argumento -> lista de operacoes
% -2� argumento -> lista que controla para que operacoes foi calculado o
% setup time
% -3� argumento -> lista de op_Tsetup(Op,Tsetup)
get_list_each_setup_time_negative([],_,[]).
get_list_each_setup_time_negative([Op|Tail],Visited,[op_Tsetup(Op,Setup_time)|Setup_time_list]):-
							is_empty(Visited),op_prod_client(Op,_,_,_,_,_,_,Tsetup,_),
							Setup_time is 0 - Tsetup,append([Visited],Op,Visited2),
							flatten(Visited2,Visited3),
							get_list_each_setup_time_negative(Tail,Visited3,Setup_time_list),!.
get_list_each_setup_time_negative([Op|Tail],Visited,[op_Tsetup(Op,Setup_time)|Setup_time_list]):-
							last(Visited,Op2),
							op_prod_client(Op,_,F,_,_,_,_,Tsetup,_),
							op_prod_client(Op2,_,F2,_,_,_,_,_,_),
							((F == F2,!,Setup_time is 0);
							get_sum_Texe_each_op(Visited,Texec),
							Setup_time is Texec - Tsetup),
							append([Visited],Op,Visited2),
							flatten(Visited2,Visited3),
							get_list_each_setup_time_negative(Tail,Visited3,Setup_time_list).

% -Verifica se uma lista e vazia ou nao, retornando true caso o seja
is_empty([]):-true.
is_empty([_|_]):-false.

% -Ordena uma lista por menor tempo de setup
% -1� argumento -> lista de setup times
% -2� argumento -> lista de setup times ordenada
sort_by_smallest_number([],[]).
sort_by_smallest_number([op_Tsetup(Op,Tsetup)|Tail],Tsetup_list):-split(op_Tsetup(Op,Tsetup),Tail,Left,Right),
				sort_by_smallest_number(Left,Left1),
				sort_by_smallest_number(Right,Right1),
				append(Left1,[op_Tsetup(Op,Tsetup)|Right1],Tsetup_list).

% -Predicado auxiliar do sort_by_smallest_number que retorna todos os
% setup times menores e maiores ao recebido no primeiro argumento
% -1� argumento -> tempo de setup a comparar
% -2� argumento -> lista de tempos de setup a comparar
% -3� argumento -> lista de tempos de setup menores que o do primeiro
% argumento
% -4� argumento -> lista de tempos de setup maiores que o do primeiro
% argumento
split(_,[],[],[]).
split(op_Tsetup(Op,Tsetup),[op_Tsetup(Op1,Tsetup1)|Tail],[op_Tsetup(Op1,Tsetup1)|Left],Right):-
													Tsetup>Tsetup1,!,split(op_Tsetup(Op,Tsetup),Tail,Left,Right).
split(op_Tsetup(Op,Tsetup),[op_Tsetup(Op1,Tsetup1)|Tail],Left,[op_Tsetup(Op1,Tsetup1)|Right]):-
													split(op_Tsetup(Op,Tsetup),Tail,Left,Right).

%--------------------------------------------- agenda ---------------------------------------------
% -Guarda dinamicamente a informacao
nova_agenda_para_cada_maq(_*Tarefas):-
	lista_op_tarefa(Tarefas, Tarefa_L_Op2),
	flatten(Tarefa_L_Op2, Tarefa_L_Op),
	nova_agenda_para_cada_maq2(Tarefa_L_Op),
	clear_setup_e_shift,!.

% -A partir da tarefa vai obter todas as operacoes
lista_op_tarefa([], []):-!.
lista_op_tarefa([Tarefa|Tail], [Tarefa_L_Op|Tail2]):-
	findall(t_opl(Tarefa,Opl),tarefa_op_list(Tarefa,Opl),Tarefa_L_Op),
	lista_op_tarefa(Tail, Tail2).

% -Predicado auxiliar ao nova_agenda_para_cada_maq que invoca
% o predicado preenche_agenda_maquina onde a informacao
% e guardada automaticamente
% -argumento 1, Lista de todas as tarefas e sua lista de operacoes
% com a estrutura [t_op(tarefa,listaOp),...]
nova_agenda_para_cada_maq2([]):-!.
nova_agenda_para_cada_maq2([Head|Tail]):-
			preenche_agenda_maquina(Head),
			nova_agenda_para_cada_maq2(Tail).

% -Invoca variados predicados de modo a criar uma lista estruturas
% trio([m_op(m,op),m_op(m,op),m_op(m,op)],[tset,tset,tset],[texec,texec,texec],numero])
% para cada uma das tarefas e de seguida usa-las para guardar 
% dinamicamente a informacao contida no predicado dinamico agendas_maq/2
% -argumento 1, t_opl(Tarefa,ListaOp)
preenche_agenda_maquina(t_opl(T,Opl)):-
	obter_sequencia_maquinas(Opl,Seq_maquinas),
	maquinaEsquerda_maquina_maquinaDireita(Seq_maquinas,Seq_maquinas,Opl,Opl,Machine_in_triples),
	setup_e_exec_maquinas_na_estrutura(Machine_in_triples,[],Machine_in_triples_with_times),
	encontrar_primeiro_setup_op(Opl,First_Setup_Op),
	mudar_maquina_na_estrutura_com_num(Machine_in_triples_with_times,First_Setup_Op,Machine_Trio_Correct_Numbers,0),
	guarda_agenda_maquina(Machine_Trio_Correct_Numbers,T).

% -Recebe duas listas de maquinas e operacoes
% e retorna uma lista com o seguinte formato [[m_op(m,op),m_op(m,op),m_op(m,op)],...]
% cada trio tem no centro a maquina e operacao pretendida, a esquerda o anterior 
% e a direita o seguinte, caso seja o primerio ou o ultimo o lado onde nao existe
% operacao sera substituido por [no]
% -argumento 1, lista de maquinas
% -argumento 2, lista de maquinas
% -argumento 3, lista de operacoes
% -argumento 4, lista de operacoes
% -argumento 5, lista com o seguinte formato [[m_op(m,op),m_op(m,op),m_op(m,op)],...]
maquinaEsquerda_maquina_maquinaDireita([],_,[],_,[]):-!.
maquinaEsquerda_maquina_maquinaDireita([Head|Tail],Seq_maquinas,[Head2|Tail2],Opl,[Triple|Tail3]):-
	maquinaEsquerda_maquina_maquinaDireita2(Head,Seq_maquinas,Head2,Opl,Triple),
	maquinaEsquerda_maquina_maquinaDireita(Tail,Seq_maquinas,Tail2,Opl,Tail3).

% -Predicado auxiliar ao maquinaEsquerda_maquina_maquinaDireita
% que retorna cada um dos trios de maquinas
% -argumento 1, maquina centrar
% -argumento 2, lista de maquinas
% -argumento 3, operacao centrar
% -argumento 4, lista de operacoes
% -argumento 5, trio com o formato [m_op(m,op),m_op(m,op),m_op(m,op)]
maquinaEsquerda_maquina_maquinaDireita2(Machine,Seq_maquinas,Op,Opl,Triple):-
	maquina_esquerda_e_direita(Machine,Seq_maquinas,Op,Opl,Before,After,0),
	((is_empty(Before),primeira_maquina(After,Machine2),append([no],[m_op(Machine,Op)],Aux),append(Aux,[Machine2],Triple));
	last(Before,Machine1),append([Machine1],[m_op(Machine,Op)],Aux),
	primeira_maquina(After,Machine2),
	append(Aux,[Machine2],Triple)),!.

% -Predicado que retorna as maquinas e operacoes anteriores e seguintes
% a uma especifica maquina e operacao
% -argumento 1, maquina a descobrir as anteriores e postriores
% -argumento 2, lista de maquinas
% -argumento 3, operacao a descobrir as anteriores e postriores
% -argumento 4, lista de operacoes
% -argumento 5, lista maquinas e operacoes anteriores 
% com a estrutura m_op(m,op)
% -argumento 6, lista maquinas e operacoes postriores 
% com a estrutura m_op(m,op)
% -argumento 7, numero para controlar a passagem pela maquina e op
% pretendidas
maquina_esquerda_e_direita(_,[],_,[],[],[],_):-!.
maquina_esquerda_e_direita(Machine,[Machine|Tail],Op,[Op|Tail2],Before,After,_):-
	Pass_machine2 is 1,
	maquina_esquerda_e_direita(Machine,Tail,Op,Tail2,Before,After,Pass_machine2),!.
maquina_esquerda_e_direita(Machine,[Machine1|Tail],Op,[Op1|Tail2],[m_op(Machine1,Op1)|Tail3],After,Pass_machine):-
	Pass_machine == 0,
	maquina_esquerda_e_direita(Machine,Tail,Op,Tail2,Tail3,After,Pass_machine),!.
maquina_esquerda_e_direita(Machine,[Machine2|Tail],Op,[Op2|Tail2],Before,[m_op(Machine2,Op2)|Tail3],Pass_machine):-
	maquina_esquerda_e_direita(Machine,Tail,Op,Tail2,Before,Tail3,Pass_machine),!.

primeira_maquina([],no):-!.
primeira_maquina([m_op(Machine,Op)|_],m_op(Machine,Op)):-!.

% -Revcebe os trios de maquinas e cria uma lista de estruturas
% trio([m_op(m,op),m_op(m,op),m_op(m,op)],[tset,tset,tset],[texec,texec,texec],2])
% -argumento 1, Lista de trios de maquinas
% -argumento 2, Lista auxiliar
% -argumento 3, lista de estruturas
% trio([m_op(m,op),m_op(m,op),m_op(m,op)],[tset,tset,tset],[texec,texec,texec],2])
setup_e_exec_maquinas_na_estrutura([],Machine_in_triples_with_times,Machine_in_triples_with_times).
setup_e_exec_maquinas_na_estrutura([Head|Tail],Aux,Machine_in_triples_with_times):-
	tempo_setup_maquinas_na_estrutura(Head,Setup_time),
	tempo_exec_maquina_estrtura(Head,Exec_time),
	append(Aux,[trio(Head,Setup_time,Exec_time,2)],Aux2),
	setup_e_exec_maquinas_na_estrutura(Tail,Aux2,Machine_in_triples_with_times).

% -Retorna uma lista com os 3 setup times para cada maquina no trio
% -argumento 1, trio de maquinas
% -argumento 2, lista com os 3 setup times para cada maquina no trio
tempo_setup_maquinas_na_estrutura([],[]):-!.
tempo_setup_maquinas_na_estrutura([no|Tail],[0|Tail2]):-tempo_setup_maquinas_na_estrutura(Tail,Tail2),!.
tempo_setup_maquinas_na_estrutura([Head|Tail],[Setup_time|Tail2]):-
					tempo_setup_maquinas_na_estrutura2(Head,Setup_time),
					tempo_setup_maquinas_na_estrutura(Tail,Tail2).

% -Predicado auxiliar ao tempo_setup_maquinas_na_estrutura
% que retorna o setup time para cada maquina
% -argumento 1, m_op(maquina,operacao)
% -argumento 2, setup time da maquina recebida por argumento
tempo_setup_maquinas_na_estrutura2(m_op(Machine,Op),Setup_time):-
				op_prod_client(Op,Machine,_,_,_,_,_,Setup_time,_).

% -Retorna uma lista com os 3 exec times para cada maquina no trio
% -argumento 1, trio de maquinas
% -argumento 2, lista com os 3 exec times para cada maquina no trio
tempo_exec_maquina_estrtura([],[]):-!.
tempo_exec_maquina_estrtura([no|Tail],[0|Tail2]):-tempo_exec_maquina_estrtura(Tail,Tail2),!.
tempo_exec_maquina_estrtura([Head|Tail],[Exec_time|Tail2]):-
					tempo_exec_maquina_estrtura2(Head,Exec_time),
					tempo_exec_maquina_estrtura(Tail,Tail2).

% -Predicado auxiliar ao tempo_exec_maquina_estrtura
% que retorna o exec time para cada maquina
% -argumento 1, m_op(maquina,operacao)
% -argumento 2, exec time da maquina recebida por argumento
tempo_exec_maquina_estrtura2(m_op(Machine,Op),Exec_time):-
				op_prod_client(Op,Machine,_,_,_,_,_,_,Exec_time).

% -Recebe uma lista de operacoes e retorna a que tem
% de realizar o setup primeiro
% -argumento 1, lista de operacoes
% -argumento 2, operacao que tem de realizar o setup primeiro
encontrar_primeiro_setup_op(Opl,First_Setup_Op):-
	get_list_each_setup_time_negative(Opl,[],Setup_time_list),
	sort_by_smallest_number(Setup_time_list,[op_Tsetup(First_Setup_Op,_)|_]).	

% -Recebe as estruturas trio(...) e mudifica numero final para -1, 0 ou 1
% dependendo se a operacao associado a maquina centrar faz o setup primeiro(0),
% se a operacao centrar se encontra primeiro, na sequencia , que a que realiaza
% o setup primeiro(-1), ou se se encontra depois (1)
% -argumento 1, lista de estruturas trio(...)
% -argumento 2, operacao que realiza o setup primeiro
% -argumento 3, lista de estruturas trio(...) com o utlimo numero correto
% -argumento 4, numero de controlo para verificar a passagem pela estrutura
% trio(...) com o numero 0
mudar_maquina_na_estrutura_com_num([],_,[],_):-!.
mudar_maquina_na_estrutura_com_num([trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,_)|Tail],
								First_Setup_Op,[trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,0)|Tail2],_):-
	Pass_First_Setup_Op2 is 1,
	mudar_maquina_na_estrutura_com_num(Tail,First_Setup_Op,Tail2,Pass_First_Setup_Op2),!.
mudar_maquina_na_estrutura_com_num([trio([Left,m_op(Machine,First_Setup_Op2),Right],Tsetup,Texec,_)|Tail],
						First_Setup_Op,[trio([Left,m_op(Machine,First_Setup_Op2),Right],Tsetup,Texec,-1)|Tail2],Pass_First_Setup_Op):-
	Pass_First_Setup_Op == 0,
	mudar_maquina_na_estrutura_com_num(Tail,First_Setup_Op,Tail2,Pass_First_Setup_Op),!.
mudar_maquina_na_estrutura_com_num([trio([Left,m_op(Machine,First_Setup_Op2),Right],Tsetup,Texec,_)|Tail],
						First_Setup_Op,[trio([Left,m_op(Machine,First_Setup_Op2),Right],Tsetup,Texec,1)|Tail2],Pass_First_Setup_Op):-
	mudar_maquina_na_estrutura_com_num(Tail,First_Setup_Op,Tail2,Pass_First_Setup_Op),!.

% -Guarda a informacao dinamicamente das estruturas trio(...)
% -argumento 1, lista de estruturas trio(...)
% -argumento 2, tarefa
guarda_agenda_maquina(Machine_Trio_Correct_Numbers,T):-
	separate_op_zero_op_nega_op_posi(Machine_Trio_Correct_Numbers,Machine_Trio_Zero,Machine_Trio_Nega,Machine_Trio_Posi),
	guarda_agenda_maquina_zero(Machine_Trio_Zero,T),
	reverse(Machine_Trio_Nega,Machine_Trio_Nega2),
	guarda_agenda_maquina_nega(Machine_Trio_Nega2,T),
	guarda_agenda_maquina_posi(Machine_Trio_Posi,T).

% -Separa as estruturas trio(...) que terminam em -1, 0 e 1
% -argumento 1, lista de estruturas trio(...)
% -argumento 2, lista de estruturas trio(...,0)
% -argumento 3, lista de estruturas trio(...,-1)
% -argumento 4, lista de estruturas trio(...,1)
separate_op_zero_op_nega_op_posi([],[],[],[]):-!.
separate_op_zero_op_nega_op_posi([trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,0)|Tail],
									[trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,0)|Tail2],
									Machine_Trio_Nega,Machine_Trio_Posi):-
	separate_op_zero_op_nega_op_posi(Tail,Tail2,Machine_Trio_Nega,Machine_Trio_Posi),!.
separate_op_zero_op_nega_op_posi([trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,-1)|Tail],
									Machine_Trio_Zero,
									[trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,-1)|Tail2],
									Machine_Trio_Posi):-
	separate_op_zero_op_nega_op_posi(Tail,Machine_Trio_Zero,Tail2,Machine_Trio_Posi),!.
separate_op_zero_op_nega_op_posi([trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,1)|Tail],
									Machine_Trio_Zero,
									Machine_Trio_Nega,
									[trio([Left,m_op(Machine,First_Setup_Op),Right],Tsetup,Texec,1)|Tail2]):-
	separate_op_zero_op_nega_op_posi(Tail,Machine_Trio_Zero,Machine_Trio_Nega,Tail2),!.

% -Guarda dinamicamente a informacao sobre a estrutura trio(...)
% que termina em 0
% argumento 1, estrutura trio(...,0)
% argumento 2, tarefa
guarda_agenda_maquina_zero([trio([_,m_op(Machine,Op),_],[_,Tsetup,_],[_,Texec,_],_)],Tarefa):-
	op_prod_client(Op,Machine,F,Prod,Client,Qt,_,Tsetup,Texec),
	((agenda_maq(Machine,List),!,retract(agenda_maq(Machine,List)),!,
		lastTarefa(List,[],T),
		tarefa(T,Process_time,_,_),
		Tsetup2 is Process_time + Tsetup,
		Aux is Texec * Qt,
		Tend is Aux + Tsetup2,
		append(List,[t(Process_time,Tsetup2,setup,[F]),t(Tsetup2,Tend,exec,[Op,Qt,Prod,Client,Tarefa])],List2),
		assertz(agenda_maq(Machine,List2)));
	Aux is Texec * Qt,
	Tend is Aux + Tsetup,
	assertz(agenda_maq(Machine,[t(0,Tsetup,setup,[F]),t(Tsetup,Tend,exec,[Op,Qt,Prod,Client,Tarefa])]))).

% -Retorna a ultima tarefa da lista recebida no primeiro argumento
% -arguemtno 1, lista com o formato 
% [t(_,_,_,_), t(_,_,_,[_,_,_,_,tarefa]),...]
% -argumento 2, tarefa auxiliar
% -argumento 3, ultima tarefa da lista
lastTarefa([],T,T):-!.
lastTarefa([t(_,_,_,[_,_,_,_,TAux])|Tail],_,T):-
	!,lastTarefa(Tail,TAux,T).
lastTarefa([_|Tail],TAux,T):-
	lastTarefa(Tail,TAux,T).

% -Guarda dinamicamente a informacao sobre as estruturas trio(...)
% que terminam em -1
% argumento 1, lista de estruturas trio(...,-1)
% argumento 2, tarefa
guarda_agenda_maquina_nega([],_):-!.
guarda_agenda_maquina_nega([Head|Tail],T):-
	guarda_agenda_maquina_nega2(Head,T),
	guarda_agenda_maquina_nega(Tail,T).

% -Predicado auxiliar ao guarda_agenda_maquina_nega
% que guarda cada uma das estruturas trio(...,-1)
guarda_agenda_maquina_nega2(trio([_,m_op(Machine,Op),m_op(Machine2,Op2)],[_,Tsetup,_],[_,Texec,_],_),Tarefa):-
	agenda_maq(Machine2,List3),
	get_before_pair(List3,Op2,Before_pair_empty_lists),
	flatten(Before_pair_empty_lists,Before_pair),
	op_prod_client(Op,Machine,F,Prod,Client,Qt,_,Tsetup,Texec),
	calculate_Tini_for_nega(Before_pair,Tsetup,Texec,Tini),
	Tsetup3 is Tini + Tsetup,
	Aux is Texec * Qt,
	Tend is Aux + Tsetup3,
	((agenda_maq(Machine,List),!,retract(agenda_maq(Machine,List)),!,
		append(List,[t(Tini,Tsetup3,setup,[F]),t(Tsetup3,Tend,exec,[Op,Qt,Prod,Client,Tarefa])],List2),
		assertz(agenda_maq(Machine,List2)));
	assertz(agenda_maq(Machine,[t(Tini,Tsetup3,setup,[F]),t(Tsetup3,Tend,exec,[Op,Qt,Prod,Client,Tarefa])]))).

% -Retorna o par da lista t(Tini,Tsetup,Palava_setup,[F]),t(Tsetup,Tend,Palava_exec,[Op,Qt,Prod,Client,Tarefa]),...]
% com a operacao recebida no argumento 2
% -argumento 1, lista t(Tini,Tsetup,Palava_setup,[F]),t(Tsetup,Tend,Palava_exec,[Op,Qt,Prod,Client,Tarefa]),...]
% -argumento 2, operacao do par a retornar
% -argumento 3, par retornado 
get_before_pair([t(Tini,Tsetup,Palava_setup,[F]),t(Tsetup,Tend,Palava_exec,[Op,Qt,Prod,Client,Tarefa])|_],Op,
								[t(Tini,Tsetup,Palava_setup,[F]),t(Tsetup,Tend,Palava_exec,[Op,Qt,Prod,Client,Tarefa])]):-!.
get_before_pair([_|Tail],Op,
								[[]|Tail2]):-get_before_pair(Tail,Op,Tail2).

% -Calcula quando e que o setup vao comecar a ser executado
% para as estruturar trio(...,-1)
% -argumento 1, par anterior guardado
% -argumento 2, setup time da maquina a guardar
% -argumento 3, exec time da maquina a guardar
% -argumento 4, tempo em que o setup da maquina se ira iniciar
calculate_Tini_for_nega([t(_,Tsetup,_,[_]),t(_,_,_,[_,_,_,_,_])],Tsetup2,Texec2,Tini):-
	Aux is Tsetup2 + Texec2,
	Tini is Tsetup - Aux.

% -Guarda dinamicamente a informacao sobre as estruturas trio(...)
% que terminam em 1
% argumento 1, lista de estruturas trio(...,1)
% argumento 2, tarefa
guarda_agenda_maquina_posi([],_):-!.
guarda_agenda_maquina_posi([Head|Tail],T):-
	guarda_agenda_maquina_posi2(Head,T),
	guarda_agenda_maquina_posi(Tail,T).

% -Predicado auxiliar ao guarda_agenda_maquina_nega
% que guarda cada uma das estruturas trio(...,1)
guarda_agenda_maquina_posi2(trio([m_op(Machine2,Op2),m_op(Machine,Op),_],[_,Tsetup,_],[_,Texec,_],_),Tarefa):-
	agenda_maq(Machine2,List3),
	get_before_pair(List3,Op2,Before_pair_empty_lists),
	flatten(Before_pair_empty_lists,Before_pair),
	op_prod_client(Op,Machine,F,Prod,Client,Qt,_,Tsetup,Texec),
	calculate_Tini_for_posi(Before_pair,Tsetup,Tini),
	Tsetup3 is Tini + Tsetup,
	Aux is Texec * Qt,
	Tend is Aux + Tsetup3,
	((agenda_maq(Machine,List),!,retract(agenda_maq(Machine,List)),!,
		append(List,[t(Tini,Tsetup3,setup,[F]),t(Tsetup3,Tend,exec,[Op,Qt,Prod,Client,Tarefa])],List2),
		assertz(agenda_maq(Machine,List2)));
	assertz(agenda_maq(Machine,[t(Tini,Tsetup3,setup,[F]),t(Tsetup3,Tend,exec,[Op,Qt,Prod,Client,Tarefa])]))).

% -Calcula quando e que o setup vao comecar a ser executado
% para as estruturar trio(...,1)
% -argumento 1, par anterior guardado
% -argumento 2, setup time da maquina a guardar
% -argumento 3, exec time da maquina a guardar
% -argumento 4, tempo em que o setup da maquina se ira iniciar
calculate_Tini_for_posi([t(_,Tsetup,_,[_]),t(_,Texec,_,[_,Qt,_,_,_])],Tsetup2,Tini):-
	Aux is Texec - Tsetup,
	Aux2 is Aux/Qt,
	Aux3 is Aux2 + Tsetup,
	Tini is Aux3 - Tsetup2.

% -Remove setups desnecessarios da agenda de cada maquina e volta
% a guardar a agenda dinamicamente
clear_setup_e_shift:-
	findall(maq_ag(M,List),agenda_maq(M,List),Agendas),
	remove_same_tool_setup(Agendas,Agendas2),
	get_tarefas_already_saved(Agendas2,[],Tarefas),
	left_shift(Agendas2,Tarefas,Agendas3),
	retractall(agenda_maq(_,_)),
	resave_with_shift_and_without_same_tool_setup(Agendas3).

% -Remove setups desnecessarios da agenda de cada maquina
% e retorna as listas sem eles
% -argumento 1, lista de estruturas maq_ag(maquina, agenda)
% -argumento 2, lista de estruturas maq_ag(maquina, agenda)
% onde a agenda nao tem setups desnecessarios
remove_same_tool_setup([],[]):-!.
remove_same_tool_setup([Head|Tail],[Agenda|Tail2]):-
	remove_same_tool_setup2(Head,Agenda),
	remove_same_tool_setup(Tail,Tail2).

% -Predicado auxiliar ao remove_same_tool_setup
% que recebe cada uma das estruturas maq_ag(maquina, agenda)
% em vez de uma lista
remove_same_tool_setup2(maq_ag(M, Agenda),maq_ag(M,Agenda2)):-
	remove_setups(Agenda, [], Agenda2).

% -Remove os setups com a mesma ferramente seguidos
% retornando a nova lista sem eles
% argumento 1, lista com setups inuteis
% argumento 2, ferramenta atual
% argumento 3, lista sem setups inuteis
remove_setups([],_,[]):-!.
remove_setups([t(Tini,Tsetup,Palavra_setup,[Ferramenta_atual])|Tail], [], 
						[t(Tini,Tsetup,Palavra_setup,[Ferramenta_atual])|Tail2]):-
	!,remove_setups(Tail, [Ferramenta_atual], Tail2).
remove_setups([t(_,_,_,[Ferramenta_atual])|Tail],[Ferramenta_atual],Tail2):-
	!,remove_setups(Tail, [Ferramenta_atual], Tail2).
remove_setups([t(Tini,Tsetup,Palavra_setup,[Ferramenta_atual])|Tail], [_], 
						[t(Tini,Tsetup,Palavra_setup,[Ferramenta_atual])|Tail2]):-
	!,remove_setups(Tail, [Ferramenta_atual], Tail2).
remove_setups([Head|Tail],[Ferramenta_atual],[Head|Tail2]):-
	!,remove_setups(Tail, [Ferramenta_atual], Tail2).

get_tarefas_already_saved([],Tarefas,Tarefas).
get_tarefas_already_saved([maq_ag(_,List)|Tail],Aux,Tarefas):-
	get_tarefas_already_saved2(List,Aux,Tarefas2),
	append(Aux,[Tarefas2],Aux2),
	flatten(Aux2,Aux3),
	get_tarefas_already_saved(Tail,Aux3,Tarefas).

get_tarefas_already_saved2([],_,[]):-!.
get_tarefas_already_saved2([t(_,_,_,[_,_,_,_,T])|Tail],Aux,[T|Tarefas]):-
	not(member(T,Aux)),append(Aux,[T],Aux2),get_tarefas_already_saved2(Tail,Aux2,Tarefas).
get_tarefas_already_saved2([_|Tail],Aux,Tarefas):-
	get_tarefas_already_saved2(Tail,Aux,Tarefas).


% -Da shift das agendas de cada maquina, caso possivel,
% de modo a ocupar cada maquina o menor tempo possivel
% -argumento 1, lista de estruturas maq_ag(m,ag)
% -argumento 2, lista de estruturas maq_ag(m,ag) shifted
left_shift(Agendas2,Tarefas,Agendas3):-
	separa_agenda_por_tarefa(Agendas2,Tarefas,Agendas_por_tarefa),
	shift(Agendas_por_tarefa,[],Agendas_por_tarefa_shifted),
	group_same_machine_agendas(Agendas_por_tarefa_shifted,[],Agendas3).



% -Separa as agendas de cada maquina pelas tarefas a que correspondem
% -argumento 1, Lista de estruturas maq_ag(m,ag)
% -argumento 2, Lista de tarefas
% -argumento 3, Lista de estruturas t_m_a(tarefa,[maq_ag(m,ag),...])
separa_agenda_por_tarefa(_,[],[]):-!.
separa_agenda_por_tarefa(Agendas,[T|Tail],[t_m_a(T,Agendas_por_tarefa)|Tail2]):-
	get_agenda_por_tarefa(Agendas,T,Agendas_por_tarefa),
	not(is_empty(Agendas_por_tarefa)),
	separa_agenda_por_tarefa(Agendas,Tail,Tail2).
separa_agenda_por_tarefa(Agendas,[_|Tail],Tail2):-
	separa_agenda_por_tarefa(Agendas,Tail,Tail2).

% -Retorna a agenda de uma maquina separada por uma tarefa recebida
% -argumento 1, lista de estruturas maq_ag(m,ag)
% -argumento 2, tarefa
% -argumento 3, lista de estruturas maq_ag(m,ag), onde ag pretence
% a tarefa do segundo argumento
get_agenda_por_tarefa([],_,[]):-!.
get_agenda_por_tarefa([maq_ag(M,List)|Tail],T,[maq_ag(M,List1)|Tail2]):-
	get_agenda_por_tarefa2(M,List,T,[],maq_ag(M,List1)),
	not(is_empty(List1)),
	get_agenda_por_tarefa(Tail,T,Tail2).
get_agenda_por_tarefa([_|Tail],T,Tail2):-
	get_agenda_por_tarefa(Tail,T,Tail2).

% -Retorna a parte da agenda da maquina recebida que pertence a tarefa
% recebida
% -argumento 1, maquina cuja a agenda foi recebida
% -argumento 2, agenda da maquina recebida
% -argumento 3, tarefa
% -argumento 4, lista auxiliar
% -argumento 5, estrutura maq_ag(m,ag), onde ag e apenas a parte da 
% agenda que pertence a tarefa recebida
get_agenda_por_tarefa2(M,[],_,Aux,maq_ag(M,Aux)):-!.
get_agenda_por_tarefa2(M,[t(Tini,Tsetup,Palavra_setup,Ferramenta), t(Tsetup,Tend,Palavra_exec,[Op,Qt,Prod,Client,T])|Tail],
																									T,Aux,Agendas_por_tarefa):-!,
			append(Aux,[t(Tini,Tsetup,Palavra_setup,Ferramenta), t(Tsetup,Tend,Palavra_exec,[Op,Qt,Prod,Client,T])],Aux2),
			get_agenda_por_tarefa2(M,Tail,T,Aux2,Agendas_por_tarefa).
get_agenda_por_tarefa2(M,[t(Tsetup,Tend,Palavra_exec,[Op,Qt,Prod,Client,T])|Tail],
															T,Aux,Agendas_por_tarefa):-!,
			append(Aux,[t(Tsetup,Tend,Palavra_exec,[Op,Qt,Prod,Client,T])],Aux2),
			get_agenda_por_tarefa2(M,Tail,T,Aux2,Agendas_por_tarefa).
get_agenda_por_tarefa2(M,[_|Tail],T,Aux,Agendas_por_tarefa):-get_agenda_por_tarefa2(M,Tail,T,Aux,Agendas_por_tarefa).

% -Aplica um shift a esquerda calculado como explicado nos powerpoints
% -argumento 1, Lista de estruturas t_m_a(tarefa,[maq_ag(m,ag),...])
% -argumento 2, lista auxiliar
% -argumento 3, Lista de estruturas t_m_a(tarefa,[maq_ag(m,ag),...])
% com o shift aplicado
shift([Head],Aux,Agendas_por_tarefa_shifted):-!,append(Aux,[Head],Agendas_por_tarefa_shifted).
shift([Head,Head2|Tail],Aux,Agendas_por_tarefa_shifted):-
	calculate_and_applicate_shift(Head,Head2,Head2_Shifted,Shift_applicated),
	applicate_shift_to_the_rest(Tail,Shift_applicated,Tail_Shifted),
	append(Aux,[Head],Aux2),
	shift([Head2_Shifted|Tail_Shifted],Aux2,Agendas_por_tarefa_shifted).

% -Calcula e aplica o shift possivel entre duas tarefas
% -argumento 1, estrutura t_m_a(tarefa,[maq_ag(m,ag),...])
% -argumento 2, estrutura t_m_a(tarefa,[maq_ag(m,ag),...])
% -arguemnto 3, estrutura t_m_a(tarefa,[maq_ag(m,ag),...]) shifted
% -argumento 4, shift usado
calculate_and_applicate_shift(t_m_a(_,Maquina_Agendas),t_m_a(T,Maquina_Agendas2),t_m_a(T,Maquina_Agendas2_Shifted),Shift_applicated):-
	get_all_possible_shifts(Maquina_Agendas,Maquina_Agendas2,All_possible_shifts),
	sort(All_possible_shifts,[Shift_applicated|_]),
	apply_shift_single(Maquina_Agendas2,Shift_applicated,Maquina_Agendas2_Shifted).


% -Retorna uma lista com todos os shifts possiveis entre duas tarefas
% -argumento 1, lista de estruturas maq_ag(m,ag) de uma tarefa
% -argumento 2, lista de estruturas maq_ag(m,ag) de uma outra tarefa
% -argumento 3, lista com todos os shifts possiveis
get_all_possible_shifts([],_,[]):-!.
get_all_possible_shifts([maq_ag(M,List)|Tail],Maquina_Agendas2,[Shift|Tail2]):-
	find_shift(M,List,Maquina_Agendas2,Shift),
	get_all_possible_shifts(Tail,Maquina_Agendas2,Tail2).

% -Retorna o shift calculado para a maquina recebida entre duas tarefas
% -argumento 1, maquina
% -argumento 2, agenda da maquina do primerio argumento
% -argumento 3, lista de estruturas maq_ag(m,ag) de outra tarefa
% -argumento 4, shift calculado
find_shift(_,_,[],0):-!.
find_shift(M,List,[maq_ag(M,[Head|_])|_],Shift):-!,
	lastTend(List,0,LastTend),
	find_tsetup(Head,FirstTsetup),
	Shift is FirstTsetup - LastTend.
find_shift(M,List,[_|Tail],Shift):-
	find_shift(M,List,Tail,Shift).

% -Retorna o tempo de fim do ultimo elemento da lista
% recebida como argumento
lastTend([],LastTend,LastTend):-!.
lastTend([t(_,LastTendAux,_,[_,_,_,_,_])|Tail],_,LastTend):-
	!,lastTend(Tail,LastTendAux,LastTend).
lastTend([_|Tail],LastTendAux,LastTend):-
	lastTend(Tail,LastTendAux,LastTend).

% -Retorna o tempo de inicio de trabalho do elemento 
% recebido como argumento
find_tsetup(t(FirstTsetup,_,_,_),FirstTsetup):-!.

% -Aplica o shift a todas as agendas de maquinas de uma tarefa
% -argumento 1, Lista de estruturas maq_ag(m,ag)
% -argumento 2, shift a aplicar
% -argumento 3, list de estruturas maq_ag(m,ag) shifted
apply_shift_single([],_,[]):-!.
apply_shift_single([maq_ag(M,List)|Tail],Shift_applicated,[maq_ag(M,List_shifted)|Tail2]):-
	apply_shift_single2(List,Shift_applicated,List_shifted),
	apply_shift_single(Tail,Shift_applicated,Tail2).

% -Aplica o shift a todas a uma agenda de uma maquina
% -argumento 1, agenda de uma maquina
% -argumento 2, shift a aplicar
% -argumento 3, agenda de uma maquina shifted
apply_shift_single2([],_,[]):-!.
apply_shift_single2([t(Tempo,Tempo2,Nao,Importa)|Tail],Shift_applicated,[t(Tempo_shifted,Tempo2_shifted,Nao,Importa)|Tail2]):-
	Tempo_shifted is Tempo - Shift_applicated,
	Tempo2_shifted is Tempo2 - Shift_applicated,
	apply_shift_single2(Tail,Shift_applicated,Tail2).

% -Aplica o shift a todas as agendas de maquinas de varias tarefas
% -argumento 1, Lista de estruturas t_m_a(Tarefa,[maq_ag(m,ag),...])
% -argumento 2, shift a aplicar
% -argumento 3, Lista de estruturas t_m_a(Tarefa,[maq_ag(m,ag),...]) shifted
applicate_shift_to_the_rest([],_,[]):-!.
applicate_shift_to_the_rest([t_m_a(T,Maquina_Agendas)|Tail],Shift_applicated,[t_m_a(T,Maquina_Agendas_Shifted)|Tail2]):-
	apply_shift_single(Maquina_Agendas,Shift_applicated,Maquina_Agendas_Shifted),
	applicate_shift_to_the_rest(Tail,Shift_applicated,Tail2).

% -Recebe agendas agrupadas por tarefas e retorna agendas agrupadas por maquinas
% -argumento 1, Lista de estruturas t_m_a(Tarefa,[maq_ag(m,ag),...])
% -argumento 2, Lista auxiliar
% -argumento 3, Lista de estruturas maq_ag(m,ag)
group_same_machine_agendas([],Agendas3,Agendas3):-!.
group_same_machine_agendas([t_m_a(_,Maquina_Agendas)|Tail],[],Agendas3):-!,
	group_same_machine_agendas(Tail,Maquina_Agendas,Agendas3).
group_same_machine_agendas([t_m_a(_,Maquina_Agendas)|Tail],Agendas3Aux,Agendas3):-
	add_agendas_to_the_existing_ones(Maquina_Agendas,Agendas3Aux,Agendas3Aux2),
	group_same_machine_agendas(Tail,Agendas3Aux2,Agendas3).

% -Junta as agendas das mesmas maquinas
% -argumento 1, Lista de estruturas maq_ag(m,ag)
% -argumento 2, Lista auxiliar
% -argumento 3, Lista de estruturas maq_ag(m,ag)
% com as agendas de maquinas iguais juntas
add_agendas_to_the_existing_ones([],Agendas3,Agendas3):-!.
add_agendas_to_the_existing_ones([maq_ag(M,List)|Tail],Agendas3Aux,Agendas3):-
	((machine_is_in_the_list(M,Agendas3Aux),!,
		add_agendas_to_the_existing_ones2(M,List,Agendas3Aux,Agendas3Aux2));
	append(Agendas3Aux,[maq_ag(M,List)],Agendas3Aux2)),
	add_agendas_to_the_existing_ones(Tail,Agendas3Aux2,Agendas3).

% -Predicado auxiliar ao add_agendas_to_the_existing_ones
add_agendas_to_the_existing_ones2(_,_,[],[]):-!.
add_agendas_to_the_existing_ones2(M,List,[maq_ag(M,List1)|Tail],[maq_ag(M,List2)|Tail2]):-
	!,append(List1,List,List2),
	add_agendas_to_the_existing_ones2(M,List,Tail,Tail2).
add_agendas_to_the_existing_ones2(M,List,[maq_ag(M1,List1)|Tail],[maq_ag(M1,List1)|Tail2]):-
	add_agendas_to_the_existing_ones2(M,List,Tail,Tail2).

machine_is_in_the_list(_,[]):-!,false.
machine_is_in_the_list(M,[maq_ag(M,_)|_]):-!,true.
machine_is_in_the_list(M,[_|Tail]):-machine_is_in_the_list(M,Tail).



% -Guarda dinamicamente as maquinas com as novas agendas
% sem setups repetidos
% -argumento 1, lista de estruturas maq_ag(mquina,agenda)
resave_with_shift_and_without_same_tool_setup([]):-!.
resave_with_shift_and_without_same_tool_setup([maq_ag(M, Agenda)|Tail]):-
	assertz(agenda_maq(M,Agenda)),
	resave_with_shift_and_without_same_tool_setup(Tail).

%--------------------------------------------------------------------------------------------------------------------
% parameterizacao
inicializa:-
	cria_op_enc, makespan,
	retractall(agenda_maq(_,_)),
	write('Criterios de avaliacao:'), nl, write('1 - Numero de Geracoes'), nl, write('2 - Tempo Limite'), nl,
	write('3 - Valor minimo a atingir'), nl, write('4 - Estabilizacao da populacao'), nl, write('Opcao: '),
	read(OP), (retract(opcao(_)); true), asserta(opcao(OP)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100,
	(retract(prob_cruzamento(_));true),	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100,
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	nl,nl, write('Pretende comecar com individuos criados por heuristicas?'),
	nl, write('1 - Sim'), nl, write('2 - Nao'),
	nl, write('Opcao: '), read(OpHeur), (retract(usarHeuristica(_)); true), asserta(usarHeuristica(OpHeur)).



gera:-
	inicializa,
	get_linha_tarefas(Result),
	execucao(Result),
	findall(l(Linha, Tarefas), retract(melhorSeqPorLinha(Linha, Tarefas)), Display),
	write('----------------------------- Resultados -----------------------------'), nl,
	display(Display),
	nl, nl, write('----------------------------- Agenda -----------------------------'), nl,
	criar_agendas(Display),
	findall(t(Maq, List), agenda_maq(Maq, List),DispTemp),
	mostrar_agenda_temporal(DispTemp).
	% gera_populacao(Pop),
	% write('Pop='),write(Pop),nl,
	% avalia_populacao(Pop,PopAv),
	% write('PopAv='),write(PopAv),nl,nl,nl,
	% ordena_populacao(PopAv,PopOrd),
	% PopOrd = [First|_],
	% opcao(OP),
	% ((OP == 1, !, gera_por_geracoes(PopOrd, First)); %Opcao de parar por numero de geracoes
	% (OP == 2, !, gera_por_tempo_limite(PopOrd, First)); %Opcao de parar por limite de tempo
	% (OP == 3, !, gera_por_valor_minimo(PopOrd, First)); %Opcao de parar por por valor minimo atingido
	% (OP == 4, !, gera_por_estabilizacao(PopOrd, First))). %Opcao de parar por populacao estabilizada
	% gera_geracao(0,NG,PopOrd,First).

mostrar_agenda_temporal([]):-!.
mostrar_agenda_temporal([t(Maq, List)|Tail]):-
	write('..... Nova Agenda .....'), nl,
	write('Maquina: '), write(Maq), nl,
	write('Agenda Temporal: '), write(List), nl, nl,
	mostrar_agenda_temporal(Tail).
	


criar_agendas([]):-!.
criar_agendas([l(_,Tarefas)|Tail]):-
	nova_agenda_para_cada_maq(Tarefas),
	criar_agendas(Tail).


display([]).
display([Head|Tail]):-
	Head = l(Linha, Tarefas),
	write('Linha: '), write(Linha),
	nl, write('Melhor Escalonamento: '), write(Tarefas),
	nl, nl, display(Tail).

execucao([]):- retract(usarHeuristica(_)).
execucao([t([Linha], Tarefas)|Tail]):-
	write('Linha: '), write(Linha), nl,
	gera_populacao(Linha, Tarefas, Pop),
	write('Pop='),write(Pop),nl,
	avalia_populacao(Pop,PopAv),
	write('PopAv='),write(PopAv),nl,nl,nl,
	ordena_populacao(PopAv,PopOrd),
	PopOrd = [First|_],
	opcao(OP),
	((OP == 1, !, gera_por_geracoes(Linha, PopOrd, First)); %Opcao de parar por numero de geracoes
	(OP == 2, !, gera_por_tempo_limite(Linha, PopOrd, First)); %Opcao de parar por limite de tempo
	(OP == 3, !, gera_por_valor_minimo(Linha, PopOrd, First)); %Opcao de parar por por valor minimo atingido
	(OP == 4, !, gera_por_estabilizacao(Linha, PopOrd, First))), %Opcao de parar por populacao estabilizada
	execucao(Tail).

gera_populacao(Linha, Tarefas, Pop):-
	populacao(InitialPop),
	% tarefas(NumT),
	length(Tarefas, NumT),
	retractall(tarefas(_)),  asserta(tarefas(NumT)),
	retractall(populacao(_)),  asserta(populacao(NumT)),
	usarHeuristica(Op),
	factorial(NumT, MaxNum),
	((InitialPop > MaxNum, !, TamPop is MaxNum, Elite = [], retract(populacao(_)), asserta(populacao(TamPop)));

	((Op == 1, edd(Linha, MelhorEdd), min_slack(Linha, MelhorMinSlack),
	((MelhorEdd == MelhorMinSlack, mutacao1(MelhorMinSlack, Result),
		append([MelhorEdd], [Result], Elite));
		append([MelhorEdd], [MelhorMinSlack], Elite)),
	TamPop is NumT - 2);
	(TamPop is NumT))),

	((Op == 1, !, gera_populacao_eur(TamPop, Tarefas, NumT, Elite, PossivelPop));
	gera_populacao(TamPop,Tarefas,NumT,PossivelPop)),
	((Op == 1, append(Elite, PossivelPop, Pop));
	(Pop = PossivelPop)).

gera_populacao(0,_,_,[]):-!.

gera_populacao(TamPop,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaTarefas,NumT,Resto),
	gera_individuo(ListaTarefas,NumT,Ind),
	not(member(Ind,Resto)).
gera_populacao(TamPop,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,ListaTarefas,NumT,L).

gera_populacao_eur(0,_,_,_,[]):-!.

gera_populacao_eur(TamPop,ListaTarefas,NumT, Elite, [Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao_eur(TamPop1,ListaTarefas,NumT,Elite,Resto),
	gera_individuo(ListaTarefas,NumT,Ind),
	not(member(Ind,Resto)), not(member(Ind, Elite)).
gera_populacao_eur(TamPop,ListaTarefas,NumT,Elite,L):-
	gera_populacao_eur(TamPop,ListaTarefas,NumT,Elite,L).

gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[V*Ind|Resto1]):-
	avalia(Ind,FullValue),
	round(FullValue, V, 2),
	avalia_populacao(Resto,Resto1).

avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(T,Dur,Prazo,Pen),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	(
		(InstFim =< Prazo,!, VT is 0)
  ;
		(VT is Pen*(InstFim-Prazo))
	),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd); PopAvOrd = PopAv.

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).


btroca([X],[X]):-!.

btroca([VX*X,VY*Y|L1],[VY*Y|L2]):-
	VX>VY,!,
	btroca([VX*X|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).

gera_por_geracoes(Linha, Pop, Melhor):-
	write('Numero de novas Geracoes: '),read(NG), (retract(geracoes(_));true), asserta(geracoes(NG)),
	gera_por_geracoes1(1, NG, Pop, Melhor, Linha).


gera_por_geracoes1(G,G,Pop,Melhor,Linha):-!,
	nl, write('Geracao '), write(G), write(':'), nl, write(Pop), nl,
	write('Melhor '), write(': '), write(Melhor), nl, nl,
	assertz(melhorSeqPorLinha(Linha, Melhor)).

gera_por_geracoes1(N,G,Pop,Melhor,Linha):-
	nl,write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	%Criar populacao nova e recolher 2 elites da populacao anterior
	cruzamentoQualquer(Pop,NPop1), %Cruzar aleatoriamente entre a populacao
	recolherElite(Pop, Elite, PopSemElite), %Recolher dois melhores da populacao passada
	length(NPop1, Length),
	((Length \== 1, !, mutacao(NPop1,NPopRep), sort(NPopRep, NPop)); true), %Remover elementos repetidos da nova populacao

	%Junta o restante da primeira populção com a segunda sem repetir elementos e sem incluir os elite
	avalia_populacao(NPop,NPopAv),
	juntar_geracoes(PopSemElite, NPopAv, CandidatosPop), %Junta nova geração com os da antiga nao elite
	remover_repetidos_pop(CandidatosPop, Elite, FinalPop), %Remove elementos que sejam igual a elite
	ordena_populacao(FinalPop,NPopOrd),

	populacao(PopTotal),
	NumeroLutas is PopTotal - 2, %numero de Lutas em falta
	NumeroMinimo is NumeroLutas * 2, %Numero mínimo de elementos para fazer torneios
	length(NPopOrd, NrPop),  %Quantidade de concorrentes

	((1 >= NrPop, !, append(Elite, NPopOrd, PopSeguinte));
		
	(NumeroLutas == NrPop, !, append(Elite, NPopOrd, PopSeguinte)); %Passam todos os que faltam

	(NumeroMinimo =< NrPop, !, torneio(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte)); %Existem o dobro ou mais concorrentes do que o numero
											 % de vagas disponíveis

	(torneio_adaptado(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte))), %Existem menos do dobro de concorrentes do que o numero
											  % de vagas


	ordena_populacao(PopSeguinte, PopSeguinteOrdenado),

	PopSeguinteOrdenado = [NovoMelhor|_],
	NovoMelhor = NovoMelhorValor*_,
	Melhor = MelhorValor*_,
	((NovoMelhorValor<MelhorValor,!,(MelhorX = NovoMelhor));(MelhorX = Melhor)),
	write('Melhor '), write(': '), write(Melhor), nl,
	N1 is N+1,

	gera_por_geracoes1(N1, G, PopSeguinteOrdenado, MelhorX, Linha).

gera_por_valor_minimo(Linha, Pop, Melhor):-
	write('Valor minimo da solucao que pretende atingir?: '),read(M), (retract(valor_minimo(_));true), asserta(valor_minimo(M)),
	write('Numero de geracoes limite: '),read(NG), (retract(geracoes(_));true), asserta(geracoes(NG)),
	gera_por_valor_minimo1(1, NG, M, Pop, Melhor, Linha).

gera_por_valor_minimo1(G,G,_,Pop,Melhor,Linha):-!,
	nl,write('Geracao '), write(G), write(':'), nl, write(Pop), nl,
	write('Melhor '), write(': '), write(Melhor), nl, nl,
	assertz(melhorSeqPorLinha(Linha, Melhor)).

% M - Valor temporal limite a atingir
gera_por_valor_minimo1(G,_,M,Pop,Melhor,Linha):-
	Melhor = Valor*_,
	Valor =< M, !,
	nl,nl,write('Geracao '), write(G), write(':'), nl, write(Pop), nl,
	write('Melhor '), write(': '), write(Melhor), nl, nl,
	assertz(melhorSeqPorLinha(Linha, Melhor)).

gera_por_valor_minimo1(N,G,M,Pop,Melhor,Linha):-
	nl,nl,write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	%Criar populacao nova e recolher 2 elites da populacao anterior
	cruzamentoQualquer(Pop,NPop1), %Cruzar aleatoriamente entre a populacao
	recolherElite(Pop, Elite, PopSemElite), %Recolher dois melhores da populacao passada
	length(NPop1, Length),
	((Length \== 1, !, mutacao(NPop1,NPopRep), sort(NPopRep, NPop)); true), %Remover elementos repetidos da nova populacao

	%Junta o restante da primeira populacao com a segunda sem repetir elementos e sem incluir os elite
	avalia_populacao(NPop,NPopAv),
	juntar_geracoes(PopSemElite, NPopAv, CandidatosPop), %Junta nova geração com os da antiga nao elite
	remover_repetidos_pop(CandidatosPop, Elite, FinalPop), %Remove elementos que sejam igual a elite
	ordena_populacao(FinalPop,NPopOrd),

	populacao(PopTotal),
	NumeroLutas is PopTotal - 2, %Numero de Lutas em falta
	NumeroMinimo is NumeroLutas * 2, %Numero mínimo de elementos para fazer torneios
	length(NPopOrd, NrPop),  %Quantidade de concorrentes

	((1 >= NrPop, !, append(Elite, NPopOrd, PopSeguinte));
		
	(NumeroLutas == NrPop, !, append(Elite, NPopOrd, PopSeguinte)); %Passam todos os que faltam

	(NumeroMinimo =< NrPop, !, torneio(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte)); %Existem o dobro ou mais concorrentes do que o numero
											 % de vagas disponíveis

	(torneio_adaptado(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte))), %Existem menos do dobro de concorrentes do que o numero
											  % de vagas


	ordena_populacao(PopSeguinte, PopSeguinteOrdenado),

	PopSeguinteOrdenado = [NovoMelhor|_],
	NovoMelhor = NovoMelhorValor*_,
	Melhor = MelhorValor*_,
	((NovoMelhorValor<MelhorValor,!,(MelhorX = NovoMelhor));(MelhorX = Melhor)),
	write('Melhor '), write(': '), write(Melhor), nl,
	N1 is N+1,

	gera_por_valor_minimo1(N1, G, M, PopSeguinteOrdenado, MelhorX, Linha).


gera_por_estabilizacao(Linha, Pop, Melhor):-
	write('Numero de geracoes seguidas: '),read(F), (retract(estabilizar(_));true), asserta(estabilizar(F)),
	gera_por_estabilizacao1(1, 1, F, Pop, Melhor, Linha).


gera_por_estabilizacao1(N,F,F,Pop,Melhor,Linha):-!,
	nl,write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	write('Melhor '), write(': '), write(Melhor), nl, nl,
	assertz(melhorSeqPorLinha(Linha, Melhor)).

%E - Quantidade de vezes repetido F - Quantidade de vezes repetido limite
gera_por_estabilizacao1(N,E, F,Pop,Melhor,Linha):-
	nl,write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	%Criar populacao nova e recolher 2 elites da populacao anterior
	cruzamentoQualquer(Pop,NPop1), %Cruzar aleatoriamente entre a populacao
	recolherElite(Pop, Elite, PopSemElite), %Recolher dois melhores da populacao passada
	length(NPop1, Length),
	((Length \== 1, !, mutacao(NPop1,NPopRep), sort(NPopRep, NPop)); true), %Remover elementos repetidos da nova populacao

	%Junta o restante da primeira populção com a segunda sem repetir elementos e sem incluir os elite
	avalia_populacao(NPop,NPopAv),
	juntar_geracoes(PopSemElite, NPopAv, CandidatosPop), %Junta nova geração com os da antiga nao elite
	remover_repetidos_pop(CandidatosPop, Elite, FinalPop), %Remove elementos que sejam igual a elite
	ordena_populacao(FinalPop,NPopOrd),

	populacao(PopTotal),
	NumeroLutas is PopTotal - 2, %numero de Lutas em falta
	NumeroMinimo is NumeroLutas * 2, %Numero mínimo de elementos para fazer torneios
	length(NPopOrd, NrPop),  %Quantidade de concorrentes

	% nl,nl,write('Numero Lutas: '), write(NumeroLutas),nl,write('Numero Minimo: '), write(NumeroMinimo),nl, write('Numero Population: '), write(NrPop),nl, nl,

	((1 >= NrPop, !, append(Elite, NPopOrd, PopSeguinte));
	
	(NumeroLutas == NrPop, !, append(Elite, NPopOrd, PopSeguinte)); %Passam todos os que faltam

	(NumeroMinimo =< NrPop, !, torneio(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte)); %Existem o dobro ou mais concorrentes do que o numero
											 % de vagas disponíveis

	(torneio_adaptado(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte))), %Existem menos do dobro de concorrentes do que o numero
											  % de vagas


	ordena_populacao(PopSeguinte, PopSeguinteOrdenado),

	PopSeguinteOrdenado = [NovoMelhor|_],
	NovoMelhor = NovoMelhorValor*_,
	Melhor = MelhorValor*_,
	((NovoMelhorValor<MelhorValor,!,(MelhorX = NovoMelhor), E1 is 1);(MelhorX = Melhor), E1 is E + 1),
	write('Melhor '), write(': '), write(Melhor), nl,
	N1 is N+1,

	gera_por_estabilizacao1(N1, E1, F, PopSeguinteOrdenado, MelhorX, Linha).

gera_por_tempo_limite(Linha, Pop, Melhor):-
	write('Tempo Limite de procura: '),read(T), (retract(tempo(_));true), asserta(tempo(T)),
	gera_por_tempo_limite1(1, 0, T, Pop, Melhor, Linha).


%T - Tempo atual, Tf - Tempo Final
gera_por_tempo_limite1(N,T,TF,Pop,Melhor,Linha):-
	T >= TF,
	nl,write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	write('Melhor '), write(': '), write(Melhor), nl, nl,
	assertz(melhorSeqPorLinha(Linha, Melhor)).

gera_por_tempo_limite1(N,T, TF,Pop,Melhor,Linha):-
	get_time(Tinit),
	nl,write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	%Criar populacao nova e recolher 2 elites da populacao anterior
	cruzamentoQualquer(Pop,NPop1), %Cruzar aleatoriamente entre a populacao
	recolherElite(Pop, Elite, PopSemElite), %Recolher dois melhores da populacao passada
	length(NPop1, Length),
	((Length \== 1, !, mutacao(NPop1,NPopRep), sort(NPopRep, NPop)); true), %Remover elementos repetidos da nova populacao

	%Junta o restante da primeira populção com a segunda sem repetir elementos e sem incluir os elite
	avalia_populacao(NPop,NPopAv),
	juntar_geracoes(PopSemElite, NPopAv, CandidatosPop), %Junta nova geração com os da antiga nao elite
	remover_repetidos_pop(CandidatosPop, Elite, FinalPop), %Remove elementos que sejam igual a elite
	ordena_populacao(FinalPop,NPopOrd),

	populacao(PopTotal),
	NumeroLutas is PopTotal - 2, %numero de Lutas em falta
	NumeroMinimo is NumeroLutas * 2, %Numero mínimo de elementos para fazer torneios
	length(NPopOrd, NrPop),  %Quantidade de concorrentes

	% nl,nl,write('Numero Lutas: '), write(NumeroLutas),nl,write('Numero Minimo: '), write(NumeroMinimo),nl, write('Numero Population: '), write(NrPop),nl, nl,

	((1 >= NrPop, !, append(Elite, NPopOrd, PopSeguinte));
	
	(NumeroLutas == NrPop, !, append(Elite, NPopOrd, PopSeguinte)); %Passam todos os que faltam

	(NumeroMinimo =< NrPop, !, torneio(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte)); %Existem o dobro ou mais concorrentes do que o numero
											 % de vagas disponíveis

	(torneio_adaptado(NPopOrd, NumeroLutas, Vencedores),
	append(Elite, Vencedores, PopSeguinte))), %Existem menos do dobro de concorrentes do que o numero
											  % de vagas


	ordena_populacao(PopSeguinte, PopSeguinteOrdenado),

	PopSeguinteOrdenado = [NovoMelhor|_],
	NovoMelhor = NovoMelhorValor*_,
	Melhor = MelhorValor*_,
	((NovoMelhorValor<MelhorValor,!,(MelhorX = NovoMelhor));(MelhorX = Melhor)),
	write('Melhor '), write(': '), write(Melhor), nl,
	N1 is N+1,

	get_time(Tfin),
	Taux is Tfin - Tinit,
	T1 is T + Taux,
	gera_por_tempo_limite1(N1, T1, TF, PopSeguinteOrdenado, MelhorX, Linha).


%Recolhe os dois melhores elementos de uma populacao
recolherElite(Pop, Elite, PopSemElite):- recolherElite1(Pop, 2, Elite, PopSemElite).

recolherElite1([], _, [], []):-!.
recolherElite1(Pop, 0, [], Pop).
recolherElite1([Melhor|Pop], N, [Melhor|FinalElite], PopSemElite):-
	N1 is N - 1, recolherElite1(Pop, N1, FinalElite, PopSemElite).


%Junta as duas gerações sem haver repetidos
juntar_geracoes(AntPop, NovPop, CandidatosPop):- juntar_geracoes1(AntPop, NovPop, [], CandidatosPop).

juntar_geracoes1([], NovPop, AtualPop, FinalPop):- append(NovPop, AtualPop, FinalPop).
juntar_geracoes1([Antigo|AntPop], NovPop, ActualPop, FinalPop):-
	((\+member(Antigo, NovPop), !, append([Antigo], ActualPop, ActualPop1),
	juntar_geracoes1(AntPop, NovPop, ActualPop1, FinalPop));
	juntar_geracoes1(AntPop, NovPop, ActualPop, FinalPop)).

%Remover repetidos de uma populacao
remover_repetidos_pop(FinalPop, [], FinalPop).
remover_repetidos_pop(CandidatosPop, [Element|Elite], FinalPop):- delete(CandidatosPop, Element, IntemedioPop1),
	remover_repetidos_pop(IntemedioPop1, Elite, FinalPop).

gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([_*Ind],[Ind]).
cruzamento([_*Ind1,_*Ind2|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

%Cruza os elementos de uma populacao aletoriamente
cruzamentoQualquer([],[]).
cruzamentoQualquer([_*Ind],[Ind]).
cruzamentoQualquer([_*Ind1|Resto],[NInd1,NInd2|Resto1]):-
	getOther(Resto,RestoSemInd2,Ind2),
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamentoQualquer(RestoSemInd2,Resto1).

getOther(Resto,RestoSemInd2,Ind2):-
	random_member(X, Resto),
	delete(Resto,X,RestoSemInd2),
	X = _*Ind2.

%Realiza torneios com mais concorrentes que o dobro das vagas
torneio(List, NumeroLutas, Vencedores):- torneio1(List, NumeroLutas, [], Vencedores).

torneio1(_, 0, Vencedores, Vencedores):-!. %Acabaram as vagas
torneio1(List, NumeroLutas, AtuaisVencedores, Final):-
	random_member(Combatente1, List), %Seleciona primeiro combatente
	delete(List, Combatente1, ListVersus),
	random_member(Combatente2, ListVersus), %Seleciona segundo combatente
	delete(ListVersus, Combatente2, ProximaRonda),
	Combatente1 = Valor1*_, Combatente2 = Valor2*_, %Obter valores dos concorrentes
	ValorTotal is Valor1 + Valor2, %Obter o valor total dos concorrentes
	((ValorTotal == 0, !, Forca1 is Valor1, Forca2 is Valor2);
	Forca1 is (Valor1/ValorTotal), Forca2 is (Valor2/ValorTotal)), %Obter força dos concorrentes
	random(0.0, Forca1, Ataque1), %Gerar ataque alteatorio do concorrente 1
	random(0.0, Forca2, Ataque2), %Gerar ataque alteatorio do concorrente 2
	((Ataque1 =< Ataque2, !,
		 append([Combatente1], AtuaisVencedores, AtuaisVencedores1)); %O primeiro combatente ganha
	append([Combatente2], AtuaisVencedores, AtuaisVencedores1)), %O segundo combatente ganha
	NumeroLutas1 is NumeroLutas - 1, %Quantidade de lutas restantes
	torneio1(ProximaRonda, NumeroLutas1, AtuaisVencedores1, Final).

%Realiza torneios com menos concorrentes do que o dobro das vagas
torneio_adaptado(List, NumeroLutas, Vencedores):-
	torneio_adaptado1(List, NumeroLutas, [], [], Vencedores1, Perdedores), %Obter vencedores e perdedores
	length(Vencedores1, NumeroVencedores), %Verificar quantas vagas foram ocupadas
	((NumeroVencedores < NumeroLutas, !,
		LutasEmFalta is NumeroLutas - NumeroVencedores,
		torneio_adaptado(Perdedores, LutasEmFalta, Vencedores2)); %Repete torneio com perdedores
	(Vencedores = Vencedores1)), %Retorna imediatamente os vencedores
	append(Vencedores1, Vencedores2, Vencedores). %Junta todos os vencedores numa lista retorna

%Igual a torneio1 mas mobiliza perdedores para ser rebiscados e tem outras condições de paragem
torneio_adaptado1([], _, Vencedores, Perdedores, Vencedores, Perdedores). %nao há mais concorrentes
torneio_adaptado1(_, 0, Vencedores, Perdedores, Vencedores, Perdedores). %nao há mais vagas
torneio_adaptado1([Ultimo], _, Vencedores, Perdedores, Vencedores1, Perdedores):-
	!, append(Vencedores, [Ultimo], Vencedores1). %Apenas existe mais um concorrente
torneio_adaptado1(List, NumeroLutas, AtuaisVencedores, Perdedores, Final, FinalPerdedores):-
	random_member(Combatente1, List), delete(List, Combatente1, ListVersus),
	random_member(Combatente2, ListVersus),
	delete(ListVersus, Combatente2, ProximaRonda),
	Combatente1 = Valor1*_, Combatente2 = Valor2*_,
	ValorTotal is Valor1 + Valor2,
	Forca1 is (Valor1/ValorTotal), Forca2 is (Valor2/ValorTotal),
	random(0.0, Forca1, Ataque1), random(0.0, Forca2, Ataque2),
((Ataque1 =< Ataque2, !, append([Combatente1], AtuaisVencedores, AtuaisVencedores1),
		append([Combatente2], Perdedores, Perdedores1));
	 append([Combatente2], AtuaisVencedores, AtuaisVencedores1),
		append([Combatente1], Perdedores, Perdedores1)),
	NumeroLutas1 is NumeroLutas - 1,
	torneio_adaptado1(ProximaRonda, NumeroLutas1, AtuaisVencedores1, Perdedores1, Final, FinalPerdedores).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).


deleteLast(L, L1):- deleteLast1(L, [], M), reverse(M, L1).

deleteLast1([_], Final, Final):-!.
deleteLast1([H|Resto], Fill, Final):- deleteLast1(Resto, [H|Fill], Final).

% Arrendonda o valor X para D casas decimais e coloca o valor em Y
round(X,Y,D) :- Z is X * 10^D, round(Z, ZA), Y is ZA / 10^D.

obter_melhor_elemento_findall:-
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	findall(Ind, permuta(ListaTarefas, Ind), TodosIndividuos),
	avalia_populacao(TodosIndividuos, TodosIndividuosAvaliados),
	ordena_populacao(TodosIndividuosAvaliados, [Melhor|TodosIndividuosOrdenados]),
	write([Melhor|TodosIndividuosOrdenados]),
	nl,nl, write('Melhor Individuo: '),
	write(Melhor).

:-dynamic melhor_tarefa/1.
:- dynamic get_linha_menor_atraso_edd/4.

obter_melhor_elemento_sem_findall:- get_time(Tinit),
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	((obter_melhor_elemento_sem_findall_aux(ListaTarefas)); true),
	retract(melhor_tarefa(Melhor)),
	get_time(Tfin), Tcomp is Tfin - Tinit,
	write('Melhor individuo: '), write(Melhor), nl,
	write('GERADO EM '),write(Tcomp),
	write(' SEGUNDOS'),nl.


obter_melhor_elemento_sem_findall_aux(ListaTarefas):- asserta(melhor_tarefa(999999*[])), !,
	permuta(ListaTarefas, Individuo),
	avalia_populacao([Individuo], IndividuoAvaliado),
	atualizar_melhor(IndividuoAvaliado),
	fail.

atualizar_melhor([Individuo|_]):- melhor_tarefa(MT),
	MT = Valor1*_,
	Individuo = Valor2*_,
	Valor2 < Valor1, retract(melhor_tarefa(_)), asserta(melhor_tarefa(Individuo)), !.



% permuta/2 gera permutacoes de listas
permuta([ ],[ ]).
permuta(L,[X|L1]):-apaga1(X,L,Li),permuta(Li,L1).

apaga1(X,[X|L],L).
apaga1(X,[Y|L],[Y|L1]):-apaga1(X,L,L1).


%---------------------------------------------- Heuristicas ----------------------------------------------

% -Divide as tarefas por linhas e aplica o edd a cada uma
% -argumento 1, Linha da qual se prepende a sequencia de tarefas
% -argumento 2, Sequencia de tarefas da linha selecionada
edd(Linha, Tarefas_sorted_by_edd):-
	findall(
		t(Tarefa,Processing_time,T,Penalty),
		tarefa(Tarefa,Processing_time,T,Penalty),
		List_tarefas
	),
	add_line_to_struct_t(List_tarefas, List_tarefas_line),
	findall(Linha_list,linhas(Linha_list),Linha_list),
	edd2(Linha, List_tarefas_line, Linha_list, Tarefas_sorted_by_edd).

% -Adiciona à estrutura t(Tarefa,Process_Time,T,Penalty), as linhas
% das tarefas ficando 
% t(Tarefa,[lista com uma ou mais linhas],Process_Time,T,Penalty)
% -argumento 1, Lista de tarefas
% -argumento 2, Lista de tarefas com as linhas adicionadas
add_line_to_struct_t([],[]).
add_line_to_struct_t([t(Tarefa,Process_time,T,Penalty)|Tail],
				[t(Tarefa,Linha_list,Process_time,T,Penalty)|Tail2]):-
	tarefa_op_list(Tarefa,Opl),
	obter_sequencia_maquinas(Opl,Seq_maquinas),
	findall(Linha_list,tipos_maq_linha(Linha_list,Seq_maquinas),Linha_list),
	add_line_to_struct_t(Tail,Tail2).

% -Retrona a sequencia de maquinas para realizar a sequencia de operacoes
% -argumento 1, lista de operacoes
% -argumento 2, lista de maquinas
obter_sequencia_maquinas([],[]).
obter_sequencia_maquinas([Op|Tail],[Machine|Tail2]):-
	op_prod_client(Op,Machine,_,_,_,_,_,_,_),
    obter_sequencia_maquinas(Tail,Tail2).

% -Predicado auxiliar ao edd que divide as tarefas por linhas e aplica o edd a cada uma
% returnando no fim a sequencia de tarefas da linha pertendida
% -argumento 1, linha cuja a sequencia de tarefas é pertendida
% -argumento 2, lista de tarefas com a linha adicionada à estrutura t
% -argumento 3, lista de linhas que existem
% -argumento 4, sequencia de tarefas da linha pertendida
edd2(Linha, List_tarefas_line, Linha_list, Tarefas_sorted_by_edd):-
	split_tarefas_per_possible_lines(List_tarefas_line,Solo_line_tarefas,Multi_line_tarefas),
	put_solo_line_tarefas_on_the_respective_line(Solo_line_tarefas,Linha_list,Linha_list2),
	split_multiline_tarefas_by_makespan_edd(Multi_line_tarefas,Linha_list2,[],Linha_list3),
	((is_empty(Multi_line_tarefas),!,apply_edd_to_selected_line(Linha,Linha_list2,Tarefas_sorted_by_edd));
		apply_edd_to_selected_line(Linha,Linha_list3,Tarefas_sorted_by_edd)).

% -Divide as tarefas em 2 grupos, um com tarefas que podes ser feitas em
% apenas uma linha e outro com tarefas que podem ser feitas em mais
% -argumento 1, lista de tarefas
% -argumento 2, lista de tarefas que so podem ser executadas numa linha
% -argumento 3, lista de tarefas que podem ser executadas em varias linhas
split_tarefas_per_possible_lines([],[],[]).
split_tarefas_per_possible_lines([t(Tarefa,Linha_list,Process_time,T,Penalty)|Tail],
				[t(Tarefa,Linha_list,Process_time,T,Penalty)|Tail2],Multi_line_tarefas):-
	proper_length(Linha_list,Nr_lines),Nr_lines == 1,!,split_tarefas_per_possible_lines(Tail,Tail2,Multi_line_tarefas).
split_tarefas_per_possible_lines([t(Tarefa,Linha_list,Process_time,T,Penalty)|Tail],
				Solo_line_tarefas,[t(Tarefa,Linha_list,Process_time,T,Penalty)|Tail2]):-
	split_tarefas_per_possible_lines(Tail,Solo_line_tarefas,Tail2).

% -Agrupa as tarefas que sao executadas em apenas uma linha, na sua respetiva linha
% -argumento 1, lista de tarefas que sao executadas em apenas uma linha
% -argumento 2, lista de linhas que existem em sistema
% -argumento 3, lista de linhas que existem em sistema com as tarefas
% respetivas associadas com o formato [[[Linha],{conjutno de tarefas}],...]
put_solo_line_tarefas_on_the_respective_line(Solo_line_tarefas,Linha_list,Linha_list2):-
	sort_by_line(Solo_line_tarefas,Linha_list,[],Sorted_Solo_line_tarefas),
	put_solo_line_tarefas_on_the_respective_line2(Sorted_Solo_line_tarefas,Linha_list,Linha_list2).

% -Predicado auxiliar ao put_solo_line_tarefas_on_the_respective_line
% onde ordena as tarefas em listas com as tarefas que pertencem à
% mesma linha
sort_by_line([],[],Sorted_Solo_line_tarefas,Sorted_Solo_line_tarefas).
sort_by_line(Solo_line_tarefas,[[Linha]|Tail],Aux,Sorted_Solo_line_tarefas):-
	sort_by_line2(Solo_line_tarefas,Linha,Result,Resto),
	append(Aux,[Result],Aux2),
	sort_by_line(Resto,Tail,Aux2,Sorted_Solo_line_tarefas).

% -Predicado auxiliar ao sort_by_line ordena as tarefas em listas 
% com as tarefas que pertencem à mesma linha
sort_by_line2([],_,[],[]).
sort_by_line2([t(Tarefa,[Linha],Process_time,T,Penalty)|Tail],Linha,
				[t(Tarefa,[Linha],Process_time,T,Penalty)|Tail2],Resto):-!,sort_by_line2(Tail,Linha,Tail2,Resto).
sort_by_line2([t(Tarefa,[Linha],Process_time,T,Penalty)|Tail],Linha2,
				Result,[t(Tarefa,[Linha],Process_time,T,Penalty)|Tail2]):-sort_by_line2(Tail,Linha2,Result,Tail2).

% -Predicado auxiliar ao put_solo_line_tarefas_on_the_respective_line
% onde as tarefas com apenas uma linha sao associadas à sua linha respetiva
put_solo_line_tarefas_on_the_respective_line2(_,[],[]).
put_solo_line_tarefas_on_the_respective_line2(Sorted_Solo_line_tarefas,[Head|Tail],[Result|Tail2]):-
				add_tarefa_to_line_list(Sorted_Solo_line_tarefas,Head,[],Result),
				put_solo_line_tarefas_on_the_respective_line2(Sorted_Solo_line_tarefas,Tail,Tail2).

% -Predicado auxiliar ao put_solo_line_tarefas_on_the_respective_line2
% onde as tarefas com apenas uma linha sao associadas à sua linha respetiva
add_tarefa_to_line_list([],Line,Result_aux,Result):-
				((is_empty(Result_aux),!,append(Result_aux,[Line],Result_aux2),Result = Result_aux2);Result = Result_aux).
add_tarefa_to_line_list([Head|Tail],Line,Result_aux,Result):-
				add_tarefa_to_line_list2(Head,Line,[],Aux),
				append(Result_aux,Aux,Result_aux2),
				add_tarefa_to_line_list(Tail,Line,Result_aux2,Result).

% -Predicado auxiliar ao add_tarefa_to_line_list
% onde as tarefas com apenas uma linha sao associadas à sua linha respetiva
add_tarefa_to_line_list2([],_,Result,Result).
add_tarefa_to_line_list2([t(Tarefa,[Linha],Process_time,T,Penalty)|Tail],[Linha],Aux,Result):-!,
	((is_empty(Aux),!,append(Aux,[[Linha]],Aux2),append(Aux2,[t(Tarefa,[Linha],Process_time,T,Penalty)],Aux3));
		append(Aux,[t(Tarefa,[Linha],Process_time,T,Penalty)],Aux3)),
	add_tarefa_to_line_list2(Tail,[Linha],Aux3,Result).
add_tarefa_to_line_list2([t(_,[_],_,_,_)|Tail],[Linha2],Aux,Result):-
	add_tarefa_to_line_list2(Tail,[Linha2],Aux,Result).

% -Distribui as tarefas que podem ser executadas em mais do que uma linha
% pelas linhas com menor tempo de execucao
% -argumento 1, Lista de tarefas pertencentes a mais do que uma linha
% -argumento 2, Lista de linhas e as suas tarefas asssociadas
% -argumento 3, lista de controlo com as tarefas que foram distribuidas
% pelas linhas ate ao momento
% -argumento 4, lista final com todas as tarefas distribuidas pelas linhas
split_multiline_tarefas_by_makespan_edd([],_,Result,Result).
split_multiline_tarefas_by_makespan_edd([t(Tarefa,Linhas,Process_time,T,Penalty)|Tail],Linha_list,Aux,Result):-
	((is_empty(Aux),!,linhas_para_tarefa(Linhas,Linha_list,[],Linha_list2),
		linha_mais_rapida_makespan_edd(Linha_list2,99999,[],Selected_Line),
		add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,Linha_list,[],Aux2));
	linhas_para_tarefa(Linhas,Aux,[],Linha_list2),
		linha_mais_rapida_makespan_edd(Linha_list2,99999,[],Selected_Line),
		add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,Aux,[],Aux2)),
	split_multiline_tarefas_by_makespan_edd(Tail,Linha_list,Aux2,Result).

% -Retorna as linhas com as respetivas tarefas associadas
% onde a tarefa pode ser realizada
% -argumento 1, linhas onde a tarefa pode ser realizada
% -argumento 2, lista de todas as linhas com as repetivas
% tarefas associadas
% -argumento 3, lista auxiliar com a inforacao das linhas
% ate ao momento
% -argumento 4, lista as linhas onde a tarefas pode ser
% executada e as respivas tarefas a executar de momento
linhas_para_tarefa([],_,Result,Result).
linhas_para_tarefa([Linha|Tail],Linha_list,Aux,Result):-
	linhas_para_tarefa2(Linha,Linha_list,[],Linha_selected),
	append(Aux,[Linha_selected],Aux2),
	linhas_para_tarefa(Tail,Linha_list,Aux2,Result).

% -Predicado auxiliar ao linhas_para_tarefa
linhas_para_tarefa2(_,[],Result,Result).
linhas_para_tarefa2(Linha,[Head|Tail],Aux,Result):-
	linhas_para_tarefa3(Linha,Head,Aux2),
	append(Aux,Aux2,Aux3),
	linhas_para_tarefa2(Linha,Tail,Aux3,Result).

% -Predicado auxiliar ao linhas_para_tarefa2
linhas_para_tarefa3(Linha,[[Linha]|Tail],[[Linha]|Tail]):-!.
linhas_para_tarefa3(_,[[_]|_],[]).

% -Retorna a linha com menor makespan das recebidas por armumento
% -argumento 1, lista de linhas onde a tarefa pode ser execvutada
% -argumento 2, makespan da linha com o menor makespan no momento
% -argumento 3, linha com o menor makespan de momento
% -argumento 4, linha com o menor makespan da lista
linha_mais_rapida_makespan_edd([],_,Selected_Line,Selected_Line).
linha_mais_rapida_makespan_edd([Head|Tail],Min_Sum,Aux,Selected_Line):-
	sum_makespan(Head,Sum),
	((Sum<Min_Sum,!,linha_mais_rapida_makespan_edd(Tail,Sum,Head,Selected_Line));
		linha_mais_rapida_makespan_edd(Tail,Min_Sum,Aux,Selected_Line)).

% -Retorna o somatorio do makespan de uma linha
% -argumento 1, lista com a linha e respetivas tarefas
% -argumento 2, somatorio do makespan das tarefas
sum_makespan([[_]|Tail],Sum):-
	sum_makespan2(Tail,Sum).

% -Predicado auxiliar ao sum_makespan
sum_makespan2([],0).
sum_makespan2([t(_,_,Process_time,_,_)|Tail],Sum):-
	sum_makespan2(Tail,Sum2), Sum is Process_time + Sum2.

% -Adiciona a tarefa que pode ser executada em mais do que uma linha
% a linha selecionada
% -argumento 1, tarefa a adicionar a linha
% -argumento 2, linha selecionada para receber a tarefa
% -argumento 3, lista de linhas com as respetivas tarefas
% -argumento 4, lista auxiliar que guarda as lista prontas atualmente
% -argumento 5, lista com as linhas e tarefas associadas incluindo a
% a tarefas recebida no argumento 1
add_tarefa_to_respective_line(_,_,[],Result,Result).
add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,[Selected_Line|Tail],Aux,Result):-!,
	append(Selected_Line,[t(Tarefa,Linhas,Process_time,T,Penalty)],Aux2),
	append(Aux,[Aux2],Aux3),
	add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,Tail,Aux3,Result).
add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,[Head|Tail],Aux,Result):-
	append(Aux,[Head],Aux3),
	add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,Tail,Aux3,Result).

% -Aplica o edd a linha selecionada e retorna a sequencia de tarefas 
% da mesma apos aplicar o edd
% -armumento 1, linha pretendida
% -arguemtno 2, lista de linhas e respetivas tarefas
% -argumento 3, sequencia de tarefas da linha pretendida apos
% apos aplicado o edd
apply_edd_to_selected_line(Linha,Linha_list3,Tarefas_sorted_by_edd):-
	linhas_para_tarefa([Linha],Linha_list3,[],Aux),
	get_the_tarefas_list(Aux,Tarefas_List),
	put_each_tarefa_in_a_list(Tarefas_List,Tarefas_List2),
	edd_for_line(Tarefas_List2,Tarefas_sorted_by_edd).

% -Recebe uma lista com a linha e as respetivas tarefas 
% e retorna apenas as tarefas
get_the_tarefas_list([Head|_],Tarefas_List):-
	get_the_tarefas_list2(Head,Tarefas_List).

% -Predicado auxiliar ao get_the_tarefas_list
get_the_tarefas_list2([[_]|Tail],Tail).

% -Coloca a informacao da estrutura t numa lista de modo
% a ser possivel a utilizacao do edd criado no sprint 
% passado
% -argumento 1, lista de tarefas com a informacao na 
% estrutura t
% -argumento 2, lista de tarefas com a informacao em
% listas
put_each_tarefa_in_a_list([],[]).
put_each_tarefa_in_a_list([t(Tarefa,_,Process_time,T,Penalty)|Tail],[[Tarefa,Process_time,T,Penalty]|Tail2]):-
	put_each_tarefa_in_a_list(Tail,Tail2).

% -Heuristica que ordena as tarefas por ordem crescente de tempo de
% -conclusao e caso haja repetidos ordenaos por prioridade
% -1� argumento, lista de tarefas ordenadas pelas regras da heuristica
% edd
edd_for_line(List_tarefas,Tarefas_sorted_by_edd):-
				sort_by_conclusion_time(List_tarefas,List_tarefas_sorted),
			    get_tarefas_same_conclusion_time(List_tarefas_sorted,[],List_same_time_tarefas_together),
			    order_same_time_tarefas_by_penalty(List_same_time_tarefas_together,List_same_time_tarefas_together_sorted),
			    get_only_tarefas(List_same_time_tarefas_together_sorted,Only_tarefas_list),
			    flatten(Only_tarefas_list, Tarefas_sorted_by_edd),!.

% -Ordena a lista recebida no primeiro argumento por ordem crescente de
% tempo de conclusao
% -1� argumento, lista de tarefas a ordenar
% -2� argumento, lista de tarefas ordenada
sort_by_conclusion_time([],[]).
sort_by_conclusion_time([Tarefa|Tail],Tarefa_list_sorted):-split2(Tarefa,Tail,Left,Right),
							   sort_by_conclusion_time(Left,Left1),
							   sort_by_conclusion_time(Right,Right1),
							   append(Left1,[Tarefa|Right1],Tarefa_list_sorted).

% -Predicado auxiliar do sort_by_conclusion_time, retorna todas as
% tarefas com tempo de concusao menor e maior que a recebida no primeiro
% argumento
% -1� argumento, tarefa a comparar
% -2� argumento, lista de tarefas a comparar
% -3� argumento, lista de taredas cujo tempo de concusao � menor que a
% recebida como primeiro argumento
% -4� argumento, lista de taredas cujo tempo de concusao � maior que a
% recebida como primeiro argumento
split2(_,[],[],[]).
split2([Tarefa,Processing_time,T,Penalty],[[Tarefa2,Processing_time2,T2,Penalty2]|Tail],
       [[Tarefa2,Processing_time2,T2,Penalty2]|Left],Right):-
			T>T2,!,split2([Tarefa,Processing_time,T,Penalty],Tail,Left,Right).
split2([Tarefa,Processing_time,T,Penalty],[[Tarefa2,Processing_time2,T2,Penalty2]|Tail],
       Left,[[Tarefa2,Processing_time2,T2,Penalty2]|Right]):-
			split2([Tarefa,Processing_time,T,Penalty],Tail,Left,Right).

% -Retorna uma lista com as tarefas com o mesmo tempo de conlusao juntas
% -1� argumento, lista de tarefas ordenadas por ordem crescente de tempo
% de conclusao
% -2� argumento, lista de controlo que guarda a lista com as listas de
% tarefas com o mesmo tempo de conclusao criadas ate ao momento
% -3� argumento, lista com todas as listas com as tarefas com o mesmo
% tempo de conclusao com o formato [[tarefa,tarefa,...],[tarefa,..],...]
get_tarefas_same_conclusion_time([],List_same_tarefas_together,List_same_tarefas_together).
get_tarefas_same_conclusion_time([Head|Tail],Aux,List_same_tarefas_together):-
			get_tarefas_same_conclusion_time2(Head,Tail,Tarefas_same_time,Resto),
			append([Head],Tarefas_same_time,Tarefas_same_time2),
			append(Aux,[Tarefas_same_time2],Aux2),
			get_tarefas_same_conclusion_time(Resto,Aux2,List_same_tarefas_together).

% -Retorna uma lista com todas as tarefas com o mesmo tempo de conclusao
% que a recebida no primeiro argumento inclusive
% -1� argumento, tarefa a comparar o tempo de conclusao
% -2� argumento, lista de tarefas a comparar o tempo de conclusao
% -3� argumento, lista de tarefas que tem o mesmo tempo de conclusao que
% a do primeiro argumento inclusive
% -4� argumento, lista de tarefas com o tempo de conclusao diferente da
% do primeiro argumento
get_tarefas_same_conclusion_time2(_,[],[],[]).
get_tarefas_same_conclusion_time2([Tarefa,Processing_time,T,Penalty],[[Tarefa2,Processing_time2,T,Penalty2]|Tail],
		[[Tarefa2,Processing_time2,T,Penalty2]|Tail2],Resto):-
						   get_tarefas_same_conclusion_time2([Tarefa,Processing_time,T,Penalty],Tail,Tail2,Resto),!.
get_tarefas_same_conclusion_time2([Tarefa,Processing_time,T,Penalty],[[Tarefa2,Processing_time2,T2,Penalty2]|Tail],
		Tarefas_same_time,[[Tarefa2,Processing_time2,T2,Penalty2]|Tail2]):-
						   get_tarefas_same_conclusion_time2([Tarefa,Processing_time,T,Penalty],Tail,Tarefas_same_time,Tail2).

% -Ordena a lista de tarefas recebidas no primeiro argumento por ordem
% decrescente de prioridade pois quanto maior o valor da prioridade
% maior a prioridade
% -1� argumento, lista de tarefas a organizar com o formato [[tarefa,tarefa,...],[tarefa,..],...]
% -2� argumento, lista de tarefas organizada
order_same_time_tarefas_by_penalty([],[]).
order_same_time_tarefas_by_penalty([Head|Tail],[Head2|Tail2]):-
						order_same_time_tarefas_by_penalty2(Head,Head2),order_same_time_tarefas_by_penalty(Tail,Tail2).

% -Ordena uma lista de tarefas com o mesmo tempo de conclusao por ordem
% decrescente de prioridade pois quanto maior o valor da prioridade
% -1� argumento, lista de tarefas com o mesmo tempo de conclusao
% -2� argumento, lista de tarefas com o mesmo tempo de conclusao
% ordenada
order_same_time_tarefas_by_penalty2([],[]).
order_same_time_tarefas_by_penalty2([Head|Tail],Sorted_head):-split3(Head,Tail,Left,Right),								                                                     order_same_time_tarefas_by_penalty2(Left,Left1),												     order_same_time_tarefas_by_penalty2(Right,Right1),												     append(Left1,[Head|Right1],Sorted_head).

% -Predicado auxiliar ao order_same_time_tarefas_by_penalty2, retorna as
% tarefas com prioridade menor e maior a do primeiro argumento
% -1� argumento, tarefa a comparar
% -2� argumento, lista de tarefas a comparar
% -3� argumento, lista de tarefas com maior prioridade que a do primeiro
% parametro
% -4� argumento, lista de tarefas com menor prioridade que a do primeiro
% parametro
split3(_,[],[],[]).
split3([Tarefa,Processing_time,T,Penalty],[[Tarefa2,Processing_time2,T2,Penalty2]|Tail],[[Tarefa2,Processing_time2,T2,Penalty2]|Left],Right):-
	Penalty2>Penalty,!,split3([Tarefa,Processing_time,T,Penalty],Tail,Left,Right).
split3([Tarefa,Processing_time,T,Penalty],[[Tarefa2,Processing_time2,T2,Penalty2]|Tail],Left,[[Tarefa2,Processing_time2,T2,Penalty2]|Right]):-
	split3([Tarefa,Processing_time,T,Penalty],Tail,Left,Right).

% -Recebe uma lista com as tarefas e a sua informacao e retorna uma
% lista so de tarefas
% -1� argumento, lista com o formato
% [[[Tarefa,Processing_time,T,Penalty],[...]],[...],...]
% -2� argumento, lista com o formato [Tarefa1,Tarefa2,...]
get_only_tarefas([],[]).
get_only_tarefas([Head|Tail],[Head2|List_with_only_the_tarefa]):- get_only_tarefas2(Head,Head2),
	get_only_tarefas(Tail,List_with_only_the_tarefa).

% -Recebe uma lista com as tarefas e a sua informacao e retorna uma
% lista so de tarefas
% -1� argumento, lista com o formato
% [[Tarefa,Processing_time,T,Penalty],[...],...]
% -2� argumento, lista com o formato [Tarefa1,Tarefa2,...]
get_only_tarefas2([],[]).
get_only_tarefas2([[Tarefa,_,_,_]|Tail],[Tarefa|Tail2]):-
	get_only_tarefas2(Tail,Tail2).
	
% -Como mostrar todas as linhas usando o edd
allEdd(Linha,Result):-
	linhas(Linha),
	get_line(Linha,Linha2),
	edd(Linha2,Result).

get_line([Linha],Linha).

% -Retorna a sequencia de tarefas da linha pretendida
% obtida atraves da heuristica min_slack
% -argumento 1, Linha pretendida
% -argumento 2, Tarefas da linha pretendida ordenada
% aplicando o min_slack
min_slack(Linha, Tarefas_sorted_by_min_slack):-
	findall(
		t(Tarefa,Processing_time,T,Penalty),
		tarefa(Tarefa,Processing_time,T,Penalty),
		List_tarefas
	),
	add_line_to_struct_t(List_tarefas, List_tarefas_line),
	findall(Linha_list,linhas(Linha_list),Linha_list),
	min_slack2(Linha, List_tarefas_line, Linha_list, Tarefas_sorted_by_min_slack).

% -Predicado auxiliar ao min_slack que divide as tarefas por linhas e aplica o min_slack
%  a cada uma returnando no fim a sequencia de tarefas da linha pertendida
% -argumento 1, linha cuja a sequencia de tarefas é pertendida
% -argumento 2, lista de tarefas com a linha adicionada à estrutura t
% -argumento 3, lista de linhas que existem
% -argumento 4, sequencia de tarefas da linha pertendida
min_slack2(Linha, List_tarefas_line, Linha_list, Tarefas_sorted_by_min_slack):-
	split_tarefas_per_possible_lines(List_tarefas_line,Solo_line_tarefas,Multi_line_tarefas),
	put_solo_line_tarefas_on_the_respective_line(Solo_line_tarefas,Linha_list,Linha_list2),
	put_multi_line_tarefas_on_the_respective_line_min_slack(Multi_line_tarefas,Linha_list2,[],Linha_list3),
	((is_empty(Multi_line_tarefas),!,apply_min_slack_to_selected_line(Linha,Linha_list2,Tarefas_sorted_by_min_slack));
		apply_min_slack_to_selected_line(Linha,Linha_list3,Tarefas_sorted_by_min_slack)).

% -Distribui as tarefas que podem ser executadas em mais do que uma linha
% pelas linhas com a maior folga para a tarefa
% -argumento 1, Lista de tarefas pertencentes a mais do que uma linha
% -argumento 2, Lista de linhas e as suas tarefas asssociadas
% -argumento 3, lista de controlo com as tarefas que foram distribuidas
% pelas linhas ate ao momento
% -argumento 4, lista final com todas as tarefas distribuidas pelas linhas
put_multi_line_tarefas_on_the_respective_line_min_slack([],_,Result,Result).
put_multi_line_tarefas_on_the_respective_line_min_slack([t(Tarefa,Linhas,Process_time,T,Penalty)|Tail],Linha_list,Aux,Result):-
	((is_empty(Aux),!,linhas_para_tarefa(Linhas,Linha_list,[],Linha_list2),
		get_linha_com_maior_folga_para_a_tarefa(t(Tarefa,Linhas,Process_time,T,Penalty),Linha_list2,-99999,[],Selected_Line),
		add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,Linha_list,[],Aux2));
	linhas_para_tarefa(Linhas,Aux,[],Linha_list2),
		get_linha_com_maior_folga_para_a_tarefa(t(Tarefa,Linhas,Process_time,T,Penalty),Linha_list2,-99999,[],Selected_Line),
		add_tarefa_to_respective_line(t(Tarefa,Linhas,Process_time,T,Penalty),Selected_Line,Aux,[],Aux2)),
	put_multi_line_tarefas_on_the_respective_line_min_slack(Tail,Linha_list,Aux2,Result).

% -Calcula e retorna a linha onde na qual a tarefa tem maior folga,
% ou seja, onde o atraso e menor
% -argumento 1, Tarefa a adicionar à linha
% -argumento 2, Lista de linhas e respetivas tarefas
% -argumento 3, maior folga ate ao momento
% -argumento 4, linha com a maior folga de momento
% -argumento 5, linha com maior folga da lista
get_linha_com_maior_folga_para_a_tarefa(_,[],_,Selected_Line,Selected_Line).
get_linha_com_maior_folga_para_a_tarefa(Tarefa,[Head|Tail],Max_Folga,Aux,Selected_Line):-
	sum_makespan(Head,Sum),
	get_folga_linha(Tarefa,Sum,Folga),
	((Folga>Max_Folga,!,get_linha_com_maior_folga_para_a_tarefa(Tarefa,Tail,Folga,Head,Selected_Line));
	get_linha_com_maior_folga_para_a_tarefa(Tarefa,Tail,Max_Folga,Aux,Selected_Line)).

% -Calcula a folga de uma tarefa numa linha especifica
% -argumento 1, tarefa cuja a folga sera calculada
% -argumento 2, sumatorio do makespan da linha
% -argumento 3, folga calculada
get_folga_linha(t(_,_,Process_time,T,Penalty),Sum,Folga):-
	Aux is T - Sum - Process_time,
	((Aux<0,!,Folga is Aux*Penalty);Folga is Aux/Penalty).


% -Aplica o min_slack a linha selecionada e retorna a sequencia de tarefas 
% da mesma apos aplicar o min_slack
% -armumento 1, linha pretendida
% -arguemtno 2, lista de linhas e respetivas tarefas
% -argumento 3, sequencia de tarefas da linha pretendida apos
% apos aplicado o min_slack
apply_min_slack_to_selected_line(Linha,Linha_list3,Tarefas_sorted_by_min_slack):-
	linhas_para_tarefa([Linha],Linha_list3,[],Aux),
	get_the_tarefas_list(Aux,Tarefas_List),
	put_each_tarefa_in_a_list(Tarefas_List,Tarefas_List2),
	min_slack_for_line(Tarefas_List2,Tarefas_sorted_by_min_slack).

% -Heuristica que ordena as tarefas de acordo com a folga que estas tem
% na altura da sua execussao
% -1� argumento, retorna uma lista de tarefas ordenadas seguindo as
% regras da heurisctica min.slack
min_slack_for_line(List_tarefas,Tarefas_sorted_by_min_slack):-
					get_sorted_list_by_min_slack(List_tarefas,Tarefas_sorted_by_min_slack).

% -Recebe uma lista de tarefas e retora estar ordenadas pelas regras do
% min.slack
% -1� argumento, lista de tarefas a ordenar
% -2� argumento, lista de tarefas ordenadas
get_sorted_list_by_min_slack(List_tarefas,Tarefas_sorted_by_min_slack):-
	get_sorted_list_by_min_slack2(List_tarefas,[],Tarefas_sorted_by_min_slack).

% -Predicado auxiliar ao get_sorted_list_by_min_slack
% -1� argumento, lista de tarefas a ordenar
% -2� argumento, lista auxiliar que vai guardando as tarefas ordenadas
% ate ao momento
% -3� argumento, lista de tarefas ordenadas
get_sorted_list_by_min_slack2([],_,[]):-!.
get_sorted_list_by_min_slack2(List_tarefas,Aux,[Tarefa|Tail]):-
						get_next_tarefa(List_tarefas,Aux,Currtent_tarefas_selected,[],Tarefa,999999),
						remove_tarefa(Tarefa,List_tarefas,List_tarefas2),
						get_sorted_list_by_min_slack2(List_tarefas2,Currtent_tarefas_selected,Tail).

% -Retorna a proxima tarefa da sequencia
% -1� argumento, lista de tarefas ainda nao processadas
% -2� argumento, lista auxiliar que vai guardado as tarefas selecionadas
% ate ao momento
% -3� argumento, lista de tarefas selecionadas ate ao momento incluindo
% a nova
% -4� argumento, proxima tarefa valida ate ao momento
% -5� argumento, proxima tarefa selecionada
% -6� argumento, valor de controlo para selecionar a proxima tarefa
get_next_tarefa([],Aux,Current_tarefas_selected,[Tarefa,Processing_time,T,Penalty],Tarefa,_):-
								append(Aux,[[Tarefa,Processing_time,T,Penalty]],Current_tarefas_selected).
get_next_tarefa([[Tarefa,Processing_time,T,Penalty]|Tail],Aux,Currtent_tarefas_selected,TarefaAux,TarefaRetornada,MinValue):-
						get_sum_processing_time(Aux,Total_processing_time),
						MinValue2 is T-Total_processing_time-Processing_time,
						((MinValue2<0,!,MinValue3 is MinValue2*Penalty);MinValue3 is MinValue2/Penalty),
						((MinValue3<MinValue,!,
							get_next_tarefa(Tail,Aux,Currtent_tarefas_selected,[Tarefa,Processing_time,T,Penalty],
								TarefaRetornada,MinValue3));
						get_next_tarefa(Tail,Aux,Currtent_tarefas_selected,TarefaAux,TarefaRetornada,MinValue)).


% -Remove a tarefa recebida no primeiro argumento da lista recebida no
% segundo
% -1� argumento, tarefa a remover
% -2� argumento, lista onde se vai remover a tarefa
% -3� argumento, lista com a tarefa removida
remove_tarefa(Tarefa,[[Tarefa,_,_,_]|Tail],Tail):-!.
remove_tarefa(Tarefa,[[Tarefa2,Processing_time,T,Penalty]|Tail],[[Tarefa2,Processing_time,T,Penalty]|Tail2]):-remove_tarefa(Tarefa,Tail,Tail2).

% -Retorna o somatorio de tempos de processamento da tarefas da lista
% recebida no primeiro argumento
% -1� argumento, lista de tarefas
% -2� argumento, somatorio de tempos de processamento da tarefas da lista
get_sum_processing_time([],0):-!.
get_sum_processing_time([[_,Processing_time,_,_]|Tail],Total_processing_time):-
							get_sum_processing_time(Tail,Total_processing_time2),
							Total_processing_time is Processing_time + Total_processing_time2.

% -Como mostrar todas as linhas usando o min_slack
allMin_slack(Linha,Result):-
	linhas(Linha),
	get_line(Linha,Linha2),
	min_slack(Linha2,Result).

get_linha_tarefas(Result):-
	findall(t(Linha, Tarefas), allEdd(Linha, Tarefas), Result).

factorial(1, 1):-!.
factorial(Num, Result):-
	Num1 is Num - 1,
	factorial(Num1, Result1),
	Result is Result1 * Num.