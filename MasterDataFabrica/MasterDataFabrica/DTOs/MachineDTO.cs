﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using Newtonsoft.Json;

namespace MasterDataFabrica.DTOs
{
    public class MachineDTO
    {
        public int machineID;
        public int machineTypeID;
        public string description;
        public bool status;

        public MachineDTO(Machine machine)
        {
            this.machineID = machine.getMachineID().GetMachineID();
            this.machineTypeID = machine.getMachineType().getMachineTypeID();
            this.description = machine.getDescription().GetDescription();
            this.status = machine.getStatusMachine().GetStatusMachine();
        }

     
        public MachineDTO(IDMachine idMachine, IDMachineType idMachineType, Description description)
        {
            StatusMachine statusAux = StatusMachine.CreateStatusMachine(true); 

            this.machineID = idMachine.GetMachineID() ;
            this.machineTypeID = idMachineType.getMachineTypeID();
            this.description = description.GetDescription();
            this.status = statusAux.GetStatusMachine();
        }
        
        public MachineDTO(IDMachine idMachine, IDMachineType idMachineType, Description description, StatusMachine status)
        {
            this.machineID = idMachine.GetMachineID() ;
            this.machineTypeID = idMachineType.getMachineTypeID();
            this.description = description.GetDescription();
            this.status = status.GetStatusMachine();
        }

        [JsonConstructor]
        public MachineDTO(int idMachine, int idMachineType, string description, bool status)
        {
            this.machineID = idMachine;
            this.machineTypeID = idMachineType;
            this.description = description;
            this.status = status;
        }

        public int MachineID() { return machineID; }

        public int MachineTypeID() { return machineTypeID; }

        public string Description() { return description; }

        public bool Status() { return status; }
    }
}
