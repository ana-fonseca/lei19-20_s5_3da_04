using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using Newtonsoft.Json;

namespace MasterDataFabrica.DTOs
{
    public class ProductionLineDTO
    {
        public int productionLineID {get;set;}
        public List<int> machineSequence {get;set;} = new List<int>();
        public string description {get;set;}

        public ProductionLineDTO (IDProductionLine productionLineID, List<MachinePosition> machineSequence, Description description)
        {
            this.productionLineID = productionLineID.GetProductionLineID();
            foreach (MachinePosition machine in machineSequence) {
                this.machineSequence.Insert(machine.index, machine.machineKey.machineKey);
            }
            this.description = description.GetDescription();
        }

        public ProductionLineDTO (ProductionLine productionLine) {
            this.productionLineID = productionLine.productionLineKey.GetProductionLineID();
            this.machineSequence = ToIntList(productionLine.GetMachines());
            this.description = productionLine.description.GetDescription();
        }

        private List<int> ToIntList(List<MachinePosition> machineIDList) {
            List<int> machinesID = new List<int>();
            foreach (MachinePosition machine in machineIDList) {
                machinesID.Insert(machine.index, machine.machineKey.machineKey);
            }

            return machinesID;
        }

        [JsonConstructor]
        public ProductionLineDTO (int productionLineID, List<int> machineSequence, string description)
        {
            this.productionLineID = productionLineID;
            this.machineSequence = machineSequence;
            this.description = description;
        }

        public int ProductionLineID() { return productionLineID; }

        public List<int> MachineSequence() { return machineSequence; }

        public string Description() { return description; }
    }
}