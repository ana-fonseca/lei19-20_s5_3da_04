using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Model;
using Newtonsoft.Json;

namespace MasterDataFabrica.DTOs
{
    public class OperationDTO
    {
        public int operationID;
        public string description;
        public int time;

        [JsonConstructor]
        public OperationDTO (int operationID, string description, int time)
        {
            this.operationID = operationID;
            this.description = description;
            this.time = time;
        }

        public OperationDTO (Operation operation) {
            this.operationID = operation.GetOperationID().getOperationID();
            this.description = operation.description.GetDescription();
            this.time = operation.GetTime();
        }

        public int OperationID() { return operationID; }

        public string Description() { return description; }

        public int Time() { return time; }
    }
}