using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using Newtonsoft.Json;

namespace MasterDataFabrica.DTOs
{
    public class MachineTypeDTO
    {
        public int machineTypeID;
        public List<int> operationList;
        public string description;
        public MachineTypeDTO (MachineType machineType)
        {
            this.machineTypeID = machineType.GetMachineTypeID().getMachineTypeID();
            this.operationList = ToIntList(machineType.GetOperationList());
            this.description = machineType.GetDescription().GetDescription();
        }

        private List<int> ToIntList(List<IDOperation> operationIDList) {
            List<int> operationsID = new List<int>();
            foreach (IDOperation operationID in operationIDList) {
                operationsID.Add(operationID.getOperationID());
            }

            return operationsID;
        }

        [JsonConstructor]
        public MachineTypeDTO (int machineTypeID, List<int> operationList, string description)
        {
            this.machineTypeID = machineTypeID;
            this.operationList = operationList;
            this.description = description;
        }

        public int MachineTypeID() { return machineTypeID; }

        public List<int> OperationList() { return operationList; }

        public string Description() { return description; }
    }
}