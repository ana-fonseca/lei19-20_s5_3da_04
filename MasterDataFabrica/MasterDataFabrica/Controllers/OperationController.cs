﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Model;
using MasterDataFabrica.Models;
using MasterDataFabrica.Repositories;
using MasterDataFabrica.DTOs;
using MasterDataFabrica.VO;
using MasterDataFabrica.Factories;
using Microsoft.AspNetCore.Cors;

namespace MasterDataFabrica.Controllers
{
    [Route("mdf/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class OperationController : ControllerBase
    {
        private MDFFactory factory = new MDFFactory();

        private readonly MDFDB _context;
        private readonly OperationRepo operationRepo;

        public OperationController(MDFDB context)
        {
            _context = context;
            operationRepo = new OperationRepo(context);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OperationDTO>>> GetOperation()
        {
            List<Operation> operations = await operationRepo.FindAll();

            if (operations.Capacity == 0) {
                return NotFound("There are no operations!");
            }

            List<OperationDTO> operationDTOs = new List<OperationDTO>();

            foreach (Operation operation in operations) {
                operationDTOs.Add(new OperationDTO(operation));
            }

            return operationDTOs;
        }

        // GET: api/Operation/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OperationDTO>> GetOperation(int id)
        {
            var operation = await operationRepo.Find(id);

            if (operation == null)
            {
                return NotFound("There is no operation with that ID!");
            }

            return new OperationDTO(operation.GetOperationID().getOperationID(), operation.description.GetDescription(), operation.GetTime());
        }

        // PUT: api/Operation/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOperation([FromRoute]int id, [FromBody] string description)
        {
            if (!await _context.Operation.AnyAsync(e => e.operationKey.operationKey == id)) {
                return NotFound("There are no operations with that ID!");
            }

            Operation operation = await _context.Operation.FirstAsync(e => e.operationKey.getOperationID() == id);
            operation.UpdateDescription(Description.CreateDescription(description));

            _context.Entry(operation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await operationRepo.Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // POST: api/Operation

        [HttpPost]
        public async Task<ActionResult<OperationDTO>> PostOperation(OperationDTO operationDTO)
        {
            Operation op = factory.CreateOperation(operationDTO);
            op.SetTime(operationDTO.time);

            var operation = await operationRepo.Add(op);

            if (operation == null) {
                return BadRequest("There already is an operation with that ID!");
            }

            return CreatedAtAction("GetOperation", new { id = op.id }, operation);
        }


        // DELETE: api/Operation/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<OperationDTO>> DeleteOperation(int id)
        {
            var operation = await operationRepo.Delete(id);

            if (operation == null) {
                return NotFound("There is no operation with that ID!");
            }

            return new OperationDTO(operation);
        }
        
        private bool OperationExists(int id)
        {
            return _context.Operation.Any(e => e.id == id);
        }
    }
}