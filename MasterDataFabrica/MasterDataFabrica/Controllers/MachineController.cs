﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Model;
using MasterDataFabrica.Models;
using MasterDataFabrica.DTOs;
using MasterDataFabrica.VO;
using MasterDataFabrica.Repositories;
using MasterDataFabrica.Factories;
using Microsoft.AspNetCore.Cors;

namespace MasterDataFabrica.Controllers
{
    [Route("mdf/machine")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class MachineController : Controller
    {
        private MDFFactory factory = new MDFFactory();

        private readonly MDFDB _context;
        private readonly MachineRepo machineRepo;
        private readonly MachineTypeRepo machineTypeRepo;

        public MachineController(MDFDB context)
        {
            _context = context;
            machineRepo = new MachineRepo(context);
            machineTypeRepo = new MachineTypeRepo(context);
        }

        // GET: api/Machine/FromMachineType
        [HttpGet("FromMachineType/{machineTypeID}")]
        public async Task<ActionResult<IEnumerable<MachineDTO>>> GetMachinesFromMachineType(int machineTypeID)
        {
            List<Machine> machineList = await machineRepo.FindByMachineType(machineTypeID);

            if (machineList.Capacity == 0)
            {
                return NotFound("There are no machines with that machine type!");
            }

            List<MachineDTO> machineDTOFromMachineType = new List<MachineDTO>();

            foreach (Machine machine in machineList)
            {
                machineDTOFromMachineType.Add(new MachineDTO(machine));
            }

            return new ActionResult<IEnumerable<MachineDTO>>(machineDTOFromMachineType);
        }


        //UC10 - Consult Machine
        [HttpGet("{machineID}")]
        public async Task<ActionResult<MachineDTO>> ConsultMachine(int machineID)
        {
            var machine = await machineRepo.Find(machineID);

            if (machine == null) {
                return NotFound("There are no machines with that ID!");
            }

            return new MachineDTO(machine);
        }

        //UC02 - Create Machine - receiving MachineID, Description & MachineTypeID
        [HttpPost]
        public async Task<IActionResult> CreateMachine([FromBody] MachineDTO machineDTO)
        {

            if (machineDTO.Description()==null)
            {
                return NotFound("A description must be submitted!");
            }

            if (!await machineTypeRepo.HasAny()){
                return NotFound("There are no machine types!");
            }

            if (!await machineTypeRepo.Exists(machineDTO.machineTypeID)){
                return NotFound("There are no machine types with that ID!");
            }

            Machine mach = factory.CreateMachine(machineDTO);

            var machine = await machineRepo.Add(mach);

            if (machine == null) {
                return BadRequest("There already is a machine with that ID!");
            }

            return CreatedAtAction(nameof(CreateMachine), new { id = machineDTO.MachineTypeID() }, machine);
        }

        //UC07 - Change Machine Type - receiving MachineID & MachineTypeID
        [HttpPut("{machineID}")]
        public async Task<ActionResult<MachineDTO>> ChangeMachine([FromRoute] int machineID,[FromBody] IDMachineType machineTypeID)
        {
            if (!await _context.Machine.AnyAsync(e => e.machineKey.machineKey == machineID)) {
                return NotFound("There are no machines with that ID!");
            }

            if (!await _context.MachineType.AnyAsync(e => (e.machineTypeKey.machineTypeKey == machineTypeID.machineTypeKey))) {
                return NotFound("There are no machine types with that ID!");
            }

            Machine machine = await _context.Machine.FirstAsync(e => (e.machineKey.machineKey == machineID));
            MachineType machineType = await _context.MachineType.FirstAsync(e => (e.machineTypeKey.machineTypeKey == machineTypeID.machineTypeKey));

            machine.ChangeType(machineType);

            _context.Entry(machine).State = EntityState.Modified;
            _context.SaveChanges();

            return new MachineDTO(machine);
        }
        
        [HttpPost("{machineID}/activate")]
        public async Task<ActionResult<MachineDTO>> ActivateMachine([FromRoute] int machineID)
        {
            if (!await _context.Machine.AnyAsync(e => e.machineKey.machineKey == machineID)) {
                return NotFound("There are no machines with that ID!");
            }

            Machine machine = await _context.Machine.FirstAsync(e => (e.machineKey.machineKey == machineID));

            machine.ChangeStatusToTrue();

            _context.Entry(machine).State = EntityState.Modified;
            _context.SaveChanges();

            return new MachineDTO(machine);
        }
                
        [HttpPost("{machineID}/deactivate")]
        public async Task<ActionResult<MachineDTO>> DeactivateMachine([FromRoute] int machineID)
        {
            if (!await _context.Machine.AnyAsync(e => e.machineKey.machineKey == machineID)) {
                return NotFound("There are no machines with that ID!");
            }

            Machine machine = await _context.Machine.FirstAsync(e => (e.machineKey.machineKey == machineID));

            machine.ChangeStatusToFalse();

            _context.Entry(machine).State = EntityState.Modified;
            _context.SaveChanges();

            return new MachineDTO(machine);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MachineDTO>>> GetMachine()
        {
            List<Machine> machines = await machineRepo.FindAll();

            if (machines.Capacity == 0) {
                return NotFound("There are no machines!");
            }

            List<MachineDTO> machineDTOs = new List<MachineDTO>();

            foreach (Machine machine in machines) {
                machineDTOs.Add(new MachineDTO(machine));
            }

            return machineDTOs;
        } 

        [HttpDelete("{machineID}")]
        public async Task<ActionResult<MachineDTO>> DeleteMachine(int machineID) {
            var machine = await machineRepo.Delete(machineID);

            if (machine == null) {
                return NotFound("There are no machines with that ID!");
            }

            return new MachineDTO(machine);
        }

    }
}
