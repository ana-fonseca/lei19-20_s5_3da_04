using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Model;
using MasterDataFabrica.Models;
using MasterDataFabrica.Repositories;
using MasterDataFabrica.DTOs;
using MasterDataFabrica.VO;
using MasterDataFabrica.Factories;
using Microsoft.AspNetCore.Cors;

namespace MasterDataFabrica.Controllers
{
    [Route("mdf/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class ProductionLineController: ControllerBase
    {
        private MDFFactory factory = new MDFFactory();

        private readonly MDFDB _context;
        private readonly ProductionLineRepo productionLineRepo;
        private readonly MachineRepo machineRepo;

        public ProductionLineController(MDFDB context)
        {
            _context = context;
            productionLineRepo = new ProductionLineRepo(context);
            machineRepo = new MachineRepo(context);
        }

        [HttpPost]
        public async Task<ActionResult<ProductionLine>> PostProductionLine([FromBody] ProductionLineDTO productionLineDTO)
        {
            if (!await machineRepo.HasAny()){
                return NotFound("There are no machines!");
            }

            List<IDMachine> verifiedIDs = await VerifyMachines(productionLineDTO.MachineSequence());
            if (verifiedIDs.Capacity == 0) {
                return NotFound("None of the submitted machines exist!");
            }

            List<MachinePosition> machines = new List<MachinePosition>();
            int i = 0;
            foreach (IDMachine machine in verifiedIDs) {
                machines.Add(new MachinePosition(machine, i));
                i++;
            }

            ProductionLine pL = factory.CreateProductionLine(productionLineDTO, machines);

            if (await productionLineRepo.Add(pL) == null) {
                return BadRequest("There already is a production line with that ID!");
            }

            return CreatedAtAction(nameof(PostProductionLine), new { id = productionLineDTO.ProductionLineID() }, productionLineDTO);
        }

         public async Task<List<IDMachine>> VerifyMachines (List<int> newMachineIDSequence) {
            List<IDMachine> verifiedIDs = new List<IDMachine>();

            foreach (int machineID in newMachineIDSequence) {
                if (await machineRepo.Exists(machineID)){
                    verifiedIDs.Add(new IDMachine(machineID));
                }
            }

            return verifiedIDs;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductionLineDTO>> GetProductionLine(int id)
        {
            var productionLine = await productionLineRepo.Find(id);

            if (productionLine == null)
            {
                return NotFound("There are no production lines with that ID!");
            }

            return new ProductionLineDTO(productionLine);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductionLineDTO>>> GetProductionLines()
        {
            List<ProductionLine> productionLines = await productionLineRepo.FindAll();

            if (productionLines.Capacity == 0) {
                return NotFound("There are no production lines!");
            }

            List<ProductionLineDTO> productionLineDTOs = new List<ProductionLineDTO>();

            foreach (ProductionLine productionLine in productionLines) {
                productionLineDTOs.Add(new ProductionLineDTO(productionLine));
            }

            return productionLineDTOs;
        }

        [HttpDelete("{productionLineID}")]
        public async Task<ActionResult<ProductionLineDTO>> DeleteMachineType(int productionLineID)
        {
            var productionLine = await productionLineRepo.Delete(productionLineID);

            if (productionLine == null) {
                return NotFound("There are no production lines with that ID!");
            }

            return new ProductionLineDTO(productionLine);
        }
    }
}