﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Model;
using MasterDataFabrica.Models;
using MasterDataFabrica.DTOs;
using MasterDataFabrica.Repositories;
using MasterDataFabrica.VO;
using MasterDataFabrica.Factories;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;

namespace MasterDataFabrica.Controllers
{
    [Route("mdf/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class MachineTypeController : ControllerBase
    {
        private MDFFactory factory = new MDFFactory();

        private readonly MDFDB _context;
        private readonly MachineTypeRepo machineTypeRepo;
        private readonly OperationRepo operationRepo;

        public MachineTypeController(MDFDB context)
        {
            _context = context;
            machineTypeRepo = new MachineTypeRepo(context);
            operationRepo = new OperationRepo(context);
        }

        // GET: api/MachineTypes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MachineTypeDTO>>> GetMachineType()
        {
            List<MachineType> machineTypes = await machineTypeRepo.FindAll();

            if (machineTypes.Capacity == 0)
            {
                return NotFound("There are no machine types!");
            }

            List<MachineTypeDTO> machineTypeDTOs = new List<MachineTypeDTO>();

            foreach (MachineType machineType in machineTypes)
            {
                machineTypeDTOs.Add(new MachineTypeDTO(machineType));
            }

            return machineTypeDTOs;
        }

        // GET: api/MachineType/5
        [HttpGet("{machineTypeID}")]
        public async Task<ActionResult<MachineTypeDTO>> GetMachineType([FromRoute] int machineTypeID)
        {
            var machineType = await machineTypeRepo.Find(machineTypeID);

            if (machineType == null)
            {
                return NotFound("There are no machine types with that ID!");
            }

            return new MachineTypeDTO(machineType);
        }

        // PUT: api/ChangeMachineType
        [HttpPut("ChangeMachineType/{machineTypeID}")]
        public async Task<ActionResult<MachineTypeDTO>> ChangeMachineType([FromRoute] int machineTypeID, [FromBody] OperationList operationList)
        {
            List<IDOperation> verifiedIDs = await VerifyOperations(operationList.operationList);

            if (!await machineTypeRepo.Exists(machineTypeID))
            {
                return NotFound("There are no machine types with that ID!");
            }

            var machineType = await machineTypeRepo.Find(machineTypeID);
            machineType.UpdateOperationList(verifiedIDs);

            _context.Entry(machineType).State = EntityState.Modified;
            _context.SaveChanges();

            return new MachineTypeDTO(machineType);
        }

        // POST: api/MachineTypes
        [HttpPost]
        public async Task<ActionResult<MachineType>> PostMachineType([FromBody] MachineTypeDTO machineTypeDTO)
        {
            if (!await operationRepo.HasAny())
            {
                return NotFound("There are no operations!");
            }

            List<IDOperation> verifiedIDs = await VerifyOperations(machineTypeDTO.OperationList());

            if (verifiedIDs.Capacity == 0)
            {
                return NotFound("None of the submitted operations are valid!");
            }

            MachineType machineType = factory.CreateMachineType(machineTypeDTO, verifiedIDs);

            if (await machineTypeRepo.Add(machineType) == null)
            {
                return BadRequest("There already is a machine type with that ID!");
            }

            return CreatedAtAction(nameof(PostMachineType), new { id = machineTypeDTO.MachineTypeID() }, machineType);
        }

        public async Task<List<IDOperation>> VerifyOperations(List<int> newOperationIDList)
        {
            List<IDOperation> verifiedIDs = new List<IDOperation>();

            foreach (int operationID in newOperationIDList)
            {
                if (await operationRepo.Exists(operationID))
                    verifiedIDs.Add(new IDOperation(operationID));
            }

            return verifiedIDs;
        }

        // DELETE: api/MachineTypes/5
        [HttpDelete("{machineTypeID}")]
        public async Task<ActionResult<MachineTypeDTO>> DeleteMachineType(int machineTypeID)
        {
            var machineType = await machineTypeRepo.Delete(machineTypeID);

            if (machineType == null)
            {
                return NotFound("There are no machine types with that ID!");
            }

            return new MachineTypeDTO(machineType);
        }
    }

    public class OperationList
    {
        public List<int> operationList { get; set; }

        [JsonConstructor]
        public OperationList(List<int> operationList)
        {
            this.operationList = operationList;
        }
    }
}
