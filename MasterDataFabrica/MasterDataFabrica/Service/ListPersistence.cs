using System.Collections.Generic;
using MasterDataFabrica.VO;

namespace MasterDataFabrica.Service
{
    public class ListPersistence
    {

        public ListPersistence() { }

        public static List<string> ToStringList(List<MachinePosition> list)
        {
            List<string> Strings = new List<string>();

            foreach (MachinePosition objT in list)
            {
                Strings.Add(objT.ToString());
            }

            return Strings;
        }

        public static List<MachinePosition> ToObjList(List<string> list) {
            List<MachinePosition> objs = new List<MachinePosition>();

            foreach (string str in list) {
                objs.Add(new MachinePosition(str));
            }

            return objs;
        }

    }
}