﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using MasterDataFabrica.Service;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MasterDataFabrica.Models
{
    public class MDFDB : DbContext
    {
        public MDFDB(DbContextOptions<MDFDB> options)
            : base(options)
        {
        }

        public DbSet<MachineType> MachineType { get; set; }

        public void AddMachineType(MachineType machineType)
        {
            this.Add(machineType);
        }

        public DbSet<Machine> Machine { get; set; }

        public DbSet<Operation> Operation { get; set; }
        public void AddOperation(Operation operation)
        {
            this.Add(operation);
        }

        public DbSet<ProductionLine> ProductionLine { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Operation>().HasKey(op => op.id);
            modelBuilder.Entity<Operation>().OwnsOne(op => op.description);
            modelBuilder.Entity<Operation>().Property(op => op.time);
            modelBuilder.Entity<Operation>().OwnsOne(op => op.operationKey).HasIndex(op => op.operationKey).IsUnique().HasName("operationKey");

            modelBuilder.Entity<MachineType>().HasKey(mt => mt.id);
            modelBuilder.Entity<MachineType>().OwnsOne(mt => mt.description);
            modelBuilder.Entity<MachineType>().OwnsOne(mt => mt.machineTypeKey).HasIndex(mt => mt.machineTypeKey).IsUnique().HasName("machineTypeKey");
            modelBuilder.Entity<MachineType>().OwnsMany(mt => mt.operationList, a =>
            {
                a.HasForeignKey("machineTypeKey");
                a.Property<int>(ca => ca.operationKey).IsRequired(true).HasColumnName("operationKey");
                a.HasKey("machineTypeKey", "operationKey");
            });

            modelBuilder.Entity<Machine>().HasKey(m => m.id);
            modelBuilder.Entity<Machine>().OwnsOne(m => m.machineKey).HasIndex(m => m.machineKey).IsUnique().HasName("machineKey");
            modelBuilder.Entity<Machine>().OwnsOne(m => m.description);
            modelBuilder.Entity<Machine>().OwnsOne(m => m.machineTypeKey).Property(mt => mt.machineTypeKey);

            modelBuilder.Entity<ProductionLine>().HasKey(pl => pl.id);
            modelBuilder.Entity<ProductionLine>().OwnsOne(pl => pl.description);
            modelBuilder.Entity<ProductionLine>().OwnsOne(pl => pl.productionLineKey).HasIndex(pl => pl.productionLineKey).IsUnique().HasName("productionLineKey");
            
            var converter = new ValueConverter<List<MachinePosition>, string>(
                v => String.Join("/", ListPersistence.ToStringList(v)),
                v => ListPersistence.ToObjList(v.Split("/", StringSplitOptions.None).ToList())
            );
            modelBuilder.Entity<ProductionLine>().Property(pl => pl.machines).HasConversion(converter).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
