using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.Model;

namespace MasterDataFabrica.Repositories
{
    public class MachineTypeRepo : IRepository<MachineType>
    {
        private readonly MDFDB _context;
        private readonly DbSet<MachineType> machineTypeContext;

        public MachineTypeRepo(MDFDB context) {
            _context = context;
            machineTypeContext = context.MachineType;
        }

        public async Task<List<MachineType>> FindAll() {
            List<MachineType> machineTypes = await machineTypeContext.ToListAsync();

            return machineTypes;
        } 

        public async Task<MachineType> Add(MachineType machineType) {
            if (await machineTypeContext.AnyAsync(e => e.machineTypeKey.machineTypeKey == machineType.machineTypeKey.machineTypeKey)) {
                return null;
            }

            machineTypeContext.Add(machineType);
            await _context.SaveChangesAsync();

            return machineType;
        }

        public async Task<MachineType> Find(int id) {
            if (!await machineTypeContext.AnyAsync(e => e.machineTypeKey.machineTypeKey == id)) {
                return null;
            }

            MachineType machineType = await machineTypeContext.FirstAsync(e => e.machineTypeKey.machineTypeKey == id);
            
            return machineType;
        }

        public async Task<MachineType> Change(MachineType machineType) {
            //TODO:
            return null;
        }

        public async Task<MachineType> Delete(int id) {
            if (!await machineTypeContext.AnyAsync(e => e.machineTypeKey.machineTypeKey == id)) {
                return null;
            }

            MachineType machineType = await machineTypeContext.FirstAsync(e => e.machineTypeKey.machineTypeKey == id);

            machineTypeContext.Remove(machineType);
            await _context.SaveChangesAsync();

            return machineType;
        }

        public async Task<bool> HasAny() {
            return await machineTypeContext.AnyAsync();
        }

        public async Task<bool> Exists(int id) {
            return await machineTypeContext.AnyAsync(e => e.machineTypeKey.machineTypeKey == id);
        }
    }
}