using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.Model;

namespace MasterDataFabrica.Repositories {
    public class MachineRepo : IRepository<Machine> {
        private readonly MDFDB _context;
        private readonly DbSet<Machine> MachineContext;

        public MachineRepo(MDFDB context) {
            _context = context;
            MachineContext = context.Machine;
        }

        public async Task<List<Machine>> FindAll() {
            List<Machine> Machines = await MachineContext.ToListAsync();

            return Machines;
        } 

        public async Task<Machine> Add(Machine machine) {
            if (await MachineContext.AnyAsync(e => e.machineKey.machineKey == machine.machineKey.machineKey)) {
                return null;
            }

            MachineContext.Add(machine);
            await _context.SaveChangesAsync();

            return machine;
        }

        public async Task<Machine> Find(int id) {
            if (!await MachineContext.AnyAsync(e => e.machineKey.machineKey == id)) {
                return null;
            }

            Machine Machine = await MachineContext.FirstAsync(e => e.machineKey.machineKey == id);
            
            return Machine;
        }

        public async Task<List<Machine>> FindByMachineType(int machineTypeKey) {
            if (!await MachineContext.AnyAsync()) {
                return new List<Machine>();
            }

            if (!await MachineContext.AnyAsync(e => e.machineTypeKey.machineTypeKey == machineTypeKey)) {
                return new List<Machine>();
            }

            List<Machine> machineList = await _context.Machine.ToListAsync();
            List<Machine> machineFromMachineType = new List<Machine>();

            foreach (Machine machine in machineList)
            {
                if (machine.machineTypeKey.machineTypeKey == machineTypeKey)
                {
                    machineFromMachineType.Add(machine);
                }
            }

            return machineFromMachineType;
        }

        public async Task<Machine> Change(Machine machine) {
            //TODO:
            return null;
        }

        public async Task<Machine> Delete(int id) {
            if (!await MachineContext.AnyAsync(e => e.machineKey.machineKey == id)) {
                return null;
            }

            Machine Machine = await MachineContext.FirstAsync(e => e.machineKey.machineKey == id);

            MachineContext.Remove(Machine);
            await _context.SaveChangesAsync();

            return Machine;
        }

        public async Task<bool> HasAny() {
            return await MachineContext.AnyAsync();
        }

        public async Task<bool> Exists(int id) {
            return await MachineContext.AnyAsync(e => e.machineKey.machineKey == id);
        }
    }
}