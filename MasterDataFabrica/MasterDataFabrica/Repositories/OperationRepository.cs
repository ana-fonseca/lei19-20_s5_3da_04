using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.Model;

namespace MasterDataFabrica.Repositories
{
    public class OperationRepo : IRepository<Operation>
    {
        private readonly MDFDB _context;
        private readonly DbSet<Operation> OperationContext;

        public OperationRepo(MDFDB context) {
            _context = context;
            OperationContext = context.Operation;
        }

        public async Task<List<Operation>> FindAll() {
            List<Operation> Operations = await OperationContext.ToListAsync();

            return Operations;
        } 

        public async Task<Operation> Add(Operation operation) {
            if (await OperationContext.AnyAsync(e => e.operationKey.operationKey == operation.operationKey.operationKey)) {
                return null;
            }

            OperationContext.Add(operation);
            await _context.SaveChangesAsync();

            return operation;
        }

        public async Task<Operation> Find(int id) {
            if (!await OperationContext.AnyAsync(e => e.operationKey.operationKey == id)) {
                return null;
            }

            Operation Operation = await OperationContext.FirstAsync(e => e.operationKey.operationKey == id);
            
            return Operation;
        }

        public async Task<Operation> Change(Operation operation) {
            //TODO:
            return null;
        }

        public async Task<Operation> Delete(int id) {
            if (!await OperationContext.AnyAsync(e => e.operationKey.operationKey == id)) {
                return null;
            }

            Operation Operation = await OperationContext.FirstAsync(e => e.operationKey.operationKey == id);

            OperationContext.Remove(Operation);
            await _context.SaveChangesAsync();

            return Operation;
        }

        public async Task<bool> HasAny() {
            return await OperationContext.AnyAsync();
        }

        public async Task<bool> Exists(int id) {
            return await OperationContext.AnyAsync(e => e.operationKey.operationKey == id);
        }
    }
}