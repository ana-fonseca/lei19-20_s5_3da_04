using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Models;
using MasterDataFabrica.Model;

namespace MasterDataFabrica.Repositories
{
    public class ProductionLineRepo : IRepository<ProductionLine>
    {
        private readonly MDFDB _context;
        private readonly DbSet<ProductionLine> ProductionLineContext;

        public ProductionLineRepo(MDFDB context) {
            _context = context;
            ProductionLineContext = context.ProductionLine;
        }

        public async Task<List<ProductionLine>> FindAll() {
            List<ProductionLine> ProductionLines = await ProductionLineContext.ToListAsync();

            return ProductionLines;
        } 

        public async Task<ProductionLine> Add(ProductionLine productionLine) {
            if (await ProductionLineContext.AnyAsync(e => e.productionLineKey.productionLineKey == productionLine.productionLineKey.productionLineKey)) {
                return null;
            }

            ProductionLineContext.Add(productionLine);
            await _context.SaveChangesAsync();

            return productionLine;
        }

        public async Task<ProductionLine> Find(int id) {
            if (!await ProductionLineContext.AnyAsync(e => e.productionLineKey.productionLineKey == id)) {
                return null;
            }

            ProductionLine ProductionLine = await ProductionLineContext.FirstAsync(e => e.productionLineKey.productionLineKey == id);
            
            return ProductionLine;
        }

        public async Task<ProductionLine> Change(ProductionLine productionLine) {
            //TODO:
            return null;
        }

        public async Task<ProductionLine> Delete(int id) {
            if (!await ProductionLineContext.AnyAsync(e => e.productionLineKey.productionLineKey == id)) {
                return null;
            }

            ProductionLine ProductionLine = await ProductionLineContext.FirstAsync(e => e.productionLineKey.productionLineKey == id);

            ProductionLineContext.Remove(ProductionLine);
            await _context.SaveChangesAsync();

            return ProductionLine;
        }

        public async Task<bool> HasAny() {
            return await ProductionLineContext.AnyAsync();
        }

        public async Task<bool> Exists(int id) {
            return await ProductionLineContext.AnyAsync(e => e.productionLineKey.productionLineKey == id);
        }
    }
}