﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataFabrica.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Machine",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    machineKey_machineKey = table.Column<int>(nullable: false),
                    machineTypeKey_machineTypeKey = table.Column<int>(nullable: false),
                    description_description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Machine", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "MachineType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    machineTypeKey_machineTypeKey = table.Column<int>(nullable: false),
                    description_description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Operation",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    operationKey_operationKey = table.Column<int>(nullable: false),
                    description_description = table.Column<string>(nullable: true),
                    time = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLine",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    productionLineKey_productionLineKey = table.Column<int>(nullable: false),
                    machines = table.Column<string>(nullable: false),
                    description_description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLine", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "MachineType_operationList",
                columns: table => new
                {
                    operationKey = table.Column<int>(nullable: false),
                    machineTypeKey = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MachineType_operationList", x => new { x.machineTypeKey, x.operationKey });
                    table.ForeignKey(
                        name: "FK_MachineType_operationList_MachineType_machineTypeKey",
                        column: x => x.machineTypeKey,
                        principalTable: "MachineType",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "machineKey",
                table: "Machine",
                column: "machineKey_machineKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "machineTypeKey",
                table: "MachineType",
                column: "machineTypeKey_machineTypeKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "operationKey",
                table: "Operation",
                column: "operationKey_operationKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "productionLineKey",
                table: "ProductionLine",
                column: "productionLineKey_productionLineKey",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Machine");

            migrationBuilder.DropTable(
                name: "MachineType_operationList");

            migrationBuilder.DropTable(
                name: "Operation");

            migrationBuilder.DropTable(
                name: "ProductionLine");

            migrationBuilder.DropTable(
                name: "MachineType");
        }
    }
}
