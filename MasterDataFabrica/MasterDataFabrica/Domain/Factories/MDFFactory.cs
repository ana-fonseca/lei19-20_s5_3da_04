using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.VO;
using MasterDataFabrica.Model;
using MasterDataFabrica.DTOs;

namespace MasterDataFabrica.Factories {
    public class MDFFactory {

        public MDFFactory() {}

        public Operation CreateOperation (OperationDTO operationDTO) {
            IDOperation operationID = IDOperation.CreateOperationID(operationDTO.operationID);
            Description description = Description.CreateDescription(operationDTO.description);

            return Operation.CreateOperation(operationID, description);
        }

        public MachineType CreateMachineType (MachineTypeDTO machineTypeDTO, List<IDOperation> operations) {
            IDMachineType machineTypeID = new IDMachineType(machineTypeDTO.machineTypeID);
            Description description = Description.CreateDescription(machineTypeDTO.description);

            return MachineType.CreateMachineTypeWithOperations(machineTypeID, description, operations);
        }

        public Machine CreateMachine (MachineDTO machineDTO) {
            IDMachine machineID = new IDMachine(machineDTO.machineID);
            IDMachineType machineTypeID = new IDMachineType(machineDTO.machineTypeID);
            Description description = Description.CreateDescription(machineDTO.description);

            return Machine.CreateMachine(machineID, machineTypeID, description);
        }

        public ProductionLine CreateProductionLine (ProductionLineDTO productionLineDTO, List<MachinePosition> machineSequence) {
            IDProductionLine productionLineID = IDProductionLine.CreateIDProductionLine(productionLineDTO.productionLineID);
            Description description = Description.CreateDescription(productionLineDTO.description);

            return ProductionLine.CreateProductLine(productionLineID, machineSequence, description);
        }

    }
}