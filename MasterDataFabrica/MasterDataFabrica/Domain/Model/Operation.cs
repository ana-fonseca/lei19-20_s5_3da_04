﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.VO;
using MasterDataFabrica.Controllers;

namespace MasterDataFabrica.Model
{
    public class Operation
    {
        public int id { get; set; }

        public IDOperation operationKey {get; set;}
        public Description description {get; set;}
        public int time {get;set;}

        public Operation(){
            
        }

        private Operation(IDOperation operationKey, Description operationDescription) {
            this.operationKey = operationKey;
            this.description = operationDescription;
        }

        private Operation(Description description){
            this.description = description;
        }

        public static Operation CreateOperation(Description description) {
            return new Operation(description);
        }

        public static Operation CreateOperation(IDOperation operationID, Description description) {
            return new Operation(operationID, description);
        }

        public Description UpdateDescription(Description description){
            this.description = description;
            return this.description;
        }
    
        public IDOperation GetOperationID() {
            return operationKey;
        }

        public int GetTime(){return time;}

        public void SetTime(int time){this.time = time;}

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(Operation)) {
                return false;
            }

            if (obj == null && this != null){
                return false;
            } 

            if (obj == null && this == null){
                return true;
            } 

            Operation other = (Operation) obj;

            if (this.operationKey == null && other.operationKey == null) {
                return this.description.Equals(other.description);
            }

            return this.operationKey.Equals(other.operationKey);
        }
    }
}
