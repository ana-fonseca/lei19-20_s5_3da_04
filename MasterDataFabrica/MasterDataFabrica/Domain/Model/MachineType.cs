﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.VO;

namespace MasterDataFabrica.Model
{
    public class MachineType
    {
        public int id { get; set; }

        public IDMachineType machineTypeKey { get; set; }
        public List<IDOperation> operationList { get; set; }

        public Description description { get; set; }

        public MachineType() {
            
        }

        private MachineType(IDMachineType machineTypeID, Description description) {
            this.machineTypeKey = machineTypeID;
            operationList = new List<IDOperation>();
            this.description = description;
        }

        public static MachineType CreateMachineType(IDMachineType machineTypeID, Description description) {
            return new MachineType(machineTypeID, description);
        }

        private MachineType(IDMachineType machineTypeID, Description description, List<IDOperation> operationList) {
            this.machineTypeKey = machineTypeID;
            this.description = description;
            this.operationList = operationList;
        }

        public static MachineType CreateMachineTypeWithOperations(IDMachineType machineTypeID, Description description, List<IDOperation> operationList) {
            return new MachineType(machineTypeID, description, operationList);
        }

        public IDMachineType GetMachineTypeID() { return machineTypeKey; }

        public Description GetDescription() { return description; }

        public List<IDOperation> GetOperationList() {
            return operationList;
        }

        public List<IDOperation> UpdateOperationList(List<IDOperation> newOperationList) {
            this.operationList = newOperationList;
            return this.operationList;
        }

        
        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(MachineType)) {
                return false;
            }

            if (obj == null && this != null){
                return false;
            } 

            if (obj == null && this == null){
                return true;
            } 

            MachineType other = (MachineType) obj;

            return this.machineTypeKey.Equals(other.machineTypeKey) ;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, machineTypeKey, operationList, description);
        }
    }
}
