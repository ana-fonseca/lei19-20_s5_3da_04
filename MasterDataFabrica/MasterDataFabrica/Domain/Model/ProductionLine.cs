﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.VO;

namespace MasterDataFabrica.Model
{
    public class ProductionLine
    {
        public int id { get; set; }

        public IDProductionLine productionLineKey { get; set; }
        public List<MachinePosition> machines { get; set; }
        public Description description { get; set; }

        private ProductionLine()
        {
            this.productionLineKey =  IDProductionLine.CreateIDProductionLine();
            this.description = Description.CreateDescription();
        }

        public static ProductionLine CreateProductionLine()
        {
            return new ProductionLine();
        }

        private ProductionLine(IDProductionLine productionLineID, List<MachinePosition> machines, Description description) 
        {
            this.productionLineKey = productionLineID;
            this.machines = machines;
            this.description = description;
        }

        public static ProductionLine CreateProductLine(IDProductionLine productionLineID, List<MachinePosition> machineSequence, Description description) 
        {
            return new ProductionLine(productionLineID, machineSequence, description);
        }

        public IDProductionLine GetProductionLineID() { return productionLineKey; }

        public List<MachinePosition> GetMachines() {return machines;}

        public List<MachinePosition> UpdateMachineSequence(List<MachinePosition> newMachineSequence) {
            this.machines = newMachineSequence;
            return this.machines;
        }

        public Description GetDescription() { return description; }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(ProductionLine)) {
                return false;
            }

            if (obj == null && this != null){
                return false;
            } 

            if (obj == null && this == null){
                return true;
            } 

            ProductionLine other = (ProductionLine) obj;

            return this.productionLineKey.Equals(other.productionLineKey);

        }
    
    }
}
