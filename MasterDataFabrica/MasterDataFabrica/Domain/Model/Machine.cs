﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.VO;
using MasterDataFabrica.Service;
namespace MasterDataFabrica.Model
{
    public class Machine
    {

        public int id { get; set; }
        public IDMachine machineKey { get; set; }
        public IDMachineType machineTypeKey { get; set; }
        public Description description { get; set; }
        public StatusMachine status { get; set; }

        public Machine() {
            
        }

        public Machine(IDMachine machineID, IDMachineType machineTypeID, Description description, StatusMachine status)
        {
            this.machineKey = machineID;
            this.machineTypeKey = machineTypeID;
            this.description = description;
            this.status = status;
        }


        public static Machine CreateMachine(IDMachine machineID, IDMachineType machineTypeID, Description description, StatusMachine status)
        {
            return new Machine( machineID,  machineTypeID,  description, status);
        }
        
        public static Machine CreateMachine(IDMachine machineID, IDMachineType machineTypeID, Description description)
        {
            StatusMachine statusAux = StatusMachine.CreateStatusMachine(true); 
            return new Machine( machineID,  machineTypeID,  description, statusAux);
        }

        public void ChangeType(MachineType machineType)
        {
            this.machineTypeKey = machineType.machineTypeKey;
        }

        public void ChangeStatusToFalse()
        {
            MachineService.makeReplaneamento();
            StatusMachine statusAux = StatusMachine.CreateStatusMachine(false); 
            this.status = statusAux;
        }
    
        public void ChangeStatusToTrue()
        {
            MachineService.makeReplaneamento();
            StatusMachine statusAux = StatusMachine.CreateStatusMachine(true); 
            this.status = statusAux;
        }

        public IDMachine getMachineID() { return machineKey; }

        public IDMachineType getMachineType() { return machineTypeKey; }

        public Description getDescription() { return description; }

        public StatusMachine getStatusMachine() { return status; }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(Machine)) {
                return false;
            }

            if (obj == null && this != null){
                return false;
            } 

            if (obj == null && this == null){
                return true;
            } 

            Machine other = (Machine) obj;

            return this.machineKey.Equals(other.machineKey);
        }

    }
}
