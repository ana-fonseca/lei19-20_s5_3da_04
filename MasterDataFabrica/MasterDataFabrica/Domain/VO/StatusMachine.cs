using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MasterDataFabrica.VO
{
    [Owned]
    public class StatusMachine
    {

        public bool isActive {get; set;}

        private StatusMachine()
        {
            this.isActive = true;
        }

        [JsonConstructor]
        private StatusMachine(bool isActive)
        {
            this.isActive = isActive;
        }

        public static StatusMachine CreateStatusMachine()
        {
            return new StatusMachine(true);
        }
        
        public static StatusMachine CreateStatusMachine(bool isActive) {
            return new StatusMachine(isActive);
        }

        public bool GetStatusMachine() { return isActive; }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(StatusMachine)) {
                return false;
            }

            StatusMachine other = (StatusMachine) obj;

            return other.isActive == this.isActive;
        }

    }
}
