using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MasterDataFabrica.VO
{
    [Owned]
    public class Description
    {

        public string description {get; set;}

        private Description()
        {
            this.description = null;
        }

        [JsonConstructor]
        private Description(string description)
        {
            this.description = description;
        }

        public static Description CreateDescription()
        {
            return new Description();
        }
        
        public static Description CreateDescription(string description) {
            return new Description(description);
        }

        public string GetDescription() { return description; }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(Description)) {
                return false;
            }

            Description other = (Description) obj;

            return other.description == this.description;
        }

    }
}
