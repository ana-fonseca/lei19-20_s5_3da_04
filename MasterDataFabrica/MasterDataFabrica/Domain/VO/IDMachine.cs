﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MasterDataFabrica.VO
{
    [Owned]
    public class IDMachine
    {

        public int machineKey {get; set;}

        protected IDMachine(){}

        [JsonConstructor]
        public IDMachine(int machineID)
        {
            this.machineKey = machineID;
        }

        public int GetMachineID()
        {
            return machineKey;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(IDMachine)) {
                return false;
            }

            IDMachine other = (IDMachine) obj;

            return other.machineKey == this.machineKey;
        }

    }
}
