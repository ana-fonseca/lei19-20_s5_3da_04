using Microsoft.EntityFrameworkCore;
using System;

namespace MasterDataFabrica.VO {

    [Owned]
    public class MachinePosition {
        public IDMachine machineKey { get; set; }
        public int index { get; set; }

        protected MachinePosition() {}

        public MachinePosition (IDMachine machineID, int index) {
            this.machineKey = machineID;
            this.index = index;
        }

        public MachinePosition (string machinePosString) {
            this.machineKey = new IDMachine(Int32.Parse(machinePosString.Split(",", StringSplitOptions.None)[0]));
            this.index = Int32.Parse(machinePosString.Split(",", StringSplitOptions.None)[1]);
        }

        public IDMachine GetMachineID() { return machineKey; }

        public int GetIndex() { return index; }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(MachinePosition)) 
                return false;

            MachinePosition other = (MachinePosition) obj;

            return other.machineKey.Equals(machineKey) && other.index == index;
        }

        public override string ToString() {
            return this.machineKey.machineKey + "," + index;
        }
    }
}