﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace MasterDataFabrica.VO
{
    [Owned]
    public class IDMachineType
    {

        public int machineTypeKey {get; set;}

        protected IDMachineType(){}

        [JsonConstructor]
        public IDMachineType (int machineTypeID)
        {
            this.machineTypeKey = machineTypeID;
        }

        public int getMachineTypeID()
        {
            return machineTypeKey;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(IDMachineType)) {
                return false;
            }

            IDMachineType other = (IDMachineType) obj;

            return other.machineTypeKey == this.machineTypeKey;
        }

    }
}
