﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFabrica.VO
{
    [Owned]
    public class IDProductionLine
    {
        public int productionLineKey {get; set;}

        protected IDProductionLine()
        {
            this.productionLineKey = -1;
        }

        public static IDProductionLine CreateIDProductionLine()
        {
            return new IDProductionLine();
        }
        private IDProductionLine(int productionLineID)
        {
            this.productionLineKey = productionLineID;
        }

        public static IDProductionLine CreateIDProductionLine(int productionLineID)
        {
            return new IDProductionLine(productionLineID);
        }

        public int GetProductionLineID()
        {
            return productionLineKey;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(IDProductionLine)) {
                return false;
            }

            IDProductionLine other = (IDProductionLine) obj;

            return other.productionLineKey == this.productionLineKey;
        }
    }
}
