﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFabrica.VO
{
    [Owned]
    public class IDOperation
    {

        public int operationKey {get; set;}

        protected IDOperation() {
            
        }

        public IDOperation (int id)
        {
            this.operationKey = id;
        }

        public static IDOperation CreateOperationID(int id){
            return new IDOperation(id);
        }

        public int getOperationID()
        {
            return operationKey;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(IDOperation)) {
                return false;
            }

            IDOperation other = (IDOperation) obj;

            return other.operationKey == this.operationKey;
        }

    }
}
