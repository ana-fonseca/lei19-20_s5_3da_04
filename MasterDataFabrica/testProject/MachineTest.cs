using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using System;
using System.Collections.Generic;
using Xunit;

namespace testProject
{
    public class MachineTest
    {

        public static IDMachine idMa1 = new IDMachine(1);
        public static IDMachineType idMT1 = new IDMachineType(1);
        public static Description desc = Description.CreateDescription("teste");

        public static IDMachine idMa2 = new IDMachine(2);
        public static IDMachineType idMT2 = new IDMachineType(2);
        public static Description desc2 = Description.CreateDescription("teste2");

        public Machine expected =  Machine.CreateMachine(idMa1, idMT1, desc);
        public Machine result;

        [Fact]
        public void testMachineConstructor1DifferentIDMaDifferentMachine() {
            result = Machine.CreateMachine(idMa2, idMT1, desc);
            Assert.False(expected.Equals(result));
        }

        [Fact]
        public void testMachineConstructor1SameIDMaSameMachine1()
        {
            result = Machine.CreateMachine(idMa1, idMT1, desc);
            Assert.True(expected.Equals(result));
        }


        [Fact]
        public void testMachineConstructor1SameIDMaSameMachine2() {
            result = Machine.CreateMachine(idMa1, idMT2, desc);
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testMachineConstructor1SameIDMaSameMachine3() {
            result = Machine.CreateMachine(idMa1, idMT1, desc2);
            Assert.True(expected.Equals(result));
        }

        //Get tests

        [Fact]
        public void testGetMachineIDTrue() {
            Assert.True(idMa1.Equals(expected.getMachineID()));
        }

        [Fact]
        public void testGetMachineIDFalse() {
            Assert.False(idMa2.Equals(expected.getMachineID()));
        }

        [Fact]
        public void testGetDescriptionTrue() {
            Assert.True(desc.Equals(expected.getDescription()));
        }

        [Fact]
        public void testGetDescriptionFalse() {
            Assert.False(desc2.Equals(expected.getDescription()));
        }

        [Fact]
        public void testGetMachineTypeTrue() {
            Assert.True(idMT1.Equals(expected.getMachineType()));
        }

        [Fact]
        public void testGetMachineTypeFalse() {
            Assert.False(idMT2.Equals(expected.getMachineType()));
        }
    }
}