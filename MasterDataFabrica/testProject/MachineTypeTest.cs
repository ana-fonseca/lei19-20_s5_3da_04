using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using System;
using System.Collections.Generic;
using Xunit;

namespace testProject
{
    public class MachineTypeTest
    {

        public static List<IDOperation> operationList = new List<IDOperation>();
        public static List<IDOperation> operationList2 = new List<IDOperation>();

        public IDOperation idOp1 = new IDOperation(1);
        public IDOperation idOp2 = new IDOperation(2);

        IDMachineType idMT1 = new IDMachineType(1);
        IDMachineType idMT2 = new IDMachineType(2);

        public Description desc = Description.CreateDescription("teste");
        public Description desc2 = Description.CreateDescription("teste2");

        [Fact]
        public void testMachineTypeConstructor1SameIDMTSameMT1()
        {   
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType expected = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testMachineTypeConstructor1SameIDMTSameMT2()
        {

            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType expected = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            operationList2.Add(new IDOperation(3));
            operationList2.Add(new IDOperation(4));
            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList2);
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testMachineTypeConstructor1SameIDMTSameMT3()
        {

            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType expected = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);


            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc2, operationList);
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testMachineTypeConstructor1DifferentIDMTDifferentMT() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType expected = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);


            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT2, desc, operationList);
            Assert.False(expected.Equals(result));
        }


        [Fact]
        public void testMachineTypeConstructor2SameIDMTSameMT() {
            MachineType expected = MachineType.CreateMachineType(idMT1, desc);

            MachineType result = MachineType.CreateMachineType(idMT1, desc);
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testMachineTypeConstructor2DifferentIDMTDifferentMT() {
            MachineType expected = MachineType.CreateMachineType(idMT1, desc);

            MachineType result = MachineType.CreateMachineType(idMT2, desc);
            Assert.False(expected.Equals(result));
        }

        [Fact]
        public void testMachineTypeConstructor2SameIDMTSameMT2() {
            MachineType expected = MachineType.CreateMachineType(idMT1, desc);

            MachineType result = MachineType.CreateMachineType(idMT1, desc2);
            Assert.True(expected.Equals(result));
        }

        //Update tests

        [Fact]
        public void testUpdateOperationListTrue() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType expResult = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            List<IDOperation> newOperationList = new List<IDOperation>();
            operationList.Add(new IDOperation(3));
            operationList.Add(new IDOperation(4));

            expResult.UpdateOperationList(newOperationList);

            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, newOperationList);
            Assert.True(expResult.Equals(result));
        }


        //Get tests

        [Fact]
        public void testGetMachineTypeIDTrue() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            Assert.True(result.GetMachineTypeID().Equals(idMT1));
        }

        [Fact]
        public void testGetMachineTypeIDFalse() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            Assert.False(result.GetMachineTypeID().Equals(idMT2));
        }

        [Fact]
        public void testGetDescriptionTrue() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            Assert.True(result.GetDescription().Equals(desc));
        }

        [Fact]
        public void testGetDescriptionFalse() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            Assert.False(result.GetDescription().Equals(Description.CreateDescription("testeFalse")));
        }

        [Fact]
        public void testGetOperationListTrue() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            Assert.True(result.GetOperationList().Equals(operationList));
        }

        [Fact]
        public void testGetOperationListFalse() {
            
            operationList.Add(idOp1);
            operationList.Add(idOp2);
            MachineType result = MachineType.CreateMachineTypeWithOperations(idMT1, desc, operationList);

            List<IDOperation> newOperationList = new List<IDOperation>();
            operationList.Add(new IDOperation(3));
            operationList.Add(new IDOperation(4));

            Assert.False(result.GetOperationList().Equals(newOperationList));
        }
    }
}