using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using System;
using System.Collections.Generic;
using Xunit;

namespace testProject
{
    public class Operationtest
    {

        public Operation expectedID = Operation.CreateOperation(new IDOperation(1), Description.CreateDescription("teste"));
        public Operation expectedNoID = Operation.CreateOperation(Description.CreateDescription("teste"));

        [Fact]
        public void testOperationConstructor1SameIDOpSameOP() {
            Operation result = Operation.CreateOperation(new IDOperation(1), Description.CreateDescription("teste"));
            Assert.True(expectedID.Equals(result));
        }

        [Fact]
        public void testOperationConstructor1DifferentIDOpDifferentOp() {
            Operation result = Operation.CreateOperation(new IDOperation(2), Description.CreateDescription("teste"));
            Assert.False(expectedID.Equals(result));
        }

        [Fact]
        public void testOperationConstructor1SameIDOpSameOP2() {
            Operation result = Operation.CreateOperation(new IDOperation(1), Description.CreateDescription("testeFalse"));
            Assert.True(expectedID.Equals(result));
        }

        [Fact]
        public void testOperationConstructor2SameDescriptionSameOP() {
            Operation result = Operation.CreateOperation(Description.CreateDescription("teste"));
            Assert.True(expectedNoID.Equals(result));
        }

        [Fact]
        public void testOperationConstructor2DifferentDescriptionDifferentOP() {
            Operation result = Operation.CreateOperation(Description.CreateDescription("testeFalse"));
            Assert.False(expectedNoID.Equals(result));
        }


        //Gets Test
        [Fact]
        public void testGetOperationIDTrue() {
            Assert.True(expectedID.GetOperationID().Equals(new IDOperation(1)));
        }

        [Fact]
        public void testGetOperationIDFalse() {
            Assert.False(expectedID.GetOperationID().Equals(new IDOperation(2)));
        }
    }
}