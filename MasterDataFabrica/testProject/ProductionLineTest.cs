using MasterDataFabrica.Model;
using MasterDataFabrica.VO;
using System;
using System.Collections.Generic;
using Xunit;

namespace testProject
{
    public class ProductionLineTest
    {

        [Fact]
        public void testProductionLineConstructor1SameIDPLSamePL() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine expected = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testProductionLineConstructor1DifferentIDPLDifferentPL() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine expected = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(2), machineSequence, Description.CreateDescription("teste"));
           Assert.False(expected.Equals(result));
        }

        [Fact]
        public void testProductionLineConstructor1SameIDPLSamePL2() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine expected = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            List<MachinePosition> newMachineSequence = new List<MachinePosition>();
            newMachineSequence.Add(new MachinePosition(new IDMachine(3), 0));
            newMachineSequence.Add(new MachinePosition(new IDMachine(4), 1));

            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), newMachineSequence, Description.CreateDescription("teste"));

            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testProductionLineConstructor1SameIDPLSamePL3() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine expected = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("testeFalse"));

            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testUpdatemachineSequenceAndSameIDPLSamePL() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine expResult = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            List<MachinePosition> newMachineSequence = new List<MachinePosition>();
            newMachineSequence.Add(new MachinePosition(new IDMachine(3), 0));
            newMachineSequence.Add(new MachinePosition(new IDMachine(4), 1));

            expResult.UpdateMachineSequence(newMachineSequence);

            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), newMachineSequence, Description.CreateDescription("teste"));

            Assert.True(expResult.Equals(result));
            Assert.True(expResult.GetMachines().Equals(result.GetMachines()));
        }

        [Fact]
        public void testUpdatemachineSequenceDifferentMTSequenceSamePL() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine expResult = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            List<MachinePosition> newMachineSequence = new List<MachinePosition>();
            newMachineSequence.Add(new MachinePosition(new IDMachine(3), 0));
            newMachineSequence.Add(new MachinePosition(new IDMachine(4), 1));

            expResult.UpdateMachineSequence(newMachineSequence);

            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            Assert.True(expResult.Equals(result));
            Assert.False(expResult.GetMachines().Equals(result.GetMachines()));
        }


        //Get tests

        [Fact]
        public void testGetProductionLineIDTrue() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            Assert.True(result.GetProductionLineID().Equals(IDProductionLine.CreateIDProductionLine(1)));
        }

        [Fact]
        public void testGetProductionLineIDFalse() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            Assert.False(result.GetProductionLineID().Equals(IDProductionLine.CreateIDProductionLine(2)));
        }

        [Fact]
        public void testGetDescriptionTrue() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            Assert.True(result.GetDescription().Equals(Description.CreateDescription("teste")));
        }

        [Fact]
        public void testGetDescriptionFalse() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            Assert.False(result.GetDescription().Equals(Description.CreateDescription("testeFalse")));
        }

        [Fact]
        public void testGetmachineSequenceTrue() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            Assert.True(result.GetMachines().Equals(machineSequence));
        }

        [Fact]
        public void testGetmachineSequenceFalse() {
            List<MachinePosition> machineSequence = new List<MachinePosition>();
            machineSequence.Add(new MachinePosition(new IDMachine(1), 0));
            machineSequence.Add(new MachinePosition(new IDMachine(2), 1));
            ProductionLine result = ProductionLine.CreateProductLine(IDProductionLine.CreateIDProductionLine(1), machineSequence, Description.CreateDescription("teste"));

            List<MachinePosition> newMachineSequence = new List<MachinePosition>();
            newMachineSequence.Add(new MachinePosition(new IDMachine(3), 0));
            newMachineSequence.Add(new MachinePosition(new IDMachine(4), 1));

            Assert.False(result.GetMachines().Equals(newMachineSequence));
        }
    }
}