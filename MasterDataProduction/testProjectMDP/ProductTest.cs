using System;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using Xunit;

namespace testProjectMDP
{
    public class ProductTest
    {
        public Product expectedID = Product.CreateProduct(IDProduct.CreateIDProduct(1), Description.CreateDescription("teste"));

        [Fact]
        public void testProductConstructor1SameIDPrSamePr() {
            Product result =Product.CreateProduct(IDProduct.CreateIDProduct(1), Description.CreateDescription("teste"));
            Assert.True(expectedID.Equals(result));
        }

        [Fact]
        public void testProductConstructor1DifferentIDPrDifferentPr() {
            Product result = Product.CreateProduct(IDProduct.CreateIDProduct(2), Description.CreateDescription("teste"));
            Assert.False(expectedID.Equals(result));
        }

        [Fact]
        public void testProductConstructor1SameIDPrSamePr2() {
            Product result = Product.CreateProduct(IDProduct.CreateIDProduct(1), Description.CreateDescription("testeFalse"));
            Assert.True(expectedID.Equals(result));
        }

        //Get tests 

        [Fact]
        public void testGetProductIDTrue() {
            Assert.True(expectedID.GetProductID().Equals(IDProduct.CreateIDProduct(1)));
        }

        [Fact]
        public void testGetProductIDFalse() {
            Assert.False(expectedID.GetProductID().Equals(IDProduct.CreateIDProduct(2)));
        }

        [Fact]
        public void testGetDescriptionTrue() {
            Assert.True(expectedID.GetDescription().Equals(Description.CreateDescription("teste")));
        }

        [Fact]
        public void testGetDescriptionFalse() {
            Assert.False(expectedID.GetDescription().Equals(Description.CreateDescription("testeFalse")));
        }
    }
}
