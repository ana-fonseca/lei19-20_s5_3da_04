using System;
using System.Collections.Generic;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using Xunit;

namespace testProjectMDP
{
    public class ManufacturePlanTest{
        [Fact]
        public void testManufacturePlanConstructor1SameIDMPSameMP() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan expected = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testManufacturePlanConstructor1DifferentIDMPDifferentMP() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan expected = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(2), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));
            Assert.False(expected.Equals(result));
        }

        [Fact]
        public void testManufacturePlanConstructor1SameIDMPSameMP2() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan expected = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            List<OperationPosition> newOperationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(3), 0));
            operationList.Add(new OperationPosition(new IDOperation(4), 1));

            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), newOperationList, Description.CreateDescription("teste"));
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testManufacturePlanConstructor1SameIDMPSameMP3() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan expected = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("testeFalse"));
            Assert.True(expected.Equals(result));
        }

        [Fact]
        public void testUpdateOperationSequence() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan expResult = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            List<OperationPosition> newOperationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(3), 0));
            operationList.Add(new OperationPosition(new IDOperation(4), 1));

            expResult.UpdateOperationSequence(newOperationList);

            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), newOperationList, Description.CreateDescription("teste"));
            Assert.True(expResult.GetOperationSequence().Equals(result.GetOperationSequence()));
        }

        [Fact]
        public void testUpdateOperationSequenceNegative() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan expResult = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            List<OperationPosition> newOperationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(3), 0));
            operationList.Add(new OperationPosition(new IDOperation(4), 1));

            expResult.UpdateOperationSequence(newOperationList);

            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));
            Assert.False(expResult.GetOperationSequence().Equals(result.GetOperationSequence()));
        }

        //Get test

        [Fact]
        public void testGetIDManufacturePlanTrue() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            Assert.True(result.GetIDManufacturePlan().Equals(new IDManufacturePlan(1)));
        }

        [Fact]
        public void testGetIDManufacturePlanFalse() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            Assert.False(result.GetIDManufacturePlan().Equals(new IDManufacturePlan(2)));
        }

        [Fact]
        public void testGetDescriptionTrue() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            Assert.True(result.GetDescription().Equals(Description.CreateDescription("teste")));
        }

        [Fact]
        public void testGetDescriptionFalse() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            Assert.False(result.GetDescription().Equals(Description.CreateDescription("testeFalse")));
        }

        [Fact]
        public void testGetOperationSequenceTrue() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            Assert.True(result.GetOperationSequence().Equals(operationList));
        }

        [Fact]
        public void testGetOperationSequenceFalse() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            List<OperationPosition> newOperationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(3), 0));
            operationList.Add(new OperationPosition(new IDOperation(4), 1));

            Assert.False(result.GetOperationSequence().Equals(newOperationList));
        }

        [Fact]
        public void testGetProductIDTrue() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            Assert.True(result.GetProductID().Equals(IDProduct.CreateIDProduct(1)));
        }

        [Fact]
        public void testGetProductIDFalse() {
            List<OperationPosition> operationList = new List<OperationPosition>();
            operationList.Add(new OperationPosition(new IDOperation(1), 0));
            operationList.Add(new OperationPosition(new IDOperation(2), 1));
            ManufacturePlan result = ManufacturePlan.CreateManufacturePlan(new IDManufacturePlan(1), IDProduct.CreateIDProduct(1), operationList, Description.CreateDescription("teste"));

            Assert.False(result.GetProductID().Equals(IDProduct.CreateIDProduct(2)));
        }
    }
}