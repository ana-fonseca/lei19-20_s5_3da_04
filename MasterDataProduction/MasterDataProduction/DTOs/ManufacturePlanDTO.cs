using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using Newtonsoft.Json;

namespace MasterDataProduct.DTOs
{
    public class ManufacturePlanDTO
    {
        public int manufacturePlanID {get; set;}

        public int productID {get; set;}
        public List<int> operationSequence {get; set;}
        public string description {get; set;}
        public int operationsTime {get; set;}

        protected ManufacturePlanDTO() { }

        public ManufacturePlanDTO(IDManufacturePlan manufacturePlanID, IDProduct productID, List<OperationPosition> operationSequence, Description description, int operationsTime)
        {
            this.manufacturePlanID = manufacturePlanID.GetManufacturePlanID();
            foreach (OperationPosition operation in operationSequence)
            {
                this.operationSequence.Insert(operation.index, operation.operationKey.operationKey);
            }
            this.productID = productID.productKey;
            this.description = description.GetDescription();
            this.operationsTime = operationsTime;
        }

[JsonConstructor]
        public ManufacturePlanDTO(int manufacturePlanID, int productID, List<int> operationSequence, string description)
        {
            this.manufacturePlanID = manufacturePlanID;
            this.productID = productID;
            this.operationSequence = operationSequence;
            this.description = description;
        }

        public ManufacturePlanDTO(ManufacturePlan manufacturePlan)
        {
            this.manufacturePlanID = manufacturePlan.GetIDManufacturePlan().GetManufacturePlanID();
            this.productID = manufacturePlan.GetProductID().GetProductID();
            this.operationSequence = ToIntList(manufacturePlan.operationSequence);
            this.description = manufacturePlan.GetDescription().GetDescription();
            this.operationsTime = manufacturePlan.GetOperationsTime();
        }

        public ManufacturePlanDTO(ProductWithManufacturePlanDTO dto) {
            this.manufacturePlanID = dto.manufacturePlanID;
            this.productID = dto.productID;
            this.description = dto.descriptionM;
        }

        private List<int> ToIntList(List<OperationPosition> operationIDList) {
            List<int> operationIDs = new List<int>();
            foreach (OperationPosition operation in operationIDList) {
                operationIDs.Insert(operation.index, operation.operationKey.operationKey);
            }

            return operationIDs;
        }

        public int ManufacturePlanID() { return manufacturePlanID; }

        public int ProductID() { return productID; }

        public List<int> OperationSequence() { return operationSequence; }

        public string Description() { return description; }

        public int OperationsTime() { return operationsTime; }
    }
}