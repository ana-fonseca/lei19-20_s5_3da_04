using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using Newtonsoft.Json;

namespace MasterDataProduct.DTOs
{
    public class ProductWithManufacturePlanDTO
    {
        public int productID {get;set;}
        public int manufacturePlanID {get;set;}
        public string descriptionP {get;set;}
        public List<int> operationSequence {get;set;}
        public string descriptionM {get;set;}

        protected ProductWithManufacturePlanDTO() {}

        [JsonConstructor]
        public ProductWithManufacturePlanDTO (int productID, int manufacturePlanID, string descriptionP, List<int> operationSequence, string descriptionM)
        {
            this.productID = productID;
            this.manufacturePlanID = manufacturePlanID;
            this.descriptionP = descriptionP;
            this.operationSequence = operationSequence;
            this.descriptionM = descriptionM;
        }

        public ProductWithManufacturePlanDTO (Product product, ManufacturePlan manufacturePlan) {
            this.productID = product.productKey.productKey;
            this.manufacturePlanID = manufacturePlan.manufacturePlanKey.manufacturePlanKey;
            this.descriptionP = product.description.description;
            this.descriptionM = manufacturePlan.description.description;
            this.operationSequence = new List<int>();
            this.operationSequence = ToIntList(manufacturePlan.operationSequence);
        }

        private List<int> ToIntList(List<OperationPosition> operationIDList) {
            List<int> operationIDs = new List<int>();
            foreach (OperationPosition operation in operationIDList) {
                operationIDs.Insert(operation.index, operation.operationKey.operationKey);
            }

            return operationIDs;
        }

        public int ProductID() { return productID; }

        public int ManufacturePlanID() { return manufacturePlanID; }

        public string DescriptionP() { return descriptionP; }

        public List<int> OperationSequence() { return operationSequence; }

        public string DescriptionM() { return descriptionM; }
    }
}