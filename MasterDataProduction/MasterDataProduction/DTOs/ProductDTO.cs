using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using Newtonsoft.Json;

namespace MasterDataProduct.DTOs
{
    public class ProductDTO
    {
        public int productID {get; set;}
        public string description {get; set;}

        protected ProductDTO() { }

        public ProductDTO (Product product)
        {
            this.productID = product.productKey.productKey;
            this.description = product.description.description;
        }
        public ProductDTO (IDProduct productID, Description description)
        {
            this.productID = productID.GetProductID();
            this.description = description.GetDescription();
        }

        [JsonConstructor]
        public ProductDTO (int productID, string description)
        {
            this.productID = productID;
            this.description = description;
        }

        public ProductDTO (ProductWithManufacturePlanDTO dto) {
            this.productID = dto.productID;
            this.description = dto.descriptionP;
        }

        public int ProductID() { return productID; }

        public string Description() { return description; }
    }
}