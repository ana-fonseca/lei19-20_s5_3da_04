﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataProduction.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ManufacturePlan",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    manufacturePlanKey_manufacturePlanKey = table.Column<int>(nullable: false),
                    productKey_productKey = table.Column<int>(nullable: false),
                    operationSequence = table.Column<string>(nullable: false),
                    description_description = table.Column<string>(nullable: true),
                    operationsTime = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManufacturePlan", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    productKey_productKey = table.Column<int>(nullable: false),
                    description_description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "manufacturePlanKey",
                table: "ManufacturePlan",
                column: "manufacturePlanKey_manufacturePlanKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "productKey",
                table: "Product",
                column: "productKey_productKey",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ManufacturePlan");

            migrationBuilder.DropTable(
                name: "Product");
        }
    }
}
