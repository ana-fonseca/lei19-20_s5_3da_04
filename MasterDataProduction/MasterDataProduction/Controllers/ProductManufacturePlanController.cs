using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using MasterDataProduct.DTOs;
using MasterDataProduct.Models;
using MasterDataProduct.Repositories;
using MasterDataProduct.Service;
using MasterDataProduct.Factories;
using Microsoft.AspNetCore.Cors;

namespace MasterDataProduct.Controllers
{
    [Route("mdp")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class MDPController : ControllerBase
    {
        private MDPFactory factory = new MDPFactory(); 

        private readonly MDPDB _context;
        private readonly ProductRepo productRepo;
        private readonly ManufacturePlanRepo manufacturePlanRepo;

        public MDPController(MDPDB context)
        {
            _context = context;
            productRepo = new ProductRepo(context);
            manufacturePlanRepo = new ManufacturePlanRepo(context);
        }

        [HttpPost]
        public async Task<ActionResult<ProductDTO>> Post([FromBody] ProductWithManufacturePlanDTO pmfDTO)
        {  
            Product product = factory.CreateProduct(new ProductDTO(pmfDTO));
            var productVar = await productRepo.Add(product);

            if (productVar == null) {
                return BadRequest("There already is a product with that ID!");
            }

            List<OperationPosition> operationSequence = await MDFConnection.VerifyOperations(pmfDTO.operationSequence);
            if (operationSequence.Capacity == 0) {
                await productRepo.Delete(product.productKey.productKey);
                return NotFound("At least one operation does not exist!");
            }

            ManufacturePlan manufacturePlan = factory.CreateManufacturePlan(new ManufacturePlanDTO(pmfDTO), operationSequence);
            
            int time = 0;

            foreach(OperationPosition op in operationSequence){
                time += op.GetTime();
            } 

            manufacturePlan.SetTime(time);
            
            var manufacturePlanVar = await manufacturePlanRepo.Add(manufacturePlan);

            if (manufacturePlanVar == null) {
                await productRepo.Delete(product.productKey.productKey);
                return BadRequest("There already is a manufacture plan with that ID!");
            }

            return CreatedAtAction(nameof(Post), new { id = product.id }, new ProductWithManufacturePlanDTO(product, manufacturePlan));    
        }

        //todo verification

        [HttpGet("Product/{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(int id)
        {
            var product = await productRepo.Find(id);

            if (product == null) {
                return NotFound("There is no product with that ID!");
            }

            return new ProductDTO(product);
        }

        [HttpGet("ManufacturePlan/{id}")]
        public async Task<ActionResult<ManufacturePlanDTO>> GetManufacturePlan(int id)
        {
            var manufacturePlan = await manufacturePlanRepo.Find(id);

            if (manufacturePlan == null) {
                return NotFound("There is no product with that ID!");
            }

            return new ManufacturePlanDTO(manufacturePlan);
        }

        [HttpGet("Product")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProducts()
        {
            List<Product> products = await productRepo.FindAll();

            if (products.Capacity == 0) {
                return NotFound("There are no products!");
            }

            List<ProductDTO> productDTOs = new List<ProductDTO>();

            foreach (Product product in products) {
                productDTOs.Add(new ProductDTO(product));
            }

            return productDTOs;
        }

        [HttpGet("ManufacturePlan")]
        public async Task<ActionResult<IEnumerable<ManufacturePlanDTO>>> GetManufacturePlans()
        {
            List<ManufacturePlan> manufacturePlans = await manufacturePlanRepo.FindAll();

            if (manufacturePlans.Capacity == 0) {
                return NotFound("There are no manufacture plans!");
            }

            List<ManufacturePlanDTO> manufacturePlanDTOs = new List<ManufacturePlanDTO>();

            foreach (ManufacturePlan manufacturePlan in manufacturePlans) {
                manufacturePlanDTOs.Add(new ManufacturePlanDTO(manufacturePlan));
            }

            return manufacturePlanDTOs;
        }

        // DELETE: api/Products/5
        [HttpDelete("ByProduct/{id}")]
        public async Task<ActionResult<ProductWithManufacturePlanDTO>> DeleteWithProduct(int id)
        {
            var product = await productRepo.Delete(id);

            if (product == null) {
                return NotFound("There is no product with that ID!");
            }

            var manufacturePlan = await manufacturePlanRepo.FindByProduct(id);
            manufacturePlan = await manufacturePlanRepo.Delete(manufacturePlan.manufacturePlanKey.manufacturePlanKey);

            return new ProductWithManufacturePlanDTO(product, manufacturePlan);
        }

        [HttpDelete("ByManufacturePlan/{id}")]
        public async Task<ActionResult<ProductWithManufacturePlanDTO>> DeleteWithManufacturePlan(int id)
        {
            var manufacturePlan = await manufacturePlanRepo.Delete(id);

            if (manufacturePlan == null) {
                return NotFound("There is no product with that ID!");
            }

            var product = await productRepo.Delete(manufacturePlan.productKey.productKey);

            return new ProductWithManufacturePlanDTO(product, manufacturePlan);
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.id == id);
        }
    }
}
