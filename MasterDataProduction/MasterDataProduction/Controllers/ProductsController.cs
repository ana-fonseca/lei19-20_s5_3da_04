﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using MasterDataProduct.DTOs;
using MasterDataProduct.Models;

namespace MasterDataProduct.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly MDPDB _context;

        public ProductsController(MDPDB context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult<ProductDTO>> PostProduct([FromBody] ProductDTO productDTO)
        {
            if (await _context.Product.AnyAsync(e => e.productKey.productKey == productDTO.productID)) {
                return BadRequest("There already is a product with that ID!");
            }

            Product product = Product.CreateProduct(IDProduct.CreateIDProduct(productDTO.productID), Description.CreateDescription(productDTO.description));

            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(PostProduct), new { id = productDTO.productID }, product);
        }

        //todo verification

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProduct(int id)
        {
            if (!await _context.Product.AnyAsync(e => e.productKey.productKey == id))
            {
                return NotFound("There are no products with that ID!");
            }
            var product = await _context.Product.FirstAsync(e => e.productKey.GetProductID() == id);

            return new ProductDTO(product);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProducts()
        {
            List<Product> products = await _context.Product.ToListAsync();

            if (products.Capacity == 0)
            {
                return NotFound("There are no products!");
            }

            List<ProductDTO> productDTOs = new List<ProductDTO>();

            foreach (Product product in products)
            {
                productDTOs.Add(new ProductDTO(product));
            }

            return productDTOs;
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Product>> DeleteProduct(int id)
        {
            if (!await _context.Product.AnyAsync(e => e.productKey.productKey == id))
            {
                return NotFound("There are no products with that ID!");
            }

            var product = await _context.Product.FirstAsync(e => e.productKey.GetProductID() == id);

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return product;
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.id == id);
        }
    }
}
