using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using MasterDataProduct.DTOs;
using MasterDataProduct.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace MasterDataProduct.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManufacturePlanController : ControllerBase
    {
        private readonly MDPDB _context;

        public ManufacturePlanController(MDPDB context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult<ManufacturePlanDTO>> PostManufacturePlan([FromBody] ManufacturePlanDTO manufacturePlanDTO)
        {
            if (!_context.Product.Any())
            {
                return NotFound("There are no products!");
            }

            if (await _context.ManufacturePlan.AnyAsync(e => e.manufacturePlanKey.manufacturePlanKey == manufacturePlanDTO.manufacturePlanID))
            {
                return BadRequest("There already exists a manufacture plan with that ID!");
            }

            var existsP = await _context.Product.AnyAsync(e => e.productKey.productKey == manufacturePlanDTO.productID);

            if (!existsP)
            {
                return NotFound("There is no product with that ID!");
            }

            if (await _context.ManufacturePlan.AnyAsync(e => e.productKey.productKey == manufacturePlanDTO.productID)) {
                return BadRequest("There already is a manufacture plan for that product!");
            }

            Product product = await _context.Product.FirstAsync(e => e.productKey.productKey == manufacturePlanDTO.productID);

            List<int> operationIDs = manufacturePlanDTO.operationSequence;
            List<OperationPosition> operationSequence = new List<OperationPosition>();

            string baseUrl = "http://localhost:5000/api/Operation";

            using (var httpHandler = new HttpClientHandler())
            {
                httpHandler.ServerCertificateCustomValidationCallback = (mst, crt, chn, plc) => true;

                using (HttpClient client = new HttpClient(httpHandler))

                using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                using (HttpContent content = res.Content)
                {
                    string data = await content.ReadAsStringAsync();
                    if (data != null)
                    {
                        var jsonObjectList = JsonConvert.DeserializeObject<List<JsonOperation>>(data);

                        List<int> validOperations = new List<int>();

                        foreach (JsonOperation jsonOperation in jsonObjectList) {
                            validOperations.Add(jsonOperation.operationID);
                        }

                        foreach (int key in operationIDs)
                        {
                            int i = 0;
                            if (validOperations.Contains(key)) {
                                operationSequence.Add(new OperationPosition(IDOperation.CreateOperationID(key), i));
                            }
                        }

                        if (operationSequence.Capacity != operationIDs.Capacity) {
                            return NotFound("At least one operation does not exist!");
                        }

                        ManufacturePlan manufacturePlan = ManufacturePlan.CreateManufacturePlan(IDManufacturePlan.CreateIDManufacturePlan(manufacturePlanDTO.manufacturePlanID), product.productKey, operationSequence, Description.CreateDescription(manufacturePlanDTO.description));

                        _context.ManufacturePlan.Add(manufacturePlan);
                        await _context.SaveChangesAsync();

                        return CreatedAtAction(nameof(PostManufacturePlan), new { id = manufacturePlanDTO.manufacturePlanID }, manufacturePlanDTO);
                    }
                    else
                    {
                        return NotFound("There are no operations!");
                    }
                }
            }
        }

        //todo verification

        [HttpGet("{id}")]
        public async Task<ActionResult<ManufacturePlan>> GetManufacturePlan(int id)
        {
            if (!await _context.ManufacturePlan.AnyAsync(e => e.manufacturePlanKey.manufacturePlanKey == id))
            {
                return NotFound("There are no manufacture plans with that ID!");
            }
            ManufacturePlan manufacturePlan = await _context.ManufacturePlan.FirstAsync(e => e.manufacturePlanKey.manufacturePlanKey == id);

            return manufacturePlan;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ManufacturePlanDTO>>> GetProducts()
        {
            List<ManufacturePlan> manufacturePlans = await _context.ManufacturePlan.ToListAsync();

            if (manufacturePlans.Capacity == 0)
            {
                return NotFound("There are no manufacture plans!");
            }

            List<ManufacturePlanDTO> manufacturePlanDTOs = new List<ManufacturePlanDTO>();

            foreach (ManufacturePlan manufacturePlan in manufacturePlans)
            {
                manufacturePlanDTOs.Add(new ManufacturePlanDTO(manufacturePlan));
            }

            return manufacturePlanDTOs;
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ManufacturePlan>> DeleteProduct(int id)
        {
            if (!await _context.ManufacturePlan.AnyAsync(e => e.manufacturePlanKey.manufacturePlanKey == id))
            {
                return NotFound("There are no manufacture plans with that ID!");
            }

            var manufacturePlan = await _context.ManufacturePlan.FirstAsync(e => e.manufacturePlanKey.manufacturePlanKey == id);

            _context.ManufacturePlan.Remove(manufacturePlan);
            await _context.SaveChangesAsync();

            return manufacturePlan;
        }

        private bool ManufacturePlanExists(int id)
        {
            return _context.ManufacturePlan.Any(e => e.manufacturePlanKey.manufacturePlanKey == id);
        }
    }

    public class JsonOperation
    {
        public int operationID { get; set; }
        public string description { get; set; }
    }
}
