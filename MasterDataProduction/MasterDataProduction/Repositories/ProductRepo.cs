using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataProduct.Models;
using MasterDataProduct.Model;

namespace MasterDataProduct.Repositories
{
    public class ProductRepo : IRepository<Product>
    {
        private readonly MDPDB _context;
        private readonly DbSet<Product> ProductContext;

        public ProductRepo(MDPDB context) {
            _context = context;
            ProductContext = context.Product;
        }

        public async Task<List<Product>> FindAll() {
            List<Product> Products = await ProductContext.ToListAsync();

            return Products;
        } 

        public async Task<Product> Add(Product Product) {
            if (await ProductContext.AnyAsync(e => e.productKey.productKey == Product.productKey.productKey)) {
                return null;
            }

            ProductContext.Add(Product);
            await _context.SaveChangesAsync();

            return Product;
        }

        public async Task<Product> Find(int id) {
            if (!await ProductContext.AnyAsync(e => e.productKey.productKey == id)) {
                return null;
            }

            Product Product = await ProductContext.FirstAsync(e => e.productKey.productKey == id);
            
            return Product;
        }

        public async Task<Product> Change(Product Product) {
            //TODO:
            return null;
        }

        public async Task<Product> Delete(int id) {
            if (!await ProductContext.AnyAsync(e => e.productKey.productKey == id)) {
                return null;
            }

            Product Product = await ProductContext.FirstAsync(e => e.productKey.productKey == id);

            ProductContext.Remove(Product);
            await _context.SaveChangesAsync();

            return Product;
        }

        public async Task<bool> HasAny() {
            return await ProductContext.AnyAsync();
        }

        public async Task<bool> Exists(int id) {
            return await ProductContext.AnyAsync(e => e.productKey.productKey == id);
        }
    }
}