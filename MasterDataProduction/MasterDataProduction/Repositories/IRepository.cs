using MasterDataProduct.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace MasterDataProduct.Repositories {
    public interface IRepository<T> {

        Task<T> Add(T obj);

        Task<List<T>> FindAll();

        Task<T> Find(int id);

        Task<T> Delete(int id);

        Task<T> Change(T obj);

        Task<bool> HasAny();

        Task<bool> Exists(int id);
    }
}