using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataProduct.Models;
using MasterDataProduct.Model;

namespace MasterDataProduct.Repositories
{
    public class ManufacturePlanRepo : IRepository<ManufacturePlan>
    {
        private readonly MDPDB _context;
        private readonly DbSet<ManufacturePlan> ManufacturePlanContext;
        private readonly DbSet<Product> ProductContext;

        public ManufacturePlanRepo(MDPDB context) {
            _context = context;
            ManufacturePlanContext = context.ManufacturePlan;
            ProductContext = context.Product;
        }

        public async Task<List<ManufacturePlan>> FindAll() {
            List<ManufacturePlan> ManufacturePlans = await ManufacturePlanContext.ToListAsync();

            return ManufacturePlans;
        } 

        public async Task<ManufacturePlan> Add(ManufacturePlan ManufacturePlan) {
            if (await ManufacturePlanContext.AnyAsync(e => e.manufacturePlanKey.manufacturePlanKey == ManufacturePlan.manufacturePlanKey.manufacturePlanKey)) {
                return null;
            }

            if (!await ProductContext.AnyAsync(e => e.productKey.productKey == ManufacturePlan.productKey.productKey))
                return null;

            ManufacturePlanContext.Add(ManufacturePlan);
            await _context.SaveChangesAsync();

            return ManufacturePlan;
        }

        public async Task<ManufacturePlan> FindByProduct(int productID) {
            if (!await ManufacturePlanContext.AnyAsync(e => e.productKey.productKey == productID)) {
                return null;
            }

            ManufacturePlan ManufacturePlan = await ManufacturePlanContext.FirstAsync(e => e.productKey.productKey == productID);
            
            return ManufacturePlan;
        }

        public async Task<ManufacturePlan> Find(int id) {
            if (!await ManufacturePlanContext.AnyAsync(e => e.manufacturePlanKey.manufacturePlanKey == id)) {
                return null;
            }

            ManufacturePlan ManufacturePlan = await ManufacturePlanContext.FirstAsync(e => e.manufacturePlanKey.manufacturePlanKey == id);
            
            return ManufacturePlan;
        }

        public async Task<ManufacturePlan> Change(ManufacturePlan ManufacturePlan) {
            //TODO:
            return null;
        }

        public async Task<ManufacturePlan> Delete(int id) {
            if (!await ManufacturePlanContext.AnyAsync(e => e.manufacturePlanKey.manufacturePlanKey == id)) {
                return null;
            }

            ManufacturePlan ManufacturePlan = await ManufacturePlanContext.FirstAsync(e => e.manufacturePlanKey.manufacturePlanKey == id);

            ManufacturePlanContext.Remove(ManufacturePlan);
            await _context.SaveChangesAsync();

            return ManufacturePlan;
        }

        public async Task<bool> HasAny() {
            return await ManufacturePlanContext.AnyAsync();
        }

        public async Task<bool> Exists(int id) {
            return await ManufacturePlanContext.AnyAsync(e => e.manufacturePlanKey.manufacturePlanKey == id);
        }
    }
}