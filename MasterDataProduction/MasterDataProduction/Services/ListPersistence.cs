using System.Collections.Generic;
using MasterDataProduct.VO;

namespace MasterDataProduct.Service
{
    public class ListPersistence
    {

        public ListPersistence() { }

        public static List<string> ToStringList(List<OperationPosition> list)
        {
            List<string> Strings = new List<string>();

            foreach (OperationPosition objT in list)
            {
                Strings.Add(objT.ToString());
            }

            return Strings;
        }

        public static List<OperationPosition> ToObjList(List<string> list) {
            List<OperationPosition> objs = new List<OperationPosition>();

            foreach (string str in list) {
                objs.Add(new OperationPosition(str));
            }

            return objs;
        }

    }
}