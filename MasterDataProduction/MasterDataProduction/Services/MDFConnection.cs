using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MasterDataProduct.VO;
using Newtonsoft.Json;

namespace MasterDataProduct.Service {
    public class MDFConnection {

        private static string mdfURL = "http://localhost:5000/mdf/";

        public MDFConnection () { }

        public static async Task<List<OperationPosition>> VerifyOperations (List<int> operationIDs) {
            string baseUrl = mdfURL + "Operation";

            using (var httpHandler = new HttpClientHandler ()) {
                httpHandler.ServerCertificateCustomValidationCallback = (mst, crt, chn, plc) => true;

                using (HttpClient client = new HttpClient (httpHandler))

                using (HttpResponseMessage res = await client.GetAsync (baseUrl))
                using (HttpContent content = res.Content) {
                    string data = await content.ReadAsStringAsync ();
                    if (data != null) {
                        var jsonObjectList = JsonConvert.DeserializeObject<List<JsonOperation>> (data);

                        List<int> validOperations = new List<int> ();
                        List<OperationPosition> operationSequence = new List<OperationPosition> ();
                        int[] times = new int[jsonObjectList.Capacity];

                        int c = 0;
                        foreach (JsonOperation jsonOperation in jsonObjectList) {
                            validOperations.Add (jsonOperation.operationID);
                            times[c] = jsonOperation.time;

                            c++;
                        }

                        int i = 0;
                        foreach (int key in operationIDs) {
                            if (validOperations.Contains (key)) {
                                OperationPosition opPosition = new OperationPosition (IDOperation.CreateOperationID (key), i);
                                opPosition.SetTime (times[i]);
                                operationSequence.Insert (i, opPosition);

                                i++;
                            }
                        }

                        if (operationSequence.Capacity != operationIDs.Capacity) {
                            return new List<OperationPosition> ();
                        }

                        return operationSequence;
                    }
                }
            }
            return new List<OperationPosition> ();
        }

    }

    public class JsonOperation {
        public int operationID { get; set; }
        public string description { get; set; }
        public int time { get; set; }
    }
}