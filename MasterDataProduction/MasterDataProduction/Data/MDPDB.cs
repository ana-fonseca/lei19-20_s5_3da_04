﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MasterDataProduct.Model;
using MasterDataProduct.VO;
using MasterDataProduct.Service;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MasterDataProduct.Models
{
    public class MDPDB : DbContext
    {
        public MDPDB(DbContextOptions<MDPDB> options)
            : base(options)
        {
        }

        public DbSet<Product> Product { get; set; }

        public DbSet<ManufacturePlan> ManufacturePlan { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(p => p.id);
            modelBuilder.Entity<Product>().OwnsOne(p => p.description);
            modelBuilder.Entity<Product>().OwnsOne(p => p.productKey).HasIndex(pk => pk.productKey).IsUnique().HasName("productKey");

            modelBuilder.Entity<ManufacturePlan>().HasKey(m => m.id);
            modelBuilder.Entity<ManufacturePlan>().OwnsOne(m => m.productKey).Property(p => p.productKey);
            modelBuilder.Entity<ManufacturePlan>().OwnsOne(m => m.description);
            modelBuilder.Entity<ManufacturePlan>().Property(m => m.operationsTime);
            modelBuilder.Entity<ManufacturePlan>().OwnsOne(m => m.manufacturePlanKey).HasIndex(mk => mk.manufacturePlanKey).IsUnique().HasName("manufacturePlanKey");

            var converter = new ValueConverter<List<OperationPosition>, string>(
                v => String.Join("/", ListPersistence.ToStringList(v)),
                v => ListPersistence.ToObjList(v.Split("/", StringSplitOptions.None).ToList())
            );
            modelBuilder.Entity<ManufacturePlan>().Property(mf => mf.operationSequence).HasConversion(converter).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
