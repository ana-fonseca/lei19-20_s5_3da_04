using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProduct.VO;
using MasterDataProduct.Model;
using MasterDataProduct.DTOs;

namespace MasterDataProduct.Factories
{ 
    public class MDPFactory {
        public MDPFactory() {}

        public Product CreateProduct (ProductDTO productDTO) {
            IDProduct productID = IDProduct.CreateIDProduct(productDTO.productID);
            Description description = Description.CreateDescription(productDTO.description);

            return Product.CreateProduct(productID, description);
        }

        public ManufacturePlan CreateManufacturePlan (ManufacturePlanDTO manufacturePlanDTO, List<OperationPosition> operationSequence) {
            IDManufacturePlan manufacturePlanID = IDManufacturePlan.CreateIDManufacturePlan(manufacturePlanDTO.manufacturePlanID);
            IDProduct productID = IDProduct.CreateIDProduct(manufacturePlanDTO.productID);
            Description description = Description.CreateDescription(manufacturePlanDTO.description);

            return ManufacturePlan.CreateManufacturePlan(manufacturePlanID, productID, operationSequence, description);
        }
    }
}