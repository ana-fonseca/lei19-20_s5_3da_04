using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProduct.VO;

namespace MasterDataProduct.Model
{

    public class ManufacturePlan
    {
        public int id { get; set; }

        public IDManufacturePlan manufacturePlanKey { get; set; }

        public IDProduct productKey {get; set;}
        public List<OperationPosition> operationSequence {get; set;}
        public Description description { get; set; }
        public int operationsTime {get; set;}

        protected ManufacturePlan () {}

         private ManufacturePlan(IDManufacturePlan manufacturePlanID, IDProduct productID, List<OperationPosition> operationSequence, Description description)
        {
            this.manufacturePlanKey = manufacturePlanID;
            this.productKey = productID;
            this.operationSequence = operationSequence;
            this.description = description;

        }

        public static ManufacturePlan CreateManufacturePlan(IDManufacturePlan manufacturePlanID, IDProduct productID, List<OperationPosition> operationSequence, Description description) {
            return new ManufacturePlan(manufacturePlanID, productID, operationSequence, description);
        }

        public IDManufacturePlan GetIDManufacturePlan()
        {
            return manufacturePlanKey;
        }

        public IDProduct GetProductID() { return productKey; }

        public List<OperationPosition> GetOperationSequence() {
            return operationSequence;
        }

        public List<OperationPosition> UpdateOperationSequence(List<OperationPosition> newOperationSequence) {
            this.operationSequence = newOperationSequence;
            return this.operationSequence;
        }

        public Description GetDescription()
        {
            return description;
        }

        public void SetTime(int operationsTime){
            this.operationsTime = operationsTime;
        }

        public int GetOperationsTime() { return operationsTime; }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(ManufacturePlan)) {
                return false;
            }

            ManufacturePlan other = (ManufacturePlan) obj;

            return other.manufacturePlanKey.Equals(this.manufacturePlanKey);
        }
    }
}