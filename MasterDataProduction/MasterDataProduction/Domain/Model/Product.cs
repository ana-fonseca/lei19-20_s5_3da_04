using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProduct.VO;

namespace MasterDataProduct.Model
{

    public class Product
    {
        public int id { get; set; }

        public IDProduct productKey { get; set; }
        public Description description { get; set; }

        protected Product() { }

        public Product(IDProduct productID, Description description)
        {
            this.productKey = productID;
            this.description = description;
        }

        public static Product CreateProduct(IDProduct productID, Description description)
        {
            return new Product(productID, description);
        }

        public IDProduct GetProductID(){ return productKey; }

        public Description GetDescription(){ return description; }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(Product)) {
                return false;
            }

            Product other = (Product) obj;

            return other.productKey.Equals(this.productKey);
        }
        
    }
}

