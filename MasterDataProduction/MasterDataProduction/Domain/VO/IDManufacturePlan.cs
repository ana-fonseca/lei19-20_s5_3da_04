using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MasterDataProduct.VO
{
    [Owned]
       public class IDManufacturePlan
    {

        public int manufacturePlanKey {get; set;}


        protected IDManufacturePlan() { }
        public IDManufacturePlan(int manufacturePlanID)
        {
            this.manufacturePlanKey = manufacturePlanID;
        }

        public static IDManufacturePlan CreateIDManufacturePlan(int manufacturePlanID)
        {
            return new IDManufacturePlan(manufacturePlanID);
        }
        public int GetManufacturePlanID()
        {
            return manufacturePlanKey;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(IDManufacturePlan)) {
                return false;
            }

            IDManufacturePlan other = (IDManufacturePlan) obj;

            return other.manufacturePlanKey == this.manufacturePlanKey;
        }
    }
}