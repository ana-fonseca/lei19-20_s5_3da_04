using Microsoft.EntityFrameworkCore;
using System;

namespace MasterDataProduct.VO
{

    [Owned]
    public class OperationPosition
    {
        public IDOperation operationKey { get; set; }
        public int index { get; set; }
        public int time {get; set;}

        protected OperationPosition() { }

        public OperationPosition(IDOperation OperationID, int index)
        {
            this.operationKey = OperationID;
            this.index = index;
        }

        public OperationPosition(string OperationPosString)
        {
            this.operationKey = new IDOperation(Int32.Parse(OperationPosString.Split(",", StringSplitOptions.None)[0]));
            this.index = Int32.Parse(OperationPosString.Split(",", StringSplitOptions.None)[1]);
            this.time = Int32.Parse(OperationPosString.Split(",", StringSplitOptions.None)[2]);
        }

        public IDOperation GetOperationID() { return operationKey; }

        public int GetIndex() { return index; }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(OperationPosition))
                return false;

            OperationPosition other = (OperationPosition)obj;

            return other.operationKey.Equals(operationKey) && other.index == index;
        }

        public override string ToString()
        {
            return this.operationKey.operationKey + "," + index + "," + time;
        }

        public int GetTime(){
            return time;
        }

        public void SetTime(int time){
            this.time = time;
        }
    }
}