using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MasterDataProduct.VO{
    [Owned]
         public class IDProduct
    {

        public int productKey {get; set;}

        protected IDProduct() { }

        private IDProduct(int productID)
        {
            this.productKey = productID;
        }

        public static IDProduct CreateIDProduct(int productID)
        {
            return new IDProduct(productID);
        }
        
        public int GetProductID()
        {
            return productKey;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(IDProduct)) {
                return false;
            }

            IDProduct other = (IDProduct) obj;

            return other.productKey == this.productKey;
        }
    }
}