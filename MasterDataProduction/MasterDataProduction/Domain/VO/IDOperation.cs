﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MasterDataProduct.VO
{
    [Owned]
    public class IDOperation
    {

        public int operationKey {get; set;}

        protected IDOperation()
        {

        }

        public IDOperation(int key)
        {
            this.operationKey = key;
        }

        public static IDOperation CreateOperationID(int key)
        {
            return new IDOperation(key);
        }

        public int GetOperationID()
        {
            return operationKey;
        }

        public override bool Equals(object obj) {
            if (obj.GetType() != typeof(IDOperation)) {
                return false;
            }

            IDOperation other = (IDOperation) obj;

            return other.operationKey == this.operationKey;
        }

    }
}
